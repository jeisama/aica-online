<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    use \Awobaz\Compoships\Compoships;
    
	protected $primaryKey 	= null;

	public $incrementing 	= false;

    public $timestamps   	= false;

	protected $fillable 	= [
		'teacher_level',
		'grade_id'
	];


	public function teacher(){
		return $this->hasMany('App\Users', 'teacher_level' , 'teacher_level');
	}

}
