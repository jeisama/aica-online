<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    use \Awobaz\Compoships\Compoships;
    
    protected $fillable = [
        'user_id',
        'kanji_name',
        'romaji_name',
        'birth_date',
        'prefecture', 
        'cram_school',
        'english_learn',
        'eng_sched',
        'acc_prof',
        'remarks'
      
    ];

    public function user(){
        return $this->hasOne('App\Users','id','user_id');
    }
}
