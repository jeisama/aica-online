<?php
namespace App\Helpers;

class ScheduleHelper{

	public static function myRoomId($user_id){

		$roomAdmin = \App\ClassRoomAdmin::where('user_id', $user_id)->get();
            
        $room_id = [];

        if($roomAdmin):

            foreach($roomAdmin as $ra):

                $room_id[] = $ra->class_room_id;
            
            endforeach;

        endif;

        return $room_id;
	}

	public static function myStudentId($room_id){

		$query = \App\ClassRoomStudent::whereIn('room_id', $room_id)->get();

		$student_id = [];

		if($query):
			
			foreach($query as $q):
			
				$student_id[] = $q->user_id;

			endforeach;

		endif;
		
		return $student_id;

	}

}