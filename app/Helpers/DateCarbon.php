<?php 
namespace App\Helpers;

class DateCarbon
{
	public static function date($created_at){
		
        $past = date('Y-m-d H:i:s', strtotime($created_at));
        
		$old   = \Carbon\Carbon::parse($past);

		$today = \Carbon\Carbon::now();

		if($today->diffInSeconds($old) <=59):

            $nm = $today->diffInSeconds($old) <=1 ? 'second':'seconds';

            $rDate = $today->diffInSeconds($old).' '.$nm.' ago.';

        elseif($today->diffInMinutes($old) <=59):

            $nm = $today->diffInMinutes($old) <=1 ? 'minute':'minutes';

            $rDate = $today->diffInMinutes($old).' '.$nm.' ago.';

        elseif($today->diffInHours($old) <=23):

            $nm = $today->diffInHours($old) <=1 ? 'hour':'hours';

            $rDate = $today->diffInHours($old).' '.$nm.' ago.';

        elseif($today->diffInDays($old) <= 7 ):

            $nm = $today->diffInDays($old) <=1 ? 'day':'days';

            $rDate = $today->diffInDays($old).' '.$nm.' ago.';

        else:

            $rDate = date('Y-m-d h:i A', strtotime($created_at));

        endif;

        return $rDate;
    }
    
    public static function afterClass($date, $diff){

        $currDate = date('Y-m-d H:i:s');

        $newDate = strtotime($date .' + '. $diff .' minute');
        $newDate = date('Y-m-d H:i:s', $newDate);

        $data = false;
        if(strtotime($currDate) >= strtotime($newDate)){
            $data = true;
        }

        return $data;
    }
}