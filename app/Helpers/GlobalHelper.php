<?php
namespace App\Helpers;

use Carbon\Carbon;

use Imagick;

class GlobalHelper
{

	/**
	* Date range
	*
	* @param $first
	* @param $last
	* @param string $step
	* @param string $format
	* @return array
	*/
	public static function dateRange($first, $last, $step = '+1 day', $format = 'Y-m-d'){
		$dates = [];
		$current = strtotime( $first );
		$last = strtotime( $last );

		while( $current <= $last ) {

			$dates[] = date( $format, $current );
			$current = strtotime( $step, $current );
		}

		return $dates;
	}

	public static function getDayofWeek($startDate, $endDate, $day_number){
		$endDate = strtotime($endDate);
        $days = array('1'=>'Monday','2' => 'Tuesday','3' => 'Wednesday','4'=>'Thursday','5' =>'Friday','6' => 'Saturday','7'=>'Sunday');
        for($i = strtotime($days[$day_number], strtotime($startDate)); $i <= $endDate; $i = strtotime('+1 week', $i))
            $date_array[]=date('Y-m-d',$i);

        return $date_array;
	}

	public static function days($day){

		switch($day) {
			case 'Monday':
				$result = 0;
				break;
			case 'Tuesday':
				$result = 1;
				break;
			case 'Wednesday':
				$result = 2;
				break;
			case 'Thursday':
				$result = 3;
				break;
			case 'Friday':
				$result = 4;
				break;
			case 'Saturday':
				$result = 5;
				break;
			case 'Sunday':
				$result = 6;
				break;
			default:
		}

		return $result;
	}


	public static function pdf_thumbnail($data){

		$pdf = $data.'[0]';

		$image = new Imagick();

		
		$image->readimage($pdf);

		$image->setImageFormat('jpg');

		$thumbnail = $image->getImageBlob();

		return $thumbnail;

	}

	
}