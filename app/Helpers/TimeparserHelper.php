<?php
namespace App\Helpers;

use Carbon\Carbon;

class TimeparserHelper{

	public static function parseTime($start, $end){

		$intervalInMin = 20;

		$starTime  = strtotime($start);

        $endTime   = strtotime($end);

        $interval  = $intervalInMin * 60; // Convert 15 min into seconds


        $chunks = array();

        for($time=$starTime; $time <= $endTime; $time += $interval){
            
            $chunks[] = date('H:i:s', $time);
        }

        //Remove last item in array
        array_pop($chunks);

        return $chunks;
	}

}