<?php
namespace App\Helpers;

use Auth;

class UserHelper{

	public static function has_franchise($user_id){
		
		$user = \App\ClassRoomAdmin::where('user_id', $user_id)->count();

		return $user;
	}

	public static function user_data($user_id){

		return \App\Users::find($user_id);

	}
}