<?php
namespace App\Helpers;

use Carbon\Carbon;

use Skecskes\Calendar\Facades\Calendar;

use Auth;

class CalendarHelper{


	public function __construct(){

	}

	public static function schedule($schedules, $cdate, $student_id, $role){
		
		$events1     = array();

		$events2     = array();

		if(count($schedules) > 0):

			//Teacher
			if($schedules['student'] != null):
				foreach($schedules['student'] as $key => $data):
					//echo $key. '<br>';
					$events1[$key] = \App\Helpers\CalendarHelper::events($data, $student_id, 'student', $role);
				endforeach;
			endif;

			if($schedules['teacher'] != null):
				foreach($schedules['teacher'] as $key => $data):
					//echo $key. '<br>';
					$events2[$key] = \App\Helpers\CalendarHelper::events($data, $student_id, 'teacher', $role);
				endforeach;
			endif;

			//Student
		endif;

		$events = array_merge_recursive($events1, $events2);
		
		$asePath = url('schedules');

		$calendar = Calendar::make();

        /*
		|----------------------------------------------------------------
		| OPTIONAL METHODS
		|----------------------------------------------------------------
        */
        $calendar->setDate($cdate);
        $calendar->setBasePath($asePath);
        $calendar->showNav(true);
        $calendar->setView(null);
        $calendar->setStartEndHours(8,20);
        $calendar->setTimeClass('ctime');
        $calendar->setEventsWrap(array('', '')); 
        $calendar->setDayWrap(array('<span class="day">','</span>'));
        $calendar->setNextIcon('<i class="mdi mdi-arrow-right"></i>');
        $calendar->setPrevIcon('<i class="mdi mdi-arrow-left"></i>');
        $calendar->setDayLabels(array(__('label.sun'), __('label.mon'), __('label.tue'), __('label.wed'), __('label.thu'), __('label.fri'), __('label.sat')));
        $calendar->setMonthLabels(array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'));
        $calendar->setDateWrap(array('<div>','</div>'));
        $calendar->setTableClass('table-calendar shadow-sm');
        $calendar->setHeadClass('table-header');
        $calendar->setNextClass('');
        $calendar->setPrevClass('');
        $calendar->setEvents($events);

        return $calendar;
	}


	public static function events($sched, $student_id, $class, $role){
		
		$today = date('Y-m-d');

		$data = array();
		
		if(count($sched) > 0):
			
			$count = 0;
			
			foreach($sched as $res):
				
				if($res->user):
				
					$style      = null;

					$start_time = null;
					
					$end_time   = null;

					$dayLevel       = null;

					$teacher = '';

					$student = '';
					
					$href    = 'javascript:void(0);';

					$link    = '';

					if($class == 'student'):
					
						$style = 'student';

						$lesson_name = $res->lesson ? $res->lesson->lesson_name : 'No lesson available.';

						$dayLevel  = 'Day '. $res->day .': '. $lesson_name;

						//$user  = 'Student: '. $res->user->kanji_name;

						$href = url('schedules/student/edit/'. $res->id);

						if(strtotime($today) <= strtotime($res->date) && date('mY', strtotime($today)) == date('mY', strtotime($res->date))):

							$link = '<a href="'. $href .'" class="edit"><i class="mdi mdi-square-edit-outline"></i></a>';

						endif;

						$start_time = $res->start_time;

						$end_time = $res->end_time;

						if($role != 5):

							$teacher = 'Teacher: '. $res->teacher->full_name;

						endif;

						if($role != 4):

							$student = 'Student: '. $res->user->romaji_name;

						endif;
					else:
					
						$style = 'teacher';
						
						$dayLevel  = $res->user->level ? $res->user->level['level'] : '';

						$teacher  = 'Teacher: '. $res->user->full_name;

						$href = url('schedules/teacher/edit/'. $res->id);

						

						if(strtotime($today) > strtotime($res->date)):
						
							$link = '';
						
						else:
						
							$link = '<a href="'. $href .'" class="edit"><i class="mdi mdi-square-edit-outline"></i></a>';
						
						endif;
						//$link = '';

						$start_time = $res->time_in;

						$end_time = $res->time_out;

					endif;

					$data[] = '
						<div class="schedules '. $style .'">
							'. $link .'
							<div class="schedule-time">'. date('h:iA', strtotime($start_time)) .' - '. date('h:iA', strtotime($end_time)) .'</div>
							<div class="student-teacher">
								<div>'. $dayLevel .'</div>
								<div>'. $teacher .'</div>
								<div>'. $student .'</div>
							</div>
						</div>
					';
					
				endif;

			endforeach;

		endif;
		
		return $data;
	}

}