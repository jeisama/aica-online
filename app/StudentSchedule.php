<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentSchedule extends Model
{

    use \Awobaz\Compoships\Compoships;
      
    protected $fillable = [
		'user_id',
		'teacher_id',
		'course_id',
		'grade_id',
		'schedule_id',
        'day',
		'date',
        'group',
		'start_time',
		'end_time',
		'created_by',
		'updated_by',
    ];
    

    public function student(){
        return $this->hasOne('App\Users','id','user_id');
    }

    public function user(){
        return $this->hasOne('App\Users','id','user_id');
    }

    public function teacher(){
    	return $this->hasOne('App\Users','id','teacher_id');
    }

    public function course(){
    	return $this->hasOne('App\Course','id','course_id');
    }

    public function grade(){
    	return $this->hasOne('App\GradeLevel','id','grade_id');
	}
	public function lesson(){
        return $this->hasOne('App\NewLesson', 'schedule_id', 'id')->with('schedule');
    }
}
