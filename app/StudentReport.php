<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentReport extends Model
{
    use \Awobaz\Compoships\Compoships;
    
    protected $fillable = [
        'lesson_id',
    	'student_id',
        'listening',
        'student',
        'writing',
        'grammar',
        'speaking',
        'date',
        'course_type',
        'lesson_type',
        'lesson_day',
        'room_id',
        'comments',
        'created_by'
    ];
}
