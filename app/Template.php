<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    use \Awobaz\Compoships\Compoships;
    
    protected $fillable = [
    	'course_type',
        'lesson_type',
        'lesson_name',
        'lesson_day',
        'form_type',
        'speaking_topic',
        'next_listening',
        'writing_topic',
        'created_by'
    ];
}
