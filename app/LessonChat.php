<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LessonChat extends Model
{
	use \Awobaz\Compoships\Compoships;
	
    protected $fillable = [
    	'lesson_id',
        'user_id',
        'message',
    ];

    public function user(){
        return $this->hasOne('App\Users','id','user_id');
    }
}
