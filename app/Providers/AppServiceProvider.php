<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        
        view()->composer('*', function($view){
            
            $user = Auth::id() ? \App\Users::with(['teacher', 'student'])->find(Auth::id()) : null;
            
            $hasFranchise = Auth::id() ? \App\Helpers\UserHelper::has_franchise(Auth::id()) : null;

            $view->with('user_data', $user)

                ->with('hasFranchise', $hasFranchise);

        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // $this->app->bind('path.public', function() {
        //     return base_path('public_html');
        // });
    }
}
