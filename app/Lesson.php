<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\Sluggable;

class Lesson extends Model
{
    use \Awobaz\Compoships\Compoships;
    
	use Sluggable;

    protected $fillable = [
    	'user_id',
        'schedule_id',
        'title',
        'slug',
        'grade_level_id',
        'description',
        'write_instruction',
        'speaking_instruction',
        'speaking_topic',
        'writing_exercise',
        'writing_exercise_stu',
        'listening_instruction',
        'reproduction_exercise',
        'homework_instruction',
        'homework_listening',
        'created_by',
        'updated_by',
        'source',
        'source_data'
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function attachment(){
        return $this->hasMany('App\LessonAttachment','lesson_id','id');
    }

    public function media(){
        return $this->hasMany('App\LessonMedia','lesson_id','id');
    }

    public function teacher(){
        return $this->hasOne('App\Teacher','user_id','user_id');
    }

    public function schedule(){
        return $this->hasOne('App\TeacherSchedule','id','schedule_id');
    }

    public function grade(){
        return $this->hasOne('App\GradeLevel','id','grade_level_id');
    }

    public function user_data(){
        return $this->hasOne('App\Users','id','user_id');
    }

    public function student(){
        return $this->hasOne('App\Users','id','user_id');
    }
 
}
