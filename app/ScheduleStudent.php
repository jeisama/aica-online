<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScheduleStudent extends Model
{
	use \Awobaz\Compoships\Compoships;
	
	protected $table = 'schedules_student';
	
    protected $fillable = [
        'schedule_id',
        'user_id'    
    ];

    public function info(){
    	return $this->hasOne('App\Student','user_id','user_id')->with('user');
    }
}
