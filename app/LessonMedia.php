<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LessonMedia extends Model
{
	use \Awobaz\Compoships\Compoships;
	
	protected $table = 'lesson_media';
	
    protected $fillable = [
    	'lesson_id',
    	'source',
        'source_data',
        'original_name'
    ];
}
