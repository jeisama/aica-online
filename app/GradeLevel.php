<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GradeLevel extends Model
{

    use \Awobaz\Compoships\Compoships;
    
    protected $fillable = [
        'label',
        'status',
        'created_by',
        'updated_by',
    ];

    public function get_course(){

    	return $this->hasOne('App\CourseGradeLevel','grade_id','id');
    	
    }

    public function level(){

        return $this->hasOne('App\Level','grade_id','id');

    }

}
