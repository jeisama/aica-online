<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Users extends Model
{
    use \Awobaz\Compoships\Compoships;
    
    protected $table = 'users';
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name',
        'gender',
        'avatar',
        'kanji_name',
        'romaji_name',
        'birth_date',
        'prefecture',
        'cram_school',
        'english_learning',
        'englist_schedule',
        'account_prof',
        'remark',
        'email',            
        'role', 
        'status',           
        'confirm_email',             
        'password',        
        'username',
        'created_by',
        'updated_by',
        'teacher_level'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */


    protected $hidden = [
        'password', 'remember_token',
    ];
    //To Delete
    public function teacher(){
        return $this->hasOne('App\Teacher','user_id','id');
    }
    //To Delete
    public function student(){
        return $this->hasOne('App\Student','user_id','id');
    }

    public function room_admin(){
        return $this->hasOne('App\ClassRoomAdmin','user_id','id');
    }

    public function level(){
        return $this->hasOne('App\TeacherLevel','id','teacher_level');
    }
    public function class_name(){
        return $this->hasOne('App\ClassRoomStudent','user_id','id');
    }
}
