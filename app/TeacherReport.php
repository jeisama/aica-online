<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeacherReport extends Model
{
    use \Awobaz\Compoships\Compoships;
    
    protected $fillable = [
        'lesson_id',
    	'teacher_id',
        'teacher',
        'date',
        'course_type',
        'lesson_type',
        'lesson_day',
        'room_id',
        'created_by'
    ];

    public function student(){
        return $this->hasOne('App\Users','id','created_by');
    }

    public function teachers(){
        return $this->hasOne('App\Users','id','teacher_id');
    }

    public function course(){
        return $this->hasOne('App\Course','id','course_type');
    }

    public function grade(){
        return $this->hasOne('App\GradeLevel','id','lesson_type');
    }

    public function lesson(){
        return $this->hasOne('App\NewLesson','id','lesson_id');
    }

}
