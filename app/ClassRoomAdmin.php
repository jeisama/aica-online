<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassRoomAdmin extends Model
{
	use \Awobaz\Compoships\Compoships;
	
    protected $table = 'class_rooms_admin';
	
    protected $fillable = [
    	'user_id',
    	'class_room_id',
    ];
}
