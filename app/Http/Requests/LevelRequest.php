<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LevelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $method = $this->method();

        $rules = null;

        switch($method):

            case 'POST':

                $rules = [
                    'level'      => 'required|unique:teacher_levels,level',
                    'status'      => 'required'
                ];

                break;

            case 'PUT':
            case 'PATCH':

                $rules = [
                    'level'      => 'required|unique:teacher_levels,level,'. $this->id,
                    'status'      => 'required'
                ];

                break;

            default: break;

        endswitch;

        return $rules;
    }
}
