<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewLessonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'writing_1'   => 'required',
            //'speaking_1'  => 'required',
            'speaking_2'  => 'required',
            //'writing_student_1' => 'required',
            //'listening_1' => 'required',
            //'listening_2' => 'required',
            //'homework_1' => 'required',
            'homework_2' => 'required'
        ];
    }
    public function messages(){
        return [
            
            'speaking_2.required'        => 'Speaking topic is required.',
            
            'homework_2.required'       =>  'Listening script is required.'
        ];
    }
}
