<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TemplateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'course_type' => 'required',
            'lesson_type' => 'required',
            'lesson_name' => 'required',
            'lesson_day' => 'required',
            'speaking_topic'   => 'sometimes|required',
            'next_listening'  => 'sometimes|required',
            'writing_topic' => 'sometimes|required',
            
            



        ];
    }
    public function messages(){
        return [
            'course_type.required' => 'Course is a required item',
            'lesson_type.required' => 'Lesson is a required item',
            'lesson_name.required' => 'Lesson name is required',
            'lesson_day.required' => 'Lesson day is required', 
            'speaking_topic.required'         => 'Writing instruction is required.',
            'next_listening.required'        => 'Listening instruction is required.', 
            'writing_topic.required' => 'Writing Topic is required'
        ];
    }
}
