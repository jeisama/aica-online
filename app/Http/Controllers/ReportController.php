<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use UserHelper;

use App\Exports\TeacherExport;

use App\Exports\StudentExport;

use ScheduleHelper;

use Excel;


class ReportController extends Controller
{

	public $user;

	private $room_id;

    private $student_id;

    public function __construct(){

        $this->middleware(function($request, $next){
            
            $this->user = UserHelper::user_data(Auth::id());

            $this->room_id      = ScheduleHelper::myRoomId($this->user->id);

            $this->student_id   = ScheduleHelper::myStudentId($this->room_id);

            return $next($request);

		});

	}
	
    public function index(){

    	if($this->user->role == 4 || $this->user->role == 5) abort(403);

    	return view('reports.index')
    		->with('title', 'Reports');

    }

    public function student(Request $request){
    	
    	if($this->user->role == 3 || $this->user->role == 4 || $this->user->role == 5) abort(403);

    	$classroom = \App\ClassRoom::get();

    	$getYear = \App\StudentSchedule::select('student_schedules.date', 'student_schedules.end_time' )->join('new_lessons', 'new_lessons.schedule_id', '=', 'student_schedules.id')->orderBy('student_schedules.date', 'asc')->first();
		//dd($getYear);
		$getStudent = \App\Users::where('role', 4)->get();

		
    	$url = $request->fullUrl();

    	$parsedUrl = parse_url($url);

    	$parsedUrl = $parsedUrl['query'] ?? '';

    	/*
    	|-----------------------
		| Queries
		| Get Reports
		|-----------------------
    	*/

		$room 	= $request->get('classroom') ?? '';

		$year 	= $request->get('year') ?? '';

		$month 	= $request->get('month') ?? '';

		$odb 	= $request->get('odb') ?? '';

		$order 	= $request->get('order') ?? '';

		$student 	= $request->get('student') ?? '';

		$q = [
			'room' => $room,
			'year' => $year,
			'month' => $month,
			'student' => $student,
		];

		$sort = [
			'odb'	=> $odb,
			'order'	=> $order
		];

		$query = \App\StudentReport::query();
		
		$query->select(
			'student_reports.date', 
			'schedule.start_time',
			'student.kanji_name', 
			'student.romaji_name',
			'course.label',
			'grade.label as grade',
			'lesson.lesson_name',
			'student_reports.lesson_day',
			'teacher.full_name',
			'student_reports.student',
			'student_reports.listening',
			'student_reports.writing',
			'student_reports.grammar',
			'student_reports.speaking'
		);

		$query->leftJoin('users as student', 'student.id', '=', 'student_reports.student_id');

		$query->leftJoin('new_lessons as lesson', 'lesson.id', '=', 'student_reports.lesson_id');

		$query->leftJoin('student_schedules as schedule', 'schedule.id', '=', 'lesson.schedule_id');

		$query->leftJoin('courses as course', 'course.id', '=', 'student_reports.course_type');

		$query->leftJoin('grade_levels as grade', 'grade.id', '=', 'student_reports.lesson_type');

		$query->leftJoin('users as teacher', 'teacher.id', '=', 'student_reports.created_by');
		
		if($this->user->role == 2):
		
			$query->whereIn('student_reports.student_id', $this->student_id);

		endif;

		if($room != '' && $room != 'all'):
			
			$query->where('student_reports.room_id', $room);

		endif;

		if($year != null):

			$query->whereYear('student_reports.date', $year);
			
		endif;

		if($month != null):

			$query->whereMonth('student_reports.date', $month);

		endif;
		if($student != null):

			$query->where('student_reports.student_id', $student);

		endif;

		if($odb == 'name' && ($order == 'asc' || $order == 'desc')):

			$query->orderBy('student.romaji_name', $order);

		elseif($odb == 'date'  && ($order == 'asc' || $order == 'desc')):

			$query->orderBy('student_reports.date', $order);

		elseif($odb == 'course'  && ($order == 'asc' || $order == 'desc')):

			$query->orderBy('course.label', $order);

		elseif($odb == 'grade'  && ($order == 'asc' || $order == 'desc')):

			$query->orderBy('grade.label', $order);

		elseif($odb == 'lesson'  && ($order == 'asc' || $order == 'desc')):

			$query->orderBy('lesson.lesson_name', $order);

		elseif($odb == 'progress'  && ($order == 'asc' || $order == 'desc')):

			$query->orderBy('student_reports.lesson_day', $order);

		elseif($odb == 'teacher'  && ($order == 'asc' || $order == 'desc')):
		
			$query->orderBy('teacher.full_name', $order);

		endif;
		
		$reports = $query->get();
		//dd($reports);
		$roomName = \App\ClassRoom::find($room);

    	return view('reports.student')
    		->with('title', 'Reports')
    		->with('classroom', $classroom)
    		->with('year', $getYear)
    		->with('q', $q)
    		->with('roomName', $roomName)
    		->with('reports', $reports)
    		->with('sort', $sort)
			->with('parsedUrl', $parsedUrl)
			->with('students', $getStudent);

    }


    public function teacher(Request $request){
    	
    	if($this->user->role == 4 || $this->user->role == 5 || $this->user->role == 2) abort(403);

    	$getYear = \App\StudentSchedule::select('student_schedules.date')->join('new_lessons', 'new_lessons.schedule_id', '=', 'student_schedules.id')->orderBy('student_schedules.date', 'asc')->first();

    	$url = $request->fullUrl();

    	$parsedUrl = parse_url($url);

    	$parsedUrl = $parsedUrl['query'] ?? '';
    	
    	/*
    	|-----------------------
		| Queries
		| Get Reports
		|-----------------------
    	*/

		$year 	= $request->get('year') ?? '';
		$month 	= $request->get('month') ?? '';
		$odb 	= $request->get('odb') ?? '';
		$order 	= $request->get('order') ?? '';

		$q = [
			'year' => $year,
			'month' => $month
		];

		$sort = [
			'odb'	=> $odb,
			'order'	=> $order
		];

		$query = \App\TeacherReport::query();

		$query->select(
			'teacher.full_name',
			'student.romaji_name',
			'teacher_reports.date',
			'teacher_reports.lesson_day',
			'teacher_reports.teacher',
			'courses.label as course',
			'grade.label as grade',
			'lesson.lesson_name',
			'schedule.start_time'
		);

		$query->leftJoin('users as teacher', 'teacher.id', '=', 'teacher_reports.teacher_id');

		$query->leftJoin('users as student', 'student.id', '=', 'teacher_reports.created_by');

		$query->leftJoin('courses', 'courses.id', '=', 'teacher_reports.course_type');

		$query->leftJoin('grade_levels as grade', 'grade.id', '=', 'teacher_reports.lesson_type');

		$query->leftJoin('new_lessons as lesson', 'lesson.id', '=' ,'teacher_reports.lesson_id');

		$query->leftJoin('student_schedules as schedule', 'schedule.id', '=', 'lesson.schedule_id');

		if($year != null):
			
			$query->whereYear('teacher_reports.date', $year);

		endif;

		if($month != null):

			$query->whereMonth('teacher_reports.date', $month);

		endif;

		if($odb == 'name' && ($order == 'asc' || $order == 'desc')):

			$query->orderBy('teacher.full_name', $order);

		elseif($odb == 'date' && ($order == 'asc' || $order == 'desc')):

			$query->orderBy('teacher_reports.date', $order);

		elseif($odb == 'course' && ($order == 'asc' || $order == 'desc')):

			$query->orderBy('courses.label', $order);

		elseif($odb == 'grade' && ($order == 'asc' || $order == 'desc')):

			$query->orderBy('grade.label', $order);

		elseif($odb == 'lesson' && ($order == 'asc' || $order == 'desc')):

			$query->orderBy('lesson.lesson_name', $order);

		elseif($odb == 'progress' && ($order == 'asc' || $order == 'desc')):

			$query->orderBy('teacher_reports.lesson_day', $order);

		elseif($odb == 'student' && ($order == 'asc' || $order == 'desc')):

			$query->orderBy('student.romaji_name', $order);

		endif;

		$reports = $query->get();
		//dd($reports);
    	return view('reports.teacher')
    		->with('title', 'Reports')
    		->with('year', $getYear)
    		->with('q', $q)
    		->with('reports', $reports)
    		->with('sort', $sort)
    		->with('parsedUrl', $parsedUrl);

	}
	
	public function export_teacher(Request $request)
    {
    	if($this->user->role == 4 || $this->user->role == 5 || $this->user->role == 2) abort(403);

    	$year = $request->get('year') ?? '';
		$month = $request->get('month') ?? '';
		$odb 	= $request->get('odb') ?? '';
		$order 	= $request->get('order') ?? '';

		$q = [
			'year' => $year,
			'month' => $month
		];

		$sort = [
			'odb'	=> $odb,
			'order'	=> $order
		];

		$query = \App\TeacherReport::query();

		$query->select(
			'teacher.full_name',
			'student.romaji_name',
			'teacher_reports.date',
			'teacher_reports.lesson_day',
			'teacher_reports.teacher',
			'courses.label as course',
			'grade.label as grade',
			'lesson.lesson_name',
			'schedule.start_time'
		);

		$query->leftJoin('users as teacher', 'teacher.id', '=', 'teacher_reports.teacher_id');

		$query->leftJoin('users as student', 'student.id', '=', 'teacher_reports.created_by');

		$query->leftJoin('courses', 'courses.id', '=', 'teacher_reports.course_type');

		$query->leftJoin('grade_levels as grade', 'grade.id', '=', 'teacher_reports.lesson_type');

		$query->leftJoin('new_lessons as lesson', 'lesson.id', '=' ,'teacher_reports.lesson_id');

		$query->leftJoin('student_schedules as schedule', 'schedule.id', '=', 'lesson.schedule_id');

		if($year != null):
			
			$query->whereYear('teacher_reports.date', $year);

		endif;

		if($month != null):

			$query->whereMonth('teacher_reports.date', $month);

		endif;

		if($odb == 'name' && ($order == 'asc' || $order == 'desc')):

			$query->orderBy('teacher.full_name', $order);

		elseif($odb == 'date' && ($order == 'asc' || $order == 'desc')):

			$query->orderBy('teacher_reports.date', $order);

		elseif($odb == 'course' && ($order == 'asc' || $order == 'desc')):

			$query->orderBy('courses.label', $order);

		elseif($odb == 'grade' && ($order == 'asc' || $order == 'desc')):

			$query->orderBy('grade.label', $order);

		elseif($odb == 'lesson' && ($order == 'asc' || $order == 'desc')):

			$query->orderBy('lesson.lesson_name', $order);

		elseif($odb == 'progress' && ($order == 'asc' || $order == 'desc')):

			$query->orderBy('teacher_reports.lesson_day', $order);

		elseif($odb == 'student' && ($order == 'asc' || $order == 'desc')):

			$query->orderBy('student.romaji_name', $order);

		endif;

		$reports = $query->get();

		$date = date('m-d-Y His');

        return Excel::download(new TeacherExport($reports, $q), $date. '-teacher.xlsx');
    }

    public function export_student(Request $request){

    	if($this->user->role == 3 || $this->user->role == 4 || $this->user->role == 5) abort(403);

    	$room = $request->get('classroom') ?? '';
		
		$year = $request->get('year') ?? '';
		
		$month = $request->get('month') ?? '';

		$odb 	= $request->get('odb') ?? '';

		$order 	= $request->get('order') ?? '';

		$student 	= $request->get('student') ?? '';

		$q = [
			'room' => $room,
			'year' => $year,
			'month' => $month,
			'student' => $student
		];

		$sort = [
			'odb'	=> $odb,
			'order'	=> $order
		];

		$query = \App\StudentReport::query();
		
		$query->select(
			'student_reports.date', 
			'schedule.start_time',
			'student.kanji_name', 
			'student.romaji_name',
			'course.label',
			'grade.label as grade',
			'lesson.lesson_name',
			'student_reports.lesson_day',
			'teacher.full_name',
			'student_reports.student',
			'student_reports.listening',
			'student_reports.writing',
			'student_reports.grammar',
			'student_reports.speaking'
		);

		$query->leftJoin('users as student', 'student.id', '=', 'student_reports.student_id');

		$query->leftJoin('new_lessons as lesson', 'lesson.id', '=', 'student_reports.lesson_id');

		$query->leftJoin('student_schedules as schedule', 'schedule.id', '=', 'lesson.schedule_id');

		$query->leftJoin('courses as course', 'course.id', '=', 'student_reports.course_type');

		$query->leftJoin('grade_levels as grade', 'grade.id', '=', 'student_reports.lesson_type');

		$query->leftJoin('users as teacher', 'teacher.id', '=', 'student_reports.created_by');
		
		if($this->user->role == 2):
		
			$query->whereIn('student_reports.student_id', $this->student_id);

		endif;

		if($room != '' && $room != 'all'):
			
			$query->where('student_reports.room_id', $room);

		endif;

		if($year != null):

			$query->whereYear('student_reports.date', $year);
			
		endif;

		if($month != null):

			$query->whereMonth('student_reports.date', $month);

		endif;

		if($student != null):

			$query->where('student_reports.student_id', $student);

		endif;

		if($odb == 'name' && ($order == 'asc' || $order == 'desc')):

			$query->orderBy('student.romaji_name', $order);

		elseif($odb == 'date'  && ($order == 'asc' || $order == 'desc')):

			$query->orderBy('student_reports.date', $order);

		elseif($odb == 'course'  && ($order == 'asc' || $order == 'desc')):

			$query->orderBy('course.label', $order);

		elseif($odb == 'grade'  && ($order == 'asc' || $order == 'desc')):

			$query->orderBy('grade.label', $order);

		elseif($odb == 'lesson'  && ($order == 'asc' || $order == 'desc')):

			$query->orderBy('lesson.lesson_name', $order);

		elseif($odb == 'progress'  && ($order == 'asc' || $order == 'desc')):

			$query->orderBy('student_reports.lesson_day', $order);

		elseif($odb == 'teacher'  && ($order == 'asc' || $order == 'desc')):
		
			$query->orderBy('teacher.full_name', $order);

		endif;
		
		$reports = $query->get();

		$roomName = \App\ClassRoom::find($room);
		
		$date = date('m-d-Y His');

		return Excel::download(new StudentExport($reports, $q, $roomName), $date.'-student.xlsx');
    }

}
