<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Storage;

use Webpatser\Uuid\Uuid;

use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

use Session;

use Illuminate\Support\Facades\Mail;

use App\Mail\MailTemplate;

use App\Mail\CommentPosted;

use App\Mail\VideoHasStarted;

use App\Http\Requests\NewLessonRequest;

use File;

use Imagick;

class LessonController extends Controller
{

    private $user;

    public function __construct(){

        //$this->middleware('admin');

        $this->middleware(function($request, $next){
            
            $this->user  = \App\Helpers\UserHelper::user_data(Auth::id());

            return $next($request);

        });

    }

    public function index(Request $request){
        $lesFuture = null;
        $lesPast = null;
        $lessons = null;
        
        if(Auth::user()->role == student){
            $lesFuture = \App\StudentSchedule::with(['course','grade','lesson','teacher'])->whereDate('date', '>=',Carbon::now())
                ->where('user_id', Auth::id())
                ->orderBy('date', 'asc')
                ->get();
              
            

            $lesPast = \App\StudentSchedule::with(['course','grade','lesson','teacher'])->whereDate('date', '<',Carbon::now())
                ->where('user_id', Auth::id())
                ->orderBy('date', 'desc')
                ->get();
           
        }elseif(Auth::user()->role == teacher){
            $lesFuture = \App\StudentSchedule::with(['course','grade','lesson','student'])->whereDate('date', '>=',Carbon::now())
                        ->where('teacher_id', Auth::id())
                        ->orderBy('date', 'asc')
                        ->get();

            $lesPast = \App\StudentSchedule::with(['course','grade','lesson','student'])->whereDate('date', '<',Carbon::now())
                        ->where('teacher_id', Auth::id())
                        ->orderBy('date', 'desc')
                        ->get();
        }
        else if(Auth::user()->role == admin || Auth::user()->role == aica){
            $lesFuture = \App\StudentSchedule::with(['course','grade','lesson','student','teacher'])->whereDate('date', '>=',Carbon::now())
                        //->where('teacher_id', Auth::id())
                        ->orderBy('date', 'asc')
                        //->limit(5)
                        ->get();

            $lesPast = \App\StudentSchedule::with(['course','grade','lesson','student','teacher'])->whereDate('date', '<',Carbon::now())
                        //->where('teacher_id', Auth::id())
                        ->orderBy('date', 'desc')
                        //->limit(5)
                        ->get();
        }
        else if(Auth::user()->role == class_room ){
            $roomAdmin = \App\ClassRoomAdmin::select('class_room_id')->where('user_id', $this->user->id)->get();

                $room_id = [];

                if($roomAdmin):

                    foreach($roomAdmin as $ra):

                        $room_id[] = $ra->class_room_id;

                    endforeach;

                endif;

                $students = \App\ClassRoomStudent::whereIn('room_id', $room_id)->get();

                $student_id = [];

                if($students):
                    foreach($students as $student):
                    
                        $student_id[] = $student->user_id;

                    endforeach;
                endif;
            $lesFuture = \App\StudentSchedule::with(['course', 'grade', 'lesson', 'teacher'])->whereIn('user_id', $student_id)->whereDate('date', '>=',Carbon::now())
                        //->where('teacher_id', Auth::id())
                        ->orderBy('date', 'asc')
                        //->limit(5)
                        ->get(); 
            $lesPast = \App\StudentSchedule::with(['course','grade','lesson','student','teacher'])->whereIn('user_id', $student_id)->whereDate('date', '<',Carbon::now())
                        //->where('teacher_id', Auth::id())
                        ->orderBy('date', 'desc')
                        //->limit(5)
                        ->get();            

        }
        
        $lessons = $lesFuture->merge($lesPast);
        //dd($lessons);
    	return view('lessons.index')
            ->with('title', 'Lessons')
            ->with('lessons', $lessons);
    		

    }

    public function create(Request $request){
        
    	$schedule = $request->get('schedule') ?? '';

    	$grade_level = \App\GradeLevel::where('status', 1)->get();
    	
        $user_id = Auth::id();
        $courses = DB::table('courses')->get();
        $templates = DB::table('templates')
                    ->join('courses', 'courses.id', '=', 'templates.course_type')
                    ->join('grade_levels', 'grade_levels.id', '=', 'templates.lesson_type')   
                    ->select('templates.*', 'courses.label AS course_label', 'grade_levels.label AS grade_label')
                    ->get();

    	$class_time  = \App\TeacherSchedule::whereNotIn('id', function($query) use($user_id){
    		$query->select('schedule_id')
    		->from('lessons')
    		->where('user_id', $user_id);
    	})->where('user_id', Auth::id())->orderBy('date','asc')->get(); 

    	return view('lessons.create')
    		->with('title', 'Create lesson')
    		->with('grade_level', $grade_level)
    		->with('class_time', $class_time)
            ->with('schedule', $schedule)
            ->with('templates', $templates)
            ->with('courses', $courses);
    }

    public function store(NewLessonRequest $request){
       // dd($request->input());
        $template = \App\Template::find($request->template);
    	$lessonId = '';
    	// $request->validate([
        //     'lesson_name' => 'required',
        //     'grade_level' => 'required',
        //     'class_time'  => 'required',
        //     // 'description' => 'required',
        // ]);

        $files = $request->file('attachment');
        $lesson = $request->all();

        $lesson['created_by'] = Auth::id();
        $lesson['course_type'] = $template->course_type;
        $lesson['lesson_type'] =  $template->lesson_type;
        //dd($data);
        $lesson = \App\NewLesson::create($lesson); 
        

        $lessonId = $lesson->id;
        
        if($files != null):
        	$i = 0;
        	foreach($files as $file):
        		$i++;
        		
        		$uuid = Uuid::generate(1)->string;

        		$file_name = $file->getClientOriginalName();
        		$extention = $file->getClientOriginalExtension();
        		$attachment= $lessonId . $uuid . $i .'.'. $extention;

        		$resource = fopen( $file->getRealPath(), 'r');
        		
                $file->move('uploads/lessons/', $attachment);
        		//Storage::disk('public')->put($attachment, $resource);

        		\App\LessonAttachment::create([
					'lesson_id'		=> $lessonId,
					'attachment'	=> $attachment,
					'original_name'	=> $file_name
        		]);
        	endforeach;
        endif;

        $media       = $request->file('media');
        
        if($media != null):

            $lesson_id      = $lessonId;
            $source_data    = null;
            $original_name  = null;
            
            for($i = 0; $i < count($media); $i++):
            
                $upload = $request->file('media')[$i];
                $uid = Uuid::generate(1)->time;
                    
                $original_name  = $upload->getClientOriginalName();
                $ext            = $upload->getClientOriginalExtension();
                $source_data    = $uid . $i .'.'. $ext;

                $upload->move('uploads/video/', $source_data);

                \App\LessonMedia::create([
                    'lesson_id'     => $lesson_id,
                    'source'        => 'local',
                    'source_data'   => $source_data,
                    'original_name' => $original_name
                ]);
            endfor;
        endif;

        /*
        if($lessonId){
            $sched_id = DB::table('lessons')
                        ->select('lessons.id', 'lessons.schedule_id')
                        ->where('lessons.id', '=', $lessonId)
                        ->first();
            
            $data = DB::table('users')
                    ->join('schedules_student', 'users.id', '=', 'schedules_student.user_id')
                    ->join('lessons', 'schedules_student.schedule_id', '=', 'lessons.schedule_id')
                    ->join('students', 'schedules_student.user_id', '=', 'students.user_id')
                    ->select('users.username', 'users.email', 'lessons.slug','students.romaji_name')
                    ->where('schedules_student.schedule_id', '=', $sched_id->schedule_id)
                    ->get();
            
            foreach($data as $mail){
                    Mail::to($mail->email)->queue(new MailTemplate($mail));
            }
        }*/
        Session::flash('success_message', __('label.lesson_added'));

        return response()->json([
            'response'  => 200,
            'redirect'  => url('lessons')
        ]);
    }

    public function edit($id){
       
        $query = \App\NewLesson::with(['attachment', 'media'])->where('created_by', Auth::id())->find($id);
        //dd($query);
        $templates = DB::table('new_lessons')
                ->join('courses', 'courses.id', '=', 'new_lessons.course_type')
                ->join('grade_levels', 'grade_levels.id', '=', 'new_lessons.lesson_type')   
                ->select('new_lessons.*', 'courses.label AS course_label', 'grade_levels.label AS grade_label')
                ->where('new_lessons.id', '=', $id)
                ->first();
        //dd($templates);
        return view('lessons.edit')          
            ->with('templates', $templates)
            ->with('title', 'Edit Template')
            ->with('query', $query);
           

    }

    public function update($id, Request $request){
        //dd($request->input());
        //$template = \App\Template::find($request->template);
        $query = \App\NewLesson::where('created_by', Auth::id())->find($id);

        if($query == null) abort(404);
        
        // $request->validate([
        //     'lesson_name' => 'required',
        //     'grade_level' => 'required',
        //     'class_time'  => 'required',
        //     //'description' => 'required',
        // ]);
        
        $files = $request->file('attachment');
        
        $data = $request->all();
        $data['updated_by'] = Auth::id();
        
        $template = \App\NewLesson::find($id);
        $template->update($data); 
        
        
        if($files != null):
            $i = 0;
            foreach($files as $file):
                $i++;
                
                $uuid = Uuid::generate(1)->string;

                $file_name = $file->getClientOriginalName();
                $extention = $file->getClientOriginalExtension();
                $attachment= $query->id . $uuid . $i .'.'. $extention;

                $resource = fopen( $file->getRealPath(), 'r');
                
                $file->move('uploads/lessons/', $attachment);

                \App\Attachment::create([
                    'type'          => 0,
                    'lesson_id'     => $query->id,
                    'attachment'    => $attachment,
                    'original_name' => $file_name
                ]);
            endforeach;
        endif;

        $media       = $request->file('media');
        
        if($media != null):

            $lesson_id      = $query->id;
            $source_data    = null;
            $original_name  = null;
            
            for($i = 0; $i < count($media); $i++):
            
                $upload = $request->file('media')[$i];
                $uid = Uuid::generate(1)->time;
                    
                $original_name  = $upload->getClientOriginalName();
                $ext            = $upload->getClientOriginalExtension();
                $source_data    = $uid . $i .'.'. $ext;

                $upload->move('uploads/video/', $source_data);

                \App\Attachment::create([
                    'type'          => 1,
                    'lesson_id'     => $lesson_id,
                    'source'        => 'local',
                    'source_data'   => $source_data,
                    'original_name' => $original_name
                ]);
            endfor;
        endif;

        
       

        Session::flash('success_message', 'Lesson has been updated');

        return response()->json([
            'response'  => 200,
            'redirect'  => url('lessons')
        ]);
    }

    public function thread($slug, Request $request){
        $students = null;
        $comments = null;
        $student_id = $request->get('student') ?? null;
        $from     = Auth::id();
        $to       = null;

    	$lesson = \App\Lesson::with(['attachment', 'user_data'])->where('slug', $slug)->first();
        
    	if($lesson == null) abort(404);

        if(Auth::user()->role == 4):
            
            $check_schedule = \App\ScheduleStudent::where('schedule_id', $lesson->schedule_id)->where('user_id', Auth::id())->first();
            
            if($check_schedule == null) abort(404);

        endif;

        if(Auth::user()->role != 4): //Admin and Teacher
            $students = \App\ScheduleStudent::with('info')->where('schedule_id', $lesson->schedule_id)->get();
            $to       = $student_id;
            $from     = $lesson->user_id;
        else://Students
            $to       = $lesson->user_id;
            $student_id = Auth::id();
        endif;
        
        $lesson_id = $lesson->id;

        if($student_id != null):
            $comments = \App\LessonThread::with('user')->where(function($query) use($lesson_id, $from, $to){
                $query->where('lesson_id', $lesson_id)
                      ->where('from', $from)
                      ->where('to', $to);
            })->orWhere(function($query) use($lesson_id, $from, $to){
                $query->where('lesson_id', $lesson_id)
                      ->where('from', $to)
                      ->where('to', $from);
            })->orderBy('id', 'asc')->get();
            //$comments = \App\LessonThread::with(['student', 'teacher'])->where('lesson_id', $lesson->id)->where('teacher', $teacher)->where('student', $student_id)->orderBy('id', 'asc')->get();
        endif;
        //dd($lesson_id);
        $chats = DB::table('lesson_chats')
                ->join('users', 'users.id', '=', 'lesson_chats.user_id')
                ->select('lesson_chats.*', 'users.username', 'users.avatar')
                ->where('lesson_chats.lesson_id', '=', $lesson_id)
                ->get();
        //dd($chats);
    	return view('lessons.thread')
    		->with('title', $lesson->title)
    		->with('lesson', $lesson)
            ->with('students', $students)
            ->with('student_id', $request->get('student'))
            ->with('comments', $comments)
            ->with('chats', $chats);

    }

    public function add_comment($slug, Request $request){
        
        if(Auth::user()->role === 1) abort(403);

        $student_id = $request->get('student');
        
        $from = Auth::id();
        $to   = null;
        $attachment = null;
        $original_name = null;

        $lesson = \App\Lesson::with(['attachment', 'teacher', 'schedule'])->where('slug', $slug)->first();
        
        if($lesson == null) abort(404);

        $request->validate([
            'note' => 'required',
        ]);

        if(Auth::user()->role === 5):
            $to = $student_id;
        elseif(Auth::user()->role === 4):
            $to = $lesson->user_id;
        endif;

        $file = $request->file('attachment');
        
        if($file != null):
            $uuid = Uuid::generate(1)->string;

            $file_name = $file->getClientOriginalName();
            $extention = $file->getClientOriginalExtension();
            $attachment= $uuid .'.'. $extention;
            $original_name = $file_name;

            $resource = fopen( $file->getRealPath(), 'r');
            $file->move('uploads/lessons/', $attachment);
            //Storage::disk('public')->put($attachment, $resource);
        endif;
       
        $comments = \App\LessonThread::create([
            'lesson_id' => $lesson->id,
            'from'      => $from,
            'to'        => $to,
            'comment'   => $request->input('note'),
            'attachment'=> $attachment,
            'original_name'=> $original_name
        ]);
        $insertedId = $comments->id;
        
        if($insertedId){
            $toData = '';
            $fromData = '';
    
            //last inserted ID
            $latest = DB::table('lesson_threads')
                        ->select('lesson_threads.to','lesson_threads.from')
                        ->where('lesson_threads.id','=', $insertedId)
                        ->first();
    
            $toRole = DB::table('users')
                        ->select('users.role')
                        ->where('users.id','=', $latest->to)
                        ->first();
            $fromRole = DB::table('users')
                        ->select('users.role')
                        ->where('users.id','=', $latest->from)
                        ->first();
            //To
            if($toRole->role === 5){
                $teacher = DB::table('users')
                            ->select('users.id','users.email', 'users.role', 'teachers.full_name as name')
                            ->join('teachers', 'users.id', '=', 'teachers.user_id')
                            ->where('teachers.user_id', '=', $latest->to)
                            ->first();
                $toData = $teacher;             
            }elseif($toRole->role === 4){
                $student = DB::table('users')
                        ->select('users.id','users.email', 'users.role', 'students.romaji_name as name')
                        ->join('students', 'users.id', '=', 'students.user_id')
                        ->where('students.user_id', '=', $latest->to)
                        ->first(); 
                $toData =  $student;            
            }
            //From

            $anchor = '#anchor'. $insertedId;
            if($fromRole->role === 5){
                $teacherF = DB::table('users')
                            ->select('users.id','users.email', 'users.role', 'teachers.full_name as name')
                            ->join('teachers', 'users.id', '=', 'teachers.user_id')
                            ->where('teachers.user_id', '=', $latest->from)
                            ->first();
                $fromData = $teacherF;  
    
            }elseif($fromRole->role === 4){
    
                $studentF = DB::table('users')
                        ->select('users.id','users.email', 'users.role', 'students.romaji_name as name')
                        ->join('students', 'users.id', '=', 'students.user_id')
                        ->where('students.user_id', '=', $latest->from)
                        ->first(); 
                $fromData =  $studentF; 
                $anchor =   '?student='. Auth::id() . $anchor;     //?student=3#anchor1
            }
    
            $mail_array = [
                "link" => url('lessons/'. $slug . $anchor),
                "to_email"=>$toData->email, 
                "to_name"=>$toData->name, 
                "from_email"=>$fromData->email, 
                "from_name"=>$fromData->name, 
                "slug"=>$slug
            ];
            
            Mail::to($mail_array['to_email'])->queue(new CommentPosted($mail_array));
        }
        return redirect(url()->previous() .'#message')->with('success_message', '<strong>Success!</strong> Your note successfully created.');
    }

    public function delete_attachment(Request $request){
        
        $id = $request->input('id');
       
        $query = \App\Attachment::find($id);

        if($query != null):
            File::delete('uploads/lessons/'. $query->attachment);
            //Storage::disk('public')->delete($query->attachment);
            $query->delete();
        endif;
    }

    public function delete_media(Request $request){
        $id = $request->input('id');
        //dd( $id);
        $query = \App\Attachment::find($id);
        
        if($query != null):
            if($query->type == 1):
                File::delete('uploads/video/'. $query->attachment);
            endif;
            $query->delete();

        endif;
    }

    public function conference($token, Request $request){

        $now = date('Y-m-d H:i:s', strtotime(Carbon::now()));

        $query = \App\NewLesson::query();
        
        //Carbon
        if($this->user->role == 4):
        
            $query->where('student_id', $this->user->id);
        
        elseif ($this->user->role == 5):
        
            $query->where('teacher_id', $this->user->id);

        endif;

        $lesson = $query->with(['student','teacher','grade','courses'])->where('token', $token)->with('schedule')->first();
       //dd($lesson);
        if(!$lesson) abort(404);


        //Validate Start and End of video
        if(env('APP_ENV') == 'production'):
            
            $startDate = $lesson->schedule->date;
            $startTime = $startDate .' '. $lesson->schedule->start_time;
            $endTime   = $startDate .' '. $lesson->schedule->end_time;

            if(strtotime($now) < strtotime($startTime) || strtotime($now) > strtotime($endTime)) abort(404);

        endif;
        //


        $images = \App\Attachment::where('lesson_id', $lesson->id)->where('type', 0)->get();
        $media  = \App\Attachment::where('lesson_id', $lesson->id)->where('type', 1)->get();
        //dd($media);
        $messages    = \App\LessonChat::with('user')->where('lesson_id', $lesson->id)->orderBy('id', 'asc')->get();

        $initImage = '';
        
        if(count($images)):
            $array = explode('.', $images[0]->attachment);
            $ext   = end($array);

            if($ext == 'pdf'):

                $img = $images[0]->attachment;

                $src  = public_path('uploads/lessons/'. $img);

                $image = new Imagick();

                $image->setResolution(300,300);
                //$image->setIteratorIndex(0);
                $image->readimage($src);

                $image->setImageFormat('jpg');

                $thumbnail = $image->getImageBlob();

                $preview = "<img src='data:image/jpg;base64,".base64_encode($thumbnail)."' />";

                $teacher = '';

                if($this->user->role == 5):
                    $teacher = '
                    <div class="pdf-control">
                        <div class="button-action pdf-prev" data-action="prev"><i class="mdi mdi-arrow-left"></i></div>
                        <div class="button-action pdf-next" data-action="next"><i class="mdi mdi-arrow-right"></i></div>
                        <div class="minimize_maximize" data-tag="maximize_pdf" data-target="pdf" id="target-pdf"><i class="mdi mdi-arrow-expand-all"></i></div>
                    </div>';
                endif;

                $initImage = $teacher. '
                
                <div class="pdf-preview">
                    '. $preview .'
                </div>
                ';
            else:
                $initImage = '<img id="previewImage" src="'. asset('uploads/lessons/'. $images[0]->attachment) .'" alt="">';
            endif;
        endif;
        //dd($lesson);
        return view('lessons.video')
            ->with('lesson', $lesson)
            ->with('messages', $messages)
            ->with('images', $images)
            ->with('media', $media)
            ->with('initImage', $initImage);
        /*
        $student_id = $request->get('student') ?? null;

        $lesson = \App\Lesson::with(['attachment', 'teacher', 'schedule'])->where('slug', $slug)->first();
        
        if($lesson == null) abort(404);

        if(Auth::user()->role == 4):
            
            $check_schedule = \App\ScheduleStudent::where('schedule_id', $lesson->schedule_id)->where('user_id', Auth::id())->first();
            
            if($check_schedule == null) abort(404);

        endif;

        $attachments = \App\LessonAttachment::where('lesson_id', $lesson->id)->get();
        $messages    = \App\LessonChat::with('user')->where('lesson_id', $lesson->id)->orderBy('id', 'asc')->get();
        $audios      = \App\LessonMedia::where('lesson_id', $lesson->id)->get();
        return view('lessons.video')
            ->with('lesson', $lesson)
            ->with('attachments', $attachments)
            ->with('messages', $messages)
            ->with('audios', $audios);
        */
    }

    public function save_chat(Request $request){
       
        \App\LessonChat::create([
            'lesson_id' => $request->input('lesson_id'),
            'user_id'   => $request->input('user_id'),
            'message'   => $request->input('message'),
        ]);
        
        return response()->json('200');
    }

    /*public function send_mail_video(Request $request){
        
        $data = DB::table('users')
                    ->join('schedules_student', 'users.id', '=', 'schedules_student.user_id')
                    ->join('lessons', 'schedules_student.schedule_id', '=', 'lessons.schedule_id')
                    ->join('students', 'schedules_student.user_id', '=', 'students.user_id')
                    ->select('users.username', 'users.email', 'lessons.slug','students.romaji_name')
                    ->where('schedules_student.schedule_id', '=', $request->input('lesson_id'))
                    ->get();
            
            foreach($data as $mail){

                    $maildata = [
                        'romaji_name' => $mail->romaji_name,
                        'slug' => $mail->slug,
                        'link' => url('lessons/'.$mail->slug)
                    ];

                    Mail::to($mail->email)->queue(new VideoHasStarted($maildata));
        }
       
        return response()->json($data);
    }*/

    public function add_attachment(Request $request){
        $attachment = $request->file('attachment');
        
        $lesson_id = $request->input('lesson_id');

        $type = 'image';

        if($attachment):
            
            $uuid = Uuid::generate(1)->time;

            $file_name = $attachment->getClientOriginalName();

            $extention = $attachment->getClientOriginalExtension();
            
            $fileName= $lesson_id . $uuid .'.'. $extention;

            $resource = fopen( $attachment->getRealPath(), 'r');
            
            $attachment->move('uploads/lessons/', $fileName);

            $attachment = \App\Attachment::create([
                'lesson_id'     => $lesson_id,
                'attachment'    => $fileName,
                'original_name' => $file_name,
                'type'          => 0
            ]);

            $image = asset('uploads/lessons/'. $attachment->attachment);

            $base = asset('uploads/lessons/'. $fileName);

            if($extention == 'pdf'):

                $type = 'pdf';

                $pdf = public_path('uploads/lessons/'. $attachment->attachment);

                $thumbnail = \App\Helpers\GlobalHelper::pdf_thumbnail($pdf);

                $image = base64_encode($thumbnail);

                $base = $fileName;

            endif;
            


            $data = [
                'image' => $image,
                'base'  => $base,
                'type'  => $type
            ];

            return response()->json($data);

        endif;

        
    }
    public function save_answer($id, Request $request){
        //dd( $request->all());
        $form_type = $request->input('form_type');
       
         //dd($request->input('writing_stu_ans'));


        $files = $request->file('attachment');
        $media       = $request->file('media');
        $lessons = '';
        //dd($files); 
        if(Auth::user()->role == student){
           if ($form_type == 1 || $form_type == 2){
              $lessons = \App\LessonThread::create([
                    'lesson_id'  =>  $id,
                    'from'       =>  Auth::user()->id,
                    'to'         =>  $request->input('created_by'),
                    'writing_stu_ans' =>  $request->input('writing_stu_ans')

                
                ]); 
           }elseif($form_type == 3){
                \App\LessonThread::create([
                'lesson_id'  =>  $id,
                'from'       =>  Auth::user()->id,
                'to'         =>  $request->input('created_by'),
                'writing_stu_ans' =>  $request->input('writing_stu_ans')
            
                ]);
           }
            
        }else{ //teacher
            $query =  DB::table('lesson_threads')
            ->where('id', $request->input('thread_id'))
            ->update([
                'writing_tea_res'    => $request->input('writing_tea_res')
            ]);

            if($files != null):
                $i = 0;
                foreach($files as $file):
                    $i++;
                    
                    $uuid = Uuid::generate(1)->string;
    
                    $file_name = $file->getClientOriginalName();
                    $extention = $file->getClientOriginalExtension();
                    $attachment= $id . $uuid . $i .'.'. $extention;
    
                    $resource = fopen( $file->getRealPath(), 'r');
                    
                    $file->move('uploads/lessons/', $attachment);
    
                    \App\Attachment::create([
                        'type'          => 0,
                        'lesson_id'     => $id,
                        'attachment'    => $attachment,
                        'original_name' => $file_name
                    ]);
                endforeach;
            endif;
    
            
            if($media != null):
    
                $lesson_id      = $id;
                $source_data    = null;
                $original_name  = null;
                
                for($i = 0; $i < count($media); $i++):
                
                    $upload = $request->file('media')[$i];
                    $uid = Uuid::generate(1)->time;
                        
                    $original_name  = $upload->getClientOriginalName();
                    $ext            = $upload->getClientOriginalExtension();
                    $source_data    = $uid . $i .'.'. $ext;
    
                    $upload->move('uploads/video/', $source_data);
    
                    \App\Attachment::create([
                        'type'          => 1,
                        'lesson_id'     => $lesson_id,
                        'source'        => 'local',
                        'attachment'   => $source_data,
                        'original_name' => $original_name
                    ]);
                endfor;
            endif;


        }
        
        return response()->json([
            'response'  => 200,
            'redirect'  => url('lessons/show/'.$id)
        ]);
    }

    public function show($id){
        $lessonDay = '';
        $studentSched = '';
        $newLesson = '';
        $comments = '';
        $tempArr = [];
        // $query = DB::table('new_lessons')
        //         ->join('courses', 'courses.id', '=', 'new_lessons.course_type')
        //         ->join('grade_levels', 'grade_levels.id', '=', 'new_lessons.lesson_type') 
        //         ->join('student_schedules', 'student_schedules.id', '=', 'new_lessons.schedule_id');
        // if(Auth::user()->role == student){
        //     $query->join('users', 'users.id', '=', 'new_lessons.teacher_id');
        // }  
        // if(Auth::user()->role == teacher){
        //     $query->join('users', 'users.id', '=', 'new_lessons.student_id');
        // }      
        // $query->select('new_lessons.*',  'courses.label AS course_label', 'grade_levels.label AS grade_label', 'student_schedules.date','student_schedules.start_time','student_schedules.end_time', 'users.full_name','users.romaji_name' )
        //         ->where('new_lessons.id', '=', $id);
                
        // $templates = $query->first();
        $templates = \App\NewLesson::with(['courses', 'grade',  'teacher', 'student','schedule'])->where('id', '=', $id)->first();
        $currDate = strtotime(date($templates->schedule->date.' '. $templates->schedule->start_time));
        //dd($templates->schedule->start_time);
        $allDates = \App\StudentSchedule::whereDate('date','<=',$templates->schedule->date)->where('user_id', $templates->student->id)->where('teacher_id', $templates->teacher->id)->orderBy('date', 'desc')->orderBy('start_time', 'desc')->get();
        foreach ($allDates as $value) {
            $datetime = strtotime(date($value->date.' '. $value->start_time));
            if($datetime < $currDate){
                $arrPush = [
                    'id' =>  $value->id,
                    'datetime' => $datetime,
                    'date' => $value->date,
                    'time' => $value->start_time
                    
                ];
                array_push($tempArr,$arrPush);
            }
        }
            
        if($tempArr != null){
            $newLesson = \App\NewLesson::where('schedule_id',$tempArr[0]['id'])->first();
            $comments = \App\StudentReport::where('lesson_id', $newLesson->id)->first();
        }
      
        
        //$currDay = intval($templates->lesson_day); 
        // if($currDay > 1 && $currDay <= 16){
        //     $lessonDay = $currDay - 1;
        //     $studentSched = \App\StudentSchedule::where('day',  $lessonDay)->where('group', $templates->schedule->group)->first();//797
        //     $newLesson = \App\NewLesson::where('schedule_id', $studentSched->id)->first();
        //     $comments = \App\StudentReport::where('lesson_id', $newLesson->id)->first();
          
        // }
        //dd($comments);
        if(!$templates) abort(404);
        $answers = DB::table('lesson_threads')
                ->select('lesson_threads.*')
                ->where('lesson_threads.lesson_id', '=', $id)
                ->first();
        $media = DB::table('attachments')
                ->select('attachments.*')
                ->where('attachments.lesson_id', '=', $id)
                ->where('attachments.type', '=', 1)
                ->get();
        $attachment = DB::table('attachments')
                ->select('attachments.*')
                ->where('attachments.lesson_id', '=', $id)
                ->where('attachments.type', '=', 0)
                ->get();
	    $chats = \App\LessonChat::with('user')->where('lesson_id', $id)->get();
        //dd($answers);

        return view('lessons.show')
                ->with('title', 'Lesson Details')
                ->with('answers', $answers)
                ->with('media', $media)
                ->with('attachment',$attachment)
                ->with('templates', $templates)
                ->with('chats', $chats)
                ->with('comments', $comments);
    }

    public function save_rating(Request $request){
       // dd($request->input());
        $data = $request->input();
        $lesson_id = $request->input('lesson_id');
        if($this->user->role == 5){
            $query =  DB::table('new_lessons')
            ->where('id', $lesson_id)
            ->update([
                'chat_flag'    => $request->input('chatFlag')
            ]);
            
            $rating = \App\StudentReport::create([
                'lesson_id'      =>   $request->input('lesson_id'),
                'student_id'     =>   $request->input('student_id'),
                'listening'      =>   $request->input('listening'),
                'student'        =>   $request->input('student'),
                'writing'        =>   $request->input('writing'),
                'grammar'        =>   $request->input('grammar'),
                'speaking'       =>   $request->input('speaking'),
                'date'           =>   $request->input('date'),
                'course_type'    =>   $request->input('course_type'),
                'lesson_type'    =>   $request->input('lesson_type'),
                'lesson_day'     =>   $request->input('lesson_day'),
                'room_id'        =>   $request->input('room_id'),
                'comments'       =>   $request->input('comment'),
                'created_by'     =>   $request->input('created_by'),
            ]);
        }
        else if($this->user->role == 4){

            \App\TeacherReport::create([
                'lesson_id'      =>   $request->input('lesson_id'),
                'teacher_id'     =>   $request->input('teacher_id'),
                'teacher'        =>   $request->input('teacher'),
                'date'           =>   $request->input('date'),
                'course_type'    =>   $request->input('course_type'),
                'lesson_type'    =>   $request->input('lesson_type'),
                'lesson_day'     =>   $request->input('lesson_day'),
                'room_id'        =>   $request->input('room_id'),
                'created_by'     =>   $request->input('created_by')
            ]);
        }
        
        return response()->json([
            'response'  => 200,
            'redirect'  => url('lessons/'.$lesson_id),
            
           
        ]);
    }

    public function get_pdf(Request $request){

        $page = $request->input('page');



        $src  = public_path('uploads/lessons/'. $request->input('src'));

        //$img = new Imagick();

        //$img->pingImage($src);

       // $num = $img->getNumberImages();

        //$next = $page >= ($num - 1) ? ($num - 1) : $page + 1;

        //$page = $page >= ($num - 1) ? ($num - 1) : $page;


        $image = new Imagick();
        
        $image->pingImage($src);

        $num = $image->getNumberImages();

        $num = $num - 1;

        $image->setResolution(300,300);

        //$next = $page >= ($num - 1) ? ($num - 1) : $page + 1;

        //$page = $page >= ($num - 1) ? ($num - 1) : $page;

        $pdf = $src.'['. $page .']';

        $image->readimage($pdf);

        $image->setImageFormat('jpg');

        $thumbnail = $image->getImageBlob();

        $data = [
            'src'       => $request->input('src'),
            'thumbnail' => base64_encode($thumbnail),
            'page'      => 0,
            'max'       => $num
        ];

        return response()->json($data);

    }

  
    
}
