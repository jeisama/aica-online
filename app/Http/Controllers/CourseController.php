<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Http\Requests\CourseRequest;

use Validator;

use Session;

class CourseController extends Controller
{
    public function __construct(){
        
        $this->middleware('admin');

    }


    /*
	|---------------------------------------------------------
	| Course Management
	|---------------------------------------------------------
    */
    public function index(){
    	
    	$courses = \App\Course::with('count')->where('status', 1)->get();
        
        //dd($courses);

    	return view('course.index')
    		->with('title', 'Course management.')
    		->with('courses', $courses);

    }

    /*
	|---------------------------------------------------------
	| Create Course
	|---------------------------------------------------------
    */
    public function create(){
    	
    	return view('course.create')
    		->with('title', 'Create course.');

    }

    /*
	|---------------------------------------------------------
	| Validate and Store Course
	|---------------------------------------------------------
    */
    public function store(CourseRequest $request){
    	
    	\App\Course::create([
            'label'     => $request->input('course'),
            'status'    => $request->input('status'),
            'created_by'=> Auth::id()
        ]);


        Session::flash('success_message', 'Course has been successfully created.');

        return response()->json([
            'response'  => 200,
            'redirect'  => url('courses')
        ]);
    }

    /*
	|---------------------------------------------------------
	| Edit Course
	|---------------------------------------------------------
    */
    public function edit($id){

    	$course = \App\Course::find($id);

    	if(!$course) abort(404);

    	return view('course.edit')
    		->with('title', 'Edit course.')
    		->with('course', $course);
    }

    /*
	|---------------------------------------------------------
	| Validate and Update Course
	|---------------------------------------------------------
    */
    public function update($id, Request $request){
    	
    	$course = \App\Course::find($id);

    	$course->update([
            'label'     => $request->input('course'),
            'status'    => $request->input('status'),
            'updated_by'=> Auth::id()
        ]);

        Session::flash('success_message', __('label.course_updated'));

        return response()->json([
            'response'  => 200,
            'redirect'  => url('courses')
        ]);
    }

    public function set_grade(Request $request){
    	
    	$course_id = $request->input('id');

    	$grades = $request->input('grade');

    	\App\CourseGradeLevel::where('course_id', $course_id)->delete();

    	if($grades):
    		foreach($grades as $grade):

    			\App\CourseGradeLevel::create([
					'course_id'	=> $course_id,
					'grade_id'	=> $grade
    			]);

    		endforeach;
    	endif;

    	Session::flash('success_message', __('label.course_change'));

    	return response()->json([
    		'response' => 200,
    		'redirect'  => url('courses')
    	]);

    }
}
