<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use Session;

use App\Http\Requests\LevelRequest;

class LevelController extends Controller
{
    
	private $user;

	public function __construct(){

		$this->middleware('admin');

		$this->middleware(function($request, $next){
            
            $this->user  = \App\Helpers\UserHelper::user_data(Auth::id());

            return $next($request);

        });

	}

	public function index(){

		$levels = \App\TeacherLevel::where('status', 1)->get();

		return view('level.index')
			->with('title', 'Teachers level.')
			->with('levels', $levels);

	}

	public function create(){

		return view('level.create')
			->with('title', 'Create teachers level.');
	
	}

	public function store(LevelRequest $request){

		\App\TeacherLevel::create($request->all());

		Session::flash('success', 'Teacher level has been created.');

        return response()->json([
            'response'  => 200,
            'redirect'  => url('level')
        ]);

	}

	public function edit($id){

		$level = \App\TeacherLevel::find($id);

		if(!$level) abort(404);

		return view('level.edit')
			->with('title', 'Edit teachers level.')
			->with('level', $level);

	}

	public function update($id, LevelRequest $request){

		\App\TeacherLevel::find($id)->update($request->all());

		Session::flash('success', 'Teacher level has been created.');

        return response()->json([
            'response'  => 200,
            'redirect'  => url('level')
        ]);
        
	}

	public function set_grade(Request $request){

		$grade = $request->input('grade');

		$level = $request->input('level');

		\App\Level::where('teacher_level', $level)->delete();

		if($grade):

			for($i = 0; $i < count($grade); $i++):
			
				\App\Level::create([
					'teacher_level'	=> $level,
					'grade_id'		=> $grade[$i],
				]);

			endfor;
		endif;

		Session::flash('success', 'Teacher level has been udpated.');

        return response()->json([
            'response'  => 200,
            'redirect'  => url('level')
        ]);
		//dd($request->input());

	}

}
