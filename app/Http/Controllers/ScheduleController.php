<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use Carbon\Carbon;

use Session;

use ScheduleHelper;

use Uuid;

use PDF;

use Illuminate\Support\Facades\URL;

use Spipu\Html2Pdf\Html2Pdf;

class ScheduleController extends Controller
{

    private $hasFranchise;

    private $user;

    private $room_id;

    private $student_id;

    public function __construct(){

        $this->middleware(function($request, $next){
            
            $this->user         = \App\Helpers\UserHelper::user_data(Auth::id());
            
            $this->hasFranchise = Auth::id() ? \App\Helpers\UserHelper::has_franchise(Auth::id()) : null;

            $this->room_id      = ScheduleHelper::myRoomId($this->user->id);

            $this->student_id   = ScheduleHelper::myStudentId($this->room_id);

            return $next($request);

        });

    }

    /*
    |-----------------------------------------------------
    | Schedule Management
    | Calendar View
    |-----------------------------------------------------
    */
    public function index(Request $request){
        
        $cdate      = $request->get('cdate') ?? date('Y-m');
        $id         = $request->get('id') ?? '';
        $mArray     = explode('-', $cdate);
        $month      = $cdate ? end($mArray) : date('m');
        
        $schedules  = array();
        
        $studentSchedule = null;

        $teacherSchedule = null;
        
        /*
        |--------------------------------------------------
        | Get My Students
        |--------------------------------------------------
        */
        $get_my_student = $this->get_my_student();

        $students   = $get_my_student['students'];

        $studentId  = $get_my_student['studentId'];

        if($this->user->role == 1)://Super Admin
            
            $studentSchedule = \App\StudentSchedule::with(['user', 'teacher', 'lesson'])->whereMonth('date', $month)->get()->groupBy('date');

            $teacherSchedule = \App\TeacherSchedule::with(['user'])->whereMonth('date', $month)->get()->groupBy('date');

        elseif($this->user->role == 2)://Classroom Admin
            
            if($students && $studentId):
            
                $studentSchedule = \App\StudentSchedule::with(['user', 'teacher',  'lesson'])->whereIn('user_id', $studentId)->whereMonth('date', $month)->get()->groupBy('date');
            
            endif;

        elseif($this->user->role == 3)://AICA Admin

            $teacherSchedule = \App\TeacherSchedule::with(['user'])->whereMonth('date', $month)->get()->groupBy('date');
            
            if($students && $studentId):
                $studentSchedule = \App\StudentSchedule::with(['user', 'teacher',  'lesson'])->whereIn('user_id', $studentId)->whereMonth('date', $month)->get()->groupBy('date');
            endif;
            //$merged = $teacherSchedule->merge($studentSchedule);
        elseif($this->user->role == 4)://Student
            
            $studentSchedule = \App\StudentSchedule::with(['user', 'teacher',  'lesson'])->where('user_id', $this->user->id)->whereMonth('date', $month)->get()->groupBy('date');

        elseif($this->user->role == 5)://Teacher
        
            $teacherSchedule = \App\TeacherSchedule::with(['user'])->where('user_id', $this->user->id)->whereMonth('date', $month)->get()->groupBy('date');
            
            $studentSchedule = \App\StudentSchedule::with(['user', 'teacher',  'lesson'])->where('teacher_id', $this->user->id)->whereMonth('date', $month)->get()->groupBy('date');

        endif;

        $schedules = [
            'student' => $studentSchedule,
            'teacher' => $teacherSchedule
        ];

        $calendar = \App\Helpers\CalendarHelper::schedule($schedules, $cdate, $this->student_id, $this->user->role);
        
        return view('schedules.index')
            ->with('title', 'Schedule Management')
            ->with('calendar', $calendar)
            ->with('cdate', $cdate)
            ->with('user', null)
            ->with('id', $id);
    }

    /*
    |-----------------------------------------------------
    | Create Schedule
    | Render Schedule Form
    |-----------------------------------------------------
    */
    /*
    public function create(){

        if($this->hasFranchise > 0):

        //$students = \App\Users::where('status', 1)->where('role', 4)->get();

            $roomAdmin = \App\ClassRoomAdmin::where('user_id', $this->user->id)->get();
                
            $room_id = [];

            if($roomAdmin):
                foreach($roomAdmin as $ra):
                    $room_id[] = $ra->class_room_id;
                endforeach;
            endif;

            $students = \App\ClassRoomStudent::select('users.id', 'users.romaji_name', 'users.kanji_name')->join('users', 'users.id', '=', 'class_rooms_student.user_id')->whereIn('class_rooms_student.room_id', $room_id)->where('users.role', 4)->get();

            $course = \App\Course::where('status', 1)->get();

        	return view('schedules.create')
        		->with('title', 'Create Schedule')
                ->with('course', $course)
                ->with('students', $students);
    	else:
            abort(403);
        endif;
    }
    */
    /*
    |-----------------------------------------------------
    | Validate and Save Schedule 
    |-----------------------------------------------------
    */
    /*
    public function store(Request $request){
        
        //$schedule = 
        $Uuid = Uuid::generate()->time;

        $student    = $request->input('student');
        $course     = $request->input('course');
        $grade      = $request->input('grade');
        $schedule   = $request->input('schedule');
        $date       = $request->input('start_date');
        $start_time = $request->input('start_time');
        $end_time   = $request->input('end_time');

        $nDate = [];

        if(count($course) > 0):
            for($i = 0; $i < count($course); $i++):
                if($course[$i] != '' && $grade[$i] != '' && $schedule[$i] != '' && $date[$i] != '' && $start_time[$i] != '' && $end_time[$i] != ''):
                
                    $numWeek = $schedule[$i] == 16 || $schedule[$i] == 1 ? 1 : 2;
                    
                    \App\StudentSchedule::create([
                        'user_id'       => $student,
                        'course_id'     => $course[$i],
                        'grade_id'      => $grade[$i],
                        'schedule_id'   => $schedule[$i],
                        'date'          => date('Y-m-d', strtotime($date[$i])),
                        'start_time'    => date('H:i:s', strtotime($start_time[$i])),
                        'end_time'      => date('H:i:s', strtotime($end_time[$i])),
                        'created_by'    => Auth::id(),
                        'group'         => $Uuid
                    ]);
                    
                    $nDate[] = date('Y-m-d', strtotime($date[$i]));

                    for($j = 1; $j < $schedule[$i]; $j++):
                        $date[$i] = strtotime($date[$i]);
                        $date[$i] = date('Y-m-d', strtotime('+'. $numWeek .' week', $date[$i]));
                        \App\StudentSchedule::create([
                            'user_id'       => $student,
                            'course_id'     => $course[$i],
                            'grade_id'      => $grade[$i],
                            'schedule_id'   => $schedule[$i],
                            'date'          => date('Y-m-d', strtotime($date[$i])),
                            'start_time'    => date('H:i:s', strtotime($start_time[$i])),
                            'end_time'      => date('H:i:s', strtotime($end_time[$i])),
                            'created_by'    => Auth::id(),
                            'group'         => $Uuid
                        ]);
                    endfor;
                endif;
            endfor;
        endif;
        
        return redirect('schedules')->with('success', 'Schedule has been successfully created.');
    }
    */

    /*
    |-----------------------------------------------------
    | Edit Schedule
    | Render Edit Form
    |-----------------------------------------------------
    */
    /*
    public function edit($id){

        $schedule = \App\StudentSchedule::find($id);

        if($schedule == null) abort(404);

        if(!in_array($schedule->user_id, $this->student_id)) abort(403);


        $course = \App\Course::where('status', 1)->get();

        $grades = \App\CourseGradeLevel::select('grade_levels.*')->join('grade_levels', 'grade_levels.id','=','course_grade_levels.grade_id')->where('course_grade_levels.course_id', $schedule->course_id)->get();
        
        return view('schedules.edit')->with([
                'title'     => 'Edit Schedule',
                'schedule'  => $schedule,
                'course'    => $course,
                'grades'    => $grades
            ]);
    }
    */
    /*
    |-----------------------------------------------------
    | Update Schedule
    |-----------------------------------------------------
    */
    /*
    public function update($id, Request $request){

        $previousUrl = $request->session()->previousUrl();
        //dd($request->session());
        $request->validate([
            'course'        => 'required',
            'grade'         => 'required',
            'schedule'      => 'required',
            'start_date'    => 'required|date',
            'start_time'    => 'required|date_format:H:i',
            'end_time'      => 'required|date_format:H:i|after:start_time',
        ]);

        $Uuid = Uuid::generate()->time;

        $schedule = \App\StudentSchedule::find($id);

        if($schedule == null) abort(404);

        if(!in_array($schedule->user_id, $this->student_id)) abort(403);

        //Delete All schedule and replace with new one
        \App\StudentSchedule::where('group', $schedule->group)->where('user_id', $schedule->user_id)->delete();

        $numWeek = $request->input('schedule') == 16 || $request->input('schedule') == 1 ? 1 : 2;
        
        $date = $request->input('start_date');

        \App\StudentSchedule::create([
            'user_id'       => $schedule->user_id,
            'course_id'     => $request->input('course'),
            'grade_id'      => $request->input('grade'),
            'schedule_id'   => $request->input('schedule'),
            'date'          => date('Y-m-d', strtotime($date)),
            'start_time'    => date('H:i:s', strtotime($request->input('start_time'))),
            'end_time'      => date('H:i:s', strtotime($request->input('end_time'))),
            'created_by'    => Auth::id(),
            'group'         => $Uuid
        ]);
        


        for($j = 1; $j < $request->input('schedule'); $j++):

            $date = strtotime($date);

            $date = date('Y-m-d', strtotime('+'. $numWeek .' week', $date));

            \App\StudentSchedule::create([
                'user_id'       => $schedule->user_id,
                'course_id'     => $request->input('course'),
                'grade_id'      => $request->input('grade'),
                'schedule_id'   => $request->input('schedule'),
                'date'          => date('Y-m-d', strtotime($date)),
                'start_time'    => date('H:i:s', strtotime($request->input('start_time'))),
                'end_time'      => date('H:i:s', strtotime($request->input('end_time'))),
                'created_by'    => Auth::id(),
                'group'         => $Uuid
            ]);
        endfor;

        return redirect('schedules')->with('success', 'Schedule has been successfully updated.');
        
    }
    */


    /*
    |-----------------------------------------------------------------
    | TEACHER
    | Teachers Schedule
    | Create
    |-----------------------------------------------------------------
    */
    public function teacher_create(){
        
        $teachers = \App\Users::where('role', 5)->where('status', 1)->orderBy('full_name', 'asc')->get();

        return view('schedules.forms.teacher.create')
            ->with('title', 'Create schedule')
            ->with('teachers', $teachers);

    }

    public function teacher_store(Request $request){
        
        $request->validate([
            'teacher'    => 'required',
            'startdate'  => 'required|date',
            'enddate'    => 'required|date|after:startdate',
        ]);

        $Uuid = Uuid::generate()->time;

        $teacher    = $request->input('teacher');

        $day        = $request->input('day');

        $timein     = $request->input('timein');

        $timeout    = $request->input('timeout');

        $startDate  = $request->input('startdate');

        $endDate    = $request->input('enddate');
 
        for($i = 0; $i < count($day); $i++):

            $data = \App\Helpers\GlobalHelper::getDayofWeek($startDate, $endDate, $day[$i]);

            for($x = 0; $x < count($data); $x++):
                \App\TeacherSchedule::create([
                    'user_id'       => $teacher,
                    'date'          => date('Y-m-d', strtotime($data[$x])),
                    'time_in'       => $timein[$i],
                    'time_out'      => $timeout[$i],
                    'date_range'    => date('Y-m-d', strtotime($startDate)) .','. date('Y-m-d', strtotime($endDate)),
                    'created_by'    => $this->user->id,
                    'group'         => $Uuid
                ]);
            endfor;
            
        endfor;

        Session::flash('success', 'Schedule has been successfully created.');

        return response()->json([
            'response'  => 200,
            'redirect'  => url('schedules')
        ]);
    }

    public function teacher_edit($id){

        $schedules = \App\TeacherSchedule::with('user')->where('id', $id)->first();

        $endDate = explode(",", $schedules->date_range);

        return view('schedules.forms.teacher.edit')
            ->with('title', 'Edit schedule.')
            ->with('schedules', $schedules )
            ->with('dateRange', $endDate);

    }

    public function teacher_update($id, Request $request){

        $query = \App\TeacherSchedule::find($id);

        $user_id    = $request->input('teacher');
        $day        = $request->input('day');
        $timein     = $request->input('timein');
        $timeout    = $request->input('timeout');
        $startdate  = $request->input('startdate');
        $enddate    = $request->input('enddate');

        $num1 = date('N', strtotime($query->date));

        $num2 = $day;

        $numDiff = abs($num1 - $num2);

        

        $get = \App\TeacherSchedule::whereDate('date', '>=', $query->date)->where('group', $query->group)->where('user_id', $user_id)->get();

        if($get):
            
            foreach($get as $res):

                if(date('N', strtotime($res->date)) == date('N', strtotime($query->date))):
                    
                    $date = new \Carbon\Carbon($res->date);
                    
                    $newDate = '';

                    if($num1 >= $num2):

                        $newDate = $date->subDays($numDiff);
                    
                    else:
                    
                        $newDate = $date->addDays($numDiff);
                    
                    endif;

                    $data = [
                        'date'      => date('Y-m-d', strtotime($newDate)),
                        'time_in'   => date('H:i:s', strtotime($timein)),
                        'time_out'  => date('H:i:s', strtotime($timeout)),
                    ];

                    $res->update($data);
                endif;

            endforeach;
        
        endif;

        Session::flash('success', 'Schedule has been successfully updated.');

        return response()->json([
            'response'  => 200,
            'redirect'  => url('schedules')
        ]);
    }

    /*
    |-----------------------------------------------------------------
    | STUDENT
    | Student Schedule
    | Create
    |-----------------------------------------------------------------
    */

    public function student_create(){

        if($this->hasFranchise > 0):

            $roomAdmin = \App\ClassRoomAdmin::where('user_id', $this->user->id)->get();
                
            $room_id = [];

            if($roomAdmin):

                foreach($roomAdmin as $ra):

                    $room_id[] = $ra->class_room_id;

                endforeach;

            endif;

            $students = \App\ClassRoomStudent::select('users.id', 'users.romaji_name', 'users.kanji_name')->join('users', 'users.id', '=', 'class_rooms_student.user_id')->whereIn('class_rooms_student.room_id', $room_id)->where('users.role', 4)->get();

            $course = \App\Course::where('status', 1)->get();
            
            return view('schedules.forms.student.create')

                ->with('title', 'Create Schedule')

                ->with('course', $course)

                ->with('students', $students);
        else:

            abort(403);
        
        endif;

    }

    public function student_store(Request $request){

        $Uuid = Uuid::generate()->time;

        $request->validate([
            'student'       => 'required',
            'course'        => 'required',
            'grade'         => 'required',
            'frequency'     => 'required'
        ]);

        $teacher    = $request->input('teacher');
        $student    = $request->input('student');
        $course     = $request->input('course');
        $grade      = $request->input('grade');
        $frequency  = $request->input('frequency');
        $day        = $request->input('day');
        $date       = $request->input('date');
        $time       = $request->input('time');

        $dateArray  = array_filter($date);

        $lastDate = end($dateArray);

        $numWeek    = $frequency == 16 ? 1 : 2;

        if(count($day) > 0):
            
            for($i = 0; $i < count($day); $i++):

                if($teacher[$i] != null && $time[$i] != null && $date[$i] != null):
                
                    $start_time = date('H:i:s', strtotime($time[$i]));

                    $end_time   = date("H:i:s", strtotime('+15 minutes', strtotime($time[$i])));

                    $schedule = \App\StudentSchedule::create([
                        'user_id'       => $student,
                        'teacher_id'    => $teacher[$i],
                        'course_id'     => $course,
                        'grade_id'      => $grade,
                        'schedule_id'   => $frequency,
                        'date'          => date('Y-m-d', strtotime($date[$i])),
                        'start_time'    => $start_time,
                        'end_time'      => $end_time,
                        'created_by'    => $this->user->id,
                        'group'         => $Uuid,
                        'day'           => $day[$i],
                    ]);

                    $this->saveTemplate($student, $teacher[$i], $course, $grade, $day[$i], $schedule->id);

                endif;
            endfor;
        else:
            $errors = [
                'errors' => [
                    'day' => ['Please select day.']
                ]
            ];

            return response()->json($errors, 422);

        endif;

        Session::flash('success', 'Schedule has been successfully created.');

        return response()->json([
            'response'  => 200,
            'redirect'  => 'false',
            'data'      => $Uuid
        ]);
    }

    public function student_edit($id){

        $now = date('Y-m-d H:i:s', strtotime(Carbon::now()));

        $getMyStudent = $this->get_my_student();

        $studentId  = $getMyStudent['studentId'];

        $schedule = \App\StudentSchedule::whereIn('user_id', $studentId)->where('id', $id)->first();

        if(!$schedule) abort(404);

        $students = $getMyStudent['students'];

        $course = \App\Course::where('status', 1)->get();

        $grades = \App\CourseGradeLevel::with(['grade_level'])->where('course_id', $schedule->course_id)->get();

        $schedules = \App\StudentSchedule::with(['teacher'])->whereIn('user_id', $studentId)->where('group', $schedule->group)->get();

        //$teachers = $this->get_teacher($schedule->grade_id);

        $getdates = $this->get_dates($schedule->grade_id);

        return view('schedules.forms.student.edit')
            ->with('title', 'Edit schedule.')
            ->with('schedule', $schedule)
            ->with('students', $students)
            ->with('schedules', $schedules)
            ->with('course', $course)
            ->with('grades', $grades)
            ->with('now', $now)
            ->with('getdates', $getdates);

    }

    public function student_update($id, Request $request){

        $id             = $request->input('id');
        $schedule_id    = $request->input('schedule_id');
        $day            = $request->input('day');
        $date           = $request->input('date');
        $time           = $request->input('time');
        $teacher        = $request->input('teacher');

        if($schedule_id):
            
            for($x = 0; $x < count($schedule_id); $x++):

                $schedule = \App\StudentSchedule::find($schedule_id[$x]);

                if($schedule):
                    
                    $start_time = date('H:i:s', strtotime($time[$x]));

                    $end_time   = date("H:i:s", strtotime('+15 minutes', strtotime($time[$x])));

                    $schedule->update([
                        'teacher_id'    => $teacher[$x],
                        'date'          => date('Y-m-d', strtotime($date[$x])),
                        'start_time'    => $start_time,
                        'end_time'      => $end_time,
                        'updated_by'    => $this->user->id,
                        'day'           => $day[$x],
                    ]);

                endif;

            endfor;

        endif;

        Session::flash('success', 'Schedule has been successfully updated.');

        return response()->json([
            'response'  => 200,
            'redirect'  => url('schedules')
        ]);

    }



    /*
    |------------------------------------------------------------------------------------------
    | Private functions
    |------------------------------------------------------------------------------------------
    */

    private function get_dates($grade){

        $dateArray = [];

        $levels = \App\Level::with(['teacher' => function($query){

            $query->where('role', 5);

        }])->where('grade_id', $grade)->get();

        if($levels):

            $user_id = [];

            foreach($levels as $level):

                if($level->teacher):

                    foreach($level->teacher as $teacher):

                        $user_id[] = $teacher->id;

                    endforeach;

                endif;

            endforeach;


            $schedules = \App\TeacherSchedule::distinct('date')->whereIn('user_id', $user_id)->whereDate('date', '>=', Carbon::now())->get();

            if($schedules):

                foreach($schedules as $schedule):
                
                    $dateArray[] = date('Y-m-d', strtotime($schedule->date));

                endforeach;

            endif;

        endif;

        return $dateArray;

    }

    private function saveTemplate($student, $teacher_id, $course, $grade, $day, $schedule_id){

        $Uuid = Uuid::generate()->time;

        //--------------------------------
        //--------------------------------
        $template = \App\Template::where('course_type', $course)->where('lesson_type', $grade)->where('lesson_day', $day)->first();
        
        if($template):

            $form_type = $template->form_type;

            //Save to new lesson
            $data = [
                'schedule_id'   => $schedule_id,
                'student_id'    => $student,
                'teacher_id'    => $teacher_id,
                'course_type'   => $course,
                'lesson_type'   => $grade,
                'lesson_day'    => $day,
                'created_by'    => $this->user->id,
                'token'         => $Uuid . $day,
                'form_type'     => $template->form_type
            ];

            if($form_type == 1):
            
                $data['lesson_name']    = $template->lesson_name;
                $data['speaking_topic'] = $template->speaking_topic;
                $data['next_listening'] = $template->next_listening;

            elseif($form_type == 2):

                $data['lesson_name']    = $template->lesson_name;
                $data['speaking_topic'] = $template->speaking_topic;
                $data['next_listening'] = $template->next_listening;

            elseif($form_type == 3):

                $data['lesson_name']    = $template->lesson_name;
                $data['writing_topic']  = $template->writing_topic;

            endif;

            $lesson = \App\NewLesson::create($data);

            //Get Media
            $Media = \App\LessonMedia::where('lesson_id', $template->id)->get();

            if($Media):

                foreach($Media as $m):

                    \App\Attachment::create([
                        'lesson_id'     => $lesson->id,
                        'attachment'    => $m->source_data,
                        'original_name' => $m->original_name,
                        'type'          => 1,
                    ]);

                endforeach;

            endif;

            //Get Attachment
            $Attachment = \App\LessonAttachment::where('lesson_id', $template->id)->get();
            
            if($Attachment):

                foreach($Attachment as $a):

                    \App\Attachment::create([
                        'lesson_id'     => $lesson->id,
                        'attachment'    => $a->attachment,
                        'original_name' => $a->original_name,
                        'type'          => 0,
                    ]);

                endforeach;

            endif;
        endif;

    }


    private function get_my_student(){

        $students = null;

        if($this->user->role == 1):
        
            $students = \App\Users::where('role', 4)->where('status', 1)->get();

        elseif(($this->user->role == 2 && $this->hasFranchise > 0) || ($this->user->role == 3 && $this->hasFranchise > 0)):

            $students = \App\ClassRoomStudent::select('users.id', 'users.romaji_name', 'users.kanji_name')->join('users', 'users.id', '=', 'class_rooms_student.user_id')->whereIn('class_rooms_student.room_id', $this->room_id)->where('users.role', 4)->get();

        endif;

        $studentId = [];

        if($students):

            foreach($students as $student):

                $studentId[] = $student->id;

            endforeach;

        endif;

        return [
            'students' => $students,
            'studentId'=> $studentId
        ];

    }

    private function get_teacher($grade){

        $level = \App\Level::where('grade_id', $grade)->get();

        $users = null;

        if($level):
            
            $level_id = [];

            foreach($level as $res):
            
                $level_id[] = $res->teacher_level;

            endforeach;

            $users = \App\Users::where('role', 5)->where('status', 1)->whereIn('teacher_level', $level_id)->get();

        endif;

        return $users;

    }

    public function export_create($uid){
		
		ini_set('memory_limit', '-1');
		
        $schedules = \App\StudentSchedule::with(['teacher','lesson'])->where('group', $uid)->get();
        $frequency = '';
        $query = $schedules[0];
        $student = \App\Users::find($query->user_id);
        $course =  \App\Course::where('id', $query->course_id)->first();
        $grade =  \App\GradeLevel::where('id', $query->grade_id)->first();
        if($query->schedule_id == 16){
            $frequency = __('label.once_week');
        }else if($query->schedule_id == 8){
            $frequency = __('label.alternate_week');
        }else{
            $frequency = __('label.one_time_only');
        }
        $data = '';
        foreach ($schedules as $result) {
            $data .= '<tr>
                        <td>Day '.$result->day.'</td>
                        <td>'.$result->date.'</td>
                        <td>'.$result->start_time.'</td>
                        <td>'.$result->teacher->full_name.'</td>
                        
                    </tr>';
        }
        $html = '<!DOCTYPE html>
        <html>
        <head>
            
            <style>
                #customers {
                    width: 100%;
                }

                #customers td, #customers th {
                    border: 1px solid #ddd;
                    padding: 8px;
                }

                #customers tr:nth-child(even){background-color: #1B97D5;}

                #customers th {
                    padding-top: 12px;
                    padding-bottom: 12px;
                    text-align: left;
                    background-color: #1B97D5;
                    color: white;
                }
                .header {
                    width: 100%;
                    background: #ccc;
                }
                .row {
                    display: block;
                }
            </style>
        </head>
       
        <body>
            <h4>生徒のスケジュール</h4>
            <hr>
            <table style="width:100%">
                
                <tbody>
                
                <tr>
                        <td style="width:15%">'. __('label.student').'</td>
                        <td style="width:85%" colspan="3">'.$student->romaji_name.'</td>
                        
                </tr>
                <tr>
                        <td style="width:15%">'.__('label.course').'</td>
                        <td style="width:35%">'.$course->label.'</td>
                        <td style="width:15%">'.__('label.grade').'</td>
                        <td style="width:35%">'.$grade->label.'</td>
                </tr>
                <tr>
                        <td style="width:15%">'.__('label.frequency').'</td>
                        <td style="width:35%">'.$frequency.'</td>
                        <td style="width:15%">'.__('label.start_day').'</td>
                        <td style="width:35%">Day '.$query->day.'</td>
                </tr>
                </tbody>
            </table> 
            <br>    
            <table id="customers">
                <thead>
                    <tr>
                        <th style="width:25%">Days</th>
                        <th style="width:25%">'.__('label.date').'</th>
                        <th style="width:25%">'.__('label.time').'</th>
                        <th style="width:25%">'.__('label.teacher').'</th>
                    </tr>
                </thead>
                <tbody>
                    '.$data.'
                </tbody>
            </table>       
        </body>
        </html>';

        $html2pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8');
        $html2pdf->setDefaultFont('arialunicid0');
        $html2pdf->writeHTML($html);
        $html2pdf->output($student->romaji_name.'.pdf', 'D'); 
        
      


    }
}
