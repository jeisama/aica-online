<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Session;


class HomeController extends Controller{

    public function index(Request $request){

    }


    public function setLocale(Request $request){
        
        $request->session()->put('locale', $request->input('locale'));

    }

    public function sample(){

    	$pdf = public_path('uploads/lessons/861544238418.6803.pdf');
		
		$thumbnail = \App\Helpers\GlobalHelper::pdf_thumbnail($pdf);

    	echo "<img src='data:image/jpg;base64,".base64_encode($thumbnail)."' />";

    }


    public function test(){
        
        $pdf = public_path('uploads/invoice.pdf');

        $img = new Imagick($pdf);

        $img->setImageFormat('jpg');

        $img->setCompressionQuality(80);

        $img->setResolution(1024,1024);

        $img->setImageBackgroundColor('white');

        $img->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);

        $img->mergeImageLayers(Imagick::LAYERMETHOD_FLATTEN);

        $img->setIteratorIndex(0);

        $file_name = 'canvas-'. $i . $uuid .'.jpg';
        
    }
   
}
