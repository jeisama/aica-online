<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;

class PagesController extends Controller
{

    private $user;

    private $hasFranchise;

    public function __construct(){

        $this->middleware(function($request, $next){
            
            $this->user  = \App\Helpers\UserHelper::user_data(Auth::id());

            $this->hasFranchise = Auth::id() ? \App\Helpers\UserHelper::has_franchise(Auth::id()) : null;

            return $next($request);

        });

    }

    public function index(Request $request){
        

        $now = date('Y-m-d H:i:s' ,strtotime(Carbon::now()));

        $thisWeekMonday = date("Y-m-d", strtotime(date('o-\\WW')));

        $nexWeekModay   = date('Y-m-d', strtotime("+7 day", strtotime($thisWeekMonday)));
        
        $lastWeekBelow  = date('Y-m-d', strtotime("-1 day", strtotime($thisWeekMonday)));

        $mondays = [
            'thisWeek'  => $thisWeekMonday,
            'nexWeek'   => $nexWeekModay,
            'lastWeek'  => $lastWeekBelow
        ]; 

        $records   = null;

        $role = $this->user->role;

        switch ($role) {

            case '1':
            case '3':
                
                $records = $this->admin($mondays);

                break;

            case '2':
                
                $records = $this->franchise($mondays);

                break;

            case '4':
                
                $records = $this->student($mondays);

                break;

            case '5':
                
                $records = $this->teacher($mondays);

                break;
            
            default:
                # code...
                break;
        }

        //dd($records);

        return view('pages.index')
                ->with('title', 'Welcome to AIC Online')
                ->with('records', $records)
                ->with('now', $now);
    }

    private function admin($mondays){

        $nexWeekFrom = $mondays['nexWeek'];

        $nexWeekTo   = date('Y-m-d', strtotime('+6 days', strtotime($nexWeekFrom)));

        $nexWeek  = \App\StudentSchedule::with(['course', 'grade', 'lesson', 'teacher', 'student'])->whereBetween('date', [$nexWeekFrom, $nexWeekTo])->get();

        $thisWeekFrom = $mondays['thisWeek'];

        $thisWeekTo   = date('Y-m-d', strtotime('+6 days', strtotime($thisWeekFrom)));

        $thisWeek = \App\StudentSchedule::with(['course', 'grade', 'lesson', 'teacher', 'student'])->whereBetween('date', [$thisWeekFrom, $thisWeekTo])->get();

        $lastWeek = \App\StudentSchedule::with(['course', 'grade', 'lesson', 'teacher', 'student'])->whereDate('date', '<=', $mondays['lastWeek'])->get();

        return $records = [

            'nexWeek'  => $nexWeek,

            'thisWeek' => $thisWeek,

            'lastWeek' => $lastWeek

        ];

    }

    private function franchise($mondays){

        $roomAdmin = \App\ClassRoomAdmin::select('class_room_id')->where('user_id', $this->user->id)->get();

        $room_id = [];

        if($roomAdmin):

            foreach($roomAdmin as $ra):

                $room_id[] = $ra->class_room_id;

            endforeach;

        endif;

        $students = \App\ClassRoomStudent::whereIn('room_id', $room_id)->get();

        $student_id = [];

        if($students):
            foreach($students as $student):
            
                $student_id[] = $student->user_id;

            endforeach;
        endif;

        $nexWeekFrom = $mondays['nexWeek'];

        $nexWeekTo   = date('Y-m-d', strtotime('+6 days', strtotime($nexWeekFrom)));

        $nexWeek  = \App\StudentSchedule::with(['course', 'grade', 'lesson', 'teacher'])->whereIn('user_id', $student_id)->whereBetween('date', [$nexWeekFrom, $nexWeekTo])->get();

        $thisWeekFrom = $mondays['thisWeek'];

        $thisWeekTo   = date('Y-m-d', strtotime('+6 days', strtotime($thisWeekFrom)));

        $thisWeek = \App\StudentSchedule::with(['course', 'grade', 'lesson', 'teacher'])->whereIn('user_id', $student_id)->whereBetween('date', [$thisWeekFrom, $thisWeekTo])->get();

        $lastWeek = \App\StudentSchedule::with(['course', 'grade', 'lesson', 'teacher'])->whereIn('user_id', $student_id)->whereDate('date', '<=', $mondays['lastWeek'])->get();

        return $records = [

            'nexWeek'  => $nexWeek,

            'thisWeek' => $thisWeek,

            'lastWeek' => $lastWeek

        ];
    }

    private function aica($mondays){

    }

    private function student($mondays){

        $nexWeekFrom = $mondays['nexWeek'];

        $nexWeekTo   = date('Y-m-d', strtotime('+6 days', strtotime($nexWeekFrom)));

        $nexWeek  = \App\StudentSchedule::with(['course', 'grade', 'lesson', 'teacher'])->where('user_id', $this->user->id)->whereBetween('date', [$nexWeekFrom, $nexWeekTo])->get();

        $thisWeekFrom = $mondays['thisWeek'];

        $thisWeekTo   = date('Y-m-d', strtotime('+6 days', strtotime($thisWeekFrom)));

        $thisWeek = \App\StudentSchedule::with(['course', 'grade', 'lesson', 'teacher'])->where('user_id', $this->user->id)->whereBetween('date', [$thisWeekFrom, $thisWeekTo])->get();

        $lastWeek = \App\StudentSchedule::with(['course', 'grade', 'lesson', 'teacher'])->where('user_id', $this->user->id)->whereDate('date', '<=', $mondays['lastWeek'])->get();

        return $records = [

            'nexWeek'  => $nexWeek,

            'thisWeek' => $thisWeek,

            'lastWeek' => $lastWeek

        ];
    }

    private function teacher($mondays){

        $nexWeekFrom = $mondays['nexWeek'];

        $nexWeekTo   = date('Y-m-d', strtotime('+6 days', strtotime($nexWeekFrom)));

        $nexWeek  = \App\StudentSchedule::with(['course', 'grade', 'lesson', 'student'])->where('teacher_id', $this->user->id)->whereBetween('date', [$nexWeekFrom, $nexWeekTo])->get();

        $thisWeekFrom = $mondays['thisWeek'];

        $thisWeekTo   = date('Y-m-d', strtotime('+6 days', strtotime($thisWeekFrom)));

        $thisWeek = \App\StudentSchedule::with(['course', 'grade', 'lesson', 'student'])->where('teacher_id', $this->user->id)->whereBetween('date', [$thisWeekFrom, $thisWeekTo])->get();

        $lastWeek = \App\StudentSchedule::with(['course', 'grade', 'lesson', 'student'])->where('teacher_id', $this->user->id)->whereDate('date', '<=', $mondays['lastWeek'])->get();
        
        return $records = [

            'nexWeek'  => $nexWeek,

            'thisWeek' => $thisWeek,

            'lastWeek' => $lastWeek

        ];

    }


}