<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App;

use Session;

use Validator;

use Illuminate\Support\Facades\DB;


class ClassroomController extends Controller
{

    public function index(Request $request){

        $class_name = $request->query('class_name');
        
        $class_orgname = $request->query('class_orgname');
        
        $query = DB::table('class_rooms');
        
        if($class_name){
            $query->where('class_name', 'LIKE', '%'.$class_name.'%');
        }
        
        if($class_name){
            $query->where('class_orgname', 'LIKE', '%'.$class_orgname.'%');
        }
        
        $results = $query->get();
        $title = 'AIC Classroom';
        return view('classroom.index')
                ->with('title', $title)
                ->with('results', $results);
                
    }

    public function create(){
        $title = 'AIC Classroom Create';
        return view('classroom.create')->with('title',$title);   
    }
    
    public function validateClass(Request $request){
        if($request->input('id')){
            $validator = Validator::make($request->all(), [
                'class_name' => 'required',
                'class_fullname' => 'required',
                'class_orgname' => 'required',
                'class_phone' => 'required|numeric',
                'class_email' => 'required',
                'class_confirm_email' => 'required|email|same:class_email',
                'class_mobile' => 'nullable|numeric'
                
            ],
             [  
                'class_name.required' => 'Class name is a required field',
                'class_fullname.required' => 'Fullname is a required field',
                'class_orgname.required' => 'Organization is a required field',
                'class_phone.required' => 'Phone is a required field',
                'class_phone.numeric' => 'Numeric only inputs (example: 0368028216)',
                'class_email.required' => 'Email is a required field',
                'class_confirm_email.required' => 'Email confirmation is a required field',
                'class_mobile.numeric' => 'Numeric only inputs (example: 0368028216)'
                
            ]);
        }else{
            $validator = Validator::make($request->all(), [
                'class_name' => 'required',
                'class_fullname' => 'required',
                'class_orgname' => 'required',
                'class_phone' => 'required|numeric',
                'class_email' => 'required',
                'class_confirm_email' => 'required|email|same:class_email',
                'class_mobile' => 'nullable|numeric'
                
            ],
            [   
                'class_name.required' => 'Class name is a required field',
                'class_fullname.required' => 'Fullname is a required field',
                'class_orgname.required' => 'Organization is a required field',
                'class_phone.required' => 'Phone is a required field',
                'class_phone.numeric' => 'Numeric only inputs (example: 0368028216)',
                'class_email.required' => 'Email is a required field',
                'class_confirm_email.required' => 'Email confirmation is a required field',
                'class_mobile.numeric' => 'Numeric only inputs (example: 0368028216)'

                
            ]);
        }
        

        if ($validator->fails()) {
            return response()->json([
                'message' => 'error',
                'data' => $validator->errors()
            ]);
        }else{
            return response()->json([
                'message' => 'success',
                'data' => $request->input()
            ]);
        }

    }
    public function store(Request $request){

        $lesson = \App\ClassRoom::create($request->all()); 
        return redirect('classroom')->with('message', 'Classroom has been created');
    }

    public function edit($id)
    {
         $query = DB::table('class_rooms')->where('id', '=', $id);
         $results = $query->first();
         $title = 'AIC Classroom Edit';
         return view('classroom.edit')
                ->with('title', $title)
                ->with('results', $results);
                 
    }

    public function update($id, Request $request)
    {
    
         $info = \App\ClassRoom::find($id);
         $info -> update($request->all());
         return redirect('classroom')->with('message', 'Update Successful');
                 
    }

    public function show($id){
        //dd($id);
         $query = DB::table('class_rooms')->where('id', '=', $id);
         $results = $query->first();
         return response()->json([
            'message' => 'success',
            'data' => $results
        ]);
    }

    public function delete($id)
    {
        // $info = \App\ClassRoom::find($id);
        // $info->del_flg = 1;

        //  return redirect('classroom');
                 
    }

    public function room_admin(Request $request){

        $user = $request->input('user');

        if($user):
            \App\ClassRoomAdmin::where('class_room_id', $request->input('id'))->delete();
            foreach($user as $usr):
                \App\ClassRoomAdmin::create([
                    'user_id'       => $usr,
                    'class_room_id' => $request->input('id')
                ]);
            endforeach;
        else:

        endif;

        Session::flash('message', 'Classroom admin has been saved.');

        return response()->json([
            'response' => 200,
            'redirect'  => url('classroom')
        ]);
    }
}
