<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Http\Requests\GradeRequest;

use Validator;

use Session;

class GradeLevelController extends Controller
{

    public function __construct(){
        
        $this->middleware('admin');

    }

    /*
    |------------------------------------------------
    | Grade Level Index
    |------------------------------------------------
    */
    public function index(){

        $grades = \App\GradeLevel::where('status', 1)->orderBy('id', 'asc')->get();

        return view('grades.index')
            ->with('title', 'Grade level management.')
            ->with('grades', $grades);
    }

    /*
    |------------------------------------------------
    | Create Grade Level
    |------------------------------------------------
    */
    public function create(){
        return view('grades.create')
            ->with('title', 'Create grade level.');
    }

    /*
    |------------------------------------------------
    | Store Grade Level
    |------------------------------------------------
    */
    public function store(GradeRequest $request){
        
        \App\GradeLevel::create([
            'label'         => $request->input('grade_level'),
            'status'        => $request->input('status'),
            'created_by'    => Auth::id()
        ]);

        Session::flash('success_message', __('label.grade_message'));

        return response()->json([
            'response'  => 200,
            'redirect'  => url('grades')
        ]);
    }

    /*
    |------------------------------------------------
    | Edit Grade Level
    |------------------------------------------------
    */
    public function edit($id){
        
        $grade = \App\GradeLevel::find($id);

        if(!$grade) abort(404);

        return view('grades.edit')
            ->with('title', 'Edit grade level.')
            ->with('grade', $grade);

    }

    /*
    |------------------------------------------------
    | Update Grade Level
    |------------------------------------------------
    */
    public function update(GradeRequest $request, $id){
        
        $grade = \App\GradeLevel::find($id);

        $grade->update([
            'label'         => $request->input('grade_level'),
            'status'        => $request->input('status'),
            'updated_by'    => Auth::id()
        ]);

        Session::flash('success_message', 'Grade level has been successfully updated.');

        return response()->json([
            'response'  => 200,
            'redirect'  => url('grades')
        ]);
    }

    /*
    |------------------------------------------------
    | Delete Grade Level
    |------------------------------------------------
    */
    public function destroy($id){
        //
    }
}
