<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use App\Student;

use App\Teacher;

use Session;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Auth;

use Webpatser\Uuid\Uuid;

use Validator;

use Image;

use File;

use UserHelper;

use ScheduleHelper;

use Excel;

class UsersController extends Controller
{
    
    public $user;
    private $room_id;
    private $student_id;

    public function __construct(){

        $this->middleware(function($request, $next){
            
            $this->user = UserHelper::user_data(Auth::id());

            $this->room_id      = ScheduleHelper::myRoomId($this->user->id);

            $this->student_id   = ScheduleHelper::myStudentId($this->room_id);
            
            return $next($request);

        });
        

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $room_id = DB::table('class_rooms_admin')
                    ->select('class_room_id')
                    ->where('user_id', Auth::id())
                    ->first();
       
        if($this->user->role == 5 || $this->user->role == 4) abort(403);
        
        $qData = [
            'name'      => $request->get('name'),
            'role'      => $request->get('role'),
            'inactive'  => $request->get('inactive')
        ];

        $query = \App\Users::query();

        if($qData['inactive']):
            $query->where(function($query){
                $query->where('status', 1)
                    ->orWhere('status', 2);
            });
        else:
            $query->where('status', 1);
        endif;

        if($qData['role']):
            $query->where('role', $qData['role']);
        endif;

        if($qData['name']):
            $query->where(function($query) use ($qData){
                $query->where('full_name', 'LIKE', '%'.$qData['name'].'%')
                    ->orWhere('kanji_name', 'LIKE', '%'.$qData['name'].'%')
                    ->orWhere('romaji_name', 'LIKE', '%'.$qData['name'].'%')
                    ->orWhere('username', 'LIKE', '%'.$qData['name'].'%');
            });
        endif;
        //dd(Auth::user()->role);
        if(Auth::user()->role == class_room){
            $class_admin = [];
            if($room_id):
            $class_users = DB::table('class_rooms_admin')
                            ->select('user_id')
                            ->where('class_room_id','=', $room_id->class_room_id)
                            ->get();
                
                if($class_users):

                    foreach($class_users as $cu):
                    
                        $class_admin[] = $cu->user_id;
        
                    endforeach;
                
                endif;
            else:
                abort(404);
            endif;
            $result = array_merge($class_admin, $this->student_id);
            //dd($result);
            //$query->where('created_by','=', Auth::id())->whereIn('id', $class_admin);
            $query->whereIn('id', $result);
                    
            $users = $query->paginate(25);

        }else if(Auth::user()->role == aica){
            $query->where('role','=','5')
                ->orWhere('role','=','3');
            $users = $query->paginate(25);
        }else{
            $users = $query->paginate(25); 
        }
        
        //dd($users);
        return view('users.index')
            ->with('title', 'User Management')
            ->with('users', $users)
            ->with('qData', $qData);
        
    }

    /*
    |-----------------------------------------------------
    | Create User View
    |-----------------------------------------------------
    */
    public function create(){

        return view('users.create')
        ->with('title', 'Create User');

    }

    /*
    |-----------------------------------------------------
    | Validate User Registration Input
    |-----------------------------------------------------
    */
    public function validate_user(Request $request){
        
        $file = $request->file();
        
        $input = $request->input();
        
        $user_role = $request->input('user_role');
       
        $validate = [
            'user_role'         => 'required',
            //'email'             => 'required|email',
            //'confirm_email'     => 'required|email|same:email',
            'username'          => 'required|unique:users|min:5',
            'password'          => 'required|alpha_num|min:6',
            'status'            => 'required'
        ];


        if($user_role == '4'):
           
            $validate['class_room']        = 'required';
            $validate['kanji_name']        = 'required';
            $validate['romaji_name']       = 'required';
            $validate['b_year']            = 'required';
            $validate['b_month']           = 'required';
            $validate['b_day']             = 'required';
            $validate['prefecture']        = 'required';
            $validate['cram_school']       = 'required';
            //$validate['english_learning']  = 'required';
            $validate['english_schedule']  = 'required';
            //$validate['account_prof']      = 'required';
        endif;

        if($user_role == '5'):

            $validate['avatar']    = 'required|image';
            $validate['full_name'] = 'required';
            $validate['gender']    = 'required';

        endif;

        if($user_role == '1' || $user_role == '2' || $user_role == '3'):
            $validate['avatar']    = 'required|image';
            $validate['full_name'] = 'required';
            $validate['gender']    = 'required';
            $validate['email']    = 'required|email|unique:users';
            $validate['confirm_email'] = 'required|email|same:email';

        endif;
        
     
        
        $validator = Validator::make($request->all(), $validate);

        if($validator->fails()):
            return response()->json([
                'response'  => 417,
                'data'      => $validator->errors()
            ]);
        else:
            
            $appData = '';
        
            if($user_role == '4'):
                $birthdate = $input['b_year'].'/'.$input['b_month'].'/'.$input['b_day'];
                $appData = '
                <div class="form_confirm">
                    <div class="confirm-row">
                        <div class="confirm-label">ロール</div>
                        <div class="confirm-data user_role">'. $input['user_role_preview'] .'</div>
                    </div>
                    <div class="confirm-row">
                        <div class="confirm-label">教室</div>
                        <div class="confirm-data class_room">'. $input['class_room_preview'] .'</div>
                    </div>
                    <div class="confirm-row">
                        <div class="confirm-label">氏名</div>
                        <div class="confirm-data kanji_name">'. $input['kanji_name'] .'</div>
                    </div>
                    <div class="confirm-row">
                        <div class="confirm-label">ローマ字表記　フルネーム</div>
                        <div class="confirm-data romaji_name">'. $input['romaji_name'] .'</div>
                    </div>
                    <div class="confirm-row">
                        <div class="confirm-label">誕生日</div>
                        <div class="confirm-data birth_date">'. $birthdate.'</div>
                   
                    </div>
                    <div class="confirm-row">
                        <div class="confirm-label">お住まい（○○県）</div>
                        <div class="confirm-data prefecture">'. $input['prefecture'] .'</div>
                    </div>
                    <div class="confirm-row">
                        <div class="confirm-label">教室名</div>
                        <div class="confirm-data cram_school">'. $input['cram_school'] .'</div>
                    </div>
                    <div class="confirm-row">
                        <div class="confirm-label">英語学習暦</div>
                        <div class="confirm-data english_learning">'. $input['english_learning'] .'</div>
                    </div>
                    <div class="confirm-row">
                        <div class="confirm-label">入会時の英語学習状況</div>
                        <div class="confirm-data english_schedule">'. $input['english_schedule'] .'</div>
                    </div>
                    <div class="confirm-row">
                        <div class="confirm-label">取得済みの語学力検定・資格など</div>
                        <div class="confirm-data account_prof">'. $input['account_prof'] .'</div>
                    </div>
                    <div class="confirm-row">
                        <div class="confirm-label">特記事項</div>
                        <div class="confirm-data remark">'. nl2br($input['remark']) .'</div>
                    </div>
                    <div class="confirm-row">
                        <div class="confirm-label">Eメール</div>
                        <div class="confirm-data email">'. $input['email'] .'</div>
                    </div>
                </div>
                
                <h5 class="modal-title">ログイン詳細</h5>

                <div class="form_confirm">
                    <div class="confirm-row">
                        <div class="confirm-label">ユーザーID</div>
                        <div class="confirm-data username">'. $input['username'] .'</div>
                    </div>
                    <div class="confirm-row">
                        <div class="confirm-label">パスワード</div>
                        <div class="confirm-data password">**********</div>
                    </div>
                    <div class="confirm-row">
                        <div class="confirm-label">状態</div>
                        <div class="confirm-data status">'. $input['status_preview'] .'</div>
                    </div>
                </div>
                ';
            else:
                $image = '';
            
                if($file):
                
                $avatar = $file['avatar'];

                $temp = $avatar->getPathName();

                $src = base64_encode(file_get_contents($temp));

                $image = '<img src="data:image/png;base64,'. $src .'">';
                
                endif;

                $admin_type = '';

                $appData = '
                    <div class="form_confirm">
                        <div class="confirm-row">
                            <div class="image_preview">
                                <div class="inner">
                                    '. $image .'
                                </div>
                            </div>
                        </div>
                        <div class="confirm-row">
                            <div class="confirm-label">'. __('label.role') .'</div>
                            <div class="confirm-data">'. $input['user_role_preview'] .'</div>
                        </div>
                        <div class="confirm-row">
                            <div class="confirm-label">'. __('label.full_name') .'</div>
                            <div class="confirm-data">'. $input['full_name'] .'</div>
                        </div>
                        <div class="confirm-row">
                            <div class="confirm-label">'. __('label.gender') .'</div>
                            <div class="confirm-data">'. $input['gender'] .'</div>
                        </div>
                        <div class="confirm-row">
                            <div class="confirm-label">'. __('label.email') .'</div>
                            <div class="confirm-data">'. $input['email'] .'</div>
                        </div>
                        <div class="confirm-row">
                            <div class="confirm-label">'. __('label.username') .'</div>
                            <div class="confirm-data">'. $input['username'] .'</div>
                        </div>
                        <div class="confirm-row">
                            <div class="confirm-label">'. __('label.password') .'</div>
                            <div class="confirm-data">***********</div>
                        </div>
                        <div class="confirm-row">
                            <div class="confirm-label">'. __('label.status') .'</div>
                            <div class="confirm-data">'. $input['status_preview'] .'</div>
                        </div>
                        
                    </div>';
                
            endif;

            return response()->json([
                'response'  => 200,
                'data'      => $appData
            ]);
        endif;
    }

    /*
    |-----------------------------------------------------
    | Store User Details
    |-----------------------------------------------------
    */
    public function store_user(Request $request){
       $birth_date = $request->input('b_year').'/'.$request->input('b_month').'/'.$request->input('b_day');
        $user_role = $request->input('user_role');

        if($user_role === '4'):
            $create_user = \App\Users::create([
                'kanji_name'        => $request->input('kanji_name'),
                'romaji_name'       => $request->input('romaji_name'),
                'birth_date'        => date('Y-m-d', strtotime($birth_date)),
                'prefecture'        => $request->input('prefecture'),
                'cram_school'       => $request->input('cram_school'),
                'english_learning'  => $request->input('english_learning'),
                'englist_schedule'  => $request->input('english_schedule'),
                'account_prof'      => $request->input('account_prof'),
                'remark'            => $request->input('remark'),
                'email'             => $request->input('email'),
                'username'          => $request->input('username'),
                'password'          => Hash::make($request->input('password')),
                'status'            => $request->input('status'),
                'role'              => $request->input('user_role'),
                'created_by'        => Auth::id()
            ]);

            \App\ClassRoomStudent::create([
                'room_id'   => $request->input('class_room'),
                'user_id'   => $create_user->id,
            ]);
        else:
        
            $avatar = $request->file('avatar');
            
            $file_name = null;

            if($avatar):
            
                $uuid = Uuid::generate(1)->time;

                $ext       = $avatar->getClientOriginalExtension();

                $file_name = $uuid .'.'. $ext;

                //Create Instance
                $image = Image::make($avatar);

                $image->resize(200, 200, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })->save('uploads/avatar/'. $file_name);

            endif;

            $userdata = [
                'full_name' => $request->input('full_name'),
                'gender'    => $request->input('gender'),
                'avatar'    => $file_name,
                'email'     => $request->input('email'),
                'username'  => $request->input('username'),
                'password'  => Hash::make($request->input('password')),
                'status'    => $request->input('status'),
                'role'      => $request->input('user_role'),
                'created_by'=> Auth::id()
            ];

            if($request->input('user_role') == 5):

                $userdata['teacher_level'] = $request->input('level');
            
            endif;

            $create_user = \App\Users::create($userdata);

        endif;
        
        Session::flash('success_message', __('label.user_create_message'));

        return response()->json($create_user);
    }

    /*
    |-----------------------------------------------------
    | Edit User
    |-----------------------------------------------------
    */
    public function edit($username){
        //dd($username);
        $user = \App\Users::where('username', $username)->first();
        
        if(!$user) abort(404);

        $prefectures = DB::table('city')->where('CountryCode', 'JPN')->groupBy('District')->get(); 

        $levels      = \App\TeacherLevel::where('status', 1)->get();
        $temp_month = [
            1 => __('label.jan'),
            2 => __('label.feb'),
            3 => __('label.mar'),
            4 => __('label.apr'),
            5 => __('label.may'),
            6 => __('label.jun'),
            7 => __('label.jul'),
            8 => __('label.aug'),
            9 => __('label.sep'),
            10 => __('label.oct'),
            11 => __('label.nov'),
            12 => __('label.dec'),
        ];
       // dd($user);
        return view('users.edit')
            ->with('title', 'Edit User')
            ->with('user', $user)
            ->with('temp_month', $temp_month)
            ->with('prefectures', $prefectures)
            ->with('levels', $levels);
    }

    /*
    |-----------------------------------------------------
    | Validate Edit
    |-----------------------------------------------------
    */
    public function validate_update(Request $request){

        $input   = $request->input();

        $target   = $request->input('target');

        $username = $request->input('username_data');

        $user = \App\Users::where('username', $username)->first();
        
        if($target === 'detail'):

            $appData = '';

            if($request->input('role') == 'default'):

                $validator = Validator::make($request->all(), [
                    'avatar'          => 'image',
                    'full_name'       => 'required',
                    'gender'          => 'required',
                    'email'           => 'required|email|unique:users,email,'. $user->id,
                    'confirm_email'   => 'required|email|same:email'
                ]);

            else:
                
                $validator = Validator::make($request->all(), [
                    'kanji_name'        => 'required',
                    'romaji_name'       => 'required',
                    //'birth_date'        => 'required|date',
                    'b_year'            => 'required',
                    'b_month'           => 'required',
                    'b_day'             => 'required',
                    'prefecture'        => 'required',
                    'cram_school'       => 'required',
                    'english_learning'  => 'required',
                    'english_schedule'  => 'required',
                    'account_prof'      => 'required',
                    'email'             => 'required|email|unique:users,email,'. $user->id,
                    'confirm_email'     => 'required|email|same:email'
                ]);
            endif;

            if($validator->fails()):
                return response()->json([
                    'response'  => 417,
                    'data'      => $validator->errors()
                ]);
            else:
                if($request->input('role') == 'default'):

                    $avatar = $request->file('avatar');
                    
                    if($avatar):
                        $temp = $avatar->getPathName();
                        $src = base64_encode(file_get_contents($temp));

                        $image = '<img src="data:image/png;base64,'. $src .'">';
                    else:
                        if($user->avatar == null || !File::exists('uploads/avatar/'. $user->avatar)):
                            $image = '<img src="'. asset('uploads/avatar/noavatar.jpg') .'">';
                        else:
                            $image = '<img src="'. asset('uploads/avatar/'. $user->avatar) .'">';
                        endif;
                    endif;

                    $admin_type = '';

                    $level = null;

                    if($request->input('level')):

                        $level = '
                            <div class="confirm-row">
                                <div class="confirm-label">'. __('label.level') .'</div>
                                <div class="confirm-data">Level '. $request->input('level') .'</div>
                            </div>';
                    
                    endif;

                    $appData = '
                        <div class="form_confirm">
                            <div class="confirm-row">
                                <div class="image_preview">
                                    <div class="inner">
                                        '. $image .'
                                    </div>
                                </div>
                            </div>
                            <div class="confirm-row">
                                <div class="confirm-label">'. __('label.full_name') .'</div>
                                <div class="confirm-data">'. $input['full_name'] .'</div>
                            </div>
                            <div class="confirm-row">
                                <div class="confirm-label">'. __('label.gender') .'</div>
                                <div class="confirm-data">'. $input['gender'] .'</div>
                            </div>
                            <div class="confirm-row">
                                <div class="confirm-label">'. __('label.email') .'</div>
                                <div class="confirm-data">'. $input['email'] .'</div>
                            </div>
                            '. $level .'
                            <input type="hidden" id="target_form" value="user_detail">
                        </div>';
                else:
                    $birthdate = $input['b_year'].'/'.$input['b_month'].'/'.$input['b_day'];
                    $appData = '
                    <div class="form_confirm">
                        <div class="confirm-row">
                            <div class="confirm-label">氏名</div>
                            <div class="confirm-data kanji_name">'. $input['kanji_name'] .'</div>
                        </div>
                        <div class="confirm-row">
                            <div class="confirm-label">ローマ字表記　フルネーム</div>
                            <div class="confirm-data romaji_name">'. $input['romaji_name'] .'</div>
                        </div>
                        <div class="confirm-row">
                            <div class="confirm-label">誕生日</div>
                            <div class="confirm-data birth_date">'.$birthdate.'</div>
                        </div>
                        <div class="confirm-row">
                            <div class="confirm-label">お住まい（○○県）</div>
                            <div class="confirm-data prefecture">'. $input['prefecture'] .'</div>
                        </div>
                        <div class="confirm-row">
                            <div class="confirm-label">教室名</div>
                            <div class="confirm-data cram_school">'. $input['cram_school'] .'</div>
                        </div>
                        <div class="confirm-row">
                            <div class="confirm-label">英語学習暦</div>
                            <div class="confirm-data english_learning">'. $input['english_learning'] .'</div>
                        </div>
                        <div class="confirm-row">
                            <div class="confirm-label">入会時の英語学習状況</div>
                            <div class="confirm-data english_schedule">'. nl2br($input['english_schedule']) .'</div>
                        </div>
                        <div class="confirm-row">
                            <div class="confirm-label">取得済みの語学力検定・資格など</div>
                            <div class="confirm-data account_prof">'. $input['account_prof'] .'</div>
                        </div>
                        <div class="confirm-row">
                            <div class="confirm-label">特記事項</div>
                            <div class="confirm-data remark">'. nl2br($input['remark']) .'</div>
                        </div>
                        <div class="confirm-row">
                            <div class="confirm-label">Eメール</div>
                            <div class="confirm-data email">'. $input['email'] .'</div>
                        </div>
                    </div>
                    <input type="hidden" id="target_form" value="user_detail">
                    ';
                endif;

                return response()->json([
                    'response'  => 200,
                    'data'      => $appData
                ]);
            endif;
        else:
            
            $validator = Validator::make($request->all(), [
                'username'          => 'required|min:5|unique:users,username,'. $user->id,
                'password'          => 'required|min:6',
                'status'            => 'required'
            ]);

            if($validator->fails()):
                return response()->json([
                    'response'  => 417,
                    'data'      => $validator->errors()
                ]);
            else:
                $appData = '
                    <div class="form_confirm">
                        <div class="confirm-row">
                            <div class="confirm-label">'. __('label.username') .'</div>
                            <div class="confirm-data">'. $input['username'] .'</div>
                        </div>
                        <div class="confirm-row">
                            <div class="confirm-label">'. __('label.password') .'</div>
                            <div class="confirm-data">***********</div>
                        </div>
                        <div class="confirm-row">
                            <div class="confirm-label">'. __('label.status') .'</div>
                            <div class="confirm-data">'. $input['status_preview'] .'</div>
                        </div>
                        <input type="hidden" id="target_form" value="login_detail">
                    </div>';
                return response()->json([
                    'response'  => 200,
                    'data'      => $appData
                ]);
            endif;

        endif;

    }

    /*
    |-----------------------------------------------------
    | Update User
    |-----------------------------------------------------
    */
    public function update(Request $request){

        $target = $request->input('target');
        
        $username = $request->input('username_data');

        $user = \App\Users::where('username', $username)->first();

        if($target === 'login'):
            $user->update([
                'username'  => $request->input('username'),
                'password'  => Hash::make($request->input('password')),
                'status'    => $request->input('status'),
                'updated_by'=> Auth::id()
            ]);

            Session::flash('success_message', 'Login detail successfully updated.');

        else:
            if($request->input('role') == 'default'):

                $filename = $user->avatar;

                $avatar = $request->file('avatar');

                if($avatar):
                    
                    $uuid = Uuid::generate(1)->time;

                    $ext       = $avatar->getClientOriginalExtension();

                    $filename = $uuid .'.'. $ext;

                    //Create Instance
                    $image = Image::make($avatar);

                    $image->resize(200, 200, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    })->save('uploads/avatar/'. $filename);
                    
                    File::delete('uploads/avatar/'. $user->avatar);
                endif;

                $data = [
                    'avatar'    => $filename,
                    'full_name' => $request->input('full_name'),
                    'gender'    => $request->input('gender'),
                    'email'     => $request->input('email'),
                    'updated_by'=> Auth::id()
                ];

                if($request->input('level')):
                    $data['teacher_level'] = $request->input('level');
                endif;

                $user->update($data);

            else:
                $birth_date = $request->input('b_year').'/'.$request->input('b_month').'/'.$request->input('b_day');
                $user->update([
                    'kanji_name'        => $request->input('kanji_name'),
                    'romaji_name'       => $request->input('romaji_name'),
                    'birth_date'        =>  $birth_date,
                    'prefecture'        => $request->input('prefecture'),
                    'cram_school'       => $request->input('cram_school'),
                    'english_learning'  => $request->input('english_learning'),
                    'englist_schedule'  => $request->input('english_schedule'),
                    'account_prof'      => $request->input('account_prof'),
                    'remark'            => $request->input('remark'),
                    'email'             => $request->input('email'),
                    'updated_by'        => Auth::id()
                ]);
            endif;

            Session::flash('success_message', __('label.update_user_message'));
        endif;

        return response()->json([ 'message' => 200 ]);

    }

    /*
    |-----------------------------------------------------
    | Archive User
    |-----------------------------------------------------
    */
    public function archive_user(Request $request){

        $id = $request->input('id');

        $user = \App\Users::find($id);
        //dd($user);

        // $data = [
        //     'id'            => $user->id,
        //     'full_name'     => $user->full_name,
        //     'gender'        => $user->gender,
        //     'avatar'        => $user->avatar,
        //     'kanji_name'    => $user->kanji_name,
        //     'romaji_name'   => $user->romaji_name,
        //     'birth_date'    => $user->birth_date,
        //     'prefecture'    => $user->prefecture,
        //     'cram_school'   => $user->cram_school,
        //     'english_learning' => $user->english_learning,
        //     'englist_schedule' => $user->englist_schedule,
        //     'account_prof'  => $user->account_prof,
        //     'remark'        => $user->remark,
        //     'email'         => $user->email,
        //     'role'          => $user->role,
        //     'status'        => $user->status,
        //     'username'      => $user->username,
        //     'confirm_email' => $user->confirm_email,
        //     'email_verified_at' => $user->email_verified_at,
        //     'password'      => $user->password,
        //     'teacher_level' => $user->teacher_level,
        //     'remember_token'=> $user->remember_token,
        //     'created_by'    => $this->user->id,
        //     'updated_by'    => $user->updated_by,
        // ];

        // \App\ArchiveUser::create($data);

        // $user->delete();
        $user->update([
            'status'  => 2,
            'updated_by'=> Auth::id()
        ]);
        Session::flash('success_message','User has been deleted');

        return response()->json('200');
    }

    /*
    |---------------------------------
    | Create user's form
    |---------------------------------
    */
    public function get_form(Request $request){

        $role    = $request->input('role');

        $user_id = $request->input('user_id');

        $data = null;

        if($role == '4'):

            $data = $this->student($user_id);
        
        else:
        
            $data = $this->admin_teacher($role);
        
        endif;

        return response()->json($data);

    }

    public function admin_teacher($role){

        $room_role = '';

        $level = '';

        $levels = \App\TeacherLevel::where('status', 1)->get();

        if($role == 5 && $levels):

            
            $option = '';

            foreach($levels as $level):
                $option .= '<option value="'. $level->id .'">'. $level->level .'</option>';
            endforeach;

            $level = '
            <div class="form-row">
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="level">'. __('label.teacher_level') .'</label>
                    <select name="level" id="level" class="form-control">
                        '. $option .'
                    </select>
                    <small id="error_level" class="form-text form-error"></small>
                </div>
            </div>';
        endif;

        $form_information = '
            <div class="line"></div>
            <h3 class="card-title">'. __('label.user_detail') .'</h3>
            
            <div class="form-row">
                <div class="form-group col col-md-6 col-lg-6">
                    <div class="form-preview">
                        <label for="avatar" id="avatar_id">
                            <div class="icon">
                                <i class="mdi mdi-plus"></i>
                                <span>'. __('label.select_avatar') .'</span>
                            </div>
                        </label>
                        <input type="file" id="avatar" name="avatar" accept="image/*">
                    </div>
                    <small id="error_avatar" class="form-text form-error"></small>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="full_name">'. __('label.full_name') .'</label>
                    <input type="text" id="full_name" class="form-control" name="full_name" value="">
                    <small id="error_full_name" class="form-text form-error"></small>
                </div>
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="gender">'. __('label.gender') .'</label>
                    <select id="gender" class="form-control" name="gender">
                        <option value="">'. __('label.select_gender') .'</option>
                        <option value="Male">'. __('label.male') .'</option>
                        <option value="Female">'. __('label.female') .'</option>
                    </select>
                    <small id="error_gender" class="form-text form-error"></small>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="email">'. __('label.email') .'</label>
                    <input type="email" id="email" class="form-control" name="email" value="">
                    <small id="error_email" class="form-text form-error"></small>
                </div>
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="confirm_email">'. __('label.confirm_email') .'</label>
                    <input type="email" id="confirm_email" class="form-control" name="confirm_email" value="">
                    <small id="error_confirm_email" class="form-text form-error"></small>
                </div>
            </div>
            '. $level .'
            <div class="line"></div>
            <h3 class="card-title">'. __('label.login_detail') .'</h3>

            <div class="form-row">
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="username">'. __('label.username') .'</label>
                    <input type="text" id="username" class="form-control" name="username" value="">
                    <small id="error_username" class="form-text form-error"></small>
                </div>
                
            </div>
            <div class="form-row">
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="password">'. __('label.password') .'</label>
                    <input type="password" id="password" class="form-control" name="password" value="">
                    <small id="error_password" class="form-text form-error"></small>
                </div>
                <div class="form-group col col-md-6 col-lg-6">
                    <span class="viewPassword" data-target="password"><i class="mdi mdi-eye"></i></span>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="status">'. __('label.status') .'</label>
                    <input type="hidden" name="status_preview" id="status_preview" value="Active">
                    <select id="status" class="form-control get_preview" data-target="status" name="status">
                        <option value="1" data-value="Active">'. __('label.active_sts') .'</option>
                        <option value="2" data-value="Inactive">'. __('label.inactive_sts') .'</option>
                    </select>
                    <small id="error_status" class="form-text form-error"></small>
                </div>
            </div>

            <div class="line"></div>
            <button type="button" class="btn btn-primary" id="validate_user">'. __('label.save') .'</button>
            <a href="'. url('user') .'" class="btn btn-warning">'. __('label.back') .'</a>
        ';

        $data = [
            'room_role' => $room_role,
            'form_information' => $form_information,
        ];
        
        return $data;

    }

    public function student($user_id){
        //dd($user_id);
        $query = \App\ClassRoomAdmin::where('user_id', $user_id)->get();
      
        $room_id = [];

        if($query):
        
            foreach($query as $q):
        
                $room_id[] = $q->class_room_id;
        
            endforeach;
        
        endif;          
        
        $rooms = \App\ClassRoom::whereIn('id', $room_id)->get();
        //dd($rooms);
        $room_option  = '';
        
        if($rooms):
            foreach($rooms as $room):
                $room_option .= '<option value="'. $room->id .'" data-value="'. $room->class_name .'">'. $room->class_name .'</option>';
            endforeach;
        endif;

        $prefectures = DB::table('city')->where('CountryCode', 'JPN')->groupBy('District')->orderBy('node','asc')->get(); 
       
        $prefecture_option = '';

        if($prefectures):
            foreach($prefectures as $prefecture):
                $prefecture_option .= '<option value="'. $prefecture->District .'">'. $prefecture->District .'</option>';
            endforeach;
        endif;
        //for birthday
        $month = '';
        $year = '';
        $day  = '';
        $temp_month = [
            1 => __('label.jan'),
            2 => __('label.feb'),
            3 => __('label.mar'),
            4 => __('label.apr'),
            5 => __('label.may'),
            6 => __('label.jun'),
            7 => __('label.jul'),
            8 => __('label.aug'),
            9 => __('label.sep'),
            10 => __('label.oct'),
            11 => __('label.nov'),
            12 => __('label.dec'),
        ];

        for($y= date('Y'); $y >= 1971; $y-- ){
            $year .= '<option value="'. $y .'">'. $y .'</option>';
        }
        // for($m = 1; $m <=12; $m++){
        //     $month .= '<option value="'. $m .'">'. date('F', mktime(0, 0, 0, $m)) .'</option>';
        
        // }
        foreach($temp_month as $key => $value){
            $month .= '<option value="'.$key .'">'. $value .'</option>';
        
        }
        for($d= 1; $d <= 31; $d++ ){
            $day .= '<option value="'. $d .'">'. $d .'</option>';
        }

        $room_role = '
            <label for="class_room">教室</label>
            <input type="hidden" name="class_room_preview" id="class_room_preview">
            <select id="class_room" class="form-control get_preview" data-target="class_room" name="class_room">
                <option value="">---お選びください---</option>
                '. $room_option .'
            </select>
            <small id="error_class_room" class="form-text form-error"></small>';

        $form_information = '
            <div class="line"></div>
            <h3 class="card-title">詳細</h3>

            <div class="form-row">
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="kanji_name" class="jp">氏名</label>
                    <input type="text" id="kanji_name" class="form-control" name="kanji_name" value="">
                    <small id="error_kanji_name" class="form-text form-error"></small>
                </div>
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="romaji_name" class="jp">ローマ字表記　フルネーム</label>
                    <input type="text" id="romaji_name" class="form-control" name="romaji_name" value="">
                    <small id="error_romaji_name" class="form-text form-error"></small>
                </div>
                
            </div>
            <div class="form-row">
                <div class="form-group col col-md-2 col-lg-2">
                    <label for="b_year" class="jp">誕生日（年/月/日）</label>
                    
                    <select name="b_year" class="form-control">
                        <option value="">年</option>
                        '. $year .'
                    </select>
                    <small id="error_b_year" class="form-text form-error"></small>
                </div>
                <div class="form-group col col-md-2 col-lg-2">
                    <label for="b_month" class="jp">&nbsp;</label>
                    
                    <select name="b_month" class="form-control">
                        <option value="">月</option>
                        '. $month .'
                    </select>
                    <small id="error_b_month" class="form-text form-error"></small>
                </div>
                <div class="form-group col col-md-2 col-lg-2">
                    <label for="b_day" class="jp">&nbsp;</label>
                    <select name="b_day" class="form-control">
                        <option value="">日</option>    
                        '. $day .'
                    </select>
                    <small id="error_b_day" class="form-text form-error"></small>
                </div>
             
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="prefecture" class="jp">お住まい（○○県）</label>
                    <select id="prefecture" class="form-control" name="prefecture">
                        <option value="">Select Prefecture</option>
                        '. $prefecture_option .'
                    </select>
                    <small id="error_prefecture" class="form-text form-error"></small>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="cram_school" class="jp">教室名</label>
                    <input type="text" id="cram_school" class="form-control" name="cram_school" value="">
                    <small id="error_cram_school" class="form-text form-error"></small>
                </div>
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="english_learning" class="jp">英語学習暦</label>
                    <select name="english_learning" id="english_learning" class="form-control">
                        <option value="">Select Span</option>
                        <option value="なし">なし</option>
                        <option value="1年未満">1年未満</option>
                        <option value="１～２年">１～２年</option>
                        <option value="２～３年">２～３年</option>
                        <option value="３～４年">３～４年</option>
                        <option value="４～５年">４～５年</option>
                        <option value="５年以上">５年以上</option>
                    </select>
                    <small id="error_english_learning" class="form-text form-error"></small>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="english_schedule" class="jp">入会時の英語学習状況</label>
                    <input type="text" id="english_schedule" class="form-control" name="english_schedule" placeholder="学校○○時間／週、塾○○時間／週、その他○○時間／週">
                    <small id="error_english_schedule" class="form-text form-error"></small>
                </div>
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="account_prof" class="jp">取得済みの語学力検定・資格など</label>
                    <input type="text" id="account_prof" class="form-control" name="account_prof" placeholder="試験名：○○、級／スコア：○○、取得年月日○○">
                    <small id="error_account_prof" class="form-text form-error"></small>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col col-md-12 col-lg-12">
                    <label for="remark" class="jp">特記事項</label>
                    <textarea id="remark" class="form-control" name="remark" cols="8"></textarea>
                    <small id="error_remark" class="form-text form-error"></small>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="email">Eメール</label>
                    <input type="email" id="email" class="form-control" name="email">
                    <small id="error_email" class="form-text form-error"></small>
                </div>
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="confirm_email">Eメール確認</label>
                    <input type="email" id="confirm_email" class="form-control" name="confirm_email">
                    <small id="error_confirm_email" class="form-text form-error"></small>
                </div>
            </div>

            <div class="line"></div>
            <h3 class="card-title">ログイン詳細</h3>
            <div class="form-row">
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="username">ユーザーID</label>
                    <input type="text" id="username" class="form-control" name="username">
                    <small id="error_username" class="form-text form-error"></small>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="password">パスワード</label>
                    <input type="password" id="password" class="form-control" name="password">
                    <small id="error_password" class="form-text form-error"></small>
                </div>
                <div class="form-group col col-md-6 col-lg-6">
                    <span class="viewPassword" data-target="password"><i class="mdi mdi-eye"></i></span>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="status">状態</label>
                    <input type="hidden" name="status_preview" id="status_preview" value="Active">
                    <select id="status" class="form-control get_preview" data-target="status" name="status">
                        <option value="1" data-value="Active">有効</option>
                        <option value="2" data-value="Inactive">無効</option>
                    </select>
                    <small id="error_status" class="form-text form-error"></small>
                </div>
            </div>

            <div class="line"></div>
            <button type="button" id="validate_user" class="btn btn-primary">Submit</button>
            <a href="'. url('user') .'" class="btn btn-warning">Cancel</a>';
        
        $data = [
            'room_role' => $room_role,
            'form_information' => $form_information,
        ];
        return $data;
    } 

    public function get_user_detail(Request $request){

        $user = \App\Users::where('username', $request->input('username'))->first();
        //dd($user);
        $title      = null;
        $content    = null;

        if($user->role == 4):
            
            $title   = $user->kanji_name;

            $status = $user->status == 1 ? 'Active':'Inactive';

            $content = '
                <div class="user-detail">
                    <div class="u-group">
                        <div class="a-container">
                            <div class="a-image">
                                <img src="'. asset('uploads/avatar/noavatar.jpg') .'">
                            </div>
                        </div>
                    </div>
                    <div class="u-group">
                        <div class="u-label">氏名</div>
                        <div class="u-value">'. $user->kanji_name .'</div>
                    </div>
                    <div class="u-group">
                        <div class="u-label">ローマ字表記　フルネーム</div>
                        <div class="u-value">'. $user->romaji_name .'</div>
                    </div>
                    <div class="u-group">
                        <div class="u-label">誕生日</div>
                        <div class="u-value">'. $user->birth_date .'</div>
                    </div>
                    <div class="u-group">
                        <div class="u-label">お住まい（○○県）</div>
                        <div class="u-value">'. $user->prefecture .'</div>
                    </div>
                    <div class="u-group">
                        <div class="u-label">教室名</div>
                        <div class="u-value">'. $user->cram_school .'</div>
                    </div>
                    <div class="u-group">
                        <div class="u-label">英語学習暦</div>
                        <div class="u-value">'. $user->english_learning .'</div>
                    </div>
                    <div class="u-group">
                        <div class="u-label">入会時の英語学習状況</div>
                        <div class="u-value">'. $user->englist_schedule .'</div>
                    </div>
                    <div class="u-group">
                        <div class="u-label">取得済みの語学力検定・資格など</div>
                        <div class="u-value">'. $user->account_prof .'</div>
                    </div>
                    <div class="u-group">
                        <div class="u-label">Eメール</div>
                        <div class="u-value">'. $user->email .'</div>
                    </div>
                    <div class="u-group last">
                        <div class="u-label">特記事項</div>
                        <div class="u-value">'. nl2br($user->remark) .'</div>
                    </div>
                    <div class="u-line">
                        <span>Login Detail</span>
                    </div>

                    <div class="u-group">
                        <div class="u-label">ユーザーID</div>
                        <div class="u-value">'. $user->email .'</div>
                    </div>

                    <div class="u-group">
                        <div class="u-label">パスワード</div>
                        <div class="u-value">********</div>
                    </div>

                    <div class="u-group last">
                        <div class="u-label">状態</div>
                        <div class="u-value">'. $status .'</div>
                    </div>
                </div>';
        else:
        
            $title   = $user->full_name;
            
            if($user->avatar == null || !File::exists('uploads/avatar/'. $user->avatar)):

                $avatar = '<img src="'. asset('uploads/avatar/noavatar.jpg') .'">';
            
            else:
            
                $avatar = '<img src="'. asset('uploads/avatar/'. $user->avatar) .'">';
            
            endif;

            $status = $user->status == 1 ? 'Active':'Inactive';

            $content = '<div class="user-detail">
                        <div class="u-group">
                            <div class="a-container">
                                <div class="a-image">
                                    '. $avatar .'
                                </div>
                            </div>
                        </div>
                        <div class="u-group">
                            <div class="u-label">'. __('label.full_name') .'</div>
                            <div class="u-value">'. $user->full_name .'</div>
                        </div>
                        <div class="u-group">
                            <div class="u-label">'. __('label.gender') .'</div>
                            <div class="u-value">'. $user->gender .'</div>
                        </div>
                        <div class="u-group last">
                            <div class="u-label">'. __('label.email') .'</div>
                            <div class="u-value">'. $user->email .'</div>
                        </div>

                        <div class="u-line">
                            <span>'. __('label.login_detail') .'</span>
                        </div>

                        <div class="u-group">
                            <div class="u-label">'. __('label.username') .'</div>
                            <div class="u-value">'. $user->username .'</div>
                        </div>

                        <div class="u-group">
                            <div class="u-label">'. __('label.password') .'</div>
                            <div class="u-value">********</div>
                        </div>

                        <div class="u-group last">
                            <div class="u-label">'. __('label.status') .'</div>
                            <div class="u-value">'. $status .'</div>
                        </div>
                    </div>';
        endif;

        $data = [
            'title'     => $title,
            'content'   => $content
        ];

        return response()->json($data);
    }

    function export(Request $request){

        $room_id = DB::table('class_rooms_admin')
                    ->select('class_room_id')
                    ->where('user_id', Auth::id())
                    ->first();
       
        if($this->user->role == 5 || $this->user->role == 4) abort(403);
        
        $qData = [
            'name'      => $request->get('name'),
            'role'      => $request->get('role'),
            'inactive'  => $request->get('inactive')
        ];

        $query = \App\Users::query();

        if($qData['inactive']):
            $query->where(function($query){
                $query->where('status', 1)
                    ->orWhere('status', 2);
            });
        else:
            $query->where('status', 1);
        endif;

        if($qData['role']):
            $query->where('role', $qData['role']);
        endif;

        if($qData['name']):
            $query->where(function($query) use ($qData){
                $query->where('full_name', 'LIKE', '%'.$qData['name'].'%')
                    ->orWhere('kanji_name', 'LIKE', '%'.$qData['name'].'%')
                    ->orWhere('romaji_name', 'LIKE', '%'.$qData['name'].'%')
                    ->orWhere('username', 'LIKE', '%'.$qData['name'].'%');
            });
        endif;
        
        if(Auth::user()->role == class_room){
            $class_admin = [];
            if($room_id):
            $class_users = DB::table('class_rooms_admin')
                            ->select('user_id')
                            ->where('class_room_id','=', $room_id->class_room_id)
                            ->get();
                
                if($class_users):

                    foreach($class_users as $cu):
                    
                        $class_admin[] = $cu->user_id;
        
                    endforeach;
                
                endif;
            else:
                abort(404);
            endif;
            
            $result = array_merge($class_admin, $this->student_id);
            
            $query->whereIn('id', $result);
                    
            $users = $query->get();

        }else if(Auth::user()->role == aica){
            $query->where('role','=','5')
                ->orWhere('role','=','3');
            $users = $query->get();
        }else{
            $users = $query->get(); 
        }
        
        $date = date('m-d-Y His');

        return Excel::download(new \App\Exports\UserExport($users), 'users-'. $date .'.xlsx');
        //dd($users);
        // return view('users.index')
        //     ->with('title', 'User Management')
        //     ->with('users', $users)
        //     ->with('qData', $qData);


    }
}
