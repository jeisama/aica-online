<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\TemplateRequest;
use Illuminate\Support\Facades\Auth;
use Uuid;
use Session;
use File;

class TemplateController extends Controller
{

    public function __construct(){

        $this->middleware('admin');

    }

    public function index(Request $request){
        $qData = [
            'course_type'      => $request->get('course_type'),
            'lesson_type'      => $request->get('lesson_type')
        ];
        //dd($qData);
        $query = DB::table('templates')
                    ->join('courses', 'courses.id', '=', 'templates.course_type')
                    ->join('grade_levels', 'grade_levels.id', '=', 'templates.lesson_type')   
                    ->select('templates.*', 'courses.label AS course_label', 'grade_levels.label AS grade_label');
        if($qData['course_type']){
            $query->where('templates.course_type',$qData['course_type']);
        }
        if($qData['lesson_type']){
            $query->where('templates.lesson_type',$qData['lesson_type']);
        }
                    
        $templates = $query->get();
       // dd($templates);
        $courses = DB::table('courses')->get();
        //dd($templates);
        
        return view('template.index')
                ->with('title', 'AIC Template Management')
                ->with('templates', $templates)
                ->with('courses', $courses)
                ->with('qData', $qData);
                
    }

    public function create(){
        $courses = DB::table('courses')->get();
                    
        return view('template.create')->with('courses', $courses);
    }

    public function store(TemplateRequest $request){
        $data = $request->all();
        //dd($data);
        $data['created_by'] = Auth::id();
        //dd($data);
        $lesson = \App\Template::create($data);
        $media       = $request->file('media');
        $files = $request->file('attachment');

        if($files != null):
        	$i = 0;
        	foreach($files as $file):
        		$i++;
        		
        		$uuid = Uuid::generate(1)->string;

        		$file_name = $file->getClientOriginalName();
        		$extention = $file->getClientOriginalExtension();
        		$attachment= $lesson->id . $uuid . $i .'.'. $extention;

        		$resource = fopen( $file->getRealPath(), 'r');
        		
                $file->move('uploads/lessons/', $attachment);
        		//Storage::disk('public')->put($attachment, $resource);

        		\App\LessonAttachment::create([
					'lesson_id'		=> $lesson->id,
					'attachment'	=> $attachment,
					'original_name'	=> $file_name
        		]);
        	endforeach;
        endif;


        if($media != null):

            $lesson_id      = $lesson->id;
            $source_data    = null;
            $original_name  = null;
            
            for($i = 0; $i < count($media); $i++):
            
                $upload = $request->file('media')[$i];
                $uid = Uuid::generate(1)->time;
                    
                $original_name  = $upload->getClientOriginalName();
                $ext            = $upload->getClientOriginalExtension();
                $source_data    = $uid . $i .'.'. $ext;

                $upload->move('uploads/video/', $source_data);

                \App\LessonMedia::create([
                    'lesson_id'     => $lesson_id,
                    'source'        => 'local',
                    'source_data'   => $source_data,
                    'original_name' => $original_name
                ]);
            endfor;
        endif;

        Session::flash('success_message', __('label.template_created'));

        return response()->json([
            'response'  => 200,
            'redirect'  => url('template')
        ]);
    }

    public function show($id){
        //dd($id);
        $data = ' ';
        $templates = DB::table('templates')
                    ->join('courses', 'courses.id', '=', 'templates.course_type')
                    ->join('grade_levels', 'grade_levels.id', '=', 'templates.lesson_type')   
                    ->select('templates.*', 'courses.label AS course_label', 'grade_levels.label AS grade_label')
                    ->where('templates.id', '=', $id)
                    ->first();
        $media = DB::table('lesson_media')
                ->select('lesson_media.*')
                ->where('lesson_media.lesson_id', '=', $id)
                ->get();
        $attachment = DB::table('lesson_attachments')
                ->select('lesson_attachments.*')
                ->where('lesson_attachments.lesson_id', '=', $id)
                ->get();
        //dd($media);
        $audios = '';
        $files = '';
        if($media){
            foreach($media as $lesson_media){
                //dd($lesson_media);

                $audios.= '<div style="display: inline-block; padding-right: 20px; text-align:center;">
                                <audio controls>
                                    <source src="'.url('uploads/video/'.$lesson_media->source_data).'" type="audio/mp3">
                                        Your browser does not support the audio element.
                                </audio><br>
                                <span style="margin: 5px 0 10px 0;">'.$lesson_media->original_name.'</span>
                            </div>';

            }
        }
       
        if($attachment){
            foreach($attachment as $file){
                //dd($lesson_media);
                $files.='<div class="text-center p-2">
                    <a href = "'.url('uploads/lessons/'.$file->attachment).'" target="_blank">'.$file->original_name.'</a>
                </div>';
            }
                
        }
        // @if($attachment)
        // @foreach($attachment as $file)
        //     <div><a href="{{asset('uploads/lessons/'. $file->attachment)}}" target="_blank"> {{ $file->original_name ?? $file->attachment }}</a> </div>
        // @endforeach
        // @endif
       //dd($templates->form_type);
        if($templates->form_type == 1){
            $data = '
                    <div class="modal-header">
                        <dl class="row">
                                <dt class="col-sm-3">'.__('label.course').' :</dt>
                                <dd class="col-sm-8">'.$templates->course_label.'</dd>
                                <dt class="col-sm-3">'.__('label.grade').' :</dt>
                                <dd class="col-sm-8">'.$templates->grade_label.'</dd>
                                <dt class="col-sm-3">'.__('label.day').' :</dt>
                                <dd class="col-sm-8" id="val_lesson_day">'.$templates->lesson_day.'</dd>
                                <dt class="col-sm-3">'.__('label.lesson_name').' :</dt>
                                <dd class="col-sm-8" id="val_lesson_name">'.$templates->lesson_name.'</dd>
                        </dl>          
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                            <div class="homework-box" style="border: 1px solid #c5c5c5; padding: 15px; width: 100%;">
                            <div class="tbl" style="display: table; width: 100%;">
                                <div style="display: table-cell; vertical-align: top; width:30px;"></div>
                                    <div style="display: table-cell; vertical-align: top;">
                                        <div class="form-row">
                                            <div class="form-group col col-md-12 col-lg-12">
                                                <label for="" class="form-control-label"><strong>Speaking part用 課題</strong></label>
                                                <p>次回の授業で以下のトピックについて先生から問われます。事前にあなたの意見と、そのように
                                                思う理由を2～3文の英文でまとめましょう。<br>
                                                どのように英語で表現すれば良いか分からない部分は、辞書などを用いて調べましょう。
                                                </p>
                                                <div style="border:solid 1px #c5c5c5; padding:10px;">
                                                    <p><strong>次週のSpeaking Topic：</strong></p>
                                                    <p style="text-align: left; font-size:23px !important;">'.nl2br($templates->speaking_topic).'</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="tbl" style="display: table; width: 100%;">
                                <div style="display: table-cell; vertical-align: top; width:30px;"></div>
                                    <div style="display: table-cell; vertical-align: top;">
                                        <div class="form-row">
                                            <div class="form-group col col-md-12 col-lg-12">
                                                
                                                <textarea rows="4" cols="50" id="stu_opinion" name="stu_opinion" class="form-control" placeholder="こちらに回答してください。" disabled></textarea>
                                                <small id="error-writing_student_2_ans" class="form-text form-error"></small>
                                            </div>
                                        </div>
                                </div>
                            </div>
                            <div class="tbl" style="display: table; width: 100%;">
                                <div style="display: table-cell; vertical-align: top; width:30px;"></div>
                                    <div style="display: table-cell; vertical-align: top;">
                                        <div class="form-row">
                                            <div class="form-group col col-md-12 col-lg-12">
                                                <label for="listening" class="form-control-label"><strong>Listening part用 課題</strong></label>
                                                <p>Step 1. 音声を再生して、流れる会話文の内容を聞き取りましょう。繰り返し聞いても内容を理解<br>
                                                できないときは、下の“展開”ボタンを押して、スクリプトを確認しながらリスニングを行いましょう。</p>
                                                <div>
                                                <div class="form-group col-lg-12">
                                                        '.$audios.'
                                                <div>
                                                    
                                            </div>
                                        </div>
                                    </div>
                                                
                                            </div>

                                        </div>
                                    </div>
                            </div>
                            <div class="tbl" style="display: table; width: 100%;">
                                <div style="display: table-cell; vertical-align: top; width:30px;"></div>
                                    <div style="display: table-cell; vertical-align: top;">
                                        <div class="form-row">
                                            <div class="form-group col col-md-12 col-lg-12">
                                                <p>Step 2. スクリプトを見ながら、音声の後に続いて音読しましょう。その際、英文の内容や意味を<br>
                                                意識しながら音読するようにしましょう。<br>
                                                わからない単語や表現がある場合は、辞書などを用いて調べましょう。
                                                </p>
                                                
                                                
                                                <label for="next_listening" class="form-control-label"><strong>今週のListening 課題：</strong></label>
                                                <button type="button" class="expander" id="btn-control">開く</button>
                                                <div style="border:solid 1px #c5c5c5; padding:10px;" id="listen-div"  class="hidden">
                                                        <div class="form-group col-lg-12">
                                                            '.nl2br($templates->next_listening).'
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col col-md-12 col-lg-12">
                                                <label for="attachment" class="form-control-label"><strong>レッスン添付ファイル</strong></label><br>
                                                <div style="border:solid 1px #c5c5c5; padding:10px;">
                                                    <div class="form-group col-lg-12">'.$files.' </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div><!--homework-box-->   
                </div><!--modal body--> ';
        }    
        if($templates->form_type == 2){
            $data = '
                    <div class="modal-header">
                    <dl class="row">
                            <dt class="col-sm-3">'.__('label.course').' :</dt>
                            <dd class="col-sm-8">'.$templates->course_label.'</dd>
                            <dt class="col-sm-3">'.__('label.grade').' :</dt>
                            <dd class="col-sm-8">'.$templates->grade_label.'</dd>
                            <dt class="col-sm-3">'.__('label.day').' :</dt>
                            <dd class="col-sm-8" id="val_lesson_day">'.$templates->lesson_day.'</dd>
                            <dt class="col-sm-3">'.__('label.lesson_name').' :</dt>
                            <dd class="col-sm-8" id="val_lesson_name">'.$templates->lesson_name.'</dd>
                    </dl>         
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                            <div class="homework-box" style="border: 1px solid #c5c5c5; padding: 15px; width: 100%;">
                            <div class="tbl" style="display: table; width: 100%;">
                                <div style="display: table-cell; vertical-align: top; width:30px;"></div>
                                    <div style="display: table-cell; vertical-align: top;">
                                        <div class="form-row">
                                            <div class="form-group col col-md-12 col-lg-12">
                                                <label for="" class="form-control-label"><strong>Speaking part用課題</strong></label><br>
                                                <p>この1週間の出来事について書きましょう。何をやったのかという文章と、更に内容を詳しくする
                                                文章を1文ずつ書きましょう。作った文章を次回の授業の中で発表しましょう。<br>
                                                </p>
                                                <textarea rows="4" cols="50" id="stu_opinion" name="stu_opinion" class="form-control" placeholder="こちらに回答してください。" disabled></textarea>
                                                <small id="error-stu_opinion" class="form-text form-error"></small>
                                            </div>
                                        </div>
                                </div>
                            </div>
                            <div class="tbl" style="display: table; width: 100%;">
                                <div style="display: table-cell; vertical-align: top; width:30px;"></div>
                                    <div style="display: table-cell; vertical-align: top;">
                                        <div class="form-row">
                                            <div class="form-group col col-md-12 col-lg-12">
                                                <label for="listening" class="form-control-label"><strong>Listening part用課題</strong></label>
                                                <p>Step 1. 音声を再生して、流れる会話文の内容を聞き取りましょう。繰り返し聞いても内容を理解<br>
                                                できないときは、下の“展開”ボタンを押して、スクリプトを確認しながらリスニングを行いましょう。
                                                </p>
                                            <div>
                                            <div class="form-group col-lg-12">
                                            '.$audios.'
                                            <div>
                                                    
                                            </div>
                                            </div>
                                                </div>
                                                
                                            </div>

                                        </div>
                                    </div>
                            </div>
                            <div class="tbl" style="display: table; width: 100%;">
                                <div style="display: table-cell; vertical-align: top; width:30px;"></div>
                                    <div style="display: table-cell; vertical-align: top;">
                                        <div class="form-row">
                                            <div class="form-group col col-md-12 col-lg-12">
                                                <p>Step 2. スクリプトを見ながら、音声の後に続いて音読しましょう。その際、英文の内容や意味を<br>
                                                意識しながら音読するようにしましょう。<br>
                                                わからない単語や表現がある場合は、辞書などを用いて調べましょう。
                                                </p>
                                                
                                                
                                                <label for="next_listening" class="form-control-label"><strong>今週のListening 課題：</strong></label>
                                                <button type="button" class="expander" id="btn-control">開く</button>
                                                <div style="border:solid 1px #c5c5c5; padding:10px;" id="listen-div"  class="hidden">
                                                        <div class="form-group col-lg-12">
                                                            '.nl2br($templates->next_listening).'
                                                        </div>
                                                </div>
                                    
                                            </div>
                                    
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col col-md-12 col-lg-12">
                                                <label for="attachment" class="form-control-label"><strong>レッスン添付ファイル</strong></label><br>
                                                <div style="border:solid 1px #c5c5c5; padding:10px;">
                                                    <div class="form-group col-lg-12">'.$files.' </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                            
                        </div><!--homework-box-->   
                </div><!--modal body--> ';
        } 
        if($templates->form_type == 3){
            $data = '
                    <div class="modal-header">
                    <dl class="row">
                            <dt class="col-sm-3">'.__('label.course').' :</dt>
                            <dd class="col-sm-8">'.$templates->course_label.'</dd>
                            <dt class="col-sm-3">'.__('label.grade').' :</dt>
                            <dd class="col-sm-8">'.$templates->grade_label.'</dd>
                            <dt class="col-sm-3">'.__('label.day').' :</dt>
                            <dd class="col-sm-8" id="val_lesson_day">'.$templates->lesson_day.'</dd>
                            <dt class="col-sm-3">'.__('label.lesson_name').' :</dt>
                            <dd class="col-sm-8" id="val_lesson_name">'.$templates->lesson_name.'</dd>
                    </dl>     
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="homework-box" style="border: 1px solid #c5c5c5; padding: 15px; width: 100%;">

                            <div class="tbl" style="display: table; width: 100%;">
                                <div style="display: table-cell; vertical-align: top; width:30px;"><strong></strong></div>
                                    <div style="display: table-cell; vertical-align: top;">
                                        <div class="form-row">
                                            <div class="form-group col col-md-12 col-lg-12">
                                                <label for="" class="form-control-label write">今回の課題（以下の課題に対して「記入欄」に英作文を記入しましょう）</label>
                                                <div style="border:solid 1px #c5c5c5; padding:10px;">
                                                    <p style="text-align: left; font-size:23px !important;">'.nl2br($templates->writing_topic).'</p>
                                                </div><br>
                                                <textarea rows="6" cols="50" id="chat_history" name="chat_history" class="form-control" placeholder="チャット履歴" disabled></textarea>
                                            </div>
                                        </div>
                                </div>
                            </div>
                    
                            <div class="tbl" style="display: table; width: 100%;">
                                <div style="display: table-cell; vertical-align: top; width:30px;"><strong></strong></div>
                                    <div style="display: table-cell; vertical-align: top;">
                                        <div class="form-row">
                                            <div class="form-group col col-md-12 col-lg-12">
                                                <label for="writing_stu_ans" class="form-control-label write">記入欄</label>
                                                <textarea rows="6" cols="50" id="writing_stu_ans" name="writing_stu_ans" class="form-control" placeholder="こちらに回答してください。" disabled></textarea>
                                    
                                            </div>
                                    
                                        </div>
                                </div>
                            </div>

                            <div class="tbl" style="display: table; width: 100%;">
                                <div style="display: table-cell; vertical-align: top; width:30px;"><strong></strong></div>
                                    <div style="display: table-cell; vertical-align: top;">
                                        <div class="form-row">
                                            <div class="form-group col col-md-12 col-lg-12">
                                                <label for="writing_tea_res" class="form-control-label write">添削結果</label>
                                                <textarea rows="6" cols="50" id="writing_tea_res" name="writing_tea_res" class="form-control" disabled></textarea>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col col-md-12 col-lg-12">
                                                <label for="writing_tea_res" class="form-control-label write">レッスン添付ファイル</label>
                                                <div style="border:solid 1px #c5c5c5; padding:10px;">
                                                    <div class="form-group col-lg-12">
                                                        '.$files.'
                                                    </div>
                                                </div>                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            
                        </div><!--home-->

                    </div><!--modal body--> ';
        }                   

         return response()->json([
            'message' => 'success',
            'data' => $data
            
        ]);
    }

    public function edit($id){
        $courses = DB::table('courses')->get();
        $templates = DB::table('templates')
        ->join('courses', 'courses.id', '=', 'templates.course_type')
        ->join('grade_levels', 'grade_levels.id', '=', 'templates.lesson_type')   
        ->select('templates.*', 'courses.label AS course_label', 'grade_levels.label AS grade_label')
        ->where('templates.id', '=', $id)
        ->first();
        $grades = \App\CourseGradeLevel::join('grade_levels', 'grade_levels.id', '=', 'course_grade_levels.grade_id')->where('course_grade_levels.course_id', $templates->course_type)->get();
        //dd($grades);
        $media = DB::table('lesson_media')
                    ->select('lesson_media.*')
                    ->where('lesson_media.lesson_id', '=', $id)
                    ->get();
        $attachment = DB::table('lesson_attachments')
                    ->select('lesson_attachments.*')
                    ->where('lesson_attachments.lesson_id', '=', $id)
                    ->get();
       // dd($attachment );
        return view('template.edit')
            ->with('courses', $courses)                
            ->with('templates', $templates)
            ->with('grades', $grades)
            ->with('media',$media)
            ->with('attachment',$attachment);
        
    }

    public function update($id, TemplateRequest $request ){
        $data = $request->all();
       //dd( $data);
        $data['updated_by'] = Auth::id();

        //dd($data);
        $template = \App\Template::find($id);
        //dd($data);
        $template->update($data); 

        $media       = $request->file('media');
        $files = $request->file('attachment');

        if($files != null):
        	$i = 0;
        	foreach($files as $file):
        		$i++;
        		
        		$uuid = Uuid::generate(1)->string;

        		$file_name = $file->getClientOriginalName();
        		$extention = $file->getClientOriginalExtension();
        		$attachment= $template->id . $uuid . $i .'.'. $extention;

        		$resource = fopen( $file->getRealPath(), 'r');
        		
                $file->move('uploads/lessons/', $attachment);
        		//Storage::disk('public')->put($attachment, $resource);

        		\App\LessonAttachment::create([
					'lesson_id'		=> $template->id,
					'attachment'	=> $attachment,
					'original_name'	=> $file_name
        		]);
        	endforeach;
        endif;


        if($media != null):

            $lesson_id      = $template->id;
            $source_data    = null;
            $original_name  = null;
            
            for($i = 0; $i < count($media); $i++):
            
                $upload = $request->file('media')[$i];
                $uid = Uuid::generate(1)->time;
                    
                $original_name  = $upload->getClientOriginalName();
                $ext            = $upload->getClientOriginalExtension();
                $source_data    = $uid . $i .'.'. $ext;

                $upload->move('uploads/video/', $source_data);

                \App\LessonMedia::create([
                    'lesson_id'     => $lesson_id,
                    'source'        => 'local',
                    'source_data'   => $source_data,
                    'original_name' => $original_name
                ]);
            endfor;
        endif;
        Session::flash('success_message', __('label.template_updated'));

        return response()->json([
            'response'  => 200,
            'redirect'  => url('template')
        ]);
    }

    public function delete_attachment(Request $request){
        
        $id = $request->input('id');
       
        $query = \App\LessonAttachment::find($id);

        if($query != null):
            File::delete('uploads/lessons/'. $query->attachment);
            //Storage::disk('public')->delete($query->attachment);
            $query->delete();
        endif;
    }

    public function delete_media(Request $request){

        $id = $request->input('id');
        //dd( $id);
        $query = \App\LessonMedia::find($id);
        
        if($query != null):

            if($query->type == 1):

                File::delete('uploads/video/'. $query->source_data);

            endif;

            $query->delete();

        endif;

    }

}
