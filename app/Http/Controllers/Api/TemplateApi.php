<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;

class TemplateApi extends Controller
{
    
	public function get_lesson(Request $request){

		$id = $request->input('id');

		$grades = \App\GradeLevel::with(['get_course' => function($query) use ($id) {

			$query->where('course_id', $id);

		}])->where('status', 1)->get();

		$data = '<input type="hidden" name="id" value="'. $id .'">';

		if($grades):
			foreach($grades as $grade):
			
				$check = '';

				if($grade->get_course):
					$check = 'checked';
				endif;

				$data .= '
						<div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="grade[]" value="'. $grade->id .'" id="check'. $grade->id .'" '. $check .'>
                            <label class="custom-control-label" for="check'. $grade->id .'" style="padding-top: 2px;">'. $grade->label .'</label>
                        </div>';

			endforeach;
		endif;

		return response()->json(['data' => $data]);
	}

}
