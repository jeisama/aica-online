<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;

class ClassroomApi extends Controller
{
    
	public function admin(Request $request){
		
		$id = $request->input('id');
		
		$type = $request->input('type');

		$query = \App\Users::query();

		if($type == 1):
			$query->with(['room_admin' => function($query) use ($id){
				$query->where('class_room_id', $id);
			}])->whereIn('role',['1', '2', '3']);
		else:
			$query->with(['room_admin' => function($query) use ($id){
				$query->where('class_room_id', $id);
			}])->where('created_at', $id)->whereIn('role',['1', '2', '3']);
		endif;

		$result = $query->get();
		
		$data = '<input type="hidden" name="id" value="'. $id .'">';

		if($result):
			foreach($result as $res):
				if($res->full_name != null):
				$check = '';
				if($res->room_admin):
					$check = 'checked';
				endif;
				$data .= '
						<div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="user[]" value="'. $res->id .'" id="check'. $res->id .'" '. $check .'>
                            <label class="custom-control-label" for="check'. $res->id .'" style="padding-top: 2px;">'. $res->full_name .'</label>
                        </div>';
                endif;
			endforeach;
		else:

		endif;


		return response()->json(['data' => $data]);
	}

}
