<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;

use File;

use Session;

class UsersApi extends Controller
{
    
    public function get_form(Request $request){

        $role    = $request->input('role');

        $user_id = $request->input('user_id');

        $data = null;

        if($role == '4'):

            $data = $this->student($user_id);
        
        else:
        
            $data = $this->admin_teacher($role);
        
        endif;

        return response()->json($data);

    }

    public function admin_teacher($role){

        $room_role = '';

        $level = '';

        $levels = \App\TeacherLevel::where('status', 1)->get();

        if($role == 5 && $levels):

            
            $option = '';

            foreach($levels as $level):
                $option .= '<option value="'. $level->id .'">'. $level->level .'</option>';
            endforeach;

            $level = '
            <div class="form-row">
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="level">Level</label>
                    <select name="level" id="level" class="form-control">
                        '. $option .'
                    </select>
                    <small id="error_level" class="form-text form-error"></small>
                </div>
            </div>';
        endif;

        $form_information = '
            <div class="line"></div>
            <h3 class="card-title">User Detail</h3>
            
            <div class="form-row">
                <div class="form-group col col-md-6 col-lg-6">
                    <div class="form-preview">
                        <label for="avatar" id="avatar_id">
                            <div class="icon">
                                <i class="mdi mdi-plus"></i>
                                <span>'. __('label.select_avatar') .'</span>
                            </div>
                        </label>
                        <input type="file" id="avatar" name="avatar" accept="image/*">
                    </div>
                    <small id="error_avatar" class="form-text form-error"></small>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="full_name">'. __('label.full_name') .'</label>
                    <input type="text" id="full_name" class="form-control" name="full_name" value="">
                    <small id="error_full_name" class="form-text form-error"></small>
                </div>
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="gender">'. __('label.gender') .'</label>
                    <select id="gender" class="form-control" name="gender">
                        <option value="">'. __('label.select_gender') .'</option>
                        <option value="Male">'. __('label.male') .'</option>
                        <option value="Female">'. __('label.female') .'</option>
                    </select>
                    <small id="error_gender" class="form-text form-error"></small>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="email">'. __('label.email') .'</label>
                    <input type="email" id="email" class="form-control" name="email" value="">
                    <small id="error_email" class="form-text form-error"></small>
                </div>
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="confirm_email">'. __('label.confirm_email') .'</label>
                    <input type="email" id="confirm_email" class="form-control" name="confirm_email" value="">
                    <small id="error_confirm_email" class="form-text form-error"></small>
                </div>
            </div>
            '. $level .'
            <div class="line"></div>
            <h3 class="card-title">'. __('label.login_detail') .'</h3>

            <div class="form-row">
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="username">'. __('label.username') .'</label>
                    <input type="text" id="username" class="form-control" name="username" value="">
                    <small id="error_username" class="form-text form-error"></small>
                </div>
                
            </div>
            <div class="form-row">
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="password">'. __('label.password') .'</label>
                    <input type="password" id="password" class="form-control" name="password" value="">
                    <small id="error_password" class="form-text form-error"></small>
                </div>
                <div class="form-group col col-md-6 col-lg-6">
                    <span class="viewPassword" data-target="password"><i class="mdi mdi-eye"></i></span>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="status">'. __('label.status') .'</label>
                    <input type="hidden" name="status_preview" id="status_preview" value="Active">
                    <select id="status" class="form-control get_preview" data-target="status" name="status">
                        <option value="1" data-value="Active">'. __('label.active_sts') .'</option>
                        <option value="2" data-value="Inactive">'. __('label.inactive_sts') .'</option>
                    </select>
                    <small id="error_status" class="form-text form-error"></small>
                </div>
            </div>

            <div class="line"></div>
            <button type="button" class="btn btn-primary" id="validate_user">'. __('label.save') .'</button>
            <a href="'. url('user') .'" class="btn btn-warning">'. __('label.back') .'</a>
        ';

        $data = [
            'room_role' => $room_role,
            'form_information' => $form_information,
        ];
        
        return $data;

    }

    public function student($user_id){

        $query = \App\ClassRoomAdmin::where('user_id', $user_id)->get();

        $room_id = [];

        if($query):
        
            foreach($query as $q):
        
                $room_id[] = $q->class_room_id;
        
            endforeach;
        
        endif;          
        
        $rooms = \App\ClassRoom::whereIn('id', $room_id)->get();

        $room_option  = '';
        
        if($rooms):
            foreach($rooms as $room):
                $room_option .= '<option value="'. $room->id .'" data-value="'. $room->class_name .'">'. $room->class_name .'</option>';
            endforeach;
        endif;

        $prefectures = DB::table('city')->where('CountryCode', 'JPN')->groupBy('District')->get(); 

        $prefecture_option = '';

        if($prefectures):
            foreach($prefectures as $prefecture):
                $prefecture_option .= '<option value="'. $prefecture->District .'">'. $prefecture->District .'</option>';
            endforeach;
        endif;

        $room_role = '
            <label for="class_room">Class Room</label>
            <input type="hidden" name="class_room_preview" id="class_room_preview">
            <select id="class_room" class="form-control get_preview" data-target="class_room" name="class_room">
                <option value="">Select Class Room</option>
                '. $room_option .'
            </select>
            <small id="error_class_room" class="form-text form-error"></small>';

        $form_information = '
            <div class="line"></div>
            <h3 class="card-title">User Detail</h3>

            <div class="form-row">
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="kanji_name" class="jp">氏名</label>
                    <input type="text" id="kanji_name" class="form-control" name="kanji_name" value="">
                    <small id="error_kanji_name" class="form-text form-error"></small>
                </div>
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="romaji_name" class="jp">ローマ字表記　フルネーム</label>
                    <input type="text" id="romaji_name" class="form-control" name="romaji_name" value="">
                    <small id="error_romaji_name" class="form-text form-error"></small>
                </div>
                
            </div>
            <div class="form-row">
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="birth_date" class="jp">誕生日(yyyy/mm/dd)</label>
                    <input type="text" id="birth_date" class="form-control datepicker" name="birth_date" value="">
                    <small id="error_birth_date" class="form-text form-error"></small>
                </div>
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="prefecture" class="jp">お住まい（○○県）</label>
                    <select id="prefecture" class="form-control" name="prefecture">
                        <option value="">Select Prefecture</option>
                        '. $prefecture_option .'
                    </select>
                    <small id="error_prefecture" class="form-text form-error"></small>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="cram_school" class="jp">教室名</label>
                    <input type="text" id="cram_school" class="form-control" name="cram_school" value="">
                    <small id="error_cram_school" class="form-text form-error"></small>
                </div>
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="english_learning" class="jp">英語学習暦</label>
                    <select name="english_learning" id="english_learning" class="form-control">
                        <option value="">Select Span</option>
                        <option value="なし">なし</option>
                        <option value="1年未満">1年未満</option>
                        <option value="１～２年">１～２年</option>
                        <option value="２～３年">２～３年</option>
                        <option value="３～４年">３～４年</option>
                        <option value="４～５年">４～５年</option>
                        <option value="５年以上">５年以上</option>
                    </select>
                    <small id="error_english_learning" class="form-text form-error"></small>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="english_schedule" class="jp">入会時の英語学習状況</label>
                    <input type="text" id="english_schedule" class="form-control" name="english_schedule" placeholder="学校○○時間／週、塾○○時間／週、その他○○時間／週">
                    <small id="error_english_schedule" class="form-text form-error"></small>
                </div>
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="account_prof" class="jp">取得済みの語学力検定・資格など</label>
                    <input type="text" id="account_prof" class="form-control" name="account_prof" placeholder="試験名：○○、級／スコア：○○、取得年月日○○">
                    <small id="error_account_prof" class="form-text form-error"></small>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col col-md-12 col-lg-12">
                    <label for="remark" class="jp">特記事項</label>
                    <textarea id="remark" class="form-control" name="remark" cols="8"></textarea>
                    <small id="error_remark" class="form-text form-error"></small>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="email">Email</label>
                    <input type="email" id="email" class="form-control" name="email">
                    <small id="error_email" class="form-text form-error"></small>
                </div>
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="confirm_email">Confirm Email</label>
                    <input type="email" id="confirm_email" class="form-control" name="confirm_email">
                    <small id="error_confirm_email" class="form-text form-error"></small>
                </div>
            </div>

            <div class="line"></div>
            <h3 class="card-title">Login Detail</h3>
            <div class="form-row">
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="username">Username</label>
                    <input type="text" id="username" class="form-control" name="username">
                    <small id="error_username" class="form-text form-error"></small>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="password">Password</label>
                    <input type="password" id="password" class="form-control" name="password">
                    <small id="error_password" class="form-text form-error"></small>
                </div>
                <div class="form-group col col-md-6 col-lg-6">
                    <span class="viewPassword" data-target="password"><i class="mdi mdi-eye"></i></span>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col col-md-6 col-lg-6">
                    <label for="status">Status</label>
                    <input type="hidden" name="status_preview" id="status_preview" value="Active">
                    <select id="status" class="form-control get_preview" data-target="status" name="status">
                        <option value="1" data-value="Active">Active</option>
                        <option value="2" data-value="Inactive">Inactive</option>
                    </select>
                    <small id="error_status" class="form-text form-error"></small>
                </div>
            </div>

            <div class="line"></div>
            <button type="button" id="validate_user" class="btn btn-primary">Submit</button>
            <a href="'. url('user') .'" class="btn btn-warning">Cancel</a>';
        
        $data = [
            'room_role' => $room_role,
            'form_information' => $form_information,
        ];
        return $data;
    } 

    public function get_user_detail(Request $request){

        $user = \App\Users::where('username', $request->input('username'))->first();

        $title      = null;
        $content    = null;

        if($user->role == 4):
            
            $title   = $user->kanji_name;

            $status = $user->status == 1 ? 'Active':'Inactive';

            $content = '
                <div class="user-detail">
                    <div class="u-group">
                        <div class="a-container">
                            <div class="a-image">
                                <img src="'. asset('uploads/avatar/noavatar.jpg') .'">
                            </div>
                        </div>
                    </div>
                    <div class="u-group">
                        <div class="u-label">氏名</div>
                        <div class="u-value">'. $user->kanji_name .'</div>
                    </div>
                    <div class="u-group">
                        <div class="u-label">ローマ字表記　フルネーム</div>
                        <div class="u-value">'. $user->romaji_name .'</div>
                    </div>
                    <div class="u-group">
                        <div class="u-label">誕生日</div>
                        <div class="u-value">'. date('Y/m/d', strtotime($user->birth_date)).'</div>
                    </div>
                    <div class="u-group">
                        <div class="u-label">お住まい（○○県）</div>
                        <div class="u-value">'. $user->prefecture .'</div>
                    </div>
                    <div class="u-group">
                        <div class="u-label">教室名</div>
                        <div class="u-value">'. $user->cram_school .'</div>
                    </div>
                    <div class="u-group">
                        <div class="u-label">英語学習暦</div>
                        <div class="u-value">'. $user->english_learning .'</div>
                    </div>
                    <div class="u-group">
                        <div class="u-label">入会時の英語学習状況</div>
                        <div class="u-value">'. $user->englist_schedule .'</div>
                    </div>
                    <div class="u-group">
                        <div class="u-label">取得済みの語学力検定・資格など</div>
                        <div class="u-value">'. $user->account_prof .'</div>
                    </div>
                    <div class="u-group">
                        <div class="u-label">Email</div>
                        <div class="u-value">'. $user->email .'</div>
                    </div>
                    <div class="u-group last">
                        <div class="u-label">特記事項</div>
                        <div class="u-value">'. nl2br($user->remark) .'</div>
                    </div>
                    <div class="u-line">
                        <span>Login Detail</span>
                    </div>

                    <div class="u-group">
                        <div class="u-label">Username</div>
                        <div class="u-value">'. $user->email .'</div>
                    </div>

                    <div class="u-group">
                        <div class="u-label">Password</div>
                        <div class="u-value">********</div>
                    </div>

                    <div class="u-group last">
                        <div class="u-label">Status</div>
                        <div class="u-value">'. $status .'</div>
                    </div>
                </div>';
        else:
        
            $title   = $user->full_name;
            
            if($user->avatar == null || !File::exists('uploads/avatar/'. $user->avatar)):

                $avatar = '<img src="'. asset('uploads/avatar/noavatar.jpg') .'">';
            
            else:
            
                $avatar = '<img src="'. asset('uploads/avatar/'. $user->avatar) .'">';
            
            endif;

            $status = $user->status == 1 ? 'Active':'Inactive';

            $content = '<div class="user-detail">
                        <div class="u-group">
                            <div class="a-container">
                                <div class="a-image">
                                    '. $avatar .'
                                </div>
                            </div>
                        </div>
                        <div class="u-group">
                            <div class="u-label">Full Name</div>
                            <div class="u-value">'. $user->full_name .'</div>
                        </div>
                        <div class="u-group">
                            <div class="u-label">Gender</div>
                            <div class="u-value">'. $user->gender .'</div>
                        </div>
                        <div class="u-group last">
                            <div class="u-label">Email</div>
                            <div class="u-value">'. $user->email .'</div>
                        </div>

                        <div class="u-line">
                            <span>Login Detail</span>
                        </div>

                        <div class="u-group">
                            <div class="u-label">Username</div>
                            <div class="u-value">'. $user->email .'</div>
                        </div>

                        <div class="u-group">
                            <div class="u-label">Password</div>
                            <div class="u-value">********</div>
                        </div>

                        <div class="u-group last">
                            <div class="u-label">Status</div>
                            <div class="u-value">'. $status .'</div>
                        </div>
                    </div>';
        endif;

        $data = [
            'title'     => $title,
            'content'   => $content
        ];

        return response()->json($data);
    }
}
