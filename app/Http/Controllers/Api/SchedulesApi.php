<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Helpers\TimeparserHelper;

use Carbon\Carbon;

use App;

class SchedulesApi extends Controller
{

    public function get_grade(Request $request){
        $locale = $request->input('locale') ?? 'jp';
    	App::setlocale($locale );
        $id = $request->input('id');

    	$grades = \App\CourseGradeLevel::join('grade_levels', 'grade_levels.id', '=', 'course_grade_levels.grade_id')->where('course_id', $id)->get();

    	$grade_data = '<option value="">'.__('label.select_item') .'</option>';

    	$schedule = '<option value="">'.__('label.select_item') .'</option>';

    	if($grades):
    		foreach($grades as $grade):
    			$grade_data .= '<option value="'. $grade->grade_id .'">'. $grade->label .'</option>';
    		endforeach;
    	endif;

    	$set2 = [2,4,5,6,8,9,11,12];
    	$set1 = [1,3,7,10];

    	if(in_array($id, $set1)):
    	
    		$schedule .= '<option value="16">'.__('label.once_week').'</option>';
    		$schedule .= '<option value="8">'.__('label.alternate_week').'</option>';

    	endif;

    	if(in_array($id, $set2)):
    		$schedule .= '<option value="1">'.__('label.one_time_only').'</option>';
    	endif;

    	return response()->json([
    		'grade'		=> $grade_data,
    		'schedule' 	=> $schedule
    	]);

	}


    /*
    |------------------------------------------------------------------------------
    | New line of codes start here.
    |------------------------------------------------------------------------------
    */

    public function set_date(Request $request){
        
        $date       = $request->input('date');

        $frequency  = $request->input('frequency');

        $start_day  = $request->input('start_day');
        
        $numWeek    = $frequency == 8 ? 2 : 1;

        $date       = Carbon::parse($date)->subWeek($numWeek);

        $time       = null;

        $rows = [];

        if($frequency == 1):

            for($i = 1; $i <= 1; $i++):

                $date =  Carbon::parse($date)->addWeek($numWeek);

                $query = \App\TeacherSchedule::select('date', 'time_in', 'time_out', 'user_id')->whereDate('date', date('Y-m-d', strtotime($date)))->get();

                $time = $this->set_date_time($query, $date);

                $setDate = $query != null ? date('Y/m/d', strtotime($date)) : null;

                $setTime = $query != null ? $time : null;

                $rows[$start_day] = [
                    
                    'date'      => $setDate,
                    
                    'time'      => $time

                ];

            endfor;

        else:

            for($i = $start_day; $i <= $frequency; $i++):

                $date =  Carbon::parse($date)->addWeek($numWeek);

                $query = \App\TeacherSchedule::select('date', 'time_in', 'time_out', 'user_id')->whereDate('date', date('Y-m-d', strtotime($date)))->get();

                $time = $this->set_date_time($query, $date);

                $setDate = $query != null ? date('Y/m/d', strtotime($date)) : null;

                $setTime = $query != null ? $time : null;

                $rows[$i] = [
                    
                    'date'      => $setDate,
                    
                    'time'      => $time

                ];

            endfor;

        endif;
        

        return response()->json($rows);

    }


    public function get_teacher(Request $request){

        $now = date('Y-m-d H:i:s', strtotime(Carbon::now()));

        $dates      = $request->input('date');

        $time       = date('H:i:s', strtotime($request->input('time')));

        $data_id    = $request->input('data_id');

        $frequency  = $request->input('frequency');

        $rows       = [];
        
        if($dates):
            
            $count = $data_id - 1;

            foreach($dates as $date):

                $count++;

                $date       = date('Y-m-d', strtotime($date));

                $query      = \App\TeacherSchedule::select('user_id', 'date', 'time_in', 'time_out')->with('user')->whereDate('date', $date)->get();

                $teacher    = $this->get_teacher_option($query, $date, $time);

                $rows[$count] = [
                    'teacher'   => $teacher
                ];

            endforeach;

        endif;

        return response()->json($rows);
        
    }

    
    public function get_dates(Request $request){
        
        $grade = $request->input('grade');

        $levels = \App\Level::with(['teacher' => function($query){

            $query->where('role', 5);

        }])->where('grade_id', $grade)->get();

        if($levels):

            $user_id = [];

            foreach($levels as $level):

                if($level->teacher):

                    foreach($level->teacher as $teacher):

                        $user_id[] = $teacher->id;

                    endforeach;

                endif;

            endforeach;


            $schedules = \App\TeacherSchedule::distinct('date')->whereIn('user_id', $user_id)->whereDate('date', '>=', Carbon::now())->get();

            $dateArray = [];

            if($schedules):

                foreach($schedules as $schedule):
                
                    $dateArray[] = date('Y-m-d', strtotime($schedule->date));

                endforeach;

            endif;

            return response()->json($dateArray);            

        endif;

        /*
        $teacher = $request->input('teacher');

        $schedules = \App\TeacherSchedule::where('user_id', $teacher)->whereDate('date', '>=', Carbon::now())->get();

        $dateArray = [];

        if($schedules):

            foreach($schedules as $schedule):
            
                $dateArray[] = date('Y-m-d', strtotime($schedule->date));

            endforeach;

        endif;

        return response()->json($dateArray);
        */
    }



    /*
    |----------------------------------------------------------------
    | Private Functions
    |----------------------------------------------------------------
    */

    private function get_teacher_option($query, $date, $time){

        $option     = '<option value="">'.__('label.select_item').'</option>';


        if($query):

            foreach($query as $res):

                if($res->user):

                    $checkRecord = \App\StudentSchedule::where('teacher_id', $res->user->id)->whereDate('date', $date)->where('start_time', $time)->count();

                    if($checkRecord <= 0):

                        $option .= '<option value="'. $res->user->id .'">'. $res->user->full_name .'</option>';

                    endif;

                endif;

            endforeach;

        endif;

        return $option;

    }


    private function set_date_time($query, $date){

        $now    = date('Y-m-d H:i:s', strtotime(Carbon::now()));

        $option = '<option value="">'.__('label.select_item') .'</option>';

        if($query):

            $time_in_array  = [];

            $time_out_array = [];

            foreach($query as $res):

                $time_in_array[]  = strtotime($res->time_in);

                $time_out_array[] = strtotime($res->time_out);

            endforeach; 

            $time_in  = date('H:i:s', min($time_in_array));

            $time_out = date('H:i:s', max($time_out_array));

            /*
            * Parse time into 15min
            */
            $parse_time = TimeparserHelper::parseTime($time_in, $time_out);

            if($parse_time):

                foreach($parse_time as $time):
                    
                    $date_time = date('Y-m-d', strtotime($date)) .' '. $time;
                    
                    if(strtotime($now) <= strtotime($date_time)):

                        $option .= '<option value="'. date('h:i A', strtotime($time)) .'">'. date('h:i A', strtotime($time)) .'</option>';

                    endif;

                endforeach;

            endif;

        endif;
        
        return $option;
        /*
        $now = date('Y-m-d H:i:s', strtotime(Carbon::now()));

        $date       = date('Y-m-d', strtotime($date));

        $query = \App\TeacherSchedule::whereDate('date', $date)->where('user_id', $teacher_id)->first();

        $option = '';

        if($query):

            $time_in  = $query->time_in;

            $time_out = $query->time_out;

            $results = TimeparserHelper::parseTime($time_in, $time_out);

            
            // * Check all time array in the student_schedules table 
            // * if existing based on time and teacher id
            
            $option = '';

            //Student Options
            if($results):
            
                $option .= '<option value="">--Select Item--</option>';

                foreach($results as $result):

                    $checkRecord = \App\StudentSchedule::where('teacher_id', $teacher_id)->whereDate('date', $date)->where('start_time', $result)->first();
                    
                    if(!$checkRecord):
                        
                        if(strtotime($now) <= strtotime($date .' '. $result)):

                        $option .= '<option value="'. date('h:i A', strtotime($result)) .'">'. date('h:i A', strtotime($result)) .'</option>';

                        endif;

                    endif;                

                endforeach;
            endif;
        endif;

        return $option;
        */
    }

}
