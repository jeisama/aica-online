<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Session;

class GlobalApi extends Controller
{
	
	public function setLocale(Request $request){
		
		$request->session()->put('locale', $request->input('locale'));

	}

}