<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Carbon\Carbon;

use Illuminate\Support\Facades\Auth;

class TimerApi extends Controller
{
    
	public function get_time(Request $request){
		
		$now = Carbon::now();
		
		$role = $request->input('role');

		$input = $request->input('input');
		
		$startJoin = $role == 5 ? 'Start Lesson':'Join Lesson';

		$data       = [];

		$dateDiff = [];

		if($input):

			$results = \App\StudentSchedule::with('lesson')->whereIn('id', $input)->get();
			
			if($results):

				foreach($results as $result):
					
					$date = $result->date;

					$start_time = $date .' '. $result->start_time;
					

					
					
					if(strtotime($now) < strtotime($start_time)):
					
						$dateDiff = '<button class="btn btn-primary btn-sm btn-block">'. $this->dateDiff($now, $start_time). '</button>';

					else: //900 = 15 MIN

						if($now->diffInSeconds($start_time) >= 1 && $now->diffInSeconds($start_time) <= 900):
							
							$dateDiff = '<a href="'. url('lessons/conference/'. $result->lesson->token) .'" target="_blank" class="btn btn-success btn-sm btn-block">'. $startJoin .'</a>';

						else:
						
							$dateDiff = '<button class="btn btn-default btn-sm btn-block">Lesson Ended.</button>';
						
						endif;

					endif;
					

					$data[] = array(
						'id' => $result->id,
						'text' => $dateDiff
					);

					//$dateDiff[] = $dateDiff;

				endforeach;
			endif;

		endif;
		
		return response()->json(['data' => $data]);

	}

	private function dateDiff($now, $start_time){

		$diff = '';

		$text = '';

		if($now->diffInSeconds($start_time) <= 59):
			
			$label = $now->diffInSeconds($start_time) <= 1 ? ' second.':' seconds.';

			$diff = $now->diffInSeconds($start_time);

		elseif($now->diffInMinutes($start_time) <= 59):

			$label = $now->diffInMinutes($start_time) <= 1 ? ' minute.':' minutes.';

			$diff = $now->diffInMinutes($start_time);

		elseif($now->diffInHours($start_time) <= 24):

			$label = $now->diffInHours($start_time) <= 1 ? ' hour.':' hours.';
			
			$diff = $now->diffInHours($start_time);

		else:

			$label = $now->diffInDays($start_time) <= 1 ? ' day.':' days.';
			
			$diff = $now->diffInDays($start_time);

		endif;

		$text = 'Lesson start in '. $diff . $label;

		return $text;

	}

}
