<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;

use File;

class TemplateMakerApi extends Controller
{
    public function get_form(Request $request){

        $course_id    = $request->input('course_id');

        $grade_id = $request->input('grade_id');

        $data = null;

        //speaking 1 
        if($course_id == '1' || $course_id == '2' || $course_id == '5'){
            if($grade_id == '1' || $grade_id == '2' || $grade_id == '3' ){
                $data = $this->speaking_form_1();
            }else{
                $data = $this->speaking_form_2();
            }
        //writing
        }elseif($course_id == '3' || $course_id == '4' || $course_id == '6' || $course_id == '7' || $course_id == '8' || $course_id == '9' || $course_id == '10' || $course_id == '11' || $course_id == '12'){

            $data = $this->writing_form_3();
        }
        
        
        return response()->json($data);

    }

    public function speaking_form_1(){

        $form_information = '
        <input type="hidden" name="form_type" value="1">
        <div class="homework-box" style="border: 1px solid #c5c5c5; padding: 15px; width: 100%;">
                                <div class="tbl" style="display: table; width: 100%;">
                                    <div style="display: table-cell; vertical-align: top; width:30px;"></div>
                                        <div style="display: table-cell; vertical-align: top;">
                                            <div class="form-row">
                                                <div class="form-group col col-md-12 col-lg-12">
                                                    <label for="" class="form-control-label"><strong>Speaking part 用課題</strong></label>
                                                    <p>次回の授業内で以下のトピックについて先生から質問があります。自分の意見と、何故そのように考えたのかという理由を下の枠内にまとめておきましょう。<br>
                                                    <small>（わからない単語や表現は辞書などで事前に調べましょう！そうすることで、皆さんの表現の幅が広がります。）</small></p>
                                                    
                                                    
                                                    <textarea rows="4" cols="50" id="speaking_topic" name="speaking_topic" class="form-control write-input" placeholder="Speaking Topicを入力してください。" style="border:1px solid #000;"></textarea>
                                                    <small id="error-speaking_topic" class="form-text form-error"></small>
                                                    
                                                </div>
                        
                                                
                                            </div>
                                    </div>
                                </div>
                                <div class="tbl" style="display: table; width: 100%;">
                                    <div style="display: table-cell; vertical-align: top; width:30px;"></div>
                                        <div style="display: table-cell; vertical-align: top;">
                                            <div class="form-row">
                                                <div class="form-group col col-md-12 col-lg-12">
                                                    
                                                    <textarea rows="4" cols="50" id="stu_opinion" name="stu_opinion" class="form-control" placeholder="こちらに回答してください。" disabled></textarea>
                                                    <small id="error-writing_student_2_ans" class="form-text form-error"></small>
                                                    
                                                </div>
                                            </div>
                                    </div>
                                </div>
                                <div class="tbl" style="display: table; width: 100%;">
                                    <div style="display: table-cell; vertical-align: top; width:30px;"></div>
                                        <div style="display: table-cell; vertical-align: top;">
                                            <div class="form-row">
                                                <div class="form-group col col-md-12 col-lg-12">
                                                    <label for="listening" class="form-control-label"><strong>Listening part用課題</strong></label>
                                                    <p>Step 1. 音声を再生して、流れる会話文の内容を聞き取りましょう。繰り返し聞いても内容を理解<br>
                                                    できないときは、下の“展開”ボタンを押して、スクリプトを確認しながらリスニングを行いましょう。</p>
                                                    <div>
                                                    <div class="form-group col-lg-12">
                                                    <div class="form-row">																
                                                    <div id="drop-zone">
                                                            <br>
                                                            <label id="clickHere">音声添付ファイル<i class="fa fa-upload"></i>
                                                                <input type="file" class="select_file" id="media" multiple accept="audio/*"/>
                                                            </label>
                                                            <div id="filelist_media"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                    </div>
                                                    
                                                </div>
            
                                            </div>
                                        </div>
                                </div>
                                <div class="tbl" style="display: table; width: 100%;">
                                    <div style="display: table-cell; vertical-align: top; width:30px;"></div>
                                        <div style="display: table-cell; vertical-align: top;">
                                            <div class="form-row">
                                                <div class="form-group col col-md-12 col-lg-12">
                                                    <p>Step 2. スクリプトを見ながら、音声の後に続いて音読しましょう。その際、英文の内容や意味を<br>
                                                    意識しながら音読するようにしましょう。<br>
                                                    わからない単語や表現がある場合は、辞書などを用いて調べましょう。
                                                    </p>
                                                    
                                                    
                                                    <label for="next_listening" class="form-control-label"><strong>今週のListening 課題：</strong></label>
                                                    <textarea rows="4" cols="50" id="next_listening" name="next_listening" class="form-control" placeholder="Listening 課題を入力してください。" style="border:1px solid #000;"></textarea>
                                                    <small id="error-next_listening" class="form-text form-error"></small>
                                        
                                                </div>
                                         
                                            </div>

                                            <div class="form-row">
                                            <div id="drop-zone">
                                                    <br>
                                                    <label id="clickHere">レッスン添付ファイル<i class="fa fa-upload"></i>
                                                      <input type="file" class="select_file" id="attachment" multiple accept="image/*, application/pdf"/>
                                                    </label>
                                                    <div id="filelist_attachment"></div>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                                
                            </div>';
        $data = [
            'form_information' => $form_information
        ];
        return $data;
    }

    public function speaking_form_2(){

        $form_information = '
        <input type="hidden" name="form_type" value="2">
        <div class="homework-box" style="border: 1px solid #c5c5c5; padding: 15px; width: 100%;">
                                <div class="tbl" style="display: table; width: 100%;">
                                    <div style="display: table-cell; vertical-align: top; width:30px;"></div>
                                        <div style="display: table-cell; vertical-align: top;">
                                            <div class="form-row">
                                                <div class="form-group col col-md-12 col-lg-12">
                                                    <label for="" class="form-control-label"><strong>Speaking part用課題</strong></label>
                                                    <p>この1週間の出来事について書きましょう。何をやったのかという文章と、更に内容を詳しくする<br>
                                                    文章を1文ずつ書きましょう。作った文章を次回の授業の中で発表しましょう。</p>
                                                    <textarea rows="4" cols="50" id="stu_opinion" name="stu_opinion" class="form-control" placeholder="こちらに回答してください。" disabled></textarea>
                                                    <small id="error-stu_opinion" class="form-text form-error"></small>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                                <div class="tbl" style="display: table; width: 100%;">
                                    <div style="display: table-cell; vertical-align: top; width:30px;"></div>
                                        <div style="display: table-cell; vertical-align: top;">
                                            <div class="form-row">
                                                <div class="form-group col col-md-12 col-lg-12">
                                                    <label for="listening" class="form-control-label"><strong>Listening part用課題</strong></label>
                                                    <p>Step 1. 音声を再生して、流れる会話文の内容を聞き取りましょう。繰り返し聞いても内容を理解<br>
                                                    できないときは、下の“展開”ボタンを押して、スクリプトを確認しながらリスニングを行いましょう。
                                                    </p>
                                                    <div>
                                                    <div class="form-group col-lg-12">
                                                    <div class="form-row">																
                                                        <div id="drop-zone">
                                                            <br>
                                                            <label id="clickHere">音声添付ファイル<i class="fa fa-upload"></i>
                                                                <input type="file" class="select_file" id="media" multiple accept="audio/*"/>
                                                            </label>
                                                            <div id="filelist_media"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                    </div>
                                                    
                                                </div>
            
                                            </div>
                                        </div>
                                </div>
                                <div class="tbl" style="display: table; width: 100%;">
                                    <div style="display: table-cell; vertical-align: top; width:30px;"><strong></strong></div>
                                        <div style="display: table-cell; vertical-align: top;">
                                            <div class="form-row">
                                                <div class="form-group col col-md-12 col-lg-12">
                                                    <p>Step 2. スクリプトを見ながら、音声の後に続いて音読しましょう。その際、英文の内容や意味を<br>
                                                    意識しながら音読するようにしましょう。<br>
                                                    わからない単語や表現がある場合は、辞書などを用いて調べましょう。
                                                    </p>
                                                    
                                                    
                                                    <label for="next_listening" class="form-control-label"><strong>今週のListening 課題：</strong></label>
                                                    <textarea rows="4" cols="50" id="next_listening" name="next_listening" class="form-control" placeholder="Listening 課題を入力してください。" style="border:1px solid #000;"></textarea>
                                                    <small id="error-next_listening" class="form-text form-error"></small>
                                        
                                                </div>
                                         
                                            </div>
                                            <div class="form-row">
                                                <div id="drop-zone">
                                                    <br>
                                                    <label id="clickHere">レッスン添付ファイル<i class="fa fa-upload"></i>
                                                    <input type="file" class="select_file" id="attachment" multiple accept="image/*, application/pdf"/>
                                                    </label>
                                                    <div id="filelist_attachment"></div>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                                
                            </div>
        ';
        $data = [
            'form_information' => $form_information
        ];
        return $data;
    }

    public function writing_form_3(){

        $form_information = '
        <input type="hidden" name="form_type" value="3">
        <div class="homework-box" style="border: 1px solid #c5c5c5; padding: 15px; width: 100%;">
                                <div class="tbl" style="display: table; width: 100%;">
                                    <div style="display: table-cell; vertical-align: top; width:30px;"><strong></strong></div>
                                        <div style="display: table-cell; vertical-align: top;">
                                            <div class="form-row">
                                                <div class="form-group col col-md-12 col-lg-12">
                                                    <label for="" class="form-control-label write">今回の課題（以下の課題に対して「記入欄」に英作文を記入しましょう）</label>
                                                    <textarea rows="4" cols="50" id="writing_topic" name="writing_topic" class="form-control write-input" placeholder="トピックを入力してください。" style="border:1px solid #000;"></textarea>
                                                    <small id="error-writing_topic" class="form-text form-error"></small><br>
                        
                                                    <textarea rows="6" cols="50" id="chat_history" name="chat_history" class="form-control" placeholder="チャット履歴" disabled></textarea>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                           
                                <div class="tbl" style="display: table; width: 100%;">
                                    <div style="display: table-cell; vertical-align: top; width:30px;"><strong></strong></div>
                                        <div style="display: table-cell; vertical-align: top;">
                                            <div class="form-row">
                                                <div class="form-group col col-md-12 col-lg-12">
                                                    <label for="writing_stu_ans" class="form-control-label write">記入欄</label>
                                                    <textarea rows="6" cols="50" id="writing_stu_ans" name="writing_stu_ans" class="form-control" placeholder="こちらに回答してください。" disabled></textarea>
                                        
                                                </div>
                                         
                                            </div>
                                    </div>
                                </div>
                                <div class="tbl" style="display: table; width: 100%;">
                                    <div style="display: table-cell; vertical-align: top; width:30px;"><strong></strong></div>
                                        <div style="display: table-cell; vertical-align: top;">
                                            <div class="form-row">
                                                <div class="form-group col col-md-12 col-lg-12">
                                                    <label for="writing_tea_res" class="form-control-label write">添削結果</label>
                                                    <textarea rows="6" cols="50" id="writing_tea_res" name="writing_tea_res" class="form-control"  disabled></textarea>
                                        
                                                </div>
                                        
                                            </div>
                                            <div class="form-row">																
                                                <div id="drop-zone">
                                                    <br>
                                                    <label id="clickHere">音声添付ファイル<i class="fa fa-upload"></i>
                                                        <input type="file" class="select_file" id="media" multiple accept="audio/*"/>
                                                    </label>
                                                    <div id="filelist_media"></div>
                                                </div>
                                            </div><br>
                                            <div class="form-row">
                                                <div id="drop-zone">
                                                    <br>
                                                    <label id="clickHere">レッスン添付ファイル<i class="fa fa-upload"></i>
                                                    <input type="file" class="select_file" id="attachment" multiple accept="image/*, application/pdf"/>
                                                    </label>
                                                    <div id="filelist_attachment"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                  
                                ';

        $data = [
            'form_information' => $form_information
        ];

        return $data;
    }
}
