<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App;

class LevelApi extends Controller
{
    
	public function get(Request $request){
		$locale = $request->input('locale') ?? 'jp';
		App::setlocale($locale );
		$level = $request->input('level');

		$grades = \App\GradeLevel::with(['level' => function($query) use($level){
			
			$query->where('teacher_level', $level);

		}])->get();
		
		$data = null;
		//dd($grades);
		if($grades):
		
			foreach($grades as $grade):
				
				$check = '';

				if($grade->level):
					$check = 'checked';
				endif;

				$data .='
				<div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="grade[]" value="'. $grade->id .'" id="check'. $grade->id .'" '. $check .'>
                    <label class="custom-control-label" for="check'. $grade->id .'" style="padding-top: 2px;">'. $grade->label .'</label>
                </div>
				';

			endforeach;
			
			$data .= '
			<div class="modal-footer">
            	<input type="hidden" name="level" value="'. $level .'">
                <button type="button" class="btn btn-warning" data-dismiss="modal">'.__('label.back').'</button>
                <button type="submit" class="btn btn-primary" id="submit-form" data-form="teacher-level">'.__('label.save').'</button>
            </div>
			';

		endif;

		return response()->json([
			'data' => $data
		]);
	}

}
