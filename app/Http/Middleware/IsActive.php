<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;

class IsActive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        if ($user->status != '1') {
            Auth::logout();
            //Session::flush();
            return redirect('/login')->with('message','Error:: Your account is registered but not active. Please contact your administrator.');//Not allowed
        }
        return $next($request);
    }
}
