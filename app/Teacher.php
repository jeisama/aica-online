<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    use \Awobaz\Compoships\Compoships;
    
    protected $fillable = [
        'user_id',
        'full_name',
        'gender',
        'file'
    ];
}
