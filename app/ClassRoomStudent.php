<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassRoomStudent extends Model
{
	use \Awobaz\Compoships\Compoships;
	
	protected $table = 'class_rooms_student';
	
    protected $fillable = [
    	'room_id',
    	'user_id',
	];
	

}
