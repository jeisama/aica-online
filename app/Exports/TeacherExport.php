<?php

namespace App\Exports;

use App\TeacherReport;

use Illuminate\Contracts\View\View;

use Maatwebsite\Excel\Concerns\FromView;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;


class TeacherExport implements FromView,ShouldAutoSize
{
	private $reports;

	private $q;

    public function __construct($reports, $q)
    {
        $this->reports = $reports;

        $this->q = $q;
    }

    public function view(): View
    {
    	//dd($this->reports);
        return view('reports.export.teacher',[
        	'reports'	=> $this->reports,
        	'q'			=> $this->q
        ]);
    }
}
