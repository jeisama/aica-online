<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;

use Maatwebsite\Excel\Concerns\FromView;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;


class StudentExport implements FromView, ShouldAutoSize
{
	private $reports;

	private $q;

    private $roomName;

    public function __construct($reports, $q, $roomName)
    {
        $this->reports = $reports;

        $this->q = $q;

        $this->roomName = $roomName;
    }

    public function view(): View
    {
    	//dd($this->reports);
        return view('reports.export.student',[
        	'reports'	=> $this->reports,
        	'q'			=> $this->q,
            'roomName'  => $this->roomName
        ]);
    }
}
