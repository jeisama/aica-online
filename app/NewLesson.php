<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewLesson extends Model
{
    use \Awobaz\Compoships\Compoships;
    
    protected $fillable = [
        'schedule_id',
    	'student_id',
        'teacher_id',
        'course_type',
        'lesson_type',
        'lesson_day',
        'lesson_name',
        'speaking_topic',
        'next_listening',
        'created_by',
        'writing_topic',
        'chat_flag',
        'token',
        'form_type'
    ];

    public function schedule(){
        return $this->hasOne('App\StudentSchedule','id','schedule_id');
    }

    public function attachment(){
        return $this->hasMany('App\LessonAttachment','lesson_id','id');
    }

    public function media(){
        return $this->hasMany('App\LessonMedia','lesson_id','id');
    }

    public function attachments(){
        return $this->hasMany('App\Attachment','lesson_id','id');
    }
    public function teacher(){
        return $this->hasOne('App\Users','id','teacher_id');
    }
    public function student(){
        return $this->hasOne('App\Users','id','student_id')->with('class_name');
    }
    public function grade(){
        return $this->hasOne('App\GradeLevel','id','lesson_type');
    }
    public function courses(){
        return $this->hasOne('App\Course','id','course_type');
    }

    public function comments(){
        return $this->hasOne('App\StudentReport','lesson_id','id');
    }

}
