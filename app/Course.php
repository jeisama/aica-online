<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use \Awobaz\Compoships\Compoships;
    
    protected $fillable = [
    	'label',
    	'status',
    	'created_by',
    	'updated_by',
    ];

    public function grade(){
    	return $this->hasOne('App\CourseGradeLevel','course_id','id');
    }

    public function count(){
    	return $this->hasMany('\App\CourseGradeLevel', 'course_id', 'id');
    }
}
