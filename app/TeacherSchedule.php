<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeacherSchedule extends Model
{
    use \Awobaz\Compoships\Compoships;
    
    protected $fillable = [
        'user_id',
        'date',
        'time_in',
        'time_out',
        'date_range',
        'created_by',
        'updated_by',
        'active',
        'group'
    ];

    public function teacher(){
        return $this->hasOne('App\Teacher','user_id','user_id');
    }

    public function lesson(){
        return $this->hasOne('App\Lesson','schedule_id','id');
    }

    public function student(){
        return $this->hasOne('App\ScheduleStudent','schedule_id','id');
    }

    public function students(){
        return $this->hasMany('App\ScheduleStudent','schedule_id','id')->with('info');
    }

    public function user(){
        return $this->hasOne('App\Users','id','user_id')->with('level');
    }
}
