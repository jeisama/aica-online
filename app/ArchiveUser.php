<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArchiveUser extends Model
{
    use \Awobaz\Compoships\Compoships;
    
    protected $fillable = [
    	'id',
        'full_name',
        'gender',
        'avatar',
        'kanji_name',
        'romaji_name',
        'birth_date',
        'prefecture',
        'cram_school',
        'english_learning',
        'englist_schedule',
        'account_prof',
        'remark',
        'email',            
        'role', 
        'status',   
        'username',        
        'confirm_email', 
        'email_verified_at',       
        'password',        
        'teacher_level',
        'remember_token',
        'created_by',
        'updated_by',
    ];
}
