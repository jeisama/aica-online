<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LessonThread extends Model
{
    use \Awobaz\Compoships\Compoships;
    
    protected $fillable = [
    	'lesson_id',
    	'from',
    	'to',
		'stu_opinion',
		'writing_stu_ans',
        'writing_tea_res',
    ];


    public function user(){
        return $this->hasOne('App\Users','id','from');
    }
}
