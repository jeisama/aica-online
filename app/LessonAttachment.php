<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LessonAttachment extends Model
{
	use \Awobaz\Compoships\Compoships;
	
    protected $fillable = [
        'lesson_id',
        'attachment',
        'original_name'
    ];

}
