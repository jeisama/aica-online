<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeacherLevel extends Model
{
    

	use \Awobaz\Compoships\Compoships;

	protected $fillable = [
		'level',
		'status',
	];

}
