<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseGradeLevel extends Model
{
	
	use \Awobaz\Compoships\Compoships;
	
	protected $primaryKey = null;

	public $incrementing = false;

    public $timestamps = false;

    public $table = 'course_grade_levels';

	protected $fillable = [
		'course_id',
		'grade_id'
	];


	public function grade_level(){
		return $this->hasOne('App\GradeLevel','id','grade_id');
	}
}
