<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\Sluggable;

class ClassRoom extends Model
{

    use Sluggable;

    use \Awobaz\Compoships\Compoships;

    protected $fillable = [
        'class_name',
        'class_fullname',
        'class_orgname',
        'class_phone',
        'class_mobile', 
        'class_email',
        'class_confirm_email',
        'slug',
             
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'class_orgname'
            ]
        ];
    }

}
