<header class="header">
    <nav class="navbar">
   
    <div class="container-fluid">
        <div class="navbar-holder d-flex align-items-center justify-content-between">
        <!-- Navbar Header-->
            <div class="navbar-header">
                <!-- Navbar Brand --><a href="{{url('/')}}" class="navbar-brand d-none d-sm-inline-block">
                <div class="brand-text d-none d-lg-inline-block">
                        <img src="{{asset('assets/img/aic_logo.png')}} " width="150px" heigth="150px" class="pr-2">
                   
                </div>
                <div class="brand-text d-none d-sm-inline-block d-lg-none"><strong>AIC</strong></div></a>
                <!-- Toggle Button--><a id="toggle-btn" href="#" class="menu-btn active"><span></span><span></span><span></span></a>
            </div>
            
            <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                
                @if(App::getLocale() == 'en')
                <li class="nav-item"><a href="javascript:void(0)" id="setLocale" data-locale="jp"><img src="{{ asset('flags/jp.png') }}"> JP</a></li>
                @else
                <li class="nav-item"><a href="javascript:void(0)" id="setLocale" data-locale="en"><img src="{{ asset('flags/us.png') }}"> EN</a></li>
                @endif
                
                <li class="nav-item"><a href="{{ url('/logout') }}" class="nav-link logout"> <span class="d-none d-sm-inline">{{ __('label.logout') }}</span><i class="mdi mdi-logout"></i></a></li>
            </ul>
        </div>
    </div>
    </nav>
</header>
