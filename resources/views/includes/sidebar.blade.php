<nav class="side-navbar">
    <!-- Sidebar Header-->
    <div class="sidebar-header d-flex align-items-center">
        <div class="avatar">
            @if($user_data->avatar == null || !File::exists('uploads/avatar/'. $user_data->avatar))
                <img src="{{ asset('uploads/avatar/noavatar.jpg') }}" class="img-fluid rounded-circle">
            @else
                <img src="{{ asset('uploads/avatar/'. $user_data->avatar) }}" class="img-fluid rounded-circle">
            @endif
        </div>
        <div class="title">
            <h1 class="h4">{{ $user_data->username }}</h1>
        </div>
    </div>
   
    <!-- Sidebar Navidation Menus-->
    <span class="heading"> {{ __('label.main_menu') }}</span>
    <ul class="list-unstyled">
        
        <li class=""><a href="{{url('/')}}"> <i class="mdi mdi-home"></i>{{ __('label.home') }} </a></li>
        
        @if($user_data->role == 1 || $user_data->role == 2 || $user_data->role == 3)
        <li>
            <a href="{{ url('user') }}"> <i class="mdi mdi-account-supervisor-circle mdi-18px"></i>{{ __('label.users') }}</a>
        </li>
        @endif
        
        <li><a href="{{ url('schedules') }}"><i class="mdi mdi-calendar-check"></i>{{ __('label.schedules') }}</a></li>

        <li><a href="{{ url('lessons') }}"><i class="mdi mdi-file-document-outline"></i>{{ __('label.lessons') }}</a></li>
        
        @if($user_data->role == 1)
        <li><a href="{{ url('classroom') }}"><i class="mdi mdi-library"></i>{{ __('label.classroom') }}</a></li>
        @endif
        
        @if($user_data->role == 1 || $user_data->role == 2 || $user_data->role == 3)
        <li>
            <a href="#reports" aria-expanded="false" data-toggle="collapse"><i class="mdi mdi-chart-bar"></i>{{__('label.report')}}</a>
            <ul id="reports" class="collapse list-unstyled ">
                
                @if($user_data->role != 3)
                <li class=""><a href="{{ url('reports/student') }}">{{__('label.student')}}</a></li>
                @endif
                
                @if($user_data->role != 2)
                <li class=""><a href="{{ url('reports/teacher') }}">{{__('label.teacher')}}</a></li>
                @endif
            
            </ul>
        </li>
        @endif
        
        @if($user_data->role == 1)

        <li><a href="#settings" aria-expanded="false" data-toggle="collapse"><i class="mdi mdi-settings"></i>{{ __('label.settings') }}</a>
            <ul id="settings" class="collapse list-unstyled ">
                <li class=""><a href="{{ url('courses') }}">{{ __('label.course') }}</a></li>
                <li class=""><a href="{{ url('grades') }}">{{ __('label.grade_level') }}</a></li>
                <li class=""><a href="{{ url('level') }}">{{__('label.teacher_level')}}</a></li>
                <li><a href="{{ url('template') }}">{{ __('label.template') }}</a></li>  
            </ul>
        </li>
        @endif
        
        

    </ul>
</nav>