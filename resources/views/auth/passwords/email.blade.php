<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ $title ?? config('app.name', 'Laravel') }}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="{{ asset('assets/vendor/font-awesome/css/font-awesome.min.css') }}">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="{{ asset('assets/css/fontastic.css')}}">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{ asset('assets/css/style.blue.css') }}" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{ asset('assets/img/favicon.png') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  <style>
        .login-page .form-holder .info {
          background: #FFF;
          color: #000;
      }
    </style>
  </head>
  <body>
    <div class="page login-page">
      <div class="container d-flex align-items-center">
        <div class="form-holder has-shadow">
          <div class="row">
            <!-- Logo & Information Panel-->
            <!-- Logo & Information Panel-->
            <div class="col-lg-6">
              <div class="info d-flex align-items-center justify-content-center">
                <div class="content text-center">
                  <div class="logo">
                    <img src="{{asset('assets/img/aic-logo-33.png')}}" class="w-75 p-3">
                  </div>
                  <h5><span style="font-size: 25px; margin-right: -34px;">Pathway to the World</span></h5>
                </div>
              </div>
            </div>
            <!-- Form Panel    -->
            <div class="col-lg-6 bg-white">
              <div class="form d-flex align-items-center">
                <div class="content">
                     @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form method="POST" class="form-validate" action="{{ route('password.email') }}">
                            @csrf
                            <div class="form-group">
                              <input id="email" type="email" name="email" required  class="input-material {{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}">
                             
                              @if ($errors->first('email'))
                              <div id="email-error-" class="is-invalid invalid-feedback" style="display: block;">{{ $errors->first('email') }}</div>
                               
                             @endif
                              <label for="email" class="label-material">Email</label>
                            </div>
                            @if ($errors->first('email'))
                          <button id="error_login" class="btn btn-primary">Go Back To Login</button>
                           @else 
                           <button id="login" class="btn btn-primary">Send Password Reset Link</button>
                           @endif
                           
                            <!-- This should be submit button but I replaced it with <a> for demo purposes-->
                        </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="copyrights text-center">
        <p>Design by <a href="https://bootstrapious.com/admin-templates" class="external">Bootstrapious</a>
        </p>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="{{asset('assets/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/popper.js/umd/popper.min.js')}}"> </script>
    <script src="{{asset('assets/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/jquery-validation/jquery.validate.js')}}"></script>
    <!-- Main File-->
    <script src="{{asset('assets/js/front.js')}}"></script>    

<script type="text/javascript">
  $(document).ready(function(){
      $('#error_login').click(function (event){
          var x = '{{url('/login')}}';
          event.preventDefault();
          window.location.href = x;
      });
  });
      
</script>
  </body>

</html>