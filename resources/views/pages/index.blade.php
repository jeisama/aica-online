@extends('layouts.app')

@section('content')
    <!-- Page Header-->
    <header class="page-header">
        <div class="container-fluid">
            <h2 class="no-margin-bottom">{{ __('label.dashboard') }}</h2>
        </div>
    </header>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">

        <div class="container-fluid">
        
            @if($user_data->role == 1)

                @include('pages.pages.admin')

            @elseif($user_data->role == 2)

                @include('pages.pages.franchise')

            @elseif($user_data->role == 3)

                @include('pages.pages.aica')

            @elseif($user_data->role == 4)

                @include('pages.pages.student')

            @elseif($user_data->role == 5)

                @include('pages.pages.teacher')

            @endif

        </div>

    </section>
@endsection

@section('js')
    <script src="{{ asset('assets/js/timer.js') }}"></script>
@endsection