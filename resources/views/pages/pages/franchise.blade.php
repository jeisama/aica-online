<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h3 class="h4">{{ __('label.next_week_lesson') }}</h3>
            </div>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                                <th width="200" class="text-center">{{ __('label.course') }}</th>
                                <th width="200" class="text-center">{{ __('label.grade') }}</th>
                                <th width="100" class="text-center">{{ __('label.day') }}</th>
                                <th width="100" class="text-center">{{ __('label.lesson_name') }}</th>
                                <th width="200" class="text-center">{{ __('label.teacher') }}</th>
                                <th width="200" class="text-center">{{ __('label.student') }}</th>
                                <th width="200" class="text-center">{{ __('label.date_time') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($records['nexWeek']) > 0)
                        @foreach($records['nexWeek'] as $nexWeek)
                        <tr>
                            <td class="text-center">{{ $nexWeek->course->label }}</td>
                            <td class="text-center">{{ $nexWeek->grade->label }}</td>
                            <td class="text-center">{{ $nexWeek->lesson ? 'Day ' . $nexWeek->lesson->lesson_day : '' }}</td>
                            <td class="text-center">
                                @if($nexWeek->lesson)
                                    <a href="{{ url('lessons/show/'. $nexWeek->lesson->id) }}">{{ $nexWeek->lesson->lesson_name }}</a> 
                                @else
                                {{ __('label.no_lesson') }}
                                @endif
                            </td>
                            <td class="text-center">{{ $nexWeek->teacher ? $nexWeek->teacher->full_name : '' }}</td>
                            <td class="text-center">{{ $nexWeek->student ? $nexWeek->student->romaji_name : '' }}</td>
                            <td class="text-center">{{ date('Y/m/d h:i', strtotime($nexWeek->date . $nexWeek->start_time)) }}</td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="6"><p>{{ __('label.no_lesson') }}</p></td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h3 class="h4">{{ __('label.this_week_lesson') }}</h3>
            </div>

            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                                <th width="200" class="text-center">{{ __('label.course') }}</th>
                                <th width="200" class="text-center">{{ __('label.grade') }}</th>
                                <th width="100" class="text-center">{{ __('label.day') }}</th>
                                <th width="100" class="text-center">{{ __('label.lesson_name') }}</th>
                                <th width="200" class="text-center">{{ __('label.teacher') }}</th>
                                <th width="200" class="text-center">{{ __('label.student') }}</th>
                                <th width="200" class="text-center">{{ __('label.date_time') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($records['thisWeek']) > 0)
                        @foreach($records['thisWeek'] as $thisWeek)
                        <tr>
                            <td class="text-center">{{ $thisWeek->course->label }}</td>
                            <td class="text-center">{{ $thisWeek->grade->label }}</td>
                            <td class="text-center">{{ $thisWeek->lesson ? 'Day ' . $thisWeek->lesson->lesson_day : '' }}</td>
                            <td class="text-center">
                                @if($thisWeek->lesson)
                                    <a href="{{ url('lessons/show/'. $thisWeek->lesson->id) }}">{{ $thisWeek->lesson->lesson_name }}</a> 
                                @else
                                {{ __('label.no_lesson') }}
                                @endif
                            </td>
                            <td class="text-center">{{ $thisWeek->teacher->full_name }}</td>
                            <td class="text-center">{{ $thisWeek->student->romaji_name }}</td>
                            <td class="text-center">{{ date('Y/m/d h:i', strtotime($thisWeek->date . $thisWeek->start_time)) }}</td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="6"><p> {{ __('label.no_lesson') }}</p></td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h3 class="h4">{{ __('label.last_week_lesson') }}</h3>
            </div>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                                <th width="200" class="text-center">{{ __('label.course') }}</th>
                                <th width="200" class="text-center">{{ __('label.grade') }}</th>
                                <th width="100" class="text-center">{{ __('label.day') }}</th>
                                <th width="100" class="text-center">{{ __('label.lesson_name') }}</th>
                                <th width="200" class="text-center">{{ __('label.teacher') }}</th>
                                <th width="200" class="text-center">{{ __('label.student') }}</th>
                                <th width="200" class="text-center">{{ __('label.date_time') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($records['lastWeek']) > 0)
                        @foreach($records['lastWeek'] as $lastWeek)
                        <tr>
                            <td class="text-center">{{ $lastWeek->course->label }}</td>
                            <td class="text-center">{{ $lastWeek->grade->label }}</td>
                            <td class="text-center">{{ $lastWeek->lesson ? 'Day ' . $lastWeek->lesson->lesson_day : '' }}</td>
                            <td class="text-center">
                                @if($lastWeek->lesson)
                                    <a href="{{ url('lessons/show/'. $lastWeek->lesson->id) }}">{{ $lastWeek->lesson->lesson_name }}</a> 
                                @else
                                {{ __('label.no_lesson') }}
                                @endif
                            </td>
                            <td class="text-center">{{ $lastWeek->teacher ? $lastWeek->teacher->full_name : '' }}</td>
                            <td class="text-center">{{ $lastWeek->student ? $lastWeek->student->romaji_name : '' }}</td>
                           <td class="text-center">{{ date('Y/m/d h:i', strtotime($lastWeek->date . $lastWeek->start_time)) }}</td>
							
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="6"><p> {{ __('label.no_lesson') }}</p></td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>