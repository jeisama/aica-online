@extends('layouts.app')

@section('content')
    <!-- Page Header-->
    <header class="page-header">
        <div class="container-fluid">
        <h2 class="no-margin-bottom">Dashboard</h2>
        </div>
    </header>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
            <div class="container-fluid">
              @if (Session::has('message'))
                <div class="alert alert-info">{{ Session::get('message') }}</div>                      
              @endif
                @if ($user_data->role != 1)
                    <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header d-flex align-items-center">
                                        <h3 class="h4">Next week schedule</h3>
                                    </div>
                                    <div class="card-body">
                                        @if($nextweek)
                                            <table class="table table-bordered shadow-sm table-nocard">
                                                <thead>
                                                    <tr>
                                                        @if($user_data->role == '4')
                                                        <th scope="col" width="150">Lessons & Grade</th>
                                                        @else 
                                                        <th scope="col" width="150">Grade Level</th>
                                                        @endif
                                                        
                                                        <th scope="col">Lesson Name</th>
                                                        @if($user_data->role == '4')
                                                        <th scope="col">Teacher</th>
                                                        @else
                                                        <th scope="col" width="150">Student's Name</th>
                                                        @endif
                                                        <th scope="col" width="250">Scheduled Date</th>
                                                        @if($user_data->role == '5')
                                                        <th scope="col" width="50" class="text-center">Action</th>
                                                        @endif
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                 
                                                    @foreach($nextweek as $next)
                                                      
                                                    <tr>
                                                            {{-- <td>{{ $user_data->role == student ?  $next->full_name : $next->grade->label }}</td> --}}
                                                            <td>{{ $user_data->role == '4' ?  'Listening & Speaking 英検3級(a)' : $next->grade->label }}</td>
                                                            <td> Listening & Speaking  <a href="{{ url('lessons/'. $next->slug) }}">{{ $next->title }}</a></td>
                                                            <td>{{ $user_data->role == '4' ? $next->full_name : $next->student->full_name }}</td>
                                                            <td>{{ date('l, Y/m/d H:i', strtotime($next->date . $next->time_in)) }}</td>
                                                            @if($user_data->role == '5')
                                                            <td class="text-center">
                                                                <a href="{{ url('lessons/edit/'. $next->slug) }}" title="Edit {{ $next->title }}"><i class="mdi mdi-playlist-edit mdi-24px" style="line-height: 0"></i></a>
                                                            </td>
                                                            @endif
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            
                                        @else
                                            <div class="alert alert-info">
                                                <h4 class="alert-heading">Opps!</h4>
                                                <p>There are no <strong>Lessons</strong> in the current query.</p>
                                            </div>
                                        @endif
                                                
                                    </div>
                                </div>
                            </div> 
                    </div>
                    <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header d-flex align-items-center">
                                        <h3 class="h4">This week schedule</h3>
                                    </div>
                                    <div class="card-body">
                                        @if($thisweek)
                                            <table class="table table-bordered shadow-sm table-nocard">
                                                <thead>
                                                    <tr>
                                                        @if($user_data->role == '4')
                                                        <th scope="col" width="150">Lessons & Grade</th>
                                                        @else 
                                                        <th scope="col" width="150">Grade Level</th>
                                                        @endif
                                                        
                                                        <th scope="col">Lesson Name</th>
                                                        <th scope="col">Teacher</th>
                                                        <th scope="col" width="250">Scheduled Date</th>
                                                        @if($user_data->role == '5')
                                                        <th scope="col" width="50" class="text-center">Action</th>
                                                        @endif
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                    @foreach($thisweek as $currweek)
                                                    <tr>
                                                        
                                                        {{-- <td>{{ $user_data->role == '4' ?  $currweek->full_name : $currweek->grade->label }}</td> --}}
                                                        <td>{{ $user_data->role == '4' ?  'Listening & Speaking 英検3級(a)' : $currweek->grade->label }}</td>
                                    
                                                        <td>Listening & Speaking <a href="{{ url('lessons/'. $currweek->slug) }}">{{ $currweek->title }}</a></td>
                                                        <td>{{ $user_data->role == '4' ? $currweek->full_name : $currweek->student->full_name }}</td>
                                                        <td>{{ date('Y/m/d H:i', strtotime($currweek->date . $currweek->time_in)) }}</td>
                                                        @if($user_data->role == '5')
                                                        <td class="text-center">
                                                            <a href="{{ url('lessons/edit/'. $currweek->slug) }}" title="Edit {{ $currweek->title }}"><i class="mdi mdi-playlist-edit mdi-24px" style="line-height: 0"></i></a>
                                                        </td>
                                                        @endif
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                           
                                        @else
                                            <div class="alert alert-info">
                                                <h4 class="alert-heading">Opps!</h4>
                                                <p>There are no <strong>Lessons</strong> in the current query.</p>
                                            </div>
                                        @endif
                                                
                                    </div>
                                </div>
                            </div> 
                    </div>
                    <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header d-flex align-items-center">
                                        <h3 class="h4">Last week schedule</h3>
                                    </div>
                                    <div class="card-body">
                                        @if($lastweek)
                                            <table class="table table-bordered shadow-sm table-nocard">
                                                <thead>
                                                    <tr>
                                                        @if($user_data->role == '4')
                                                        <th scope="col" width="150">Lessons & Grade</th>
                                                        @else 
                                                        <th scope="col" width="150">Grade Level</th>
                                                        @endif
                                                        
                                                        <th scope="col">Lesson Name</th>
                                                        <th scope="col">Teacher</th>
                                                        <th scope="col" width="250">Scheduled Date</th>
                                                        @if($user_data->role == '5')
                                                        <th scope="col" width="50" class="text-center">Action</th>
                                                        @endif
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                    @foreach($lastweek as $last)
                                                    <tr>
                                                        
                                                        {{-- <td>{{ $user_data->role == '4' ?  $last->full_name : $last->grade->label }}</td> --}}
                                                        <td>{{ $user_data->role == '4' ?  'Listening & Speaking 英検3級(a)' : $last->grade->label }}</td>
                                                        <td>Listening & Speaking  <a href="{{ url('lessons/'. $last->slug) }}">{{ $last->title }}</a></td>
                                                        <td>{{ $user_data->role == '4' ? $last->full_name : $last->student->full_name }}</td>
                                                        <td>{{ date('Y/m/d H:i', strtotime($last->date . $last->time_in)) }}</td>
                                                        @if($user_data->role == '5')
                                                        <td class="text-center">
                                                            <a href="{{ url('lessons/edit/'. $last->slug) }}" title="Edit {{ $last->title }}"><i class="mdi mdi-playlist-edit mdi-24px" style="line-height: 0"></i></a>
                                                        </td>
                                                        @endif
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                           
                                        @else
                                            <div class="alert alert-info">
                                                <h4 class="alert-heading">Opps!</h4>
                                                <p>There are no <strong>Lessons</strong> in the current query.</p>
                                            </div>
                                        @endif
                                                
                                    </div>
                                </div>
                            </div> 
                    </div>
                @else <!--Temp admin content-->
                    <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header d-flex align-items-center">
                                        <h3 class="h4">Next week schedule</h3>
                                    </div>
                                    <div class="card-body">
                                        @if($nextweek)
                                            <table class="table table-bordered shadow-sm table-nocard">
                                                <thead>
                                                    <tr>
                                                        @if($user_data->role == '4')
                                                        <th scope="col" width="150">Teacher's Name</th>
                                                        @else 
                                                        <th scope="col" width="150">Grade Level</th>
                                                        @endif
                                                        
                                                        <th scope="col">Lesson Name</th>
                                                        <th scope="col">Teacher</th>
                                                        <th scope="col" width="250">Scheduled Date</th>
                                                        @if($user_data->role == '5')
                                                        <th scope="col" width="50" class="text-center">Action</th>
                                                        @endif
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                    @foreach($nextweek as $next)
                                                    <tr>
                                                          
                                                        <td>{{ $user_data->role == '4' ?  $next->full_name : $next->grade->label }}</td>
                                                        <td><a href="{{ url('lessons/'. $next->slug) }}">{{ $next->title }}</a></td>
                                                        <td>{{ $user_data->role == '4' ? $next->full_name : $next->teacher->full_name }}</td>
                                                        <td>{{ date('l, F d Y', strtotime($next->date)) }}</td>
                                                        @if($user_data->role == '5')
                                                        <td class="text-center">
                                                            <a href="{{ url('lessons/edit/'. $next->slug) }}" title="Edit {{ $next->title }}"><i class="mdi mdi-playlist-edit mdi-24px" style="line-height: 0"></i></a>
                                                        </td>
                                                        @endif
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        
                                        @else
                                            <div class="alert alert-info">
                                                <h4 class="alert-heading">Opps!</h4>
                                                <p>There are no <strong>Lessons</strong> in the current query.</p>
                                            </div>
                                        @endif
                                                
                                    </div>
                                </div>
                            </div> 
                    </div>
                    <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header d-flex align-items-center">
                                        <h3 class="h4">This week schedule</h3>
                                    </div>
                                    <div class="card-body">
                                        @if($thisweek)
                                            <table class="table table-bordered shadow-sm table-nocard">
                                                <thead>
                                                    <tr>
                                                        @if($user_data->role == '4')
                                                        <th scope="col" width="150">Teacher's Name</th>
                                                        @else 
                                                        <th scope="col" width="150">Grade Level</th>
                                                        @endif
                                                        
                                                        <th scope="col">Lesson Name</th>
                                                        <th scope="col">Teacher</th>
                                                        <th scope="col" width="250">Scheduled Date</th>
                                                        @if($user_data->role == '5')
                                                        <th scope="col" width="50" class="text-center">Action</th>
                                                        @endif
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                    @foreach($thisweek as $currweek)
                                                    <tr>
                                                        
                                                        <td>{{ $user_data->role == '4' ?  $currweek->full_name : $currweek->grade->label }}</td>
                                                        <td><a href="{{ url('lessons/'. $currweek->slug) }}">{{ $currweek->title }}</a></td>
                                                        <td>{{ $user_data->role == '4' ? $currweek->full_name : $currweek->teacher->full_name }}</td>
                                                        <td>{{ date('l, F d Y', strtotime($currweek->date)) }}</td>
                                                        @if($user_data->role == '5')
                                                        <td class="text-center">
                                                            <a href="{{ url('lessons/edit/'. $currweek->slug) }}" title="Edit {{ $currweek->title }}"><i class="mdi mdi-playlist-edit mdi-24px" style="line-height: 0"></i></a>
                                                        </td>
                                                        @endif
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            
                                        @else
                                            <div class="alert alert-info">
                                                <h4 class="alert-heading">Opps!</h4>
                                                <p>There are no <strong>Lessons</strong> in the current query.</p>
                                            </div>
                                        @endif
                                                
                                    </div>
                                </div>
                            </div> 
                    </div>
                    <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header d-flex align-items-center">
                                        <h3 class="h4">Last week schedule</h3>
                                    </div>
                                    <div class="card-body">
                                        @if($lastweek)
                                            <table class="table table-bordered shadow-sm table-nocard">
                                                <thead>
                                                    <tr>
                                                        @if($user_data->role == '4')
                                                        <th scope="col" width="150">Teacher's Name</th>
                                                        @else 
                                                        <th scope="col" width="150">Grade Level</th>
                                                        @endif
                                                        
                                                        <th scope="col">Lesson Name</th>
                                                        <th scope="col">Teacher</th>
                                                        <th scope="col" width="250">Scheduled Date</th>
                                                        @if($user_data->role == '5')
                                                        <th scope="col" width="50" class="text-center">Action</th>
                                                        @endif
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                    @foreach($lastweek as $last)
                                                    <tr>
                                                        
                                                        <td>{{ $user_data->role == '4' ?  $last->full_name : $last->grade->label }}</td>
                                                        <td><a href="{{ url('lessons/'. $last->slug) }}">{{ $last->title }}</a></td>
                                                        <td>{{ $user_data->role == '4' ? $last->full_name : $last->teacher->full_name }}</td>
                                                        <td>{{ date('l, F d Y', strtotime($last->date)) }}</td>
                                                        @if($user_data->role == '5')
                                                        <td class="text-center">
                                                            <a href="{{ url('lessons/edit/'. $last->slug) }}" title="Edit {{ $last->title }}"><i class="mdi mdi-playlist-edit mdi-24px" style="line-height: 0"></i></a>
                                                        </td>
                                                        @endif
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        
                                        @else
                                            <div class="alert alert-info">
                                                <h4 class="alert-heading">Opps!</h4>
                                                <p>There are no <strong>Lessons</strong> in the current query.</p>
                                            </div>
                                        @endif           
                                    </div>
                                </div>
                            </div> 
                    </div>
                @endif
            </div>
    </section>
@endsection