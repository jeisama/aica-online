<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		 <!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ $title ?? 'Welcome to AIC ONLINE' }}</title>
        <base href="{{ url('/') }}">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="robots" content="all,follow">
        <!-- Bootstrap CSS-->
        <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}">
        <!-- Font Awesome CSS-->
        {{-- <link rel="stylesheet" href="{{ asset('assets/vendor/font-awesome/css/font-awesome.min.css') }}"> --}}
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/MaterialDesignWebfont/css/materialdesignicons.min.css') }}">
        <!-- Fontastic Custom icon font-->
        <link rel="stylesheet" href="{{ asset('assets/css/fontastic.css')}}">
        <!-- Google fonts - Poppins -->
        {{-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700"> --}}
        <link href="https://fonts.googleapis.com/css?family=Noto+Sans+JP" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="{{ asset('assets/css/style.blue.css') }}" id="theme-stylesheet">
        <!-- Custom stylesheet - for your changes-->
        <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
        <!-- Favicon-->
        <link rel="shortcut icon" href="{{ asset('assets/img/favicon.png') }}">
        @yield('css')  
      </head>
    <body>
    <div class="page">
        <!-- Main Navbar-->
        @include('includes.navbar')
        <div class="page-content d-flex align-items-stretch"> 
            <!-- Side Navbar -->
            @include('includes.sidebar')
            <div class="content-inner">
                @yield('content')
                <!-- Page Footer-->
                @include('includes.footer')
            </div>
        </div>
    </div>

    <!-- JavaScript files-->
    <script src="{{asset('assets/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/popper.js/umd/popper.min.js')}}"> </script>
    <script src="{{asset('assets/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/jquery.cookie/jquery.cookie.js')}}"> </script>
    <!-- <script src="{{asset('assets/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
    <!-- Main File-->
    <script src="{{asset('assets/js/front.js')}}"></script>  
    
    <script src="{{ asset('assets/js/app.js') }}"></script>

    @yield('js') 

    </body>
</html>
