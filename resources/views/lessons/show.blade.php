@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-select/bootstrap-select.css') }}">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" />
@endsection
<style>
    #drop-zone {
    width: 100%;
    min-height: 150px;
    border: 3px dashed rgba(0, 0, 0, .3);
    border-radius: 5px;
    position: relative;
    font-size: 20px;
    color: #7E7E7E;
    padding-left: 1em;
    }
    #drop-zone input {
    position: absolute;
    cursor: pointer;
    left: 0px;
    top: 0px;
    opacity: 0;
    }
    /*Important*/
    #drop-zone.mouse-over {
    border: 3px dashed rgba(0, 0, 0, .3);
    color: #7E7E7E;
    }
    /*If you dont want the button*/
    #clickHere {
    display: inline-block;
    cursor: pointer;
    color: white;
    font-size: 17px;
    width: 250px;
    border-radius: 4px;
    background-color: #4679BD;
    padding: 10px;
    }
    #clickHere:hover {
    background-color: #376199;
    }
    #filename {
    margin-top: 10px;
    margin-bottom: 10px;
    font-size: 14px;
    line-height: 1.5em;
    }
    .file-preview {
    background: #ccc;
    border: 5px solid #fff;
    box-shadow: 0 0 4px rgba(0, 0, 0, 0.5);
    display: inline-block;
    width: 60px;
    height: 60px;
    text-align: center;
    font-size: 14px;
    margin-top: 5px;
    }
    .closeBtn:hover {
    color: red;
    display:inline-block;
    }
    .hidden{
       display:none;
    }
    textarea:disabled {
        background: none !important;
        border: solid 1px #c5c5c5;
    }
    textarea::placeholder{
        font-size: 24px !important;
        
    }
    .teacher-answer{
        border:solid 1px #000 !important;
    }
    audio {
        height: 45px !important;
    }
    .write{
        font-size: 16px !important;
    }
    textarea::placeholder{
        font-size: 24px !important;
        
    }
}
</style>
@section('content')
{{-- 
<div class="breadcrumb-holder container-fluid">
    <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i> Home</a></li>
        <li class="breadcrumb-item"><a href="{{ url('/lessons') }}">Lessons</a></li>
        <li class="breadcrumb-item active">Details Lesson</li>
    </ul>
</div>
--}}


<div class="breadcrumb-holder container-fluid">
    <div class="inner">
        <div class="links">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i> {{__('label.home')}}</a></li>
                <li class="breadcrumb-item"><a href="{{ url('/lessons') }}">{{__('label.lesson')}}</a></li>
                <li class="breadcrumb-item active">{{$templates->lesson_name}}</li>
            </ul>
        </div>
        @if (env('APP_ENV') == 'production')
        @if (strtotime(date('Y-m-d H:i:s')) > strtotime($templates->schedule->date . $templates->schedule->start_time) && strtotime(date('Y-m-d H:i:s')) < strtotime($templates->schedule->date . $templates->schedule->end_time) )
        <div class="buttons">
            @if ( Auth::user()->role == student || Auth::user()->role == admin || Auth::user()->role == class_room || Auth::user()->role == aica)
                <a href="{{ url('lessons/conference/'.$templates->token) }}" class="btn btn-primary btn-sm" target="_self"> {{ __('label.join_video')}}</a>
            @else
                <a href="{{ url('lessons/conference/'.$templates->token) }}" class="btn btn-primary btn-sm" target="_blank"> {{ __('label.start_video')}}</a>
            @endif
        </div>
        @endif
        @else
        <div class="buttons">
            @if ( Auth::user()->role == student || Auth::user()->role == admin || Auth::user()->role == class_room || Auth::user()->role == aica)
                <a href="{{ url('lessons/conference/'.$templates->token) }}" class="btn btn-primary btn-sm" target="_self"> {{ __('label.join_video')}}</a>
            @else
                <a href="{{ url('lessons/conference/'.$templates->token) }}" class="btn btn-primary btn-sm" target="_blank"> {{ __('label.start_video')}}</a>
            @endif
        </div>
        @endif
    </div>
</div>
<section class="forms">
    <div class="container-fluid">
    @if (Session::has('success_message'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {!! session('success_message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    @if (Session::has('error_message'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {!! session('error_message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <div class="card">
        <div class="card-header d-flex align-items-center">
            <h3 class="h4">{{__('label.lesson_details')}}</h3>
        </div>
        <div class="card-body">
            {{ Form::open(['files' => true,  'id' => 'lesson-form', 'onsubmit' => 'event.preventDefault()']) }}	
            {{-- start of new form --}}
            <input type="hidden" name="course_type" value="{{$templates->course_type}}">
            <input type="hidden" name="lesson_type" value="{{$templates->lesson_type}}">
            <input type="hidden" name="form_type" value="{{$templates->form_type}}">
            <input type="hidden" name="lesson_id" value="{{$templates->id}}">
            <input type="hidden" name="created_by" value="{{$templates->created_by}}">
            <div class="row">
                <div class="col-sm-5 col-md-5">
                    <dl class="row">
                        <dt class="col-sm-3">{{__('label.course')}} :</dt>
                        <dd class="col-sm-8">{{$templates->courses->label}}</dd>
                        <dt class="col-sm-3">{{__('label.day')}} :</dt>
                        <dd class="col-sm-8">{{$templates->lesson_day}}</dd>
                        @if (Auth::user()->role == student)
                        <dt class="col-sm-3">{{__('label.teacher')}}:</dt>
                        <dd class="col-sm-8">{{$templates->teacher->full_name}}</dd>
                        @elseif(Auth::user()->role == teacher)
                        <dt class="col-sm-3">{{__('label.student')}}:</dt>
                        <dd class="col-sm-8">{{$templates->student->romaji_name}}</dd>
                        @else 
                        <dt class="col-sm-3">{{__('label.teacher')}}:</dt>
                        <dd class="col-sm-8">{{$templates->teacher->full_name}}</dd>
                        <dt class="col-sm-3">{{__('label.student')}}:</dt>
                        <dd class="col-sm-8">{{$templates->student->romaji_name}}</dd>
                        @endif
                    </dl>
                </div>
                <div class="col-sm-7 col-md-7">
                    <dl class="row">
                        <dt class="col-sm-3">{{__('label.grade')}} :</dt>
                        <dd class="col-sm-8">{{$templates->grade->label}}</dd>
                        <dt class="col-sm-3">{{__('label.lesson_name')}} :</dt>
                        <dd class="col-sm-8" id="val_lesson_name">{{$templates->lesson_name}}</dd>
                        <dt class="col-sm-3">{{__('label.date')}} :</dt>
                        <dd class="col-sm-8" id="val_lesson_name">{{ date('l, Y/m/d H:i', strtotime($templates->schedule->date . $templates->schedule->start_time)) }}</dd>
                    </dl>
                </div>
            </div>
            @if ($templates->form_type == 1)
            <div class="homework-box" style="border: 1px solid #c5c5c5; padding: 15px; width: 100%;">
                <div class="tbl" style="display: table; width: 100%;">
                    <div style="display: table-cell; vertical-align: top; width:30px;"></div>
                    <div style="display: table-cell; vertical-align: top;">
                        <div class="form-row">
                            <div class="form-group col col-md-12 col-lg-12">
                                <label for="" class="form-control-label"><strong>Speaking part用 課題</strong></label>
                                <p>次回の授業で以下のトピックについて先生から問われます。事前にあなたの意見と、そのように
                                    思う理由を2～3文の英文でまとめましょう。<br>
                                    どのように英語で表現すれば良いか分からない部分は、辞書などを用いて調べましょう。
                                    </small>
                                </p>
                                
                                <div style="border:solid 1px #c5c5c5; padding:10px;">
                                    <p><strong>次週のSpeaking Topic：</strong></p>
                                    <p style="text-align: left; font-size: 23px !important;">{!! nl2br($templates->speaking_topic) !!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--teacher/student ans -->
                @if (Auth::user()->role == student)
                <div class="tbl" style="display: table; width: 100%;">
                    <div style="display: table-cell; vertical-align: top; width:30px;"></div>
                    <div style="display: table-cell; vertical-align: top;">
                        <div class="form-row">
                            <div class="form-group col col-md-12 col-lg-12">
                                
                                {{-- <label for="writing_stu_ans" class="form-control-label"><strong>生徒回答場所</strong></label><br> --}}
                                @if ($answers)
                                <div style="border:solid 1px #c5c5c5; padding: 10px;">
                                    <p style="text-align: left; font-size: 18px !important;">{!! nl2br($answers->writing_stu_ans) !!}</p>
                                </div>
                                <br>
                                {{-- <label for="writing_tea_res" class="form-control-label"><strong>教師添削後返却場所</strong></label> --}}
                                
                                @else
                                <textarea rows="4" cols="50" id="writing_stu_ans" name="writing_stu_ans" class="form-control" {{Auth::user()->role == student ? ' ' : 'disabled'}} placeholder="こちらに回答してください。" style="border:solid 1px #000;"></textarea>
                                <br> 
                                {{-- <label for="writing_tea_res" class="form-control-label"><strong>教師添削後返却場所</strong></label><br> --}}
                                {{-- <textarea rows="4" cols="50" id="writing_tea_res" name="writing_tea_res" class="form-control" {{Auth::user()->role == teacher && $answers ? ' ' : 'disabled'}}></textarea> --}}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @elseif(Auth::user()->role == teacher)
                <div class="tbl" style="display: table; width: 100%;">
                    <div style="display: table-cell; vertical-align: top; width:30px;">
                   
                    </div>
                    <div style="display: table-cell; vertical-align: top;">
                        <div class="form-row">
                            <div class="form-group col col-md-12 col-lg-12">
                                
                                {{-- <label for="writing_stu_ans" class="form-control-label"><strong>生徒回答場所</strong></label><br> --}}
                                @if($answers)
                                    <div style="border:solid 1px #c5c5c5; padding: 10px;">
                                    <p style="text-align: left; font-size: 18px !important;">{!! nl2br($answers->writing_stu_ans) !!}</p>
                                </div>
                                @else
                                    <textarea rows="4" cols="50" id="writing_stu_ans" name="writing_stu_ans" class="form-control" {{Auth::user()->role == student ? ' ' : 'disabled'}} placeholder="生徒回答場所"></textarea>
                                @endif
                                <br>
                                {{-- <label for="writing_tea_res" class="form-control-label"><strong>教師添削後返却場所</strong></label> --}}
                                
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                <!--teacher student ans -->
                <div class="tbl" style="display: table; width: 100%;">
                    <div style="display: table-cell; vertical-align: top; width:30px;"></div>
                    <div style="display: table-cell; vertical-align: top;">
                        <div class="form-row">
                            <div class="form-group col col-md-12 col-lg-12">
                                <label for="listening" class="form-control-label"><strong>Listening part用課題</strong></label>
                                <p>Step 1. 音声を再生して、流れる会話文の内容を聞き取りましょう。繰り返し聞いても内容を理解
                                    できないときは、下の“展開”ボタンを押して、スクリプトを確認しながらリスニングを行いましょう。</p>
                                <div style="padding:10px; ">
                                    <div class="form-group col-lg-12" style=" width: auto;">
                                        @if(Auth::user()->role != admin) 
                                        @if($media)
                                        @foreach($media as $audio)
                                        <div style="display: inline-block; padding-right: 20px; text-align:center;">
                                            <audio controls controlsList="nodownload">
                                                <source src="{{asset('uploads/video/'. $audio->attachment)}}" type="audio/mp3">
                                                    Your browser does not support the audio element.
                                              </audio><br>
                                              <span style="margin: 5px 0 10px 0;">{{ $audio->original_name ?? $audio->attachment }}</span>
                                        </div>
                                        @endforeach
                                        @endif
                                        @elseif(Auth::user()->role == admin) 
                                        @if($media)
                                        @foreach($media as $audio)
                                        <div style="display: inline-block; padding-right: 20px; text-align:center;">
                                                <audio controls>
                                                    <source src="{{asset('uploads/video/'. $audio->attachment)}}" type="audio/mp3">
                                                        Your browser does not support the audio element.
                                                  </audio><br>
                                                  <span style="margin: 5px 0 10px 0;">{{ $audio->original_name ?? $audio->attachment }}</span>
                                                  <a href="javascript:void(0);" class="delete_attachment" data-target="delete-media" data-id="{{ $audio->id }}"><i class="mdi mdi-close"></i></a>
                                        </div>
                                        @endforeach
                                        @endif
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tbl" style="display: table; width: 100%;">
                    <div style="display: table-cell; vertical-align: top; width:30px;"></div>
                    <div style="display: table-cell; vertical-align: top;">
                        <div class="form-row">
                            <div class="form-group col col-md-12 col-lg-12">
                                <p>Step 2. スクリプトを見ながら、音声の後に続いて音読しましょう。その際、英文の内容や意味を
                                    意識しながら音読するようにしましょう。<br>
                                    わからない単語や表現がある場合は、辞書などを用いて調べましょう。
                                </p>
                                {{-- <p class="text-center">------------------------------------ 展開・省略 ------------------------------------</p> --}}
                                <label for="next_listening" class="form-control-label"><strong>今週のListening 課題：</strong></label>
                                <button type="button" class="expander" id="btn-control">開く</button>
                                
                                <div style="border:solid 1px #c5c5c5; padding:10px;" id="listen-div" class='hidden'>
                                    <p>{!! nl2br($templates->next_listening) !!}</p>
                                </div>
                                <small id="error-next_listening" class="form-text form-error"></small>
                            </div>
                        </div>
                    </div>
                </div>
                <!--file lists -->
                <div class="tbl" style="display: table; width: 100%;">
                    <div style="display: table-cell; vertical-align: top; width:30px;"><strong></strong></div>
                    <div style="display: table-cell; vertical-align: top;">
                        <div class="form-row">
                            <div class="form-group col col-md-12 col-lg-12">
                                <label for="attachment" class="form-control-label"><strong>レッスン添付ファイル</strong></label><br>
                                <div style=" padding:10px;">
                                    <div class="form-group col-lg-12">
                                        @if(Auth::user()->role != admin) 
                                        @if($attachment)
                                        @foreach($attachment as $file)
                                        <div><a href="{{asset('uploads/lessons/'. $file->attachment)}}" target="_blank"> {{ $file->original_name ?? $file->attachment }}</a> </div>
                                        @endforeach
                                        @endif
                                        @elseif(Auth::user()->role == admin) 
                                        @if($attachment)
                                        @foreach($attachment as $file)
                                        <div><a href="{{asset('uploads/lessons/'. $file->attachment)}}" target="_blank"> {{ $file->original_name ?? $file->attachment }}</a> 
                                            
                                            <a href="javascript:void(0);" data-target="delete-attachment" class="delete_attachment" data-id="{{ $file->id }}">
                                            <i class="mdi mdi-close"></i></a>

                                        </div>
                                        @endforeach
                                        @endif
                                        @endif											
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--- chat display -->
                <div class="tbl" style="display: table; width: 100%;">
                    <div style="display: table-cell; vertical-align: top; width:30px;"><strong></strong></div>
                    <div style="display: table-cell; vertical-align: top;">
                        <div class="lesson_comments" id="comments" style="overflow: auto; height: 350px;">
                            <label for="listening" class="form-control-label"><strong>チャット履歴</strong></label>	
                            @if($chats)
                            @foreach($chats as $chat)
                            @if($chat->message)
                            <div class="comment-data">
                                <div class="comment-avatar">
                                    @if($chat->user->avatar == null || !File::exists('uploads/avatar/'. $chat->user->avatar))
                                    <img src="{{ asset('uploads/avatar/noavatar.jpg') }}"  width="35" height="35">
                                    @else
                                    <img src="{{ asset('uploads/avatar/'. $chat->user->avatar) }}" width="35" height="35">
                                    @endif
                                </div>
                                <div class="comment-body">
                                    <div class="name">{{ $chat->user->username }} <small>{{ \App\Helpers\DateCarbon::date($chat->created_at) }}</small></div>
                                    <div class="note">
                                        {!! nl2br(e($chat->message)) !!}
                                    </div>
                                </div>
                            </div>
                            @endif
                            @endforeach
                            @endif
                        </div>
                    </div>
                </div>
                <!--- chat display -->
                <!--file lists -->
                <!--upload files -->
                @if ((Auth::user()->role == admin) && !($answers && $answers->writing_tea_res && $answers->writing_stu_ans)) 
                <div class="tbl" style="display: table; width: 100%;">
                    <div style="display: table-cell; vertical-align: top; width:30px;"><strong></strong></div>
                    <div style="display: table-cell; vertical-align: top;">
                        <div class="form-row">
                            <div class="form-group col col-md-12 col-lg-12">
                                <div style="padding:10px;">
                                    <div class="form-row">
                                        <div id="drop-zone">
                                            <br>
                                            <label id="clickHere">音声添付ファイル<i class="fa fa-upload"></i>
                                            <input type="file" class="select_file" id="media" multiple accept="audio/*"/>
                                            </label>
                                            <div id='filelist_media'></div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="form-row">
                                        <div id="drop-zone">
                                            <br>
                                            <label id="clickHere">レッスン添付ファイル<i class="fa fa-upload"></i>
                                            <input type="file" class="select_file" id="attachment" multiple accept="image/*, application/pdf"/>
                                            </label>
                                            <div id='filelist_attachment'></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                <!--upload files -->
                @endif
                @if ($templates->form_type == 2)
                <div class="homework-box" style="border: 1px solid #c5c5c5; padding: 15px; width: 100%;">
                    <!--teacher/student ans -->
                    @if (Auth::user()->role == student)
                    <div class="tbl" style="display: table; width: 100%;">
                        <div style="display: table-cell; vertical-align: top; width:30px;"></div>
                        <div style="display: table-cell; vertical-align: top;">
                            <div class="form-row">
                                <div class="form-group col col-md-12 col-lg-12">
                                    <label for="writing_stu_ans" class="form-control-label"><strong>Speaking part用課題</strong></label><br>
                                    <p>この1週間の出来事について書きましょう。何をやったのかという文章と、更に内容を詳しくする
                                        文章を1文ずつ書きましょう。作った文章を次回の授業の中で発表しましょう。</p>
                                    {{-- <label for="writing_stu_ans" class="form-control-label"><strong>生徒回答場所</strong></label> --}}
                                    @if ($answers)
                                    <div style="border:solid 1px #c5c5c5; padding: 10px;">
                                        <p style="text-align: left; font-size: 18px !important;">{!! nl2br($answers->writing_stu_ans) !!}</p>
                                    </div>
                                    <br>
                                    {{-- <label for="writing_tea_res" class="form-control-label"><strong>教師添削後返却場所</strong></label> --}}
                                    
                                    @else
                                        <textarea rows="4" cols="50" id="writing_stu_ans" name="writing_stu_ans" class="form-control" {{Auth::user()->role == student ? ' ' : 'disabled'}} placeholder="こちらに回答してください。"style="border:solid 1px #000;" ></textarea>
                                    
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    @elseif(Auth::user()->role == teacher)
                    <div class="tbl" style="display: table; width: 100%;">
                        <div style="display: table-cell; vertical-align: top; width:30px;">
                         
                        </div>
                        <div style="display: table-cell; vertical-align: top;">
                            <div class="form-row">
                                <div class="form-group col col-md-12 col-lg-12">
                                    <label for="writing_stu_ans" class="form-control-label"><strong>Speaking part用課題</strong></label><br>
                                    <p>この1週間の出来事について書きましょう。何をやったのかという文章と、更に内容を詳しくする
                                        文章を1文ずつ書きましょう。作った文章を次回の授業の中で発表しましょう。</p>
                                    {{-- <label for="writing_stu_ans" class="form-control-label"><strong>生徒回答場所</strong></label><br> --}}
                                    @if($answers)
                                        <div style="border:solid 1px #c5c5c5; padding: 10px;">
                                            <p style="text-align: left; font-size: 18px !important;">{!! nl2br($answers->writing_stu_ans) !!}</p>
                                        </div>
                                    @else
                                        <textarea rows="4" cols="50" id="writing_stu_ans" name="writing_stu_ans" class="form-control" {{Auth::user()->role == student ? ' ' : 'disabled'}} placeholder="生徒回答場所"></textarea>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    <!--teacher student ans -->
                    <div class="tbl" style="display: table; width: 100%;">
                        <div style="display: table-cell; vertical-align: top; width:30px;"></div>
                        <div style="display: table-cell; vertical-align: top;">
                            <div class="form-row">
                                <div class="form-group col col-md-12 col-lg-12">
                                    <label for="listening" class="form-control-label"><strong>Listening part用課題</strong></label>
                                    <p>Step 1. 音声を再生して、流れる会話文の内容を聞き取りましょう。繰り返し聞いても内容を理解
                                        できないときは、下の“展開”ボタンを押して、スクリプトを確認しながらリスニングを行いましょう。</p>
                                    <div style=" padding:10px;">
                                        <div class="form-group col-lg-12">
                                            @if(Auth::user()->role != admin) 
                                            @if($media)
                                            @foreach($media as $audio)
                                            <div style="display: inline-block; padding-right: 20px; text-align:center;">
                                                    <audio controls controlsList="nodownload">
                                                        <source src="{{asset('uploads/video/'. $audio->attachment)}}" type="audio/mp3">
                                                            Your browser does not support the audio element.
                                                      </audio><br>
                                                      <span style="margin: 5px 0 10px 0;">{{ $audio->original_name ?? $audio->attachment }}</span>
                                                </div>
                                            @endforeach
                                            @endif
                                            @elseif(Auth::user()->role == admin) 
                                            @if($media)
                                            @foreach($media as $audio)
                                            <div>
                                                    <div style="display: inline-block; padding-right: 20px; text-align:center;">
                                                            <audio controls>
                                                                <source src="{{asset('uploads/video/'. $audio->attachment)}}" type="audio/mp3">
                                                                    Your browser does not support the audio element.
                                                              </audio><br>
                                                              <span style="margin: 5px 0 10px 0;">{{ $audio->original_name ?? $audio->attachment }}</span>
                                                              <a href="javascript:void(0);" class="delete_attachment" data-target="delete-media" data-id="{{ $audio->id }}"><i class="mdi mdi-close"></i></a>
                                                    </div>
                                            </div>
                                            @endforeach
                                            @endif
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tbl" style="display: table; width: 100%;">
                        <div style="display: table-cell; vertical-align: top; width:30px;"></div>
                        <div style="display: table-cell; vertical-align: top;">
                            <div class="form-row">
                                <div class="form-group col col-md-12 col-lg-12">
                                    <p>Step 2. スクリプトを見ながら、音声の後に続いて音読しましょう。その際、英文の内容や意味を
                                        意識しながら音読するようにしましょう。<br>
                                        わからない単語や表現がある場合は、辞書などを用いて調べましょう。
                                    </p>
                                    {{-- <p class="text-center">------------------------------------ 展開・省略 ------------------------------------</p> --}}
                                    <label for="next_listening" class="form-control-label"><strong>今週のListening 課題：</strong></label>
                                    <button type="button" class="expander" id="btn-control">開く</button>
                                    <div style="border:solid 1px #c5c5c5; padding:10px;" id="listen-div"  class='hidden'>
                                        <div class="form-group col-lg-12">
                                            {{$templates->next_listening}}
                                        </div>
                                    </div>
                                    <small id="error-next_listening" class="form-text form-error"></small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--file lists -->
                    <div class="tbl" style="display: table; width: 100%;">
                        <div style="display: table-cell; vertical-align: top; width:30px;"><strong></strong></div>
                        <div style="display: table-cell; vertical-align: top;">
                            <div class="form-row">
                                <div class="form-group col col-md-12 col-lg-12">
                                    <label for="attachment" class="form-control-label"><strong>レッスン添付ファイル</strong></label><br>
                                    <div style=" padding:10px;">
                                        <div class="form-group col-lg-12">
                                            @if (Auth::user()->role != admin)
                                            @if($attachment)
                                            @foreach($attachment as $file)
                                            <div><a href="{{asset('uploads/lessons/'. $file->attachment)}}" target="_blank"> {{ $file->original_name ?? $file->attachment }}</a> </div>
                                            @endforeach
                                            @endif
                                            @elseif(Auth::user()->role == admin) 
                                            @if($attachment)
                                            @foreach($attachment as $file)
                                            <div>
                                                <a href="{{asset('uploads/lessons/'. $file->attachment)}}" target="_blank"> {{ $file->original_name ?? $file->attachment }}</a> 
                                                <a href="javascript:void(0);" data-target="delete-attachment" class="delete_attachment" data-id="{{ $file->id }}"><i class="mdi mdi-close"></i></a>
                                            </div>
                                            @endforeach
                                            @endif
                                            @endif											
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--- chat display -->
                    <div class="tbl" style="display: table; width: 100%;">
                        <div style="display: table-cell; vertical-align: top; width:30px;"><strong></strong></div>
                        <div style="display: table-cell; vertical-align: top;">
                            <div class="lesson_comments" id="comments" style="overflow: auto; height: 350px;">
                                <label for="listening" class="form-control-label"><strong>チャット履歴</strong></label>	
                                @if($chats)
                                @foreach($chats as $chat)
                                @if($chat->message)
                                <div class="comment-data">
                                    <div class="comment-avatar">
                                        @if($chat->user->avatar == null || !File::exists('uploads/avatar/'. $chat->user->avatar))
                                        <img src="{{ asset('uploads/avatar/noavatar.jpg') }}"  width="35" height="35">
                                        @else
                                        <img src="{{ asset('uploads/avatar/'. $chat->user->avatar) }}" width="35" height="35">
                                        @endif
                                    </div>
                                    <div class="comment-body">
                                        <div class="name">{{ $chat->user->username }} <small>{{ \App\Helpers\DateCarbon::date($chat->created_at) }}</small></div>
                                        <div class="note">
                                            {!! nl2br(e($chat->message)) !!}
                                        </div>
                                    </div>
                                </div>
                                @endif
                                @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                    <!--- chat display -->						
                    <!--file lists -->
                    <!--upload files -->
                    @if ((Auth::user()->role == admin) && !($answers && $answers->writing_tea_res && $answers->writing_stu_ans)) 
                    <div class="tbl" style="display: table; width: 100%;">
                        <div style="display: table-cell; vertical-align: top; width:30px;"><strong></strong></div>
                        <div style="display: table-cell; vertical-align: top;">
                            <div class="form-row">
                                <div class="form-group col col-md-12 col-lg-12">
                                    <div style="padding:10px;">
                                        <div class="form-row">
                                            <div id="drop-zone">
                                                <br>
                                                <label id="clickHere">音声添付ファイル<i class="fa fa-upload"></i>
                                                <input type="file" class="select_file" id="media" multiple accept="audio/*"/>
                                                </label>
                                                <div id='filelist_media'></div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="form-row">
                                            <div id="drop-zone">
                                                <br>
                                                <label id="clickHere">レッスン添付ファイル<i class="fa fa-upload"></i>
                                                <input type="file" class="select_file" id="attachment" multiple accept="image/*, application/pdf"/>
                                                </label>
                                                <div id='filelist_attachment'></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    <!--upload files -->
                </div>
                <!--homework-box-->  
                @endif

                @if ($templates->form_type == 3)
                <div class="homework-box" style="border: 1px solid #c5c5c5; padding: 15px; width: 100%;">
                    <div class="tbl" style="display: table; width: 100%;">
                        <div style="display: table-cell; vertical-align: top; width:30px;"><strong></strong></div>
                        <div style="display: table-cell; vertical-align: top;">
                            <div class="form-row">
                                <div class="form-group col col-md-12 col-lg-12">
                                    <label for="" class="form-control-label write">今回の課題（以下の課題に対して「記入欄」に英作文を記入しましょう）</label>
                                    
                                        <p style="text-align: left; font-size:24px;">{{$templates->writing_topic}}</p>
                                
                                    {{-- 
                                    <textarea rows="4" cols="50" id="writing_topic" name="writing_topic" class="form-control" placeholder="トピックを入力してください。" >{{$templates->writing_topic}}</textarea>
                                    --}}
                                    <small id="error-writing_topic" class="form-text form-error"></small>
                                    
                                    <div class="lesson_comments" id="comments" style="overflow: auto; height: 350px;">
                                        
                                        @if($chats)
                                        @foreach($chats as $chat)
                                        @if($chat->message)
                                        <div class="comment-data">
                                            <div class="comment-avatar">
                                                @if($chat->user->avatar == null || !File::exists('uploads/avatar/'. $chat->user->avatar))
                                                <img src="{{ asset('uploads/avatar/noavatar.jpg') }}"  width="35" height="35">
                                                @else
                                                <img src="{{ asset('uploads/avatar/'. $chat->user->avatar) }}" width="35" height="35">
                                                @endif
                                            </div>
                                            <div class="comment-body">
                                                <div class="name">{{ $chat->user->username }} <small>{{ \App\Helpers\DateCarbon::date($chat->created_at) }}</small></div>
                                                <div class="note">
                                                    {!! nl2br(e($chat->message)) !!}
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if (Auth::user()->role == student)
                    <div class="tbl" style="display: table; width: 100%;">
                        <div style="display: table-cell; vertical-align: top; width:30px;"><strong></strong></div>
                        <div style="display: table-cell; vertical-align: top;">
                            <div class="form-row">
                                <div class="form-group col col-md-12 col-lg-12">
                                    <label for="writing_stu_ans" class="form-control-label write">記入欄</label>
                                    @if ($answers)
                                        <div style="border:solid 1px #c5c5c5; padding: 10px;">
                                            <p style="text-align: left; font-size: 18px !important;">{!! nl2br($answers->writing_stu_ans) !!}</p>
                                        </div>
                                        <br>
                                        <label for="writing_tea_res" class="form-control-label write">添削結果</label>
                                        @if (!$answers->writing_tea_res)
                                            <textarea rows="4" cols="50" id="writing_tea_res" name="writing_tea_res" class="form-control"
                                            {{Auth::user()->role == teacher && $answers ? ' ' : 'disabled'}} ></textarea>
                                        @else
                                            <div style="border:solid 1px #c5c5c5; padding: 10px;">
                                                <p style="text-align: left; font-size: 18px !important;">{!! nl2br($answers->writing_tea_res) !!}</p>
                                            </div>
                                        @endif
                                    @else
                                        <textarea rows="4" cols="50" id="writing_stu_ans" name="writing_stu_ans" class="form-control" {{Auth::user()->role == student ? ' ' : 'disabled'}} style="border:solid 1px #000;" placeholder="こちらに回答してください"></textarea>
                                        <br> 
                                        <label for="writing_tea_res" class="form-control-label write">添削結果</label><br>
                                        <textarea rows="4" cols="50" id="writing_tea_res" name="writing_tea_res" class="form-control"
                                        {{Auth::user()->role == teacher && $answers ? ' ' : 'disabled'}}></textarea>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    @elseif(Auth::user()->role == teacher)
                    <div class="tbl" style="display: table; width: 100%;">
                        <div style="display: table-cell; vertical-align: top; width:30px;">
                            <strong></strong>
                        </div>
                        <div style="display: table-cell; vertical-align: top;">
                            <div class="form-row">
                                <div class="form-group col col-md-12 col-lg-12">
                                    <label for="writing_stu_ans" class="form-control-label write">記入欄</label><br>
                                    @if($answers)
                                        <div style="border:solid 1px #c5c5c5; padding: 10px;">
                                            <p style="text-align: left;  font-size: 18px !important;">{!! nl2br($answers->writing_stu_ans) !!}</p>
                                        </div>
                                    @else
                                        <textarea rows="4" cols="50" id="writing_stu_ans" name="writing_stu_ans" class="form-control" {{Auth::user()->role == student ? ' ' : 'disabled'}} placeholder="こちらに回答してください"></textarea>
                                    @endif
                                        <br>
                                        <label for="writing_tea_res" class="form-control-label write">添削結果</label>
                                    @if ($answers && $answers->writing_tea_res)
                                        <div style="border:solid 1px #c5c5c5; padding: 10px;">
                                            <p style="text-align: left; font-size: 18px !important;">{!! nl2br($answers->writing_tea_res) !!}</p>
                                        </div>
                                    @else
                                        <textarea rows="4" cols="50" id="writing_tea_res" name="writing_tea_res" class="form-control {{Auth::user()->role == teacher && $answers ? 'teacher-answer' : ' '}}"
                                        {{Auth::user()->role == teacher && $answers ? ' ' : 'disabled'}} ></textarea>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    <!--file lists -->
                    <div class="tbl" style="display: table; width: 100%;">
                        <div style="display: table-cell; vertical-align: top; width:30px;"><strong></strong></div>
                        <div style="display: table-cell; vertical-align: top;">
                            <div class="form-row">
                                <div class="form-group col col-md-12 col-lg-12">
                                    <label for="attachment" class="form-control-label write">レッスン添付ファイル</label><br>
                                    <div style=" padding:10px;">
                                        <div class="form-group col-lg-12">
                                            @if(Auth::user()->role != admin)  
                                            @if($attachment)
                                            @foreach($attachment as $file)
                                            <div><a href="{{asset('uploads/lessons/'. $file->attachment)}}" target="_blank"> {{ $file->original_name ?? $file->attachment }}</a> </div>
                                            @endforeach
                                            @endif
                                            @elseif(Auth::user()->role == admin) 
                                            @if($attachment)
                                            @foreach($attachment as $file)
                                            <div>
                                                <a href="{{asset('uploads/lessons/'. $file->attachment)}}" target="_blank"> {{ $file->original_name ?? $file->attachment }}</a> 
                                                <a href="javascript:void(0);" data-target="delete-attachment" class="delete_attachment" data-id="{{ $file->id }}"><i class="mdi mdi-close"></i></a>
                                            </div>
                                            @endforeach
                                            @endif
                                            @endif											
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--file lists -->
                    <!--upload files -->
                    @if ((Auth::user()->role == admin) && !($answers && $answers->writing_tea_res && $answers->writing_stu_ans)) 
                    <div class="tbl" style="display: table; width: 100%;">
                        <div style="display: table-cell; vertical-align: top; width:30px;"><strong></strong></div>
                        <div style="display: table-cell; vertical-align: top;">
                            <div class="form-row">
                                <div class="form-group col col-md-12 col-lg-12">
                                    <div style="padding:10px;">
                                        {{-- 
                                        <div class="form-row">
                                            <div id="drop-zone">
                                                <br>
                                                <label id="clickHere">音声添付ファイル<i class="fa fa-upload"></i>
                                                <input type="file" class="select_file" id="media" multiple accept="audio/*"/>
                                                </label>
                                                <div id='filelist_media'></div>
                                            </div>
                                        </div>
                                        --}}
                                        <br>
                                        <div class="form-row">
                                            <div id="drop-zone">
                                                <br>
                                                <label id="clickHere">レッスン添付ファイル<i class="fa fa-upload"></i>
                                                <input type="file" class="select_file" id="attachment" multiple accept="image/*, application/pdf"/>
                                                </label>
                                                <div id='filelist_attachment'></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    <!--upload files -->
               
                </div>
                @endif <br>
                @if (Auth::user()->role != student)
                <div class="tbl" style="display: table; width: 100%;">
                    <div style="display: table-cell; vertical-align: top; width:30px;"><strong></strong></div>
                    <div style="display: table-cell; vertical-align: top;">
                        <div class="form-row">
                            <div class="form-group col col-md-12 col-lg-12">
                            <label for="listening" class="form-control-label"><strong>Teacher's Comments</strong></label>
                            <div style="border:solid 1px #c5c5c5; padding: 10px;">
                                    <p style="text-align: left; font-size: 18px !important;">
                                            @if ($comments != null)
                                            {!!  nl2br($comments->comments) !!}
                                           @else
                                               no comment
                                           @endif    
                                    
                                    </p>
                                </div>
                                    
                                   
                            </div>
                        </div>
                    </div>
                </div>   
                @endif
             
                
                <!--submit button -->
                <div class="form-row">
                    <div class="form-group col col-md-6 col-lg-6">
                        {{-- <button type="submit" id="submit-form" data-form="thread-lesson" class="btn btn-primary">Update</button>  --}}
                        @if (Auth::user()->role == 4)
                        @if($answers == null )
                            <button type="submit" id="submit-form" data-form="lesson-form" class="btn btn-primary">{{__('label.update')}}</button>   
                        @endif
                        @elseif(Auth::user()->role == 5 && $templates->form_type == '3')
                        @if ($answers && !$answers->writing_tea_res)
                            <input type="hidden" name="thread_id" value="{{$answers->id}}">
                            <button type="submit" id="submit-form" data-form="lesson-form" class="btn btn-primary">{{__('label.update')}}</button>
                        @elseif($answers && $answers->writing_tea_res && $answers->writing_stu_ans)

                        @else
                            <button type="submit" id="submit-form" data-form="lesson-form" class="btn btn-primary">{{__('label.update')}}</button>
                        @endif
                        @endif
                        <a href="{{ url('lessons') }}" class="btn btn-warning">{{__('label.cancel')}}</a>
                        <span class="form-proccessing hidden"><img src="{{ asset('assets/img/Loading/loading.gif') }}"> {{__('label.submit_process')}}</span>
                    </div>
                </div>
                <div id="uploads">
                    <!-- Upload Content -->
                </div>
                <div class="clearfix"></div>
                {{ Form::close() }} 
            </div>
        </div>
    </div>
</section>
@endsection
@section('js')
<script src="{{ asset('assets/vendor/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script>
    $(document).ready(function(){
        //let form_data =  new FormData($('#lesson-form')[0]);
    	let _token = $('meta[name="csrf-token"]').attr('content');
    	$('.delete_media').each(function(){
    		$(this).click(function(){
    			let id = $(this).attr('data');
    		});
    	});
    
    	$('.delete_attachment').each(function(){
    		$(this).click(function(){
    			let id = $(this).attr('data-id');
    			let target = $(this).attr('data-target');
    			let ans= confirm('Are you sure you want to delete this item?');
    			if(ans){
    				$.ajax({
    					type: 'post',
    					url: '{{ url('lessons') }}/' + target,
    					data:{
    						id: id,
    						_token: _token
    					},success:function(data){
    						//$('#parent-'+ id).remove();
    						window.location = window.location;
    					}
    				});
    			}
    		});
    	});
    
    	/*Upload attachement or files*/
    	let form_data =  new FormData($('#lesson-form')[0]);
    
    	var fileMedia      = [];
    
    	var fileAttachment = [];
    	
    	$('.select_file').each(function(){
    
    		$(this).change(function(){
    			
    			let target = $(this).attr('id');
    
    			var files = $(this)[0].files;
    
    			var formData = form_data;
    			
    			for (var i = 0; i < files.length; i++) {
    
    				var file = files[i];
                    
    				if(target == 'media'){
    					
    					fileMedia.push(file);
    
    					formData.append('media[]', file, file.name);
    
    				}else{
    
    					fileAttachment.push(file);
    
    					formData.append('attachment[]', file, file.name);
    
    				}
    
    				let lines = `
    					<div id="file_`+ i +`">` + file.name + `&nbsp;&nbsp;<span class="fa fa-times-circle fa-md removeLine" data-index="`+ i +`" data-target="`+ target +`" title="remove"></span></div>`;
    					
    				$('#filelist_'+ target).append(lines);
    
    			}
    		});
    	});
    
    
    	// Remove attachment
    
    	$(document).on('click', '.removeLine', function(){
    		console.log(form_data);
    		let target = $(this).attr('data-target');
    
    		var index  = $(this).attr('data-index');
    
    		$(this).closest('div').remove(); 
    		
    		if(target == 'media'){
    			
    			form_data.delete('media[]');
    
    			delete fileMedia[index];
    			
    			for(let key in fileMedia){
    				form_data.append('media[]', fileMedia[key], fileMedia[key]['name']);
    			}
    			console.log(fileMedia);
    		}else{
    
    			form_data.delete('attachment[]');
    
    			delete fileAttachment[index];
    			
    			for(let key in fileAttachment){
    				form_data.append('attachment[]', fileAttachment[key], fileAttachment[key]['name']);
    			}
    			console.log(fileAttachment);
    		}
    		
    	});
    	//submit form modified
        //$(document).on('click', '#update-lesson', function(){
    	$('#update-lesson').click(function(){	
            let form_data2 = new FormData($('#update-lesson')[0]);
    
    
            for (var pair of form_data2.entries()) {
                form_data.append(pair[0], pair[1]);
            }

    		let action = $('#lesson-form').attr('action');
    		$.ajax({
    			url: action, 
    			type: 'POST',
    			dataType: 'json',
    			data: form_data,
    			cache: false,
    			contentType: false,
    			processData: false,
    			beforeSend: function(){
    
    				$('.form-text').text('');
    				$('.form-proccessing').removeClass('hidden');
    
    			},
    			success: function(response){
    
    				window.location.replace(response.redirect);
    
    				$('#'+ form).find('.form-proccessing').addClass('hidden');
    
    			},
    			error: function(json){
    
    				if(json.status === 422){
    
    					$('.form-proccessing').addClass('hidden');
    
    					$.each(json.responseJSON.errors, function (key, value) {
    						$('#error-'+ key).text(value);
    					});
    
    				}
    			}
    		});
    	});
    
    
    });
    
</script>
<script>
    $(document).ready(function(){
    	let chat_flag = '{{$templates->chat_flag}}';
    	let lesson_id = '{{$templates->id}}';
    	let _token = $('meta[name="csrf-token"]').attr('content');
    	console.log(chat_flag);
    	if(chat_flag == 1){
    		$("#comments").css("display", "block");
    		
    	}else{
    		$("#comments").css("display", "none");
    		
        }
        $(document).on('click', '.expander', function(){
                $('#listen-div').is(":visible") ? $('#btn-control').text("開く") : $('#btn-control').text("閉じる");
                $('#listen-div').slideToggle();
               
        });
         
    
    });
</script>
@endsection