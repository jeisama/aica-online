@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-select/bootstrap-select.css') }}">
@endsection

@section('content')
	<div class="breadcrumb-holder container-fluid">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i> Home</a></li>
            <li class="breadcrumb-item"><a href="{{ url('/lessons') }}">Lessons</a></li>
            <li class="breadcrumb-item active">Create Lesson</li>
        </ul>
    </div>

    <section class="forms">
    	<div class="container-fluid">

    		@if (Session::has('success_message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {!! session('success_message') !!}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif

            @if (Session::has('error_message'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {!! session('error_message') !!}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif

			<div class="card">
				<div class="card-header d-flex align-items-center">
					<h3 class="h4">Lesson Details</h3>
				</div>

				<div class="card-body">
					{{ Form::open(['files' => true]) }}
					<div class="form-row">
						<div class="form-group col col-md-12 col-lg-8">
							<label for="lesson_name" class="form-control-label">Lesson Name</label>
							<input type="text" id="lesson_name" name="lesson_name" value="{{ old('lesson_name') }}" class="form-control">
							@if($errors->has('lesson_name'))
                                <div class="invalid-feedback">{{ $errors->first('lesson_name') }}</div>
                            @endif
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col col-md-12 col-lg-4">
							<label for="grade_level" class="form-control-label">Grade Level</label>
							<select id="grade_level" name="grade_level" class="form-control">
								<option value="">Select Grade</option>
								@if($grade_level !=null)
								@foreach($grade_level as $grade)
								<option value="{{ $grade->id }}" {{ old('grade_level') == $grade->id ? 'selected':'' }}>{{ $grade->label }}</option>
								@endforeach
								@endif
							</select>
							@if($errors->has('grade_level'))
                                <div class="invalid-feedback">{{ $errors->first('grade_level') }}</div>
                            @endif
						</div>
						<div class="form-group col col-md-12 col-lg-4">
							<label for="class_time" class="form-control-label">Class Time</label>
							<select id="class_time" name="class_time" class="selectpicker" data-live-search="true">
								<option value="">Select Time</option>
								@if($class_time != null)
								@foreach($class_time as $time)
								<option value="{{ $time->id }}" {{ $schedule == $time->id ? 'selected':'' }} {{ old('class_time') == $time->id ? 'selected':'' }}>{{ $time->date .' ('. date('H:i A', strtotime($time->time_in)) .' - '. date('H:i A', strtotime($time->time_out)) .')' }}</option>
								@endforeach
								@endif
							</select>
							@if($errors->has('class_time'))
                                <div class="invalid-feedback">{{ $errors->first('class_time') }}</div>
                            @endif
						</div>
					</div>
					<div class="form-group">
						<label class="description">Description</label>
						<textarea class="ckeditor" id="description" name="description" rows="20">{{ old('description') }}</textarea>
						@if($errors->has('description'))
                            <div class="invalid-feedback">{{ $errors->first('description') }}</div>
                        @endif
					</div>

					<div class="form-row">
						<div class="form-group col col-md-12 col-lg-4">
							<label for="attachment" class="form-control-label">Attachments</label>
							<div>
							<input type="file" name="attachment[]" id="attachment" multiple>
							</div>
						</div>
					</div>
					<br>
					
					<div class="form-row">
						<div class="form-group col-lg-12">
							<label for="attachment" class="form-control-label">Select Audio (mp3)</label>
							<div>
								<input type="file" name="media[]" id="attachment" multiple accept="audio/*">
							</div>
						</div>
					</div>

					<div class="line"></div>

					<div class="form-group">
						<button type="submit" class="btn btn-primary">Save Lesson</button>
						<a href="{{ url('lessons') }}" class="btn btn-default">Cancel</a>
					</div>
					{{ Form::close() }}
				</div>
			</div>
    	</div>
    </section>
@endsection

@section('js')
	<script src="{{ asset('assets/vendor/bootstrap-select/bootstrap-select.min.js') }}"></script>
	<script src="{{ asset('assets/vendor/ckeditor/ckeditor.js') }}"></script>
	<script>
		$(document).ready(function(){
			$(document).on('click', '.add_more', function(){
				$(this).remove();
				let row = `<div class="form-row">
							<div class="form-group col col-lg-3">
								<label for="source0" class="form-control-label">Source</label>
								<select id="source0" name="source[]" class="form-control select_source">
									<option value="">Select Source</option>
									<option value="local">Local</option>
									<option value="youtube">Youtube</option>
								</select>
							</div>
							<div class="form-group col col-lg-7">
								
								<div class="local hidden" style="padding-top: 30px;">
									<div class="custom-file">
									  	<input type="file" class="custom-file-input" name="media[]" id="media0">
									  	<label class="custom-file-label" for="media0">Select Media</label>
									</div>
								</div>
								
								<div class="youtube hidden">
									<label for="youtube0" class="form-control-label">Enter youtube link.</label>
									<input type="text" name="youtube[]" class="form-control">
								</div>
							</div>
							<div class="form-group col col-lg-1" style="padding-top: 30px;">
								<button type="button" class="btn btn-default btn-block hidden add_more">Add more.</button>
							</div>
						</div>`;

				$('#media_content').append(row);
			});

			$(document).on('change', '.select_source', function(){
				let val = $('option:selected', this).val();
				
				if(val != ''){
					if(val == 'local'){
						$(this).closest('.form-row').find('.local').removeClass('hidden');
						$(this).closest('.form-row').find('.youtube').addClass('hidden');
					}
					if(val == 'youtube'){
						$(this).closest('.form-row').find('.local').addClass('hidden');
						$(this).closest('.form-row').find('.youtube').removeClass('hidden');
					}
					$(this).closest('.form-row').find('.btn').removeClass('hidden');
				}else{
					$(this).closest('.form-row').find('.btn').addClass('hidden');
					$(this).closest('.form-row').find('.local').addClass('hidden');
					$(this).closest('.form-row').find('.youtube').addClass('hidden');
				}
			});
		});
	</script>
@endsection