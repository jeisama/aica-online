@extends('layouts.app')

@section('css')
<style>
img {
    border-radius: 50%;
}

</style>
@endsection

@section('content')

    <div class="breadcrumb-holder container-fluid">
        <ul class="breadcrumb">
            
            <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i> Home</a></li>
            <li class="breadcrumb-item"><a href="{{ url('lessons') }}">Lessons</a></li>
            <li class="breadcrumb-item active">{{ $lesson->title }}</li>
            
        </ul>
      
    </div>
    <section class="forms lessons">
        <div class="container-fluid">
            <div class="card">
                <div class="card-body" style="display: relative;">
                    @if (Auth::user()->role !== '5')
                        <a class="btn btn-primary float-right" href="{{ url('lessons/conference/'. $lesson->slug)}}" target="_blank">Join Video Lesson</a>
                    @else
                    <a class="btn btn-primary float-right" href="{{ url('lessons/conference/'. $lesson->slug)}}" target="_blank" id="lesson_start" data-id="{{$lesson->schedule_id}}">Start Video Lesson</a>
                    @endif
                    <div class="lesson-title">Lesson: {{ $lesson->title }}</div>
                    <div class="teacher_date">
                        <div class="avatar">
                            <span class="inner">
                                @if($lesson->user_data->avatar == null || !File::exists('uploads/avatar/'. $lesson->user_data->avatar))
                                    <img src="{{ asset('uploads/avatar/noavatar.jpg') }}">
                                @else
                                    <img src="{{ asset('uploads/avatar/'. $lesson->user_data->avatar) }}">
                                @endif
                            </span>
                        </div>
                        <div class="name">
                            {{ $lesson->teacher->full_name }} &nbsp;&nbsp;
                            <i class="mdi mdi-calendar-range"></i> {{ date('l, F d Y H:i', strtotime($lesson->schedule->date .' '.$lesson->schedule->time_in)) }}
                        </div>
                    </div>
                    @php
                        $date = \App\Helpers\DateCarbon::afterClass($lesson->schedule->date .' '.$lesson->schedule->time_in, 15) ;
                        
                    @endphp
                    {{-- start of new form --}}
                   <div class="homework-box" style="border: 1px solid #c5c5c5; padding: 15px; width: 95%;">
                    {{ Form::open(['url' => 'lessons/save-answer']) }}
                    <input type="hidden" name ="lesson_id" value="{{ $lesson->id }}">
					<div class="tbl" style="display: table; width: 100%;">
							<div style="display: table-cell; vertical-align: top; width:30px;"><strong>1</strong></div>
								<div style="display: table-cell; vertical-align: top;">
									<div class="form-row">
										<div class="form-group col col-md-12 col-lg-11">
											<label for="writing_desc" class="form-control-label"><strong>Writing課題　（目安：10分）</strong></label>
											<p>{{ $lesson->write_instruction }}</p>
										</div>
									</div>
							</div>
						</div>
							
						<div class="tbl" style="display: table; width: 100%;">
							<div style="display: table-cell; vertical-align: top; width:30px;"><strong>2</strong></div>
								<div style="display: table-cell; vertical-align: top;">
									<div class="form-row">
										<div class="form-group col col-md-12 col-lg-11">
											<label for="speaking_instruction" class="form-control-label"><strong>Speaking課題　（目安：5分）</strong></label>
											<p>{{ $lesson->speaking_instruction }}</p>
										</div>
										<div class="form-group col col-md-12 col-lg-11" style="border: 1px solid #000;">
											<p><strong>次週のSpeaking Topic:</strong></p>
											<p>{{ $lesson->speaking_instruction }}</p>
										</div>
									</div>
							</div>
						</div>
						<div class="tbl" style="display: table; width: 100%;">
							<div style="display: table-cell; vertical-align: top; width:30px;"><strong>3</strong></div>
								<div style="display: table-cell; vertical-align: top;">
									<div class="form-row">
										<div class="form-group col col-md-12 col-lg-11">
                                            <label for="writing_desc_stu" class="form-control-label"><strong>{{ $lesson->writing_exercise }}</strong></label>
                                            
											<textarea rows="4" cols="50" id="writing_exercise_stu" name="writing_exercise_stu" class="form-control"  value="{{ $lesson->writing_exercise_stu }}"
											{{Auth::user()->role != '4'  ? 'disabled' : ' ' }}>{{ $lesson->writing_exercise_stu }}</textarea>
										</div>
										
									</div>
							</div>
						</div>
						<div class="tbl" style="display: table; width: 100%;">
							<div style="display: table-cell; vertical-align: top; width:30px;"><strong>4</strong></div>
								<div style="display: table-cell; vertical-align: top;">
									<div class="form-row">
										<div class="form-group col col-md-12 col-lg-11">
											<label for="listening_instruction" class="form-control-label"><strong>Listening課題　（目安：15分）</strong></label>
											<p>{{ $lesson->listening_instruction }}</p>
											{{-- <label for="reproduction_desc" class="form-control-label">今週のReproduction課題：</label>
											<textarea id="reproduction_exercise" name="reproduction_exercise" class="form-control" value="{{ $lesson->listening_instruction }}" >{{ $lesson->reproduction_exercise }}</textarea> --}}
                                        </div>
                                        <div class="form-group col col-md-12 col-lg-11" style="border: 1px solid #000;">
                                            <p><strong>今週のReproduction課題：</strong></p>
                                            <p>{{ $lesson->reproduction_exercise }}</p>
                                        </div>
										
									</div>
							</div>
						</div>
						<div class="tbl" style="display: table; width: 100%;">
								<div style="display: table-cell; vertical-align: top; width:30px;"><strong>5</strong></div>
									<div style="display: table-cell; vertical-align: top;">
										<div class="form-row">
											<div class="form-group col col-md-12 col-lg-11">
												<label for="homework_instruction" class="form-control-label">{{ $lesson->homework_instruction }}</label>
                                            </div>
                                            <div class="form-group col col-md-12 col-lg-11" style="border: 1px solid #000;">
                                                    <p><strong>今週のListening課題：</strong></p>
                                                    <textarea rows="4" cols="50" id="homework_listening" name="homework_listening" class="form-control" {{Auth::user()->role != '4'  ? 'disabled' : ' ' }}>{{ $lesson->homework_listening }}</textarea>
                                                    
                                            </div>
											
										</div>
								</div>
							</div> 
							{{-- END of new form --}}
                        
                        <div class="form-group">
                            @if (Auth::user()->role == 4 && ($lesson->homework_listening == null || $lesson->writing_exercise_stu == null))
                                <button type="submit" class="btn btn-primary">Save Changes</button>
                                <a href="{{ url('lessons') }}" class="btn btn-default">Cancel</a>
                            @endif
                               
                        </div>   
                    {{Form::close()}}
                    </div>

                   @if (($date && Auth::user()->role == '4') || (Auth::user()->role != '4'))
                   
                            @if(count($lesson->attachment) > 0)
                                <br>
                            <div class="lesson-title">レッスンの添付ファイル</div>
                                @foreach($lesson->attachment as $attachment)
                            <div>
                            <a href="{{ asset('uploads/lessons/'. $attachment->attachment) }}" target="_blank">{{ $attachment->original_name }}</a></div>
                                @endforeach
                            @endif 
                            <br>
                            @if(count($chats) > 0)        
                            <br>
                           <div class="lesson-title">チャート履歴</div>
                           <div class="comments" id="comments" style="overflow: auto; height: 350px;">  
                                @foreach ($chats as $chat)
                                    <div class="comment-data">  
                                        <div class="comment-avatar">
                                            @if($chat->avatar == null || !File::exists('uploads/avatar/'. $chat->avatar))
                                                <img src="{{ asset('uploads/avatar/noavatar.jpg') }}"  width="35" height="35">
                                            @else
                                                <img src="{{ asset('uploads/avatar/'. $chat->avatar) }}" width="35" height="35">
                                            @endif
                                        </div>
                                        <div class="comment-body">
                                                <div class="name">{{ $chat->username }}</div>
                                                <div class="carbon">{{ \App\Helpers\DateCarbon::date($chat->created_at) }}</div>
                                                <div class="note">
                                                        {!! nl2br(e($chat->message)) !!}
                                                </div>
                                            </div> 
                                    </div>
                                @endforeach
                           </div>
                           @endif
                   @endif
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
<script>
    $(document).ready(function(){
        let baseUrl   = $('base').attr('href');
        let _token = $('meta[name="csrf-token"]').attr('content');
        let lessonID = $('#lesson_start').attr('data-id');
        let user = {{Auth::user()->role}};
        console.log("user " , user);
        if(user != '3' ){
            $("#btnListen").css({"display":"none"});
            $("#btnWrite").css({"display":"none"});
        }
        $('#student').change(function(){
            let value = $('option:selected', this).val();
            if(value != ''){
                $('form#student_form').submit();
            }
        });
        $(document).click(function(){ 

        });

        $('.submit').each(function(){
            $(this).click(function(){
                let target = $(this).attr('data-target');
                let input = $('#'+ target).val();
                console.log("input ", input);

            });
        });
        $( "#lesson_start" ).click(function( event ) {
                //event.preventDefault();
                 $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: baseUrl +'/lessons/video-start-email',
                    data: {
                        _token: _token,
                        lesson_id: lessonID
                    },success:function(response){
                        console.log(response);
                    }
                });
        });
       

    });
</script>
@endsection