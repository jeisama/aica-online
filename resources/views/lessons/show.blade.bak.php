@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-select/bootstrap-select.css') }}">
@endsection
@section('content')
{{-- 
<div class="breadcrumb-holder container-fluid">
    <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i> Home</a></li>
        <li class="breadcrumb-item"><a href="{{ url('/lessons') }}">Lessons</a></li>
        <li class="breadcrumb-item active">Details Lesson</li>
    </ul>
</div>
--}}
<div class="breadcrumb-holder container-fluid">
    <div class="inner">
        <div class="links">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i> Home</a></li>
                <li class="breadcrumb-item"><a href="{{ url('/lessons') }}">Lessons</a></li>
                <li class="breadcrumb-item active">{{$templates->lesson_name}}</li>
            </ul>
        </div>
        @if (env('APP_ENV') == 'production')
        @if (strtotime(date('Y-m-d H:i:s')) > strtotime($templates->date . $templates->start_time) && strtotime(date('Y-m-d H:i:s')) < strtotime($templates->date . $templates->end_time) )
        <div class="buttons">
            <a href="{{ url('lessons/conference/'.$templates->token) }}" class="btn btn-primary btn-sm" target="_blank"> {{ Auth::user()->role == student ? 'Join Video':'Start Video' }} </a>
        </div>
        @endif
        @else
        <div class="buttons">
            <a href="{{ url('lessons/conference/'.$templates->token) }}" class="btn btn-primary btn-sm" target="_blank"> {{ Auth::user()->role == student ? 'Join Video':'Start Video' }}</a>
        </div>
        @endif
    </div>
</div>
<section class="forms">
    <div class="container-fluid">
    @if (Session::has('success_message'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {!! session('success_message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    @if (Session::has('error_message'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {!! session('error_message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <div class="card">
        <div class="card-header d-flex align-items-center">
            <h3 class="h4">Lesson Details</h3>
        </div>
        <div class="card-body">
            {{ Form::open(['files' => true,  'id' => 'thread-lesson', 'onsubmit' => 'event.preventDefault()']) }}	
            {{-- start of new form --}}
            <input type="hidden" name="course_type" value="{{$templates->course_type}}">
            <input type="hidden" name="lesson_type" value="{{$templates->lesson_type}}">
            <input type="hidden" name="form_type" value="{{$templates->form_type}}">
            <input type="hidden" name="lesson_id" value="{{$templates->id}}">
            <input type="hidden" name="created_by" value="{{$templates->created_by}}">
            <div class="row">
                <div class="col-sm-5 col-md-5">
                    <dl class="row">
                        <dt class="col-sm-3">Course :</dt>
                        <dd class="col-sm-8">{{$templates->course_label}}</dd>
                        <dt class="col-sm-3">Day :</dt>
                        <dd class="col-sm-8">{{$templates->lesson_day}}</dd>
                        @if (Auth::user()->role == student)
                        <dt class="col-sm-3">Teacher:</dt>
                        <dd class="col-sm-8">{{$templates->full_name}}</dd>
                        @elseif(Auth::user()->role == teacher)
                        <dt class="col-sm-3">Student:</dt>
                        <dd class="col-sm-8">{{$templates->romaji_name}}</dd>
                        @endif
                    </dl>
                </div>
                <div class="col-sm-7 col-md-7">
                    <dl class="row">
                        <dt class="col-sm-3">Grade :</dt>
                        <dd class="col-sm-8">{{$templates->grade_label}}</dd>
                        <dt class="col-sm-3">Lesson Name :</dt>
                        <dd class="col-sm-8" id="val_lesson_name">{{$templates->lesson_name}}</dd>
                        <dt class="col-sm-3">Date :</dt>
                        <dd class="col-sm-8" id="val_lesson_name">{{ date('l, Y/m/d H:i', strtotime($templates->date . $templates->start_time)) }}</dd>
                    </dl>
                </div>
            </div>
            @if ($templates->form_type == 1)
            <div class="homework-box" style="border: 1px solid #c5c5c5; padding: 15px; width: 100%;">
                <div class="tbl" style="display: table; width: 100%;">
                    <div style="display: table-cell; vertical-align: top; width:30px;"><strong>1</strong></div>
                    <div style="display: table-cell; vertical-align: top;">
                        <div class="form-row">
                            <div class="form-group col col-md-12 col-lg-12">
                                <label for="" class="form-control-label"><strong>Speaking 課題</strong></label>
                                <p>次回の授業内で以下のトピックについて先生から質問があります。自分の意見と、何故そのように考えたのかという理由を下の枠内にまとめておきましょう。<br>
                                    <small>（わからない単語や表現は辞書などで事前に調べましょう！そうすることで、皆さんの表現の幅が広がります。）</small>
                                </p>
                                <label for="speaking_topic" class="form-control-label"><strong>次週のSpeaking Topic:</strong></label>
                                <div style="border:solid 1px #000; padding:10px;">
                                    <p>{!! nl2br($templates->speaking_topic) !!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--teacher/student ans -->
                @if (Auth::user()->role == student)
                <div class="tbl" style="display: table; width: 100%;">
                    <div style="display: table-cell; vertical-align: top; width:30px;"><strong>2</strong></div>
                    <div style="display: table-cell; vertical-align: top;">
                        <div class="form-row">
                            <div class="form-group col col-md-12 col-lg-12">
                                <label for="writing_stu_ans" class="form-control-label"><strong>2～3文程度で自分の意見と、理由をまとめよう！</strong></label><br>
                                <label for="writing_stu_ans" class="form-control-label"><strong>生徒回答場所</strong></label><br>
                                @if ($answers)
                                <div style="border:solid 1px #000; padding: 10px;">
                                    <p style="text-align: left;">{!! nl2br($answers->writing_stu_ans) !!}</p>
                                </div>
                                <br>
                                <label for="writing_tea_res" class="form-control-label"><strong>教師添削後返却場所</strong></label>
                                @if (!$answers->writing_tea_res)
                                <textarea rows="4" cols="50" id="writing_tea_res" name="writing_tea_res" class="form-control"
                                {{Auth::user()->role == teacher && $answers ? ' ' : 'disabled'}}></textarea>
                                @else
                                <div style="border:solid 1px #000; padding: 10px;">
                                    <p style="text-align: left;">{!! nl2br($answers->writing_tea_res) !!}</p>
                                </div>
                                @endif
                                @else
                                <textarea rows="4" cols="50" id="writing_stu_ans" name="writing_stu_ans" class="form-control" {{Auth::user()->role == student ? ' ' : 'disabled'}}> </textarea>
                                <br> 
                                <label for="writing_tea_res" class="form-control-label"><strong>教師添削後返却場所</strong></label><br>
                                <textarea rows="4" cols="50" id="writing_tea_res" name="writing_tea_res" class="form-control"
                                {{Auth::user()->role == teacher && $answers ? ' ' : 'disabled'}}></textarea>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @elseif(Auth::user()->role == teacher)
                <div class="tbl" style="display: table; width: 100%;">
                    <div style="display: table-cell; vertical-align: top; width:30px;">
                        <strong>2</strong>
                    </div>
                    <div style="display: table-cell; vertical-align: top;">
                        <div class="form-row">
                            <div class="form-group col col-md-12 col-lg-12">
                                <label for="writing_stu_ans" class="form-control-label"><strong>2～3文程度で自分の意見と、理由をまとめよう！</strong></label><br>
                                <label for="writing_stu_ans" class="form-control-label"><strong>生徒回答場所</strong></label><br>
                                @if($answers)
                                <div style="border:solid 1px #000; padding: 10px;">
                                    <p style="text-align: left;">{!! nl2br($answers->writing_stu_ans) !!}</p>
                                </div>
                                @else
                                <textarea rows="4" cols="50" id="writing_stu_ans" name="writing_stu_ans" class="form-control" {{Auth::user()->role == student ? ' ' : 'disabled'}}> </textarea>
                                @endif
                                <br>
                                <label for="writing_tea_res" class="form-control-label"><strong>教師添削後返却場所</strong></label>
                                @if ($answers && $answers->writing_tea_res)
                                <div style="border:solid 1px #000; padding: 10px;">
                                    <p style="text-align: left;">{!! nl2br($answers->writing_tea_res) !!}</p>
                                </div>
                                @else
                                <textarea rows="4" cols="50" id="writing_tea_res" name="writing_tea_res" class="form-control"
                                {{Auth::user()->role == teacher && $answers ? ' ' : 'disabled'}}></textarea>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                <!--teacher student ans -->
                <div class="tbl" style="display: table; width: 100%;">
                    <div style="display: table-cell; vertical-align: top; width:30px;"><strong>3</strong></div>
                    <div style="display: table-cell; vertical-align: top;">
                        <div class="form-row">
                            <div class="form-group col col-md-12 col-lg-12">
                                <label for="listening" class="form-control-label"><strong>Listening 課題（目安：15分）</strong></label>
                                <p>まずは音声のみで理解できるかトライしてみましょう。</p>
                                <div style="border:solid 1px #000; padding:10px;">
                                    <div class="form-group col-lg-12">
                                        @if (strtotime(date('Y-m-d H:i:s')) > strtotime($templates->date . $templates->end_time) && (Auth::user()->role == student)) 
                                        @if($media)
                                        @foreach($media as $audio)
                                        <div><a href="{{asset('uploads/video/'. $audio->attachment)}}" target="_blank"> {{ $audio->original_name ?? $audio->attachment }}</a> </div>
                                        @endforeach
                                        @endif
                                        @elseif(Auth::user()->role != student) 
                                        @if($media)
                                        @foreach($media as $audio)
                                        <div><a href="{{asset('uploads/video/'. $audio->attachment)}}" target="_blank"> {{ $audio->original_name ?? $audio->attachment }}</a> </div>
                                        @endforeach
                                        @endif
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tbl" style="display: table; width: 100%;">
                    <div style="display: table-cell; vertical-align: top; width:30px;"><strong>4</strong></div>
                    <div style="display: table-cell; vertical-align: top;">
                        <div class="form-row">
                            <div class="form-group col col-md-12 col-lg-12">
                                <p>聞き取れなかった部分は以下のスクリプトを見て確認しましょう。何度も繰り返し聞いて、音声とスクリプトが一致するようになったら、音声の後に続いて自分で読む練習をしましょう。音声どおり読めるようになったか次回授業でチェックを行います。<br>
                                    <small>（スクリプトを見ても意味がわからない表現があれば、辞書などで調べましょう。同時にreadingの力も身につきます。）</small>
                                </p>
                                <p class="text-center">------------------------------------ 展開・省略 ------------------------------------</p>
                                <label for="next_listening" class="form-control-label"><strong>今週のListening 課題：</strong></label>
                                <div style="border:solid 1px #000; padding:10px;">
                                    <p>{!! nl2br($templates->next_listening) !!}</p>
                                </div>
                                <small id="error-next_listening" class="form-text form-error"></small>
                            </div>
                        </div>
                    </div>
                </div>
                <!--file lists -->
                <div class="tbl" style="display: table; width: 100%;">
                    <div style="display: table-cell; vertical-align: top; width:30px;"><strong></strong></div>
                    <div style="display: table-cell; vertical-align: top;">
                        <div class="form-row">
                            <div class="form-group col col-md-12 col-lg-12">
                                <label for="attachment" class="form-control-label"><strong>レッスン添付ファイル</strong></label><br>
                                <div style="border:solid 1px #000; padding:10px;">
                                    <div class="form-group col-lg-12">
                                        @if (strtotime(date('Y-m-d H:i:s')) > strtotime($templates->date . $templates->end_time) && (Auth::user()->role == student)) 
                                        @if($attachment)
                                        @foreach($attachment as $file)
                                        <div><a href="{{asset('uploads/lessons/'. $file->attachment)}}" target="_blank"> {{ $file->original_name ?? $file->attachment }}</a> </div>
                                        @endforeach
                                        @endif
                                        @elseif(Auth::user()->role != student) 
                                        @if($attachment)
                                        @foreach($attachment as $file)
                                        <div><a href="{{asset('uploads/lessons/'. $file->attachment)}}" target="_blank"> {{ $file->original_name ?? $file->attachment }}</a> </div>
                                        @endforeach
                                        @endif
                                        @endif											
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--- chat display -->
                <div class="tbl" style="display: table; width: 100%;">
                    <div style="display: table-cell; vertical-align: top; width:30px;"><strong></strong></div>
                    <div style="display: table-cell; vertical-align: top;">
                        <div class="lesson_comments" id="comments" style="overflow: auto; height: 350px;">
                            <label for="listening" class="form-control-label"><strong>チャット履歴</strong></label>	
                            @if($chats)
                            @foreach($chats as $chat)
                            @if($chat->message)
                            <div class="comment-data">
                                <div class="comment-avatar">
                                    @if($chat->user->avatar == null || !File::exists('uploads/avatar/'. $chat->user->avatar))
                                    <img src="{{ asset('uploads/avatar/noavatar.jpg') }}"  width="35" height="35">
                                    @else
                                    <img src="{{ asset('uploads/avatar/'. $chat->user->avatar) }}" width="35" height="35">
                                    @endif
                                </div>
                                <div class="comment-body">
                                    <div class="name">{{ $chat->user->username }} <small>{{ \App\Helpers\DateCarbon::date($chat->created_at) }}</small></div>
                                    <div class="note">
                                        {!! nl2br(e($chat->message)) !!}
                                    </div>
                                </div>
                            </div>
                            @endif
                            @endforeach
                            @endif
                        </div>
                    </div>
                </div>
                <!--- chat display -->
                <!--file lists -->
                <!--upload files -->
                @if ((Auth::user()->role == teacher) && !($answers && $answers->writing_tea_res && $answers->writing_stu_ans)) 
                <div class="tbl" style="display: table; width: 100%;">
                    <div style="display: table-cell; vertical-align: top; width:30px;"><strong></strong></div>
                    <div style="display: table-cell; vertical-align: top;">
                        <div class="form-row">
                            <div class="form-group col col-md-12 col-lg-12">
                                <div style="padding:10px;">
                                    <div class="form-row">
                                        <div class="form-group col col-md-12 col-lg-4">
                                            <label for="attachment" class="form-control-label"><strong>音声添付ファイル</strong></label><br>
                                            <input type="file" name="media[]" id="attachment" multiple accept="audio/*">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col col-md-12 col-lg-4">
                                            <label for="attachment" class="form-control-label"><strong>レッスン添付ファイル</strong></label><br>
                                            <input type="file" name="attachment[]" id="attachment" multiple>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                <!--upload files -->
                @endif
                @if ($templates->form_type == 2)
                <div class="homework-box" style="border: 1px solid #c5c5c5; padding: 15px; width: 100%;">
                    <!--teacher/student ans -->
                    @if (Auth::user()->role == student)
                    <div class="tbl" style="display: table; width: 100%;">
                        <div style="display: table-cell; vertical-align: top; width:30px;"><strong>1</strong></div>
                        <div style="display: table-cell; vertical-align: top;">
                            <div class="form-row">
                                <div class="form-group col col-md-12 col-lg-12">
                                    <label for="writing_stu_ans" class="form-control-label"><strong>この1週間の出来事を書きましょう。何をやったのかという文章と、更に内容を詳しくする文
                                    章を最低でも1文ずつ書きましょう。作った文章を次回の授業の中で発表しましょう。</strong></label><br>
                                    <label for="writing_stu_ans" class="form-control-label"><strong>生徒回答場所</strong></label>
                                    @if ($answers)
                                    <div style="border:solid 1px #000; padding: 10px;">
                                        <p style="text-align: left;">{!! nl2br($answers->writing_stu_ans) !!}</p>
                                    </div>
                                    <br>
                                    <label for="writing_tea_res" class="form-control-label"><strong>教師添削後返却場所</strong></label>
                                    @if (!$answers->writing_tea_res)
                                    <textarea rows="4" cols="50" id="writing_tea_res" name="writing_tea_res" class="form-control"
                                    {{Auth::user()->role == teacher && $answers ? ' ' : 'disabled'}}></textarea>
                                    @else
                                    <div style="border:solid 1px #000; padding: 10px;">
                                        <p style="text-align: left;">{!! nl2br($answers->writing_tea_res) !!}</p>
                                    </div>
                                    @endif
                                    @else
                                    <textarea rows="4" cols="50" id="writing_stu_ans" name="writing_stu_ans" class="form-control" {{Auth::user()->role == student ? ' ' : 'disabled'}}> </textarea>
                                    <br> 
                                    <label for="writing_tea_res" class="form-control-label"><strong>教師添削後返却場所</strong></label><br>
                                    <textarea rows="4" cols="50" id="writing_tea_res" name="writing_tea_res" class="form-control"
                                    {{Auth::user()->role == teacher && $answers ? ' ' : 'disabled'}}></textarea>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    @elseif(Auth::user()->role == teacher)
                    <div class="tbl" style="display: table; width: 100%;">
                        <div style="display: table-cell; vertical-align: top; width:30px;">
                            <strong>1</strong>
                        </div>
                        <div style="display: table-cell; vertical-align: top;">
                            <div class="form-row">
                                <div class="form-group col col-md-12 col-lg-12">
                                    <label for="writing_stu_ans" class="form-control-label"><strong>この1週間の出来事を書きましょう。何をやったのかという文章と、更に内容を詳しくする文
                                    章を最低でも1文ずつ書きましょう。作った文章を次回の授業の中で発表しましょう。</strong></label><br>
                                    <label for="writing_stu_ans" class="form-control-label"><strong>生徒回答場所</strong></label><br>
                                    @if($answers)
                                    <div style="border:solid 1px #000; padding: 10px;">
                                        <p style="text-align: left;">{!! nl2br($answers->writing_stu_ans) !!}</p>
                                    </div>
                                    @else
                                    <textarea rows="4" cols="50" id="writing_stu_ans" name="writing_stu_ans" class="form-control" {{Auth::user()->role == student ? ' ' : 'disabled'}}> </textarea>
                                    @endif
                                    <br>
                                    <label for="writing_tea_res" class="form-control-label"><strong>教師添削後返却場所</strong></label>
                                    @if ($answers && $answers->writing_tea_res)
                                    <div style="border:solid 1px #000; padding: 10px;">
                                        <p style="text-align: left;">{!! nl2br($answers->writing_tea_res) !!}</p>
                                    </div>
                                    @else
                                    <textarea rows="4" cols="50" id="writing_tea_res" name="writing_tea_res" class="form-control"
                                    {{Auth::user()->role == teacher && $answers ? ' ' : 'disabled'}}></textarea>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    <!--teacher student ans -->
                    <div class="tbl" style="display: table; width: 100%;">
                        <div style="display: table-cell; vertical-align: top; width:30px;"><strong>2</strong></div>
                        <div style="display: table-cell; vertical-align: top;">
                            <div class="form-row">
                                <div class="form-group col col-md-12 col-lg-12">
                                    <label for="listening" class="form-control-label"><strong>Listening 課題（目安：15分）</strong></label>
                                    <p>まずは音声のみで理解できるかトライしてみましょう。</p>
                                    <div style="border:solid 1px #000; padding:10px;">
                                        <div class="form-group col-lg-12">
                                            @if (strtotime(date('Y-m-d H:i:s')) > strtotime($templates->date . $templates->end_time) && (Auth::user()->role == student)) 
                                            @if($media)
                                            @foreach($media as $audio)
                                            <div><a href="{{asset('uploads/video/'. $audio->attachment)}}" target="_blank"> {{ $audio->original_name ?? $audio->attachment }}</a> </div>
                                            @endforeach
                                            @endif
                                            @elseif(Auth::user()->role != student) 
                                            @if($media)
                                            @foreach($media as $audio)
                                            <div><a href="{{asset('uploads/video/'. $audio->attachment)}}" target="_blank"> {{ $audio->original_name ?? $audio->attachment }}</a> </div>
                                            @endforeach
                                            @endif
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tbl" style="display: table; width: 100%;">
                        <div style="display: table-cell; vertical-align: top; width:30px;"><strong>3</strong></div>
                        <div style="display: table-cell; vertical-align: top;">
                            <div class="form-row">
                                <div class="form-group col col-md-12 col-lg-12">
                                    <p>聞き取れなかった部分は以下のスクリプトを見て確認しましょう。何度も繰り返し聞いて、
                                        音声とスクリプトが一致するようになったら、音声の後に続いて自分で読む練習をしましょ
                                        う。音声どおり読めるようになったか次回授業でチェックを行います。<br>
                                        <small>（スクリプトを見ても意味がわからない表現があれば、辞書などで調べましょう。同時にreadingの力も身につきます。）</small>
                                    </p>
                                    <p class="text-center">------------------------------------ 展開・省略 ------------------------------------</p>
                                    <label for="next_listening" class="form-control-label"><strong>今週のListening 課題：</strong></label>
                                    <div style="border:solid 1px #000; padding:10px;">
                                        <div class="form-group col-lg-12">
                                            {{$templates->next_listening}}
                                        </div>
                                    </div>
                                    <small id="error-next_listening" class="form-text form-error"></small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--file lists -->
                    <div class="tbl" style="display: table; width: 100%;">
                        <div style="display: table-cell; vertical-align: top; width:30px;"><strong></strong></div>
                        <div style="display: table-cell; vertical-align: top;">
                            <div class="form-row">
                                <div class="form-group col col-md-12 col-lg-12">
                                    <label for="attachment" class="form-control-label"><strong>レッスン添付ファイル</strong></label><br>
                                    <div style="border:solid 1px #000; padding:10px;">
                                        <div class="form-group col-lg-12">
                                            @if (strtotime(date('Y-m-d H:i:s')) > strtotime($templates->date . $templates->end_time) && (Auth::user()->role == student)) 
                                            @if($attachment)
                                            @foreach($attachment as $file)
                                            <div><a href="{{asset('uploads/lessons/'. $file->attachment)}}" target="_blank"> {{ $file->original_name ?? $file->attachment }}</a> </div>
                                            @endforeach
                                            @endif
                                            @elseif(Auth::user()->role != student) 
                                            @if($attachment)
                                            @foreach($attachment as $file)
                                            <div><a href="{{asset('uploads/lessons/'. $file->attachment)}}" target="_blank"> {{ $file->original_name ?? $file->attachment }}</a> </div>
                                            @endforeach
                                            @endif
                                            @endif											
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--- chat display -->
                    <div class="tbl" style="display: table; width: 100%;">
                        <div style="display: table-cell; vertical-align: top; width:30px;"><strong></strong></div>
                        <div style="display: table-cell; vertical-align: top;">
                            <div class="lesson_comments" id="comments" style="overflow: auto; height: 350px;">
                                <label for="listening" class="form-control-label"><strong>チャット履歴</strong></label>	
                                @if($chats)
                                @foreach($chats as $chat)
                                @if($chat->message)
                                <div class="comment-data">
                                    <div class="comment-avatar">
                                        @if($chat->user->avatar == null || !File::exists('uploads/avatar/'. $chat->user->avatar))
                                        <img src="{{ asset('uploads/avatar/noavatar.jpg') }}"  width="35" height="35">
                                        @else
                                        <img src="{{ asset('uploads/avatar/'. $chat->user->avatar) }}" width="35" height="35">
                                        @endif
                                    </div>
                                    <div class="comment-body">
                                        <div class="name">{{ $chat->user->username }} <small>{{ \App\Helpers\DateCarbon::date($chat->created_at) }}</small></div>
                                        <div class="note">
                                            {!! nl2br(e($chat->message)) !!}
                                        </div>
                                    </div>
                                </div>
                                @endif
                                @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                    <!--- chat display -->						
                    <!--file lists -->
                    <!--upload files -->
                    @if ((Auth::user()->role == teacher) && !($answers && $answers->writing_tea_res && $answers->writing_stu_ans)) 
                    <div class="tbl" style="display: table; width: 100%;">
                        <div style="display: table-cell; vertical-align: top; width:30px;"><strong></strong></div>
                        <div style="display: table-cell; vertical-align: top;">
                            <div class="form-row">
                                <div class="form-group col col-md-12 col-lg-12">
                                    <div style="padding:10px;">
                                        <div class="form-row">
                                            <div class="form-group col col-md-12 col-lg-4">
                                                <label for="attachment" class="form-control-label"><strong>音声添付ファイル</strong></label><br>
                                                <input type="file" name="media[]" id="attachment" multiple accept="audio/*">
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col col-md-12 col-lg-4">
                                                <label for="attachment" class="form-control-label"><strong>レッスン添付ファイル</strong></label><br>
                                                <input type="file" name="attachment[]" id="attachment" multiple>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    <!--upload files -->
                </div>
                <!--homework-box-->  
                @endif
                @if ($templates->form_type == 3)
                <div class="homework-box" style="border: 1px solid #c5c5c5; padding: 15px; width: 100%;">
                    <div class="tbl" style="display: table; width: 100%;">
                        <div style="display: table-cell; vertical-align: top; width:30px;"><strong></strong></div>
                        <div style="display: table-cell; vertical-align: top;">
                            <div class="form-row">
                                <div class="form-group col col-md-12 col-lg-12">
                                    <label for="" class="form-control-label"><strong>Writing課題（目安：10分）</strong></label>
                                    <div style="border:solid 1px #000; padding: 10px;">
                                        <p style="text-align: left;">{{$templates->writing_topic}}</p>
                                    </div>
                                    {{-- 
                                    <textarea rows="4" cols="50" id="writing_topic" name="writing_topic" class="form-control" placeholder="トピックを入力してください。" >{{$templates->writing_topic}}</textarea>
                                    --}}
                                    <small id="error-writing_topic" class="form-text form-error"></small>
                                    <div class="lesson_comments" id="comments" style="overflow: auto; height: 350px;">
                                        <div class="title">チャット履歴</div>
                                        @if($chats)
                                        @foreach($chats as $chat)
                                        @if($chat->message)
                                        <div class="comment-data">
                                            <div class="comment-avatar">
                                                @if($chat->user->avatar == null || !File::exists('uploads/avatar/'. $chat->user->avatar))
                                                <img src="{{ asset('uploads/avatar/noavatar.jpg') }}"  width="35" height="35">
                                                @else
                                                <img src="{{ asset('uploads/avatar/'. $chat->user->avatar) }}" width="35" height="35">
                                                @endif
                                            </div>
                                            <div class="comment-body">
                                                <div class="name">{{ $chat->user->username }} <small>{{ \App\Helpers\DateCarbon::date($chat->created_at) }}</small></div>
                                                <div class="note">
                                                    {!! nl2br(e($chat->message)) !!}
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if (Auth::user()->role == student)
                    <div class="tbl" style="display: table; width: 100%;">
                        <div style="display: table-cell; vertical-align: top; width:30px;"><strong></strong></div>
                        <div style="display: table-cell; vertical-align: top;">
                            <div class="form-row">
                                <div class="form-group col col-md-12 col-lg-12">
                                    <label for="writing_stu_ans" class="form-control-label"><strong>生徒回答場所</strong></label>
                                    @if ($answers)
                                    <div style="border:solid 1px #000; padding: 10px;">
                                        <p style="text-align: left;">{!! nl2br($answers->writing_stu_ans) !!}</p>
                                    </div>
                                    <br>
                                    <label for="writing_tea_res" class="form-control-label"><strong>教師添削後返却場所</strong></label>
                                    @if (!$answers->writing_tea_res)
                                    <textarea rows="4" cols="50" id="writing_tea_res" name="writing_tea_res" class="form-control"
                                    {{Auth::user()->role == teacher && $answers ? ' ' : 'disabled'}}></textarea>
                                    @else
                                    <div style="border:solid 1px #000; padding: 10px;">
                                        <p style="text-align: left;">{!! nl2br($answers->writing_tea_res) !!}</p>
                                    </div>
                                    @endif
                                    @else
                                    <textarea rows="4" cols="50" id="writing_stu_ans" name="writing_stu_ans" class="form-control" {{Auth::user()->role == student ? ' ' : 'disabled'}}> </textarea>
                                    <br> 
                                    <label for="writing_tea_res" class="form-control-label"><strong>教師添削後返却場所</strong></label><br>
                                    <textarea rows="4" cols="50" id="writing_tea_res" name="writing_tea_res" class="form-control"
                                    {{Auth::user()->role == teacher && $answers ? ' ' : 'disabled'}}></textarea>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    @elseif(Auth::user()->role == teacher)
                    <div class="tbl" style="display: table; width: 100%;">
                        <div style="display: table-cell; vertical-align: top; width:30px;">
                            <strong></strong>
                        </div>
                        <div style="display: table-cell; vertical-align: top;">
                            <div class="form-row">
                                <div class="form-group col col-md-12 col-lg-12">
                                    <label for="writing_stu_ans" class="form-control-label"><strong>生徒回答場所</strong></label><br>
                                    @if($answers)
                                    <div style="border:solid 1px #000; padding: 10px;">
                                        <p style="text-align: left;">{!! nl2br($answers->writing_stu_ans) !!}</p>
                                    </div>
                                    @else
                                    <textarea rows="4" cols="50" id="writing_stu_ans" name="writing_stu_ans" class="form-control" {{Auth::user()->role == student ? ' ' : 'disabled'}}> </textarea>
                                    @endif
                                    <br>
                                    <label for="writing_tea_res" class="form-control-label"><strong>教師添削後返却場所</strong></label>
                                    @if ($answers && $answers->writing_tea_res)
                                    <div style="border:solid 1px #000; padding: 10px;">
                                        <p style="text-align: left;">{!! nl2br($answers->writing_tea_res) !!}</p>
                                    </div>
                                    @else
                                    <textarea rows="4" cols="50" id="writing_tea_res" name="writing_tea_res" class="form-control"
                                    {{Auth::user()->role == teacher && $answers ? ' ' : 'disabled'}}></textarea>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    <!--file lists -->
                    <div class="tbl" style="display: table; width: 100%;">
                        <div style="display: table-cell; vertical-align: top; width:30px;"><strong></strong></div>
                        <div style="display: table-cell; vertical-align: top;">
                            <div class="form-row">
                                <div class="form-group col col-md-12 col-lg-12">
                                    <label for="attachment" class="form-control-label"><strong>レッスン添付ファイル</strong></label><br>
                                    <div style="border:solid 1px #000; padding:10px;">
                                        <div class="form-group col-lg-12">
                                            @if (strtotime(date('Y-m-d H:i:s')) > strtotime($templates->date . $templates->end_time) && (Auth::user()->role == student)) 
                                            @if($attachment)
                                            @foreach($attachment as $file)
                                            <div><a href="{{asset('uploads/lessons/'. $file->attachment)}}" target="_blank"> {{ $file->original_name ?? $file->attachment }}</a> </div>
                                            @endforeach
                                            @endif
                                            @elseif(Auth::user()->role != student) 
                                            @if($attachment)
                                            @foreach($attachment as $file)
                                            <div><a href="{{asset('uploads/lessons/'. $file->attachment)}}" target="_blank"> {{ $file->original_name ?? $file->attachment }}</a> </div>
                                            @endforeach
                                            @endif
                                            @endif											
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--file lists -->
                    <!--upload files -->
                    @if ((Auth::user()->role == teacher) && !($answers && $answers->writing_tea_res && $answers->writing_stu_ans)) 
                    <div class="tbl" style="display: table; width: 100%;">
                        <div style="display: table-cell; vertical-align: top; width:30px;"><strong></strong></div>
                        <div style="display: table-cell; vertical-align: top;">
                            <div class="form-row">
                                <div class="form-group col col-md-12 col-lg-12">
                                    <div style="padding:10px;">
                                        <div class="form-row">
                                            <div class="form-group col col-md-12 col-lg-4">
                                                <label for="attachment" class="form-control-label"><strong>レッスン添付ファイル</strong></label><br>
                                                <input type="file" name="attachment[]" id="attachment" multiple>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    <!--upload files -->
                </div>
                @endif<br>
                <!--submit button -->
                <div class="form-row">
                    <div class="form-group col col-md-6 col-lg-6">
                        {{-- <button type="submit" id="submit-form" data-form="thread-lesson" class="btn btn-primary">Update</button>  --}}
                        @if (Auth::user()->role == 4)
                        @if($answers == null )
                        <button type="submit" id="submit-form" data-form="thread-lesson" class="btn btn-primary">Update</button>   
                        @endif
                        @elseif(Auth::user()->role == 5)
                        @if ($answers && !$answers->writing_tea_res)
                        <input type="hidden" name="thread_id" value="{{$answers->id}}">
                        <button type="submit" id="submit-form" data-form="thread-lesson" class="btn btn-primary">Update</button>
                        @elseif($answers && $answers->writing_tea_res && $answers->writing_stu_ans)
                        @else
                        <button type="submit" id="submit-form" data-form="thread-lesson" class="btn btn-primary">Update</button>
                        @endif
                        @endif
                        <a href="{{ url('lessons') }}" class="btn btn-warning">Cancel</a>
                        <span class="form-proccessing hidden"><img src="{{ asset('assets/img/Loading/loading.gif') }}"> Proccessing, please wait a moment...</span>
                    </div>
                </div>
                {{ Form::close() }} 
            </div>
        </div>
    </div>
</section>
@endsection
@section('js')
<script src="{{ asset('assets/vendor/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('assets/vendor/ckeditor/ckeditor.js') }}"></script>
<script>
    $(document).ready(function(){
    	let _token = $('meta[name="csrf-token"]').attr('content');
    
    	$(document).on('click', '.add_more', function(){
    		$(this).remove();
    		let row = `<div class="form-row">
    					<div class="form-group col col-lg-3">
    						<label for="source0" class="form-control-label">Source</label>
    						<select id="source0" name="source[]" class="form-control select_source">
    							<option value="">Select Source</option>
    							<option value="local">Local</option>
    							<option value="youtube">Youtube</option>
    						</select>
    					</div>
    					<div class="form-group col col-lg-7">
    						
    						<div class="local hidden" style="padding-top: 30px;">
    							<div class="custom-file">
    							  	<input type="file" class="custom-file-input" name="media[]" id="media0">
    							  	<label class="custom-file-label" for="media0">Select Media</label>
    							</div>
    						</div>
    						
    						<div class="youtube hidden">
    							<label for="youtube0" class="form-control-label">Enter youtube link.</label>
    							<input type="text" name="youtube[]" class="form-control">
    						</div>
    					</div>
    					<div class="form-group col col-lg-1" style="padding-top: 30px;">
    						<button type="button" class="btn btn-default btn-block hidden add_more">Add more.</button>
    					</div>
    				</div>`;
    
    		$('#media_content').append(row);
    	});
    
    	$(document).on('change', '.select_source', function(){
    		let val = $('option:selected', this).val();
    		
    		if(val != ''){
    			if(val == 'local'){
    				$(this).closest('.form-row').find('.local').removeClass('hidden');
    				$(this).closest('.form-row').find('.youtube').addClass('hidden');
    			}
    			if(val == 'youtube'){
    				$(this).closest('.form-row').find('.local').addClass('hidden');
    				$(this).closest('.form-row').find('.youtube').removeClass('hidden');
    			}
    			$(this).closest('.form-row').find('.btn').removeClass('hidden');
    		}else{
    			$(this).closest('.form-row').find('.btn').addClass('hidden');
    			$(this).closest('.form-row').find('.local').addClass('hidden');
    			$(this).closest('.form-row').find('.youtube').addClass('hidden');
    		}
    	});
    
    	$('.delete_media').each(function(){
    		$(this).click(function(){
    			let id = $(this).attr('data');
    		});
    	});
    
    	$('.delete_attachment').each(function(){
    		$(this).click(function(){
    			let id = $(this).attr('data-id');
    			let target = $(this).attr('data-target');
    			let ans= confirm('Are you sure you want to delete this item?');
    			if(ans){
    				$.ajax({
    					type: 'post',
    					url: '{{ url('lessons') }}/' + target,
    					data:{
    						id: id,
    						_token: _token
    					},success:function(data){
    						//$('#parent-'+ id).remove();
    						window.location = window.location;
    					}
    				});
    			}
    		});
    	});
    
    });
</script>
<script>
    $(document).ready(function(){
    	let chat_flag = '{{$templates->chat_flag}}';
    	let lesson_id = '{{$templates->id}}';
    	let _token = $('meta[name="csrf-token"]').attr('content');
    	console.log(chat_flag);
    	if(chat_flag == 1){
    		$("#comments").css("display", "block");
    		
    	}else{
    		$("#comments").css("display", "none");
    		
    	}
    
    });
</script>
@endsection