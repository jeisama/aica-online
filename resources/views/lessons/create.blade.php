@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-select/bootstrap-select.css') }}">
@endsection

@section('content')
	<div class="breadcrumb-holder container-fluid">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i> Home</a></li>
            <li class="breadcrumb-item"><a href="{{ url('/lessons') }}">Lessons</a></li>
            <li class="breadcrumb-item active">Create Lesson</li>
        </ul>
    </div>

    <section class="forms">
    	<div class="container-fluid">

    		@if (Session::has('success_message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {!! session('success_message') !!}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif

            @if (Session::has('error_message'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {!! session('error_message') !!}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif

			<div class="card">
				<div class="card-header d-flex align-items-center">
					<h3 class="h4">Lesson Details</h3>
				</div>
				
				<div class="card-body">
					{{ Form::open(['files' => true, 'id' => 'create-lesson', 'onsubmit' => 'event.preventDefault()']) }}
						<input type="hidden" name="course_type" id="val_data_course_type">
						<input type="hidden" name="lesson_type" id="val_data_lesson_type">
						<input type="hidden" name="lesson_day" id="val_data_lesson_day">
					<div class="form-row">
							<div class="form-group col col-md-12 col-lg-8">
								<label for="lesson_name" class="form-control-label">Select template</label>
								<select id="template" class="form-control" name="template">
									<option value="">-- Select Item --</option>
									@foreach ($templates as $item)
										<option value="{{$item->id}}">{{$item->lesson_name}}</option>
									@endforeach
								</select>
							</div>
						</div>


				
					<div class="form-row">
						<div class="form-group col col-md-12 col-lg-8">
							<label for="lesson_name" class="form-control-label">Lesson Name</label>
							<input type="hidden" id="val_data_lesson_name" name="lesson_name">
							<span id="val_lesson_name"></span>
				
						</div>
					</div>
					
					<div class="homework-box" style="border: 1px solid #c5c5c5; padding: 15px; width: 100%;">
							<div class="tbl" style="display: table; width: 100%;">
								<div style="display: table-cell; vertical-align: top; width:30px;"><strong>1</strong></div>
									<div style="display: table-cell; vertical-align: top;">
										<div class="form-row">
											<div class="form-group col col-md-12 col-lg-12">
												<label for="writing_1" class="form-control-label"><strong>Writing課題　（目安：10分）</strong></label>
												<span id="val_writing_1"></span>
												<input type="hidden" name="writing_1" id="val_data_writing_1">
												{{-- <textarea rows="4" cols="50" id="val_writing_1" name="writing_1" class="form-control"></textarea>
												<small id="error-writing_1" class="form-text form-error"></small> --}}
											</div>
											
										</div>
								</div>
							</div>     
							<div class="tbl" style="display: table; width: 100%;">
								<div style="display: table-cell; vertical-align: top; width:30px;"><strong>2</strong></div>
									<div style="display: table-cell; vertical-align: top;">
										<div class="form-row">
											<div class="form-group col col-md-12 col-lg-12">
												<label for="speaking_1" class="form-control-label"><strong>Speaking課題　（目安：5分）</strong></label>
												<span id="val_speaking_1"></span>
												<input type="hidden" name="speaking_1" id="val_data_speaking_1">
												{{-- <textarea rows="4" cols="50" id="val_speaking_1" name="speaking_1" class="form-control"></textarea>
												<small id="error-speaking_1" class="form-text form-error"></small> --}}
											</div>
											<div class="form-group col col-md-12 col-lg-12">
											<label for="speaking_2" class="form-control-label"><strong>次週のSpeaking Topic:</strong></label><br> 
												<textarea rows="4" cols="50" id="val_speaking_2" name="speaking_2" placeholder="トピックを入力してください。" class="form-control"></textarea>
												<small id="error-speaking_2" class="form-text form-error"></small>
												
											</div>
											
										</div>
								</div>
							</div>
							<div class="tbl" style="display: table; width: 100%;">
								<div style="display: table-cell; vertical-align: top; width:30px;"><strong>3</strong></div>
									<div style="display: table-cell; vertical-align: top;">
										<div class="form-row">
											<div class="form-group col col-md-12 col-lg-12">
												<label for="writing_student_1" class="form-control-label"><strong><span id="val_writing_student_1"></span></strong></label>
												<input type="hidden" name="writing_student_1" id="val_data_writing_student_1">
												<textarea rows="4" cols="50" id="writing_student_2" name="writing_student_2" class="form-control" placeholder="生徒回答場所" disabled></textarea>
												<small id="error-writing_student_2_ans" class="form-text form-error"></small>
												<textarea rows="4" cols="50" id="writing_student_2_ans" name="writing_student_2_ans" class="form-control" placeholder="教師添削後返却場所" disabled></textarea>
											</div>
										</div>
								</div>
							</div>
							<div class="tbl" style="display: table; width: 100%;">
								<div style="display: table-cell; vertical-align: top; width:30px;"><strong>4</strong></div>
									<div style="display: table-cell; vertical-align: top;">
										<div class="form-row">
											<div class="form-group col col-md-12 col-lg-12">
												<label for="listening_1" class="form-control-label"><strong>Listening課題　（目安：15分）</strong></label><br>
												<span id="val_listening_1"></span>
												<input type="hidden" name="listening_1" id="val_data_listening_1">
											</div>
											<div class="form-group col col-md-12 col-lg-12">
													<label for="listening_2" class="form-control-label"><strong>今週のReproduction課題：</strong></label>
													<div class="form-row">
															<div class="form-group col-lg-12">
																<label for="attachment" class="form-control-label">Select Audio (mp3)</label>
																<div>
																	<input type="file" name="media[]" id="attachment" multiple accept="audio/*">
																</div>
															</div>
														</div>
											</div>
											
										</div>
									</div>
							</div>
							<div class="tbl" style="display: table; width: 100%;">
								<div style="display: table-cell; vertical-align: top; width:30px;"><strong>5</strong></div>
									<div style="display: table-cell; vertical-align: top;">
										<div class="form-row">
											<div class="form-group col col-md-12 col-lg-12">
												<label for="homework_1" class="form-control-label"><strong><span id="val_homework_1"></span></strong></label>
												<input type="hidden" name="homework_1" id="val_data_homework_1">
												{{-- <textarea rows="4" cols="50" id="val_homework_1" name="homework_1" class="form-control"></textarea>
												<small id="error-homework_1" class="form-text form-error"></small><br> --}}
												<label for="homework_2" class="form-control-label"><strong>今週のListening課題：</strong></label>
												<textarea rows="4" cols="50" id="val_homework_2" name="homework_2" class="form-control" placeholder="スクリプトを入力してください。"></textarea>
												<small id="error-homework_2" class="form-text form-error"></small>
											</div>
											
											
										</div>
								</div>
							</div>	       
						</div>
					<br>
					<div class="form-row">
						<div class="form-group col col-md-12 col-lg-4">
							<label for="attachment" class="form-control-label">Attachments</label>
							<div>
							<input type="file" name="attachment[]" id="attachment" multiple>
							</div>
						</div>
					</div>
					
					
					{{-- <div class="form-row">
						<div class="form-group col-lg-12">
							<label for="attachment" class="form-control-label">Select Audio (mp3)</label>
							<div>
								<input type="file" name="media[]" id="attachment" multiple accept="audio/*">
							</div>
						</div>
					</div> --}}

					<div class="line"></div>

					<div class="form-group">
						<button type="submit" id="submit-form" data-form="create-lesson" class="btn btn-primary">{{__('label.save')}}</button>
						<a href="{{ url('lessons') }}" class="btn btn-default">{{__('label.cancel')}}</a>
						<span class="form-proccessing hidden"><img src="{{ asset('assets/img/Loading/loading.gif') }}"> {{__('label.submit_process')}}</span>
					</div>

					{{ Form::close() }}
				</div>
			</div>
    	</div>
    </section>
@endsection

@section('js')
	<script src="{{ asset('assets/vendor/bootstrap-select/bootstrap-select.min.js') }}"></script>
	<script src="{{ asset('assets/vendor/ckeditor/ckeditor.js') }}"></script>
	<script>
		$(document).ready(function(){
			$(document).on('click', '.add_more', function(){
				$(this).remove();
				let row = `<div class="form-row">
							<div class="form-group col col-lg-3">
								<label for="source0" class="form-control-label">Source</label>
								<select id="source0" name="source[]" class="form-control select_source">
									<option value="">Select Source</option>
									<option value="local">Local</option>
									<option value="youtube">Youtube</option>
								</select>
							</div>
							<div class="form-group col col-lg-7">
								
								<div class="local hidden" style="padding-top: 30px;">
									<div class="custom-file">
									  	<input type="file" class="custom-file-input" name="media[]" id="media0">
									  	<label class="custom-file-label" for="media0">Select Media</label>
									</div>
								</div>
								
								<div class="youtube hidden">
									<label for="youtube0" class="form-control-label">Enter youtube link.</label>
									<input type="text" name="youtube[]" class="form-control">
								</div>
							</div>
							<div class="form-group col col-lg-1" style="padding-top: 30px;">
								<button type="button" class="btn btn-default btn-block hidden add_more">Add more.</button>
							</div>
						</div>`;

				$('#media_content').append(row);
			});

			$(document).on('change', '.select_source', function(){
				let val = $('option:selected', this).val();
				
				if(val != ''){
					if(val == 'local'){
						$(this).closest('.form-row').find('.local').removeClass('hidden');
						$(this).closest('.form-row').find('.youtube').addClass('hidden');
					}
					if(val == 'youtube'){
						$(this).closest('.form-row').find('.local').addClass('hidden');
						$(this).closest('.form-row').find('.youtube').removeClass('hidden');
					}
					$(this).closest('.form-row').find('.btn').removeClass('hidden');
				}else{
					$(this).closest('.form-row').find('.btn').addClass('hidden');
					$(this).closest('.form-row').find('.local').addClass('hidden');
					$(this).closest('.form-row').find('.youtube').addClass('hidden');
				}
			});
			$(document).on('change', '#template', function(){
				let option = '<option value="">--Select Item--</option>';

				let id = $('option:selected', this).val();

				if(id != ''){
					$.ajax({
                            type: 'get',
                            dataType: 'json',
                            url: '{{ url('template/show') }}/'+ id,
                            success:function(response){
								let data = response.data;
                                Object.keys(data[0]).forEach(function(key) {
									
									$('#val_'+ key).text(data[0][key]);
									$('#val_data_'+ key).val(data[0][key]);
									console.log(key);
								});
                                
                            }
                    });
				}

				});
	            $(document).on('change', '#course_type', function(){

					let option = '<option value="">--Select Item--</option>';

					let id = $('option:selected', this).val();

					if(id != ''){
						$.ajax({
							type: 'post',
							dataType: 'json',
							url: '{{ url('api/schedule/get-grade') }}',
							data: {
								id: id
							},
							success: function(response){
								$('#lesson_type').prop('disabled', false).html(response.grade);

							}
						});
					}else{
						$('#lesson_type').prop('disabled', true).html(option);
					
					}

					});
			
		});
	</script>
@endsection