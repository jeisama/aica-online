<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $lesson->lesson_name }}</title>
    <base href="{{ url('/') }}" target="_blank">
    <meta name="description" content="">
    <meta name="socket-io" content="{{ chat_socket }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
   
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- Font Awesome CSS-->
    <!-- Fontastic Custom icon font-->
    <!-- Google fonts - Poppins -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{ asset('assets/css/style.blue.css') }}" id="theme-stylesheet">
    <link href="{{ asset('assets/vendor/MaterialDesignWebfont/css/materialdesignicons.css') }}" media="all" rel="stylesheet" type="text/css">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
    <link href="{{ asset('assets/vendor/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/jPlayer-2.9.2/dist/skin/blue.monday/css/jplayer.blue.monday.css') }}" rel="stylesheet" type="text/css" />
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{ asset('assets/img/favicon.ico') }}">
    
  </head>
  <body>
  
    <video id="main-video"  style="display: none;" controls playsinline autoplay></video>
    <style>

    #other-videos:first-child{
        position: relative;
        width: 100%;
        height: 100%;
        border: 3px solid #73AD21;
    }
    video{
        width: 100%;
    }
    </style>

<div class="page">
  <!-- Main Navbar-->
    <header class="header">
        <nav class="navbar">
            <div class="container-fluid">
               
                <div class="navbar-holder d-flex align-items-center justify-content-between">
                      
                    <!-- Navbar Header-->
                    <div class="navbar-header">
                        <!-- Navbar Brand --><a href="#" class="navbar-brand d-none d-sm-inline-block">
                        <div class="brand-text d-none d-lg-inline-block"><span></span>AIC-Online &nbsp;</span></div>
                        <div class="brand-text d-none d-sm-inline-block d-lg-none"><strong>BD</strong></div></a>
                        @if ($user_data->role == teacher)
                        Lesson Time - <span id="time">00:00 </span>
                        @endif 
                    </div>
                    <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                        <li class="nav-item mr-3">{{ $lesson->lesson_name }}</button></li>
                        <li class="nav-item">
                          @if ($user_data->role == teacher)
                          <button type="button" id="teach-btn" class="btn btn-primary float-right endSession" >End Video Class</button>    
                          @endif
                          
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
   
    <input type="hidden" id="getRoomName" value="{{ $lesson->token }}">
    <input type="hidden" id="user_id" value="{{ $user_data->id }}">
    <input type="hidden" id="lesson_id" value="{{ $lesson->id }}">
    <input type="hidden" id="user_role" value="{{ $user_data->role }}">
    <input class="usernameInput" type="hidden" maxlength="14" value="{{ $user_data->username }}"/>
    
    <section class="video-call">
        <div class="chat">
            <div class="inner chatArea scrollable_container">
                <ul class="messages" id="chatMessageIo">
                    
                @if($messages)
                @foreach($messages as $message)
                <li class="message {{ $message->user_id == $user_data->id ? 'right':'left' }}">
                    <span class="avatar"><i class="mdi mdi-account-circle"></i></span>
                    <span class="username">{{ $message->user->username }}</span>
                    <span class="messageBody">{{ $message->message }}</span>
                </li>
                @endforeach
                @endif
                    
                </ul>
            </div>
            <div class="innerInput">
                <input type="hidden" id="user_id" value="{{ $user_data->id }}">
                <textarea  class="inputMessage" id="inputMessage" placeholder="Type here..." cols="2"></textarea>
            </div>
        </div>

        <div class="video_media">
            <div class="video_data">
                <div class="bordered-box">
                    <div class="others" id="other_video"></div>
                    <div class="initiator" id="initiator_video">
                        @if ($user_data->role == teacher)
                        <div class="minimize_maximize" data-tag="maximize"></div>
                        @endif
                    </div>
                </div>
            </div>

            <div class="playlist">
                <div class="bordered-box">
                    @if ($user_data->role != teacher)
                        <div class="disabled_jPlayer"></div>
                    @endif
                    <!--/ jPlayer /-->
                    <div id="jquery_jplayer_2" class="jp-jplayer">
                    </div>

                    <div id="jp_container_2" class="jp-audio" role="application" aria-label="media player">
                        <div class="jp-type-playlist">
                            <div class="jp-gui jp-interface">
                                
                                <div class="jp-controls">
                                    <button class="jp-previous" role="button" tabindex="0">previous</button>
                                    <button class="jp-play" role="button" tabindex="0">play</button>
                                    <button class="jp-next" role="button" tabindex="0">next</button>
                                </div>

                                <div class="jp-progress hidden">
                                    <div class="jp-seek-bar">
                                        <div class="jp-play-bar"></div>
                                    </div>
                                </div>

                                <div class="jp-volume-controls hidden">
                                    <button class="jp-mute" role="button" tabindex="0">mute</button>
                                    <button class="jp-volume-max" role="button" tabindex="0">max volume</button>
                                    <div class="jp-volume-bar">
                                        <div class="jp-volume-bar-value"></div>
                                    </div>
                                </div>
                                
                                <div class="jp-time-holder hidden">
                                    <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
                                    <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
                                </div>

                                <div class="jp-toggles hidden">
                                    <button class="jp-repeat" role="button" tabindex="0">repeat</button>
                                    <button class="jp-shuffle" role="button" tabindex="0">shuffle</button>
                                </div>
                            </div>

                            <div class="jp-playlist" style="overflow: auto; overflow-x: hidden;">
                                <ul id="playerLists">
                                    <li>&nbsp;f</li>
                                </ul>
                            </div>

                            <div class="jp-no-solution">
                                <span>Update Required</span>
                                To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
                            </div>
                            @if($user_data->role == 5)
                            {{--<div class="add_audio">
                                {{ Form::open(['file' => true, 'id' => 'add_audio']) }}
                                <input type="hidden" name="lesson_id" value="{{ $lesson->id }}">
                                <input type="file" id="input_audio" name="audio[]" accept="audio/*">
                                <label for="input_audio"><i class="mdi mdi-music-circle-outline"></i> <small>Add Audio</small></label>    
                                {{ Form::close() }}
                            </div>--}}
                            @endif
                        </div>
                    </div><!--/ jPlayer /-->
                </div>
            </div>
        </div>
        
        <div class="slider_scribble">
            <div id="widget-container">
                @if ($user_data->role == teacher)
                <div class="close_scribble">
                    <i class="mdi mdi-close"></i>
                </div>
                @endif
            </div>
            <div class="slider_data" id="slider_data">
                <div class="prev" id="prev_image">
                    @if(count($images))
                   
                    <?php
                    $array = explode('.', $images[0]->attachment);
                    $ext   = end($array);
                    ?>
                    @if($ext == 'pdf')
                    <div class="pdf-control">
                        <div class="button-action pdf-prev" data-action="prev"><i class="mdi mdi-arrow-left"></i></div>
                        <div class="button-action pdf-next" data-action="next"><i class="mdi mdi-arrow-right"></i></div>
                        <input type="hidden" id="pdf-page" value="1">
                    </div>
                    <object data="{{ asset('uploads/lessons/'. $images[0]->attachment) }}#toolbar=0&view=FitV&scrollbar=0&page=1" type="application/pdf" style="width: 100%; height: 100%; overflow-x: hidden; overflow-y: hidden;">
                    </object>
                    @else
                    <img id="previewImage" src="{{ asset('uploads/lessons/'. $images[0]->attachment) }}" alt="">
                    @endif

                    @endif
                </div>
                <div class="list_upload scrollable_container">
                    <ul class="{{ $user_data->role == teacher ? 'overview':'' }}" id="slideoverview">
                        @if($images != null)
                        @foreach($images as $image)
                        <?php
                            $explode = explode('.', $image->attachment);
                            $ext     = end($explode);
                        ?>
                        @if($ext == 'jpeg' || $ext == 'jpg' || $ext == 'png' || $ext == 'gif')
                        <li><div><img src="{{ asset('uploads/lessons/'. $image->attachment) }}" data-type="image"></div></li>
                        @endif

                        @if($ext == 'pdf')
                        <li><div><img src="{{ asset('assets/img/pdf-logo.jpg') }}" data-base="{{ asset('uploads/lessons/'. $image->attachment) }}" data-type="pdf"></div></li>
                        @endif

                        @endforeach
                        @endif
                    </ul>
                </div>
                @if($user_data->role == teacher)
                <div class="addImage">
                    <form enctype="multipart/form-data" method="post" accept-charset="utf-8" id="add_attachment">
                    <input type="hidden" name="_token" value="{{ csrf_token()}}">
                    <label for="attachment" title="Upload image">
                        <i class="mdi mdi-plus"></i>
                    </label>
                    <input type="hidden" name="lesson_id" value="{{ $lesson->id }}">
                    <input type="file" id="attachment" name="attachment" accept="image/*, application/pdf">
                    </form>
                </div>
                <div class="showCanvas" title="Show Canvas">
                    <i class="mdi mdi-square-edit-outline"></i>
                </div>
                @endif
            </div>
        </div>
    </section>
    
    <!-- Modal For Teacher-->
    <div class="modal fade" id="ratingModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                @if ($user_data->role == teacher)
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        <div class="row">
                            <div class="col">{{$lesson->schedule->date}}</div>
                            <div class="col text-center">{{$lesson->courses->label}} <br> {{$lesson->grade->label}}</div>
                            <div class="col text-right">Day {{$lesson->lesson_day}}</div>
                        </div>
                    </h5>
                </div>
                <div class="modal-body">
                       
                    <div class="form-row">
                        <div class="col-md-2 text-right">
                            <span class="category"><i class="mdi mdi-emoticon-happy mdi-36px" data-toggle="tooltip"  data-placement="bottom" title="態度"></i></span> 
                        </div>
                        <div class="col-md-10">
                            <div class="rating" id="student">
                                <span><input type="radio" name="student" id="str5" value="5"><label for="str5" data-toggle="tooltip"  data-placement="bottom" title=""><i class="mdi mdi-star mdi-36px"></i></label></span>
                                <span><input type="radio" name="student" id="str4" value="4"><label for="str4" data-toggle="tooltip"  data-placement="bottom" title=""><i class="mdi mdi-star mdi-36px"></i></label></span>
                                <span><input type="radio" name="student" id="str3" value="3"><label for="str3" data-toggle="tooltip"  data-placement="bottom" title="特に気になることはなく、積極的に授業に参加しており、相手へのリスペクトが欠けることは無かった。"><i class="mdi mdi-star mdi-36px"></i></label></span>
                                <span><input type="radio" name="student" id="str2" value="2"><label for="str2" data-toggle="tooltip"  data-placement="bottom" title="ごく稀に生徒の言動・態度がネガティブな印象を与えることがあったが、実際のコミュニケーションの場でも起こりうる程度のものである。"><i class="mdi mdi-star mdi-36px"></i></label></span>
                                <span><input type="radio" name="student" id="str1" value="1"><label for="str1" data-toggle="tooltip"  data-placement="bottom" title="会話に対する意欲が見られない。または、態度・所作などの問題で実際のコミュニケーションの場でも相手に不快感を与えることが予想される。"><i class="mdi mdi-star mdi-36px"></i></label></span>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-row">
                        <div class="col-md-2">
                            <span class="category"><img src="{{asset('assets/img/customer-service-svgrepo-com.svg')}}" width="30" height="35" data-toggle="tooltip" data-placement="bottom" 
                                title="聞いてわかる力"></span> 
                        </div>
                        <div class="col-md-10 ">
                            <div class="rating" id="listening"> 
                                <span><input type="radio" name="listening" id="str6" value="5"><label for="str6"  data-toggle="tooltip" data-placement="bottom" title="通常のスピード・喋り方で伝えても、教師側の説明や指示を完全に理解することができる。"><i class="mdi mdi-star mdi-36px"></i></label></span>
                                <span><input type="radio" name="listening" id="str7" value="4"><label for="str7" data-toggle="tooltip" data-placement="bottom" title="指示をくり返したり、スピードを緩めたり、言い換えて説明すれば、こちらの意図する内容が問題無く伝わる。" ><i class="mdi mdi-star mdi-36px"></i></label></span>
                                <span><input type="radio" name="listening" id="str8" value="3"><label for="str8" data-toggle="tooltip" data-placement="bottom" title="全体的には会話が成立しているように見えるが、部分的に指示や説明が伝わっていないと感じることがある。"><i class="mdi mdi-star mdi-36px"></i></label></span>
                                <span><input type="radio" name="listening" id="str9" value="2"><label for="str9" data-toggle="tooltip" data-placement="bottom" title="教師の指示・説明が伝わらないことが頻繁にあり、授業がスムーズに進行しない。"><i class="mdi mdi-star mdi-36px"></i></label></span>
                                <span><input type="radio" name="listening" id="str10" value="1"><label for="str10"  data-toggle="tooltip" data-placement="bottom" title="授業の進行が頻繁に滞る。指示・説明が通らない。"><i class="mdi mdi-star mdi-36px"></i></label></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-2 text-right">
                            <span class="category"><i class="mdi mdi-pencil mdi-36px" data-toggle="tooltip" data-placement="bottom" 
                                title="文章を作る力"></i></span> 
                        </div>
                        <div class="col-md-10">
                            <div class="rating" id="writing">
                                <span><input type="radio" name="writing" id="str11" value="5"><label for="str11" data-toggle="tooltip"  data-placement="bottom" title="英語圏や英語を公用語として使用する場においても問題無く発話することができ、会話の展開や進行速度に関しても違和感が無い。"><i class="mdi mdi-star mdi-36px"></i></label></span>
                                <span><input type="radio" name="writing" id="str12" value="4"><label for="str12" data-toggle="tooltip"  data-placement="bottom" title="全体的に求められる内容を、十分な発話量で発話できているが、返答のスピードが遅かったり、不自然に単語のみで回答したりと、通常の英語環境での会話と比べると違和感がある。"><i class="mdi mdi-star mdi-36px"></i></label></span>
                                <span><input type="radio" name="writing" id="str13" value="3"><label for="str13" data-toggle="tooltip"  data-placement="bottom" title="意思疎通はできるが、極端に返答時間が遅かったり、極端に単語のみでの返答が続いたり、自分の知っている表現を使用する会話の方向に無理に展開しようとしたりする等、時折コミュニケーションに不自然さや難しさを感じる。"><i class="mdi mdi-star mdi-36px"></i></label></span>
                                <span><input type="radio" name="writing" id="str14" value="2"><label for="str14" data-toggle="tooltip"  data-placement="bottom" title="言葉だけでは何を伝えようとしているのかわからないことがしばしばある。"><i class="mdi mdi-star mdi-36px"></i></label></span>
                                <span><input type="radio" name="writing" id="str15" value="1"><label for="str15" data-toggle="tooltip"  data-placement="bottom" title="言葉だけでは何を伝えようとしているのかわからない。"><i class="mdi mdi-star mdi-36px"></i></label></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-2 text-right">
                            <span class="category"><img src="{{asset('assets/img/brain-on-head-svgrepo-com.svg')}}" width="30" height="35" data-toggle="tooltip" data-placement="bottom" 
                                title="文法知識"></span> </span> 
                        </div>
                        <div class="col-md-10">
                            <div class="rating" id="grammar">
                                <span><input type="radio" name="grammar" id="str16" value="3"><label for="str16" data-toggle="tooltip"  data-placement="bottom" title="文法のミスは気にならない程度である。"><i class="mdi mdi-star mdi-36px"></i></label></span>
                                <span><input type="radio" name="grammar" id="str17" value="2"><label for="str17" data-toggle="tooltip"  data-placement="bottom" title="文法ミスは多々あるが、コミュニケーションをとるうえでは問題ない。"><i class="mdi mdi-star mdi-36px"></i></label></span>
                                <span><input type="radio" name="grammar" id="str18" value="1"><label for="str18" data-toggle="tooltip"  data-placement="bottom" title="文法ミスが非常に多く、それが原因で意図する内容を伝えることや会話の展開に難しさを感じる。"><i class="mdi mdi-star mdi-36px"></i></label></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-2 col-sm-12 text-right">
                            <span class="category"><i class="mdi mdi-headset mdi-36px" data-toggle="tooltip" data-placement="bottom" 
                                title="発音・流暢さ"></i></span>  
                        </div>
                        <div class="col-md-10">
                            <div class="rating" id="speaking">
                                <span><input type="radio" name="speaking" id="str19" value="3"><label for="str19" data-toggle="tooltip"  data-placement="bottom" title="コミュニケーションを取るうえでは全く問題ない。"><i class="mdi mdi-star mdi-36px"></i></label></span>
                                <span><input type="radio" name="speaking" id="str20" value="2"><label for="str20" data-toggle="tooltip"  data-placement="bottom" title="時折聞き取りづらいことはあるが、コミュニケーション自体は成立する。"><i class="mdi mdi-star mdi-36px"></i></label></span>
                                <span><input type="radio" name="speaking" id="str21" value="1"><label for="str21" data-toggle="tooltip"  data-placement="bottom" title="会話に支障が出るレベルで、発音やスピード、抑揚に難がある。"><i class="mdi mdi-star mdi-36px"></i></label></span>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="chatFlag" value="1"> 
                    {{-- <div class="form-row">
                            <div class="col-md-12 col-sm-12 text-right">
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck1" name="chat" checked>
                                    <label class="form-check-label" for="exampleCheck1">Save Chat</label>
                                </div>
                            </div>
                    </div> --}}
                </div>
                @elseif($user_data->role == student)
                <div class="modal-header">
                    <h5 class="modal-title text-center" id="exampleModalLabel">
                        <div class="row">
                            <div class="col">{{$lesson->schedule->date}}</div>
                            <div class="col text-center">{{$lesson->grade->label}}</div>
                            <div class="col text-right">Day {{$lesson->lesson_day}}</div>
                        </div>
                        <div class="row">
                            <div class="col text-center">{{$lesson->teacher->full_name}}</div>
                        </div>
                    </h5>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col-md-2 text-right">
                            <span class="category"><i class="mdi mdi-emoticon-happy mdi-36px" data-toggle="tooltip"  data-placement="bottom" title="態度"></i></span> 
                        </div>
                        <div class="col-md-10">
                            <div class="rating" id="teacher">
                                <span><input type="radio" name="teacher" id="str26" value="5"><label for="str26" data-toggle="tooltip"  data-placement="bottom" title=""><i class="mdi mdi-star mdi-36px"></i></label></span>
                                <span><input type="radio" name="teacher" id="str27" value="4"><label for="str27" data-toggle="tooltip"  data-placement="bottom" title=""><i class="mdi mdi-star mdi-36px"></i></label></span>
                                <span><input type="radio" name="teacher" id="str28" value="3"><label for="str28" data-toggle="tooltip"  data-placement="bottom" title="特に気になることはなく、積極的に授業に参加しており、相手へのリスペクトが欠けることは無かった。"><i class="mdi mdi-star mdi-36px"></i></label></span>
                                <span><input type="radio" name="teacher" id="str29" value="2"><label for="str29" data-toggle="tooltip"  data-placement="bottom" title="ごく稀に生徒の言動・態度がネガティブな印象を与えることがあったが、実際のコミュニケーションの場でも起こりうる程度のものである。"><i class="mdi mdi-star mdi-36px"></i></label></span>
                                <span><input type="radio" name="teacher" id="str30" value="1"><label for="str30" data-toggle="tooltip"  data-placement="bottom" title="会話に対する意欲が見られない。または、態度・所作などの問題で実際のコミュニケーションの場でも相手に不快感を与えることが予想される。"><i class="mdi mdi-star mdi-36px"></i></label></span>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                <div class="modal-footer">
                    {{-- <a href="javascript:window.open('','_self').close();" id="rating"  class="btn btn-primary">Submit</a> --}}
                    <a href="" id="rating"  class="btn btn-primary">Submit</a>
					<span class="form-proccessing hidden"><img src="{{ asset('assets/img/Loading/loading.gif') }}"> Proccessing, please wait a moment...</span>
                </div>
            </div>
           
        </div>
        {{-- Modal body end --}}
    </div>

<script src="{{ asset('assets/js/canvas-designer-widget.js') }}"></script>
<script src="{{ asset('assets/dist/FileBufferReader.js') }}"></script>

<script src="{{ asset('assets/js/adapterjs/0.15.3/adapter.min.js') }}"></script>
<script src="{{ asset('assets/dist/RTCMultiConnection.js') }}"></script>
<script src="{{ asset('assets/js/socket.io/2.1.1/socket.io.js') }}"></script>

<script src="{{ asset('assets/js/canvas-designer/dev/webrtc-handler.js') }}"></script>
<script src="{{ asset('assets/js/canvas-designer/canvas-designer-widget.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="{{ asset('assets/vendor/popper.js/umd/popper.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>

<!-- <script src="https://www.youtube.com/iframe_api"></script> -->

<!--/ Scrollable /-->
<script src="{{ asset('assets/js/main.js') }}"></script>
<script src="{{ asset('assets/vendor/perfect-scrollbar/dist/perfect-scrollbar.min.js') }}"></script>

<!--/ jPlayer /-->
<script type="text/javascript" src="{{ asset('assets/vendor/jPlayer-2.9.2/dist/jplayer/jquery.jplayer.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendor/jPlayer-2.9.2/dist/add-on/jplayer.playlist.min.js') }}"></script>

<script>
    $(document).ready(function(){
        let baseUrl   = $('base').attr('href');
        let _token = $('meta[name="csrf-token"]').attr('content');

        setTableHeight();

        setVideoHeight();

        initCanvas();

        minMax();

        jPlayerPlaylist();

        $('.minimize_maximize').click(function(){
            let tag = $(this).attr('data-tag');

            if(tag == 'maximize'){
                $(this).attr('data-tag', 'minimize');
                $('.video_data').addClass('maximized');
            }else{
                $(this).attr('data-tag', 'maximize');
                $('.video_data').removeClass('maximized');
            }
            
            minMax();
        });

        $(window).on('resize', function(){
            setTableHeight();
            setVideoHeight();
            jPlayerPlaylist();
        });

        function initCanvas(){
            let width = $('#widget-container').width();
            $('#widget-container').css({ 'right': '-'+ width +'px' });
        }

        function minMax(){
            let tag = $('.minimize_maximize').attr('data-tag');
            let icon = '';
            if(tag == 'maximize'){
                icon = '<i class="mdi mdi-arrow-expand-all"></i>';
            }else{
                icon = '<i class="mdi mdi-arrow-collapse-all"></i>';
            }
            $('.minimize_maximize').html(icon);
        }

        function setTableHeight(){
            let winHeight = $(window).height();
            let slider_data = $('#slider_data').offset().top;
            let outerHeight = $('#slider_data').outerHeight();

            let height = winHeight - (slider_data + outerHeight + 3);
            
            $('#table_lower').css({
               'height': height + 'px'
            });
            $('#table_lower .td').css({
               'height': height + 'px'
            });
        }
        
        function setVideoHeight(){
            let h = $('#video_data').innerHeight();
                h = h - 10;
            let w = $('#video_data').innerWidth();
            $('#lesson_video').attr('height', h);
            $('#video-placeholder').attr('height', h);
            $('#video-placeholder').attr('width', w);
            //alert();video-placeholder
        }

        function jPlayerPlaylist(){
            let playlist = $('.playlist').innerHeight();
            let jpinterface = $('.jp-interface').height();
            let height = playlist - (jpinterface + 15) ;
            $('.jp-playlist').css({
                'height': height + 'px'
            });
        }
        

        $('[data-toggle="tooltip"]').tooltip(); 
        
        
        
        //Expand
        //mdi-arrow-collapse
        $(document).on('click', '.expand', function(){
            let icon = `<i class="mdi mdi-arrow-collapse-vertical"></i>`;
            let target = $(this).attr('data-target');
            let width  = $('#'+ target).outerWidth();
           
            if(target == 'scribble_data'){
                width  = parseInt(width) + 5;
            }
            $('#'+ target).addClass('expand-vertical').css({
                'width': width+'px'
            });
            $(this).removeClass('expand').addClass('collapse').html(icon);
        });

        $(document).on('click', '.collapse', function(){
            let icon = `<i class="mdi mdi-arrow-expand-vertical"></i>`;
            let target = $(this).attr('data-target');
            let width  = $('#'+ target).outerWidth();

            $('#'+ target).removeClass('expand-vertical').removeAttr('style');
            $(this).removeClass('collapse').addClass('expand').html(icon);
        });

        $('.close_scribble').click(function(){
            let width = $('#widget-container').width();
            $('#widget-container').css({ 'right': '-'+ width +'px' });
        });

        $('.showCanvas').click(function(){
            $('#widget-container').css({ 'right': '0' });
        });

        /*
        * pdf-prev
        * pdf-next
        */
        /*
        $(document).on('click', '.button-action', function(){
            let pdfpage = $('#pdf-page').val();
            let action  = $(this).attr('data-action');
            let setPage = 1;
            if(action == 'next'){
                setPage = parseInt(pdfpage) + 1;
            }else if(action == 'prev' && pdfpage > 1){
                setPage = parseInt(pdfpage) - 1;
            }

            $('#pdf-page').val(setPage);
           
            let object_data = $('#prev_image').find('object').attr('data');


            let url = new URL(object_data);
            
            let origin = url.origin;
            let pathname = url.pathname;
            let hash = '#toolbar=0&view=FitV&scrollbar=1&page='+ setPage;

            let new_url = origin + pathname + hash;
            
            $('#prev_image').find('object').attr('data', new_url);
        }); 
        */
    });
</script>

<!--/ jPlayerControls /-->
<script>
    $(document).ready(function(){
        new jPlayerPlaylist({
       
            jPlayer: "#jquery_jplayer_2",
            cssSelectorAncestor: "#jp_container_2"
        }, [
            @if($media)
            @foreach($media as $mp3)
            {
                title:"{{ $mp3->original_name }}",
                mp3:"{{ asset('uploads/video/'. $mp3->attachment) }}"
            },
            @endforeach
            @endif
        ], 
       
        {
      
            loop: true,
            swfPath: "{{ asset('assets/vendor/jPlayer-2.9.2/dist/jplayer') }}",
            supplied: "oga, mp3",
            wmode: "window",
            ended: function() { // The $.jPlayer.event.ended event
                $(this).jPlayer("stop"); // Repeat the media
            },
            useStateClassSkin: true,
            autoBlur: false,
            smoothPlayBar: true,
            keyEnabled: true,

        });
        
    });

    
</script><!--/ jPlayerControls /-->

<script>
var scrollable = document.querySelectorAll('.scrollable_container');

new PerfectScrollbar(scrollable[0],{
    suppressScrollX: true
});

new PerfectScrollbar(scrollable[1], {
  suppressScrollX: true
});

var widget_container = document.getElementById('widget-container');

var isOpen    = false;
var sessionid = '{{ $lesson->id }}';

@if($user_data->role == teacher)
    isOpen = true;
@endif

@if($user_data->role == student)
    isOpen = false;
@endif

var connection = new RTCMultiConnection();

connection.socketURL = '{{ multi_rtc_connection }}';

connection.extra.userFullName = 'JohnDoe';

connection.socketMessageEvent = 'canvas-dashboard-demo';

connection.autoCloseEntireSession = false;

connection.maxParticipantsAllowed = 3;

var designer = new CanvasDesigner();

var hhhh = '7000';
// you can place widget.html anywhere
designer.widgetHtmlURL = "{{ widget_html }}";
designer.widgetJsURL = "{{ widget_js }}";
        

designer.addSyncListener(function(data) {
    connection.send(data);
});

designer.setSelected('pencil');

designer.setTools({
    pencil: true,
    text: true,
    image: true,
    pdf: true,
    eraser: true,
    line: true,
    arrow: true,
    dragSingle: true,
    dragMultiple: true,
    arc: true,
    rectangle: true,
    quadratic: false,
    bezier: true,
    marker: true,
    zoom: false,
    lineWidth: false,
    colorsPicker: false,
    extraOptions: false,
    code: false,
    undo: true
});

// here goes RTCMultiConnection

connection.chunkSize = 16000;
connection.enableFileSharing = true;

connection.session = {
    audio: true,
    video: true,
    data: true
};
connection.sdpConstraints.mandatory = {
    OfferToReceiveAudio: true,
    OfferToReceiveVideo: true
};


connection.onopen = function(event) {
    connection.onUserStatusChanged(event);

    if (designer.pointsLength <= 0) {
        // make sure that remote user gets all drawings synced.
        setTimeout(function() {
            connection.send('plz-sync-points');
        }, 1000);
    }
};

connection.onclose = connection.onerror = connection.onleave = function(event) {
    connection.onUserStatusChanged(event);
};

connection.onmessage = function(event) {
    if (event.data.chatMessage) {
        appendChatMessage(event);
        return;
    }

    if (event.data.checkmark === 'received') {
        var checkmarkElement = document.getElementById(event.data.checkmark_id);
        if (checkmarkElement) {
            checkmarkElement.style.display = 'inline';
        }
        return;
    }

    if (event.data === 'plz-sync-points') {
        designer.sync();
        return;
    }

    designer.syncData(event.data);
};

// extra code

function beforeOpenRoom(callback) {
    // capture canvas-2d stream
    // and share in realtime using RTCPeerConnection.addStream
    // requires: dev/webrtc-handler.js
    designer.captureStream(function(stream) {
        stream.isScreen = true;
        stream.streamid = stream.id;
        stream.type = 'local';
        
        /*
        var video = document.createElement('video');
        video.muted = true;
        video.srcObject = stream;
        video.play();
        */

        connection.attachStreams.push(stream);
        connection.onstream({
            stream: stream,
            type: 'local',
            streamid: stream.id,
            // mediaElement: video
        });

        callback();
    });
}

connection.onstream = function(event) {
    
    var isInitiator = connection.isInitiator;

    if (event.stream.isScreen) {
        var video = document.getElementById('main-video');
        video.setAttribute('data-streamid', event.streamid);
        video.style.display = 'none';
        video.srcObject = event.stream;
    } else {
        event.mediaElement.controls = false;
        //initiator_video
        //other_video
        
        if(event.type == 'local'){
            var other_video = document.querySelector('#other_video');
            other_video.appendChild(event.mediaElement);
        }else{
        
            var initiator_video = document.querySelector('#initiator_video');
            initiator_video.appendChild(event.mediaElement);
        }
        
    }

    connection.onUserStatusChanged(event);
};

connection.onstreamended = function(event) {
    var video = document.querySelector('video[data-streamid="' + event.streamid + '"]');
    if (!video) {
        video = document.getElementById(event.streamid);
        if (video) {
            video.parentNode.removeChild(video);
            return;
        }
    }
    if (video) {
        video.srcObject = null;
        video.style.display = 'none';
    }
};


// to make sure file-saver dialog is not invoked.
connection.autoSaveToDisk = false;

var progressHelper = {};

connection.onFileProgress = function(chunk, uuid) {
    var helper = progressHelper[chunk.uuid];
    helper.progress.value = chunk.currentPosition || chunk.maxChunks || helper.progress.max;
    updateLabel(helper.progress, helper.label);
};

connection.onFileStart = function(file) {
    var div = document.createElement('div');
    div.className = 'message';

    if (file.userid === connection.userid) {
        div.innerHTML = '<b>You:</b><br><label>0%</label> <progress></progress>';
        div.style.background = '#cbffcb';
    } else {
        div.innerHTML = '<b>' + getFullName(file.userid) + ':</b><br><label>0%</label> <progress></progress>';
    }

    div.title = file.name;
    conversationPanel.appendChild(div);
    progressHelper[file.uuid] = {
        div: div,
        progress: div.querySelector('progress'),
        label: div.querySelector('label')
    };
    progressHelper[file.uuid].progress.max = file.maxChunks;

    conversationPanel.scrollTop = conversationPanel.clientHeight;
    conversationPanel.scrollTop = conversationPanel.scrollHeight - conversationPanel.scrollTop;
};

function updateLabel(progress, label) {
    if (progress.position == -1) return;
    var position = +progress.position.toFixed(2).split('.')[1] || 100;
    label.innerHTML = position + '%';
}

designer.appendTo(widget_container, function() {

    if (isOpen == true) {
        beforeOpenRoom(function() {
            
            if(sessionid == null || sessionid == ''){
                connection.open(sessionid, function(isRoomOpened, roomid, error) {
                    if (error) {
                        if (error === connection.errors.ROOM_NOT_AVAILABLE) {
                            alert('Someone already created this room. Please either join or create a separate room.');
                            return;
                        }
                        alert(error);
                    }

                    connection.socket.on('disconnect', function() {
                        location.reload();
                    });
                });
            }else{
                connection.join(sessionid, function(isRoomJoined, roomid, error) {

                    if(isRoomJoined == false){
                        connection.open(sessionid, function(isRoomOpened, roomid, error) {
                            if (error) {
                                if (error === connection.errors.ROOM_NOT_AVAILABLE) {
                                    alert('Someone already created this room. Please either join or create a separate room.');
                                    return;
                                }
                                alert(error);
                            }
                        });
                    }

                    connection.socket.on('disconnect', function() {
                        location.reload();
                    });
                });
            }            
        });
        
    } else {
        connection.join(sessionid, function(isRoomJoined, roomid, error) {
            if (error) {
                if (error === connection.errors.ROOM_NOT_AVAILABLE) {
                    alert('This room does not exist. Please either create it or wait for moderator to enter in the room.');
                    return;
                }
                if (error === connection.errors.ROOM_FULL) {
                    alert('Room is full.');
                    return;
                }
                alert(error);
            }

            connection.socket.on('disconnect', function() {
                location.reload();
            });
        });
    }
});
</script>

<script>
    $(document).ready(function(){
        let _csrf = $('meta[name="csrf-token"]').attr('content');
        //window.onbeforeunload = function () {return false;}
        $('span').click(function () {
                $(this).closest('.rating').find('span').removeClass('checked');
                $(this).closest('span').addClass('checked');

                

        });
        

        function startTimer(duration, display) {
            var timer = duration, minutes, seconds;
            setInterval(function () {
                minutes = parseInt(timer / 60, 10)
                seconds = parseInt(timer % 60, 10);

                minutes = minutes < 10 ? "0" + minutes : minutes;
                seconds = seconds < 10 ? "0" + seconds : seconds;

                display.text(minutes + ":" + seconds);
				++timer;
                if (timer > 60 * 15) {
                   
                    $('#time').css("color", "red");


                }
            }, 1000);
            

        }

        $(function ($) {
            var startZero = 0,
                display = $('#time');
            startTimer(startZero, display);
        });
        
        $('#teach-btn').click(function (){
            var time = $("#time").text();
            finalTime = localStorage.setItem('timeStop',time);
            
        });

        $('#rating').click(function (event){
            event.preventDefault();
            finalTime = localStorage.getItem('timeStop');
            // let student = $( "input[type=radio][name=student]:checked" ).val();
            // let listening = $( "input[type=radio][name=listening]:checked" ).val();
            // let writing = $( "input[type=radio][name=writing]:checked" ).val();
            // let grammar = $( "input[type=radio][name=grammar]:checked" ).val();
            // let speaking = $( "input[type=radio][name=speaking]:checked" ).val();
            // let teacher = $( "input[type=radio][name=teacher]:checked" ).val();
            // let lesson_id = $("input[name='lesson_id']").val();
            let lesson_id = '{{$lesson->id}}';
            let chatFlag = $("input[name='chatFlag']").val();
            console.log(chatFlag);
            // if(chatFlag){
            //     chatFlag = 1;
            // }else{
            //     chatFlag = 0;
            // }

                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: '{{ url('lessons/save-rating') }}',
                    data: {
                        lesson_id : lesson_id,
                        chatFlag: chatFlag,
                        _token: _csrf
                    },
                    success: function(response){
                            console.log(response);
                            if(response.response == "200"){
                                window.close();  
                            }else{
                                console.log('failed');
                            }
                    }
                });
        });

        
    });
</script>
</body>
</html>
