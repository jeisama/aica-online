@extends('layouts.app')

@section('css')

@endsection

@section('content')
    <div class="breadcrumb-holder container-fluid">
        <ul class="breadcrumb">
            
            <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i> Home</a></li>
            <li class="breadcrumb-item"><a href="{{ url('lessons') }}">Lessons</a></li>
            <li class="breadcrumb-item active">{{ $lesson->title }}</li>
            
        </ul>
        
    </div>
    
    <section class="forms lessons">
        <div class="container-fluid">
            <div class="card">
                <div class="card-body" style="display: relative;">
                    @if (Auth::user()->role !== 2)
                        <a class="btn btn-primary pull-right" href="{{ url('lessons/conference/'. $lesson->slug)}}" target="_blank">Join Video Lesson</a>
                    @else
                <a class="btn btn-primary pull-right" href="{{ url('lessons/conference/'. $lesson->slug)}}" target="_blank" id="lesson_start" data-id="{{$lesson->schedule_id}}">Start Video Lesson</a>
                {{-- <a class="btn btn-primary pull-right" href="javascript:void(0);" id="lesson_start" data-id="{{$lesson->schedule_id}}">Start Video Lesson</a> --}}
                    @endif
                    <div class="lesson-title">Lesson: {{ $lesson->title }}</div>
                    <div class="teacher_date">
                        <div class="avatar">
                            <span class="inner">
                                @if($lesson->teacher->file != '' && file_exists('uploads/profiles/'.$lesson->teacher->file) == 1)
                                <img src="{{ asset('uploads/profiles/'. $lesson->teacher->file) }}" alt="{{ $lesson->teacher->full_name }}">
                                @else
                                <img src="{{ $lesson->teacher->gender === 1 ? asset('uploads/profiles/male.png') : asset('uploads/profiles/female.png') }}" alt="{{ $lesson->teacher->full_name }}">
                                @endif
                            </span>
                        </div>
                        <div class="name">
                            {{ $lesson->teacher->full_name }} &nbsp;&nbsp;
                            <i class="mdi mdi-calendar-range"></i> {{ date('l, F d Y', strtotime($lesson->schedule->date)) }}
                        </div>
                    </div>
                    
                    {{-- <div class="lesson-title">Description</div>

                    {!! $lesson->description !!} --}}

                    @if(count($lesson->attachment) > 0)
                    <br>
                    <div class="lesson-title">Attachment</div>
                    @foreach($lesson->attachment as $attachment)

                        <div><a href="{{ asset('uploads/lessons/'. $attachment->attachment) }}" target="_blank">{{ $attachment->original_name }}</a></div>
                    @endforeach
                    @endif
                    {{--
                    @if($lesson->source != null && $lesson->source == 'upload')
                    <video controls="controls" width="560" height="315">

                        <source src="{{ asset('uploads/video/'. $lesson->source_data) }}" type="video/mp4">
                    </video>
                    @endif

                    @if($lesson->source != null && $lesson->source === 'url')
                    {{ preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $lesson->source_data, $matches) }}
                    <iframe width="560" height="315" src="//www.youtube.com/embed/{{ $matches[1] }}?rel=0" frameborder="0" allowfullscreen></iframe>
                    @endif
                    --}}
                    {{-- <div class="line-double"></div> --}}

                    {{-- @if($user_data->role != 3)

                   {{ Form::open(['url' => url()->current() .'#student_form', 'method' => 'get', 'id' => 'student_form']) }}
                    <div class="form-row">
                        <div class="form-group col col-md-12 col-lg-4">
                            <label for="student">Select Student</label>
                            <select id="student" name="student" class="form-control">
                                <option value="">Select Student</option>
                                @if($students != null)
                                @foreach($students as $student)
                                <option value="{{ $student->info->user_id }}" {{ $student_id == $student->info->user_id ? 'selected':'' }}>{{ $student->info->user->username }}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    {{ Form::close() }}
                    <div class="line-double"></div>

                    @endif --}}

                    {{-- <h4>Comments</h4>

                    <div class="comments" id="comments">
                    @if($comments != null)
                        @foreach($comments as $comment)
                        <div class="comment-data" id="anchor{{ $comment->id }}">
                            <div class="comment-avatar">
                                <i class="mdi mdi-account-circle mdi-36px"></i>
                            </div>
                            <div class="comment-body">
                                <div class="name">{{ $comment->user->username }}</div>
                                <div class="carbon">{{ \App\Helpers\DateCarbon::date($comment->created_at) }}</div>
                                <div class="note">
                                    {!! nl2br(e($comment->comment)) !!}


                                    @if($comment->attachment != null)
                                    <br>
                                    <a href="{{ asset('uploads/lessons/'. $comment->attachment) }}" target="_blank">{{ $comment->original_name }}</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endforeach
                    @else

                    @endif
                    </div> --}}

                    {{-- <div class="line"></div> --}}
                    {{-- <h4>Write Comment</h4> --}}

                    @if($user_data->role === 3 || ($user_data->role === 2 && $student_id != null))

                    @if (Session::has('success_message'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {!! session('success_message') !!}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif

                    {{ Form::open(['url' => url()->full() , 'id' => 'message', 'files' => true]) }}
                    <div class="form-group">
                        <label for="note">Note</label>
                        <textarea name="note" id="note" class="form-control" rows="7"></textarea>
                        @if($errors->has('note'))
                            <div class="invalid-feedback">{{ $errors->first('note') }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <input type="file" name="attachment">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary">Submit</button>
                    </div>
                    {{ Form::close() }}
                    @endif

                </div>
            </div>
            
        </div>

    </section>
@endsection

@section('js')
<script>
    $(document).ready(function(){
        let baseUrl   = $('base').attr('href');
        let _token = $('meta[name="csrf-token"]').attr('content');
        let lessonID = $('#lesson_start').attr('data-id');
       
        
        $('#student').change(function(){
            let value = $('option:selected', this).val();
            if(value != ''){
                $('form#student_form').submit();
            }
        });
        
        $( "#lesson_start" ).click(function( event ) {
                //event.preventDefault();
                 $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: baseUrl +'/lessons/video-start-email',
                    data: {
                        _token: _token,
                        lesson_id: lessonID
                    },success:function(response){
                        console.log(response);
                    }
                });
        });
       

    });
</script>
@endsection