@extends('layouts.app')

@section('css')
@if(Auth::user()->role == 1)
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-select/bootstrap-select.css') }}">
@endif
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/jquery-ui/jquery-ui.css') }}">
@endsection

@section('content')
<div class="breadcrumb-holder container-fluid">
        <div class="inner">
            <div class="links">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i> {{__('label.home')}}</a></li>
                    <li class="breadcrumb-item active">{{__('label.lesson')}}</li>
                </ul>
            </div>
            {{-- @if(Auth::user()->role == 5) 
            <div class="buttons">
                <a href="{{ url('lessons/create') }}" class="btn btn-primary btn-sm"><i class="mdi mdi-plus"></i> Create Lesson</a>
            </div>
            @endif --}}
        </div>        
    </div>
    
    <section class="forms">
        <div class="container-fluid">
            
            @if (Session::has('success_message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {!! session('success_message') !!}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif

            @if (Session::has('error_message'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {!! session('error_message') !!}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif

            <div class="card">
                <div class="card-header d-flex align-items-center">
                    <h3>{{__('label.schedule_details')}}</h3>
                </div>
                <div class="card-body">
                    @if($user_data->role == 4 || $user_data->role == 5)
                        <div class="row">
                                <div class="col">
                                    <div class="card">
                                        <div class="card-header">
                                            <h3 class="h4">{{__('label.course')}}</h3>
                                        </div>
                                        <div class="card-body nopadding">
                                            <div class="table-responsive">
                                                <table class="table table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">{{__('label.course')}}</th>
                                                            <th class="text-center">{{__('label.grade')}}</th>
                                                            <th class="text-center">{{__('label.lesson_name')}}</th>
                                                            <th class="text-center">{{__('label.day')}}</th>
                                                            @if ($user_data->role == 4)
                                                                <th>{{__('label.teacher')}}</th>    
                                                            @elseif($user_data->role == 5)
                                                                <th class="text-center">{{__('label.student')}}</th>
                                                            @endif
                                                            <th class="text-center">{{__('label.date')}}</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        
                                                        @foreach($lessons as $lesson)
                                                        <tr>
                                                            <td class="text-center">{{ $lesson->course->label }}  </td>
                                                            <td class="text-center">{{ $lesson->grade->label }}</td>
                                                            <td class="text-center">
                                                                @if($lesson->lesson)
                                                                <a href="{{url('lessons/show/'. $lesson->lesson->id)}}" class="details"> {{$lesson->lesson ? $lesson->lesson->lesson_name : '' }}</a>
                                                                @else
                                                                {{$lesson->lesson ? $lesson->lesson->lesson_name : 'No Lessons Available' }}
                                                                @endif
                                                                </td>
                                                            <td class="text-center">Day  {{ $lesson->lesson ? $lesson->lesson->lesson_day : $lesson->day }}</td>
                                                            <td class="text-center">{{$user_data->role == 4 ? $lesson->teacher->full_name : $lesson->student->romaji_name}}</td>
                                                            <td class="text-center">{{ date('l, Y/m/d H:i', strtotime($lesson->date . $lesson->start_time)) }}</td>
                                                       
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="row">
                                    <div class="col">
                                        <div class="card">
                                            <div class="card-header">
                                                <h3 class="h4">{{__('label.course')}}</h3>
                                            </div>
                                            <div class="card-body nopadding">
                                                <div class="table-responsive">
                                                    <table class="table table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th class="text-center">{{__('label.course')}}</th>
                                                                <th class="text-center">{{__('label.grade')}}</th>
                                                                <th class="text-center">{{__('label.lesson_name')}}</th>
                                                                <th class="text-center">{{__('label.day')}}</th>
                                                                <th class="text-center">{{__('label.teacher')}}</th>    
                                                                <th class="text-center">{{__('label.student')}}</th>
                                                                <th class="text-center">{{__('label.date')}}</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            
                                                            @foreach($lessons as $lesson)
                                                            <tr>
                                                                <td class="text-center">{{ $lesson->course->label }}  </td>
                                                                <td class="text-center">{{ $lesson->grade->label }}</td>
                                                                <td class="text-center">
                                                                    @if($lesson->lesson)
                                                                    <a href="{{url('lessons/show/'. $lesson->lesson->id)}}" class="details"> {{$lesson->lesson ? $lesson->lesson->lesson_name : '' }}</a>
                                                                    @else
                                                                    {{$lesson->lesson ? $lesson->lesson->lesson_name : 'No Lessons Available' }}
                                                                    @endif
                                                                    </td>
                                                                <td class="text-center">Day {{ $lesson->lesson ? $lesson->lesson->lesson_day : $lesson->day }}</td>
                                                                <td class="text-center">{{$lesson->teacher ? $lesson->teacher->full_name : ''}}</td>
                                                                <td class="text-center">{{$lesson->student ? $lesson->student->romaji_name : ''}}</td>
                                                                <td class="text-center">{{ date('l, Y/m/d H:i', strtotime($lesson->date . $lesson->start_time)) }}</td>
                                                        
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                        @endif
                </div>
            </div>
        </div>
         <!-- Modal -->
    <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                        <div class="modal-header">
                                <dl class="row">
                                        <dt class="col-sm-3">Course :</dt>
                                        <dd class="col-sm-8" id="val_course_label"></dd>
                                        <dt class="col-sm-3">Grade :</dt>
                                        <dd class="col-sm-8" id="val_grade_label"></dd>
                                        <dt class="col-sm-3">Lesson Name :</dt>
                                        <dd class="col-sm-8" id="val_lesson_name"></dd>
                                </dl>        
                                
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                                <div class="homework-box" style="padding: 15px; width: 95%;">
                                    <div class="tbl" style="display: table; width: 100%;">
                                        <div style="display: table-cell; vertical-align: top; width:30px;">1</div>
                                            <div style="display: table-cell; vertical-align: top;">
                                                <div class="form-row">
                                                    <div class="form-group col col-md-12 col-lg-11">
                                                        <label for="writing_desc" class="form-control-label">Writing課題　（目安：10分）</label>
                                                        <div class="box-content">
                                                            <span id="val_writing_1"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="tbl" style="display: table; width: 100%;">
                                        <div style="display: table-cell; vertical-align: top; width:30px;">2</div>
                                            <div style="display: table-cell; vertical-align: top;">
                                                <div class="form-row">
                                                    <div class="form-group col col-md-12 col-lg-11">
                                                        <label for="speaking_instruction" class="form-control-label">Speaking課題　（目安：5分）</label>
                                                        <div class="box-content">
                                                                <span id="val_speaking_1"></span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col col-md-12 col-lg-11">
                                                        <label for="speaking_topic" class="form-control-label">次週のSpeaking Topic:</label>
                                                        <div class="box-content">
                                                                <span id="val_speaking_2"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="tbl" style="display: table; width: 100%;">
                                        <div style="display: table-cell; vertical-align: top; width:30px;">3</div>
                                            <div style="display: table-cell; vertical-align: top;">
                                                <div class="form-row">
                                                    <div class="form-group col col-md-12 col-lg-11">
                                                        <label for="writing_student_1" class="form-control-label">Writing Exercise</label>
                                                        <div class="box-content">
                                                                <span id="val_writing_student_1"></span>
                                                        </div>
                                                        <textarea  id="writing_student_2" name="writing_exercise_stu" class="form-control"  disabled></textarea>
                                                
                                                    </div>
                                                    
                                                </div>
                                        </div>
                                    </div>
                                    <div class="tbl" style="display: table; width: 100%;">
                                        <div style="display: table-cell; vertical-align: top; width:30px;">4</div>
                                            <div style="display: table-cell; vertical-align: top;">
                                                <div class="form-row">
                                                    <div class="form-group col col-md-12 col-lg-11">
                                                        <label for="listening_1" class="form-control-label">Listening課題　（目安：15分）</label>
                                                        <div class="box-content">
                                                                <span id="val_listening_1"></span>
                                                        </div><br>
                                                        <label for="listening_1" class="form-control-label">今週のReproduction課題：</label>
                                                        <div class="box-content">
                                                                <span id="val_listening_2"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="tbl" style="display: table; width: 100%;">
                                        <div style="display: table-cell; vertical-align: top; width:30px;">5</div>
                                            <div style="display: table-cell; vertical-align: top;">
                                                <div class="form-row">
                                                    <div class="form-group col col-md-12 col-lg-11">
                                                        <label for="homework_1" class="form-control-label">Homework Listening Intruction</label>
                                                        <div class="box-content">
                                                                <span id="val_homework_1"></span>
                                                        </div><br>
                                                        <label for="homework_listening" class="form-control-label">今週のListening課題：</label>
                                                        <div class="box-content">
                                                                <span id="val_homework_2"></span>
                                                        </div>
                                                        <textarea id="homework_listening" name="homework_listening" class="form-control" disabled></textarea>
                                                        
                                                    </div>
                                                    
                                                </div>
                                        </div>
                                    </div> 
                                </div><!--homework-box-->
                        </div>
                        <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        
                        </div>
                </div>
            </div>
      </div>
<!-- Modal -->
        <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                            <div class="modal-header">
                                    <dl class="row">
                                            <dt class="col-sm-3">Course :</dt>
                                            <dd class="col-sm-8" id="val_course_label"></dd>
                                            <dt class="col-sm-3">Grade :</dt>
                                            <dd class="col-sm-8" id="val_grade_label"></dd>
                                            <dt class="col-sm-3">Lesson Name :</dt>
                                            <dd class="col-sm-8" id="val_lesson_name"></dd>
                                    </dl>        
                                    
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                    <div class="homework-box" style="padding: 15px; width: 95%;">
                                        <div class="tbl" style="display: table; width: 100%;">
                                            <div style="display: table-cell; vertical-align: top; width:30px;">1</div>
                                                <div style="display: table-cell; vertical-align: top;">
                                                    <div class="form-row">
                                                        <div class="form-group col col-md-12 col-lg-11">
                                                            <label for="writing_desc" class="form-control-label">Writing課題　（目安：10分）</label>
                                                            <div class="box-content">
                                                                <span id="val_writing_1"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="tbl" style="display: table; width: 100%;">
                                            <div style="display: table-cell; vertical-align: top; width:30px;">2</div>
                                                <div style="display: table-cell; vertical-align: top;">
                                                    <div class="form-row">
                                                        <div class="form-group col col-md-12 col-lg-11">
                                                            <label for="speaking_instruction" class="form-control-label">Speaking課題　（目安：5分）</label>
                                                            <div class="box-content">
                                                                    <span id="val_speaking_1"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col col-md-12 col-lg-11">
                                                            <label for="speaking_topic" class="form-control-label">次週のSpeaking Topic:</label>
                                                            <div class="box-content">
                                                                    <span id="val_speaking_2"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="tbl" style="display: table; width: 100%;">
                                            <div style="display: table-cell; vertical-align: top; width:30px;">3</div>
                                                <div style="display: table-cell; vertical-align: top;">
                                                    <div class="form-row">
                                                        <div class="form-group col col-md-12 col-lg-11">
                                                            <label for="writing_student_1" class="form-control-label">Writing Exercise</label>
                                                            <div class="box-content">
                                                                    <span id="val_writing_student_1"></span>
                                                            </div>
                                                            <textarea  id="writing_student_2" name="writing_exercise_stu" class="form-control"  disabled></textarea>
                                                    
                                                        </div>
                                                        
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="tbl" style="display: table; width: 100%;">
                                            <div style="display: table-cell; vertical-align: top; width:30px;">4</div>
                                                <div style="display: table-cell; vertical-align: top;">
                                                    <div class="form-row">
                                                        <div class="form-group col col-md-12 col-lg-11">
                                                            <label for="listening_1" class="form-control-label">Listening課題　（目安：15分）</label>
                                                            <div class="box-content">
                                                                    <span id="val_listening_1"></span>
                                                            </div><br>
                                                            <label for="listening_1" class="form-control-label">今週のReproduction課題：</label>
                                                            <div class="box-content">
                                                                    <span id="val_listening_2"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="tbl" style="display: table; width: 100%;">
                                            <div style="display: table-cell; vertical-align: top; width:30px;">5</div>
                                                <div style="display: table-cell; vertical-align: top;">
                                                    <div class="form-row">
                                                        <div class="form-group col col-md-12 col-lg-11">
                                                            <label for="homework_1" class="form-control-label">Homework Listening Intruction</label>
                                                            <div class="box-content">
                                                                    <span id="val_homework_1"></span>
                                                            </div><br>
                                                            <label for="homework_listening" class="form-control-label">今週のListening課題：</label>
                                                            <div class="box-content">
                                                                    <span id="val_homework_2"></span>
                                                            </div>
                                                            <textarea id="homework_listening" name="homework_listening" class="form-control" disabled></textarea>
                                                            
                                                        </div>
                                                        
                                                    </div>
                                            </div>
                                        </div> 
                                    </div><!--homework-box-->
                            </div>
                            <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                    </div>
                </div>
            </div>
    </section>
@endsection

@section('js')
@if(Auth::user()->role == 1)
<script src="{{ asset('assets/vendor/bootstrap-select/bootstrap-select.min.js') }}"></script>
@endif
<script src="{{ asset('assets/vendor/jquery-ui/jquery-ui.js') }}"></script>
<script>
    $(document).ready(function(){
        $('.datepicker').datepicker();
        
        $('.details').each(function(){
                $(this).click(function(){
                    let id = $(this).attr('data-id');
                        $.ajax({
                            type: 'get',
                            dataType: 'json',
                            url: '{{ url('lessons/show') }}/'+ id,
                            success:function(response){
                                //console.log(response);
                                showModal(response.data);
                            }
                    });
                    
                });
            });
            function showModal(data){
                //console.log(data[0]);
                Object.keys(data).forEach(function(key) {
                    $('#val_'+ key).text(data[key]);
                    console.log(key);
                });
                $('#exampleModalLong').modal('show');
            }

    });
</script>
@endsection