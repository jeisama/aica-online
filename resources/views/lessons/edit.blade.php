@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-select/bootstrap-select.css') }}">
@endsection

@section('content')
	<div class="breadcrumb-holder container-fluid">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i> Home</a></li>
            <li class="breadcrumb-item"><a href="{{ url('/lessons') }}">Lessons</a></li>
            <li class="breadcrumb-item active">Edit Lesson</li>
        </ul>
    </div>

    <section class="forms">
			
    	<div class="container-fluid">

    		@if (Session::has('success_message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {!! session('success_message') !!}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif

            @if (Session::has('error_message'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {!! session('error_message') !!}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif

			<div class="card">
			
				<div class="card-header d-flex align-items-center">
					<h3 class="h4">Lesson Details</h3>
				</div>
					
					<div class="card-body">
					{{ Form::open(['files' => true,  'id' => 'update-lesson', 'onsubmit' => 'event.preventDefault()']) }}	
						{{-- start of new form --}}
						<input type="hidden" name="course_type" value="{{$templates->course_type}}">
						<input type="hidden" name="lesson_type" value="{{$templates->lesson_type}}">
						<div class="form-row">
								<div class="form-group col col-md-12 col-lg-8">
									<label for="lesson_name" class="form-control-label">Lesson Name</label>
									<input type="text" id="val_lesson_name" name="lesson_name"  class="form-control" value="{{ $templates->lesson_name}}">
								</div>
						</div>
						<div class="homework-box" style="border: 1px solid #c5c5c5; padding: 15px; width: 100%;">
                                <div class="tbl" style="display: table; width: 100%;">
                                    <div style="display: table-cell; vertical-align: top; width:30px;"><strong>1</strong></div>
                                        <div style="display: table-cell; vertical-align: top;">
                                            <div class="form-row">
                                                <div class="form-group col col-md-12 col-lg-12">
                                                    <label for="writing_1" class="form-control-label"><strong>Writing課題　（目安：10分）</strong></label>
                                                    <textarea rows="4" cols="50" id="writing_1" name="writing_1" class="form-control"> {{ $templates->writing_1}}</textarea>
                                                    <small id="error-writing_1" class="form-text form-error"></small>
                                                </div>
                                                
                                            </div>
                                    </div>
                                </div>     
                                <div class="tbl" style="display: table; width: 100%;">
                                    <div style="display: table-cell; vertical-align: top; width:30px;"><strong>2</strong></div>
                                        <div style="display: table-cell; vertical-align: top;">
                                            <div class="form-row">
                                                <div class="form-group col col-md-12 col-lg-12">
                                                    <label for="speaking_1" class="form-control-label"><strong>Speaking課題　（目安：5分）</strong></label>
                                                    <textarea rows="4" cols="50" id="speaking_1" name="speaking_1" class="form-control">{{ $templates->speaking_1}}</textarea>
                                                    <small id="error-speaking_1" class="form-text form-error"></small>
                                                </div>
                                                <div class="form-group col col-md-12 col-lg-12">
                                                <label for="speaking_2" class="form-control-label"><strong>Please input speaking topic</strong></label><br>
                                                    <input type="text" name="speaking_2" placeholder="次週のSpeaking Topic:" class="form-control" value="{{ $templates->speaking_2}}">
                                                    <small id="error-speaking_2" class="form-text form-error"></small>
                                                </div>
                                                <div class="form-group col col-md-12 col-lg-12">
                                                    <textarea rows="4" cols="50" id="speaking_3" name="speaking_3" class="form-control" disabled></textarea>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                                <div class="tbl" style="display: table; width: 100%;">
                                    <div style="display: table-cell; vertical-align: top; width:30px;"><strong>3</strong></div>
                                        <div style="display: table-cell; vertical-align: top;">
                                            <div class="form-row">
                                                <div class="form-group col col-md-12 col-lg-12">
                                                    <label for="writing_student_1" class="form-control-label"><strong>Writing Exercise</strong></label>
                                                    <input type="text" id="writing_student_1" name="writing_student_1" class="form-control" value="{{ $templates->writing_student_1}}">
                                                    <small id="error-writing_student_1" class="form-text form-error"></small>
                                                    <textarea rows="4" cols="50" id="writing_student_2" name="writing_student_2" class="form-control" disabled></textarea>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                                <div class="tbl" style="display: table; width: 100%;">
                                    <div style="display: table-cell; vertical-align: top; width:30px;"><strong>4</strong></div>
                                        <div style="display: table-cell; vertical-align: top;">
                                            <div class="form-row">
                                                <div class="form-group col col-md-12 col-lg-12">
                                                    <label for="listening_1" class="form-control-label"><strong>Listening課題　（目安：15分）</strong></label>
                                                    <textarea rows="4" cols="50" id="listening_1" name="listening_1" class="form-control">{{ $templates->listening_1}}</textarea>
                                                    <small id="error-listening_1" class="form-text form-error"></small>
                                                    <br>
                                                </div>
                                                <div class="form-group col col-md-12 col-lg-12">
                                                        <label for="listening_2" class="form-control-label"><strong>Please input listening instruction</strong></label>
                                                        <input type="text" id="listening_2" name="listening_2" class="form-control" placeholder="Listening課題　（目安：15分）"
                                                        value="{{ $templates->listening_2}}">
                                                        <small id="error-listening_2" class="form-text form-error"></small><br>
                                                        
                                                </div>
                                                <div class="form-group col col-md-12 col-lg-12">
                                                    <textarea rows="4" cols="50" id="listening_3" name="reproduction_exercise" class="form-control" disabled></textarea>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                <div class="tbl" style="display: table; width: 100%;">
                                    <div style="display: table-cell; vertical-align: top; width:30px;"><strong>5</strong></div>
                                        <div style="display: table-cell; vertical-align: top;">
                                            <div class="form-row">
                                                <div class="form-group col col-md-12 col-lg-12">
                                                    <label for="homework_1" class="form-control-label"><strong>Homework Listening Intruction</strong></label>
                                                <textarea rows="4" cols="50" id="homework_1" name="homework_1" class="form-control">{{ $templates->homework_1}}</textarea>
                                                    <small id="error-homework_1" class="form-text form-error"></small><br>
                                                    <label for="homework_2" class="form-control-label"><strong>Please input homework intruction</strong></label>
                                                    <input type="text" id="homework_2" name="homework_2" class="form-control" placeholder="今週のListening課題：" value="{{ $templates->homework_2}}">
                                                    <small id="error-homework_2" class="form-text form-error"></small>
                                                </div>
                                                <div class="form-group col col-md-12 col-lg-12">
                                                    <textarea rows="4" cols="50" id="homework_3" name="homework_3" class="form-control" disabled></textarea>
                                                </div>
                                                
                                            </div>
                                    </div>
                                </div>
                      
                                        
							</div>
							   
							<br>
						{{-- <br>
						<div class="form-row">
							<div class="form-group col col-md-12 col-lg-4">
								<label for="attachment" class="form-control-label">Attachments</label>
								<div>
									<input type="file" name="attachment[]" id="attachment" multiple>
								</div>

								@if(count($query->attachment) > 0)
									<br>
									@foreach($query->attachment as $attachment)
									<div id="parent-{{ $attachment->id }}">{{ $attachment->original_name }} <a href="javascript:void(0);" data-target="delete-attachment" class="delete_attachment" data-id="{{ $attachment->id }}"><i class="mdi mdi-close"></i></a></div>
									@endforeach
								@endif
							</div>
						</div>
						<br>
					<div class="form-row">
						<div class="form-group col-lg-12">
							<label for="attachment" class="form-control-label">Select Audio (mp3)</label>
							<div>
								<input type="file" name="media[]" id="attachment" multiple accept="audio/*">
							</div>
						</div>
					</div>

					<div>
						@if($query->media)
						@foreach($query->media as $media)
						<div>{{ $media->source_data }} <a href="javascript:void(0);" class="delete_attachment" data-target="delete-media" data-id="{{ $media->id }}"><i class="mdi mdi-close"></i></a></div>
						@endforeach
						@endif
					</div>
					<br> --}}
					<div class="form-row">
						<div class="form-group col col-md-6 col-lg-6">
								<button type="submit" id="submit-form" data-form="update-lesson" class="btn btn-primary">{{__('label.update')}}</button>
								<a href="{{ url('templates') }}" class="btn btn-warning">{{__('label.cancel')}}</a>
								<span class="form-proccessing hidden"><img src="{{ asset('assets/img/Loading/loading.gif') }}"> {{__('label.submit_process')}}</span>
						</div>
					</div>
					
					{{ Form::close() }} 
				</div>
				
			</div>
			
		</div>
		
    </section>
@endsection

@section('js')
	<script src="{{ asset('assets/vendor/bootstrap-select/bootstrap-select.min.js') }}"></script>
	<script src="{{ asset('assets/vendor/ckeditor/ckeditor.js') }}"></script>
	<script>
		$(document).ready(function(){
			
			let _token = $('meta[name="csrf-token"]').attr('content');

			$(document).on('click', '.add_more', function(){
				$(this).remove();
				let row = `<div class="form-row">
							<div class="form-group col col-lg-3">
								<label for="source0" class="form-control-label">Source</label>
								<select id="source0" name="source[]" class="form-control select_source">
									<option value="">Select Source</option>
									<option value="local">Local</option>
									<option value="youtube">Youtube</option>
								</select>
							</div>
							<div class="form-group col col-lg-7">
								
								<div class="local hidden" style="padding-top: 30px;">
									<div class="custom-file">
									  	<input type="file" class="custom-file-input" name="media[]" id="media0">
									  	<label class="custom-file-label" for="media0">Select Media</label>
									</div>
								</div>
								
								<div class="youtube hidden">
									<label for="youtube0" class="form-control-label">Enter youtube link.</label>
									<input type="text" name="youtube[]" class="form-control">
								</div>
							</div>
							<div class="form-group col col-lg-1" style="padding-top: 30px;">
								<button type="button" class="btn btn-default btn-block hidden add_more">Add more.</button>
							</div>
						</div>`;

				$('#media_content').append(row);
			});

			$(document).on('change', '.select_source', function(){
				let val = $('option:selected', this).val();
				
				if(val != ''){
					if(val == 'local'){
						$(this).closest('.form-row').find('.local').removeClass('hidden');
						$(this).closest('.form-row').find('.youtube').addClass('hidden');
					}
					if(val == 'youtube'){
						$(this).closest('.form-row').find('.local').addClass('hidden');
						$(this).closest('.form-row').find('.youtube').removeClass('hidden');
					}
					$(this).closest('.form-row').find('.btn').removeClass('hidden');
				}else{
					$(this).closest('.form-row').find('.btn').addClass('hidden');
					$(this).closest('.form-row').find('.local').addClass('hidden');
					$(this).closest('.form-row').find('.youtube').addClass('hidden');
				}
			});

			$('.delete_media').each(function(){
				$(this).click(function(){
					let id = $(this).attr('data');
				});
			});

			$('.delete_attachment').each(function(){
				$(this).click(function(){
					let id = $(this).attr('data-id');
					let target = $(this).attr('data-target');
					let ans= confirm('Are you sure you want to delete this item?');
					if(ans){
						$.ajax({
							type: 'post',
							url: '{{ url('lessons') }}/' + target,
							data:{
								id: id,
								_token: _token
							},success:function(data){
								//$('#parent-'+ id).remove();
								window.location = window.location;
							}
						});
					}
				});
			});
		});
	</script>
@endsection