@extends('layouts.app')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/jquery-ui/jquery-ui.css') }}">
@endsection

@section('content')

    <!-- Breadcrumb -->
    <div class="breadcrumb-holder container-fluid">
        <div class="inner">
            <div class="links">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i> {{__('label.home')}}</a></li>
                    <li class="breadcrumb-item"><a href="{{ url('schedules') }}">{{__('label.schedule')}}</a></li>
                    <li class="breadcrumb-item active">{{__('label.create')}}</li>
                </ul>
            </div>
        </div>        
    </div><!-- Breadcrumb -->

    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="col col-md-12 col-lg-10">
                    @if(Session::has('success_message'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        New user has been successfully created.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif

                     <div class="card">
                        <div class="card-body">
                            <h1 class="card-title">{{__('label.create_stu_sched')}}</h1>
                            <div class="line"></div>

                            {{ Form::open() }}

                            <div class="form-row">
                                <div class="form-group col col-md-4 col-lg-4">
                                    <label for="student">Student</label>
                                    <select id="student" class="form-control" name="student" required>
                                        <option value="">Select Student</option>
                                        @if($students)
                                        @foreach($students as $student)
                                        <option value="{{ $student->id }}">{{ $student->romaji_name }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                    <small id="error-student" class="form-text form-error"></small>
                                </div>
                            </div>
                            
                            <div class="line"></div>

                            <div class="schedule-group" data-id="1">
                                <div class="form-row">
                                    <div class="form-group col col-md-4 col-lg-4">
                                        <label for="course">Course</label>
                                        <select id="course" class="form-control course" name="course[]" required>
                                            <option value="">--Select Item--</option>
                                            @if($course)
                                            @foreach($course as $c)
                                            <option value="{{ $c->id }}">{{ $c->label }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                        <small id="error-course" class="form-text form-error"></small>
                                    </div>
                                    <div class="form-group col col-md-4 col-lg-4">
                                        <label for="grade1">Grade</label>
                                        <select id="grade1" class="form-control grade" name="grade[]" disabled required>
                                            <option value="">--Select Item--</option>
                                        </select>
                                        <small id="error-grade" class="form-text form-error"></small>
                                    </div>
                                    <div class="form-group col col-md-4 col-lg-4">
                                        <label for="schedule1">Schedule</label>
                                        <select id="schedule1" class="form-control schedule" name="schedule[]" disabled required>
                                            <option value="">--Select Item--</option>
                                            <option value=""></option>
                                        </select>
                                        <small id="error-schedule" class="form-text form-error"></small>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col col-md-4 col-lg-4">
                                        <label for="start_date">Start Date</label>
                                        <input type="text" id="start_date" class="form-control start_date datepicker" name="start_date[]" required>
                                        <small id="error-start_date" class="form-text form-error"></small>
                                    </div>
                                    <div class="form-group col col-md-2 col-lg-2">
                                        <label for="start_time">Start Time</label>
                                        <input type="text" id="start_time" class="form-control start_time timepicker" name="start_time[]" required>
                                        <small id="error-start_time" class="form-text form-error"></small>
                                    </div>
                                    <div class="form-group col col-md-2 col-lg-2">
                                        <label for="end_time1">End Time</label>
                                        <input type="text" id="end_time1" class="form-control end_time" name="end_time[]" readonly="true" required>
                                        <small id="error-end_time" class="form-text form-error"></small>
                                    </div>
                                    <div class="form-group col col-md-2 col-lg-2" style="padding-top: 29px;">
                                        <span class="btn btn-default add_schedule"><i class="mdi mdi-plus"></i></span>
                                    </div>
                                </div>

                                <div class="line"></div>
                            </div>
                            <div id="appData"></div>
                            <div class="form-row">
                                <div class="form-group col col-md-6 col-lg-6">
                                    <button type="submit" data-form="create-schedule" class="btn btn-primary">Save</button>
                                    <a href="{{ url('schedules') }}" class="btn btn-warning">Cancel</a>
                                    <span class="form-proccessing hidden"><img src="{{ asset('assets/img/Loading/loading.gif') }}"> {{__('label.submit_process')}}</span>
                                </div>
                            </div>

                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection


@section('js')
    
    <script src="{{ asset('assets/js/app.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/vendor/timepicker/include/ui-1.10.0/jquery.ui.core.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendor/timepicker/jquery.ui.timepicker.js?v=0.3.3') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-ui/jquery-ui.js') }}"></script>

    <script>
        
    $(function(){

        datepicker();

        timepicker();
        
        var count = 1;

        $(document).on('click', '.add_schedule', function(){
            
            count += 1;

            let course = ``;

            let newSchedule = `
            <div class="schedule-group" data-id="`+ count +`">
                <div class="form-row">
                    <div class="form-group col col-md-4 col-lg-4">
                        <label for="course">Course</label>
                        <select id="course`+ count +`" class="form-control course" name="course[]">
                            <option value="">--Select Item--</option>
                            @if($course)
                            @foreach($course as $c)
                            <option value="{{ $c->id }}">{{ $c->label }}</option>
                            @endforeach
                            @endif
                        </select>
                        <small id="error-course" class="form-text form-error"></small>
                    </div>
                    <div class="form-group col col-md-4 col-lg-4">
                        <label for="grade`+ count +`">Grade</label>
                        <select id="grade`+ count +`" class="form-control grade" name="grade[]" disabled>
                            <option value="">--Select Item--</option>
                        </select>
                        <small id="error-grade" class="form-text form-error"></small>
                    </div>
                    <div class="form-group col col-md-4 col-lg-4">
                        <label for="schedule`+ count +`">Schedule</label>
                        <select id="schedule`+ count +`" class="form-control schedule" name="schedule[]" disabled>
                            <option value="">--Select Item--</option>
                            <option value=""></option>
                        </select>
                        <small id="error-schedule" class="form-text form-error"></small>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col col-md-4 col-lg-4">
                        <label for="start_date">Start Date</label>
                        <input type="text" id="start_date`+ count +`" class="form-control start_date datepicker" name="start_date[]">
                        <small id="error-start_date" class="form-text form-error"></small>
                    </div>
                    <div class="form-group col col-md-2 col-lg-2">
                        <label for="start_time">Start Time</label>
                        <input type="text" id="start_time`+ count +`" class="form-control start_time timepicker" name="start_time[]">
                        <small id="error-start_time" class="form-text form-error"></small>
                    </div>
                    <div class="form-group col col-md-2 col-lg-2">
                        <label for="end_time`+ count +`">End Time</label>
                        <input type="text" id="end_time`+ count +`" class="form-control end_time" name="end_time[]" readonly="true">
                        <small id="error-end_time" class="form-text form-error"></small>
                    </div>
                    <div class="form-group col col-md-2 col-lg-2" style="padding-top: 29px;">
                        <span class="btn btn-default add_schedule"><i class="mdi mdi-plus"></i></span>
                    </div>
                </div>

                <div class="line"></div>
            </div>`;

            $('#appData').append(newSchedule);

            datepicker();

            timepicker();

            $(this).removeClass('add_schedule').removeClass('btn-default').addClass('btn-warning').addClass('remove_schedule').html('<i class="mdi mdi-close"></i>');
        });

        $(document).on('click', '.remove_schedule', function(){
            $(this).closest('.schedule-group').remove();
        });

        $(document).on('change', '.timepicker', function(){
            
            let time = $(this).val();
            
            let groudId = $(this).closest('.schedule-group').attr('data-id');

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: '{{ url('api/schedule/end-time') }}',
                data: {
                    time: time
                },
                success: function(response){
                    $('#end_time'+ groudId).val(response.time);
                }
            });

        });

        $(document).on('change', '.course', function(){

            let option = '<option value="" >--Select Item--</option>';

            let id = $('option:selected', this).val();

            let group = $(this).closest('.schedule-group');
            
            if(id != ''){
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: '{{ url('api/schedule/get-grade') }}',
                    data: {
                        id: id
                    },
                    success: function(response){
                        group.find('.grade').prop('disabled', false).html(response.grade);
                        group.find('.schedule').prop('disabled', false).html(response.schedule);

                    }
                });
            }else{
                group.find('.grade').prop('disabled', true).html(option);
                group.find('.schedule').prop('disabled', true).html(option);
            }

        });

        function datepicker(){
            $('.datepicker').datepicker();
        }

        function timepicker(){
            $('.timepicker').timepicker();
        }

    });

    </script>
@endsection
