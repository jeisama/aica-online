
@extends('layouts.app')

@section('css')

<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-select/bootstrap-select.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/jquery-ui/jquery-ui.css') }}">
@endsection

@section('content')

    <!-- Breadcrumb -->
    <div class="breadcrumb-holder container-fluid">
        <div class="inner">
            <div class="links">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i> Home</a></li>
                    <li class="breadcrumb-item active">Schedule</li>
                </ul>
            </div>
            @if($user_data->role != 5 || $user_data != 4)
            <div class="buttons">
                <a href="{{ url('schedules/create') }}" class="btn btn-primary btn-sm"><i class="mdi mdi-plus"></i> Create</a>
            </div>
            @endif
        </div>        
    </div><!-- Breadcrumb -->

    

@endsection


@section('js')
    <script type="text/javascript" src="{{ asset('assets/vendor/timepicker/include/ui-1.10.0/jquery.ui.core.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendor/timepicker/jquery.ui.timepicker.js?v=0.3.3') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-ui/jquery-ui.js') }}"></script>
    <script>
        $(function(){

            
            select_picker();
            timepicker();

            $( ".datepicker" ).datepicker({
                dateFormat: "yy-mm-dd"
            });

            $('#repeat').change(function(){
                if($(this).is(':checked')){
                    GetCalendar();
                    $('#date_to').prop('readonly', true);
                }else{

                    $('#date_to').prop('readonly', false);
                }
            });

            /*
            |--------------------------------------------------
            | Remove Schedule
            |--------------------------------------------------
            */

            $('#table_form').on('click', '.remove_schedule', function(){
                $(this).click(function(){
                    $(this).closest('tr').fadeOut(function(){
                        $(this).remove();
                    });
                });
            });


            /*
            |--------------------------------------------------
            | Convert Students into single array to string
            |--------------------------------------------------
            */
            $('#table_form').on('change', '.students', function(){
                var students = [];
                $('option:selected',this).each(function(){
                    students.push($(this).val());
                });

                $(this).closest('tr').find('.student').val(students.join());
            });


            /*
            |--------------------------------------------------
            | Add New Schedule
            | Append
            |--------------------------------------------------
            */
            $('#table_form').on('click', '.add_schedule', function(){
                let day = $(this).closest('tr').find('.day').val();
                let time_from = $(this).closest('tr').find('.time_from').val();
                let time_to = $(this).closest('tr').find('.time_to').val();
                var students = [];
                $(this).closest('tr').find('.students > option:selected').each(function(){
                    students.push($(this).val());
                });

                let table_row = `<tr>
                    <th>
                        <select name="day[]" class="form-control day">
                            <option value="">Select Day</option>
                            <option value="1">Monday</option>
                            <option value="2">Tuesday</option>
                            <option value="3">Wednesday</option>
                            <option value="4">Thursday</option>
                            <option value="5">Friday</option>
                            <option value="6">Saturday</option>
                            <option value="7">Sunday</option>
                        </select>
                    </th>
                    <td>
                        <input type="text" name="time_from[]" class="form-control timepicker time_from" autocomplete="off">
                    </td>
                    <td>
                        <input type="text" name="time_to[]" class="form-control timepicker time_to" autocomplete="off">
                    </td>
                    <td>
                        <input type="hidden" name="student[]" class="student">
                        <select multiple title="Select Students" class="selectpicker students">
                            @if(count($students) > 0)
                            @foreach($students as $student)
                            <option value="{{ $student->id }}">{{ $student->student->kanji_name }}</option>
                            @endforeach
                            @endif
                        </select>
                    </td>
                    <td class="text-center addAction">
                        <button type="button" class="btn btn-default btn-sm add_schedule"><i class="fa fa-plus"></i></button>
                        
                    </td>
                </tr>`;

                if(day == '' || time_from == '' || time_to == '' || students.length <= 0 ){
                    alert('All field are required.');
                    return false;
                }
                $('#table_form tbody').append(table_row);
                $(this).closest('tr').find('.addAction').html('<button type="button" class="btn btn-default btn-sm remove_schedule"><i class="fa fa-minus"></i></button>');
                select_picker();
                timepicker();
            });

            
            /*
            |--------------------------------------------------
            | Functions
            |--------------------------------------------------
            */

            function timepicker(){
                $('.timepicker').timepicker();
            }

            function select_picker(){
                $('select.selectpicker').selectpicker({
                    caretIcon: 'fas fa-caret-down'
                });
            }

            /*
            |--------------------------------------------------
            | Ajax Request
            |--------------------------------------------------
            */
            //GetCalendar();

            function GetCalendar(){
                let date_from = $('#date_from').val();
                if(date_from == ''){
                    date_from = '{{ date('Y-m-d') }}';
                }
                $.getJSON({
                    type: "GET",
                    dataType: "json",
                    url: "{{ url('api/schedule/calendar-repeat/') }}/"+ date_from,
                    success: function(response){
                        $('#date_to').val(response.date);
                    }
                });
            }
        });
    </script>
@endsection
