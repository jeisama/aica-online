@extends('layouts.app')

@section('css')

<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-select/bootstrap-select.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/jquery-ui/jquery-ui.css') }}">
@endsection

@section('content')
    <!-- Dashboard Counts Section-->
    <div class="breadcrumb-holder container-fluid">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i> Home</a></li>
            <li class="breadcrumb-item"><a href="{{ url('/schedules') }}">Schedule</a></li>
            <li class="breadcrumb-item active">Edit Schedule</li>
        </ul>
    </div>

    <section class="dashboard-counts no-padding-bottom forms">
        <div class="container-fluid">

            @if (Session::has('success_message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {!! session('success_message') !!}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif

            @if (Session::has('error_message'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {!! session('error_message') !!}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif

            <div class="card ">
                <div class="card-header d-flex align-items-center">
                    <h3 class="h4">Schedule Details</h3>
                </div>

                <div class="card-body">
                    {{ Form::open() }}
                        <div class="form-row">
                            <div class="form-group col col-md-12 col-lg-3">
                                <label for="teacher" class="form-control-label">Teacher's Name</label>
                                <input type="text" class="form-control" value="{{ $schedule->teacher->full_name }}" disabled="true">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col col-md-12 col-lg-3">
                                <label for="date" class="form-control-label">Date</label>
                                <input type="text" name="date" class="form-control datepicker" value="{{ date('Y-m-d', strtotime($schedule->date)) }}" autocomplete="off">
                                @if($errors->has('date'))
                                    <div class="invalid-feedback">{{ $errors->first('date') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col col-md-12 col-lg-3">
                                <label for="from" class="form-control-label">From</label>
                                <input type="text" name="from" class="form-control timepicker" value="{{ date('H:i', strtotime($schedule->time_in)) }}" autocomplete="off">
                                @if($errors->has('from'))
                                    <div class="invalid-feedback">{{ $errors->first('from') }}</div>
                                @endif
                            </div>
                            <div class="form-group col col-md-12 col-lg-3">
                                <label for="to" class="form-control-label">To</label>
                                <input type="text" name="to" class="form-control timepicker" value="{{ date('H:i', strtotime($schedule->time_out)) }}" autocomplete="off">
                                @if($errors->has('to'))
                                    <div class="invalid-feedback">{{ $errors->first('to') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col col-md-12 col-lg-3">
                                <label for="student" class="form-control-label">Students</label>
                                <select name="student[]" multiple title="Select Students" class="selectpicker students">
                                    @if(count($students) > 0)
                                    @foreach($students as $student)
                                    <option value="{{ $student->id }}" {!! in_array($student->id, $studentId) ? 'selected="true"':'' !!}>{{ $student->student->kanji_name }}</option>
                                    @endforeach
                                    @endif
                                </select>
                                @if($errors->has('student'))
                                    <div class="invalid-feedback">{{ $errors->first('student') }}</div>
                                @endif
                            </div>
                        </div>
                        <br>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="active" id="deleted" {{ $schedule->active == 0 ? 'checked':'' }}>
                            <label class="form-check-label" for="deleted">
                                Mark as deleted.
                            </label>
                        </div>
                        <br>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="include" id="include">
                            <label class="form-check-label" for="include">
                                Include other schedule with same date and time.
                            </label>
                        </div>

                        <div class="line"></div>
                        
                        <div class="form-group">
                            <button class="btn btn-primary">Save Changes</button>
                            <a href="{{ route('schedule.index') }}" class="btn btn-default">Cancel</a>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </section>

@endsection


@section('js')
    <script type="text/javascript" src="{{ asset('assets/vendor/timepicker/include/ui-1.10.0/jquery.ui.core.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendor/timepicker/jquery.ui.timepicker.js?v=0.3.3') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-ui/jquery-ui.js') }}"></script>
    <script>
        $(function(){

            $('.datepicker').datepicker({
                dateFormat: "yy-mm-dd"
            });

            $('.timepicker').timepicker();

            $('select.selectpicker').selectpicker({
                caretIcon: 'fas fa-caret-down'
            });

        });
    </script>
@endsection
