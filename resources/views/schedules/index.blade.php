@extends('layouts.app')

@section('css')

@endsection

@section('content')
    
    <!-- Page Header-->
    <div class="breadcrumb-holder container-fluid">
        <div class="inner">
            <div class="links">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i> {{__('label.home')}}</a></li>
                <li class="breadcrumb-item active">{{__('label.schedule')}}</li>
                </ul>
            </div>
            
            <div class="buttons">
                @if($hasFranchise)
            <a href="{{ url('schedules/student/create') }}" class="btn btn-primary btn-sm"><i class="mdi mdi-plus"></i> {{__('label.create_stu_sched')}}</a>
                @endif

                @if($user_data->role == 3 || $user_data->role == 1)
                <a href="{{ url('schedules/teacher/create') }}" class="btn btn-primary btn-sm"><i class="mdi mdi-plus"></i> {{__('label.create_tea_sched')}}</a>
                @endif
            </div>
            

            
        </div>        
    </div>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            
            @if (Session::has('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {!! session('success') !!}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif

            @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {!! session('error') !!}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
            {{--
            @if(($user_data->role == 1) || ($user_data->role == 2 && $hasFranchise > 0) || ($user_data->role == 3 && $hasFranchise > 0))
            {{ Form::open(['method' => 'get', 'id' => 'form_search']) }}
            <input type="hidden" name="cdate" value="{{ $cdate }}">
            <div class="form-row">
            <div class="form-group col col-md-12 col-lg-3">
                <label for="student" class="form-control-label">Student's Name</label>
                <select name="id" id="student" class="form-control">
                    <option value="">--Select Item--</option>
                   @if($students)
                   @foreach($students as $student)
                   <option value="{{ $student->id }}" {{ $id == $student->id ? 'selected':'' }}>{{ $student->romaji_name }}</option>
                   @endforeach
                   @endif
                </select>
            </div>
            </div>
            {{ Form::close() }}
            @endif
            --}}

            <div class="legends">
                <h5>{{__('label.legend')}}</h5>
                <ul>
                    <li class="student">
                    <span class="box"></span> <span class="lbl">{{__('label.student')}}</span>
                    </li>
                    <li class="teacher">
                        -<span class="box"></span> <span class="lbl">{{__('label.teacher')}}</span>
                    </li>
                </ul>
            </div>

            {!! $calendar->generate() !!}

            <div class="row"></div>
        </div>

    
    </section>

    
    <!-- Modal Event_Data -->
    <div class="modal fade" id="event_data" tabindex="-1" role="dialog" aria-labelledby="event_data" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content" id="lesson_body">
                
                <div class="loading">
                    <img src="{{ asset('assets/img/Loading/loading.gif') }}" alt="Loading">
                    <div>Loading, please wait a moment...</div>
                </div>
            </div>
        </div>
    </div><!-- Modal Event_Data -->

    <!-- Modal Event_Data -->
    <div class="modal fade" id="all_schedule" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title" id="data_date"></div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="set_all_schedule">
                    <div class="loading">
                        <img src="{{ asset('assets/img/Loading/loading.gif') }}" alt="Loading">
                        <div>Loading, please wait a moment...</div>
                    </div>
                    <!-- <ul class="schedule_lists">
                        <li class="event_data" data-toggle="modal" data-target="#event_data" data-id="1" data-date="2018-10-29">07:00 AM - 08:00 AM</li>
                        <li class="event_data" data-toggle="modal" data-target="#event_data" data-id="1" data-date="2018-10-29">07:00 AM - 08:00 AM</li>
                        <li class="event_data" data-toggle="modal" data-target="#event_data" data-id="1" data-date="2018-10-29">07:00 AM - 08:00 AM</li>
                    </ul> -->
                </div>
            </div>
        </div>
    </div><!-- Modal Event_Data -->
    

@endsection


@section('js')
    <script>
        $(function(){

            let _token = $('meta[name="csrf-token"]').attr('content');

            $('#student').change(function(){
                let val = $('#teacher option:selected').val();
                if(val != ''){
                    $('#form_search').submit();
                }
            });

            $(document).on('click','.event_data', function(){
                
                $('#all_schedule').modal('hide');

                let id = $(this).attr('data-id');
                let role = '{{ Auth::user()->role }}';
                let param = {
                    url: '{{ url('/api/schedule/get-schedule') }}',
                    data: {
                        _token: _token,
                        id: id,
                        role: role
                    },
                    append: 'lesson_body'
                }

                ajax(param);
            });


            $('.more_schedule').each(function(){
                $(this).click(function(){
                    let date = $(this).attr('data-date');
                    let id   = $(this).attr('data-id');
                    let user = '';

                    $('#data_date').text(date);

                    let param = {
                        url: '{{ url('/api/schedule/get-all-schedule') }}',
                        data: {
                            _token: _token,
                            date: date,
                            user: user
                        },
                        append: 'set_all_schedule'
                    }

                    ajax(param);
                });
            });

            function ajax(param){
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: param.url,
                    data: param.data,
                    success:function(response){
                        $('#'+ param.append).html(response.data);
                    }
                });
            }
            
            setScheduleWith();

            function setScheduleWith(){
                let width = $('.table-calendar').outerWidth();
                    width = width / 7;
                    width = width - 35;
                $('.lesson-name').css({
                    'width': width +'px'
                });
            }
        });
    </script>       
@endsection
