@extends('layouts.app')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/jquery-ui/custom.css') }}">
<style>
    .custom-checkbox{
    margin-top: 8px;
    }
    .custom-checkbox.others{
    }
    .custom-checkbox label{
    padding-top: 2px;
    }
    #selection{
    display: none;
    }
    .readOnly:read-only {
    background: #fff;
    }
</style>
@endsection
@section('content')
<!-- Breadcrumb -->
<div class="breadcrumb-holder container-fluid">
    <div class="inner">
        <div class="links">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i> Home</a></li>
                <li class="breadcrumb-item"><a href="{{ url('schedules') }}">Schedule</a></li>
                <li class="breadcrumb-item active">Create</li>
            </ul>
        </div>
    </div>
</div>
<!-- Breadcrumb -->
<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col col-md-12 col-lg-10">
                @if(Session::has('success_message'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    New user has been successfully created.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                <div class="card">
                    <div class="card-body">
                        <h1 class="card-title">Create</h1>
                        <div class="line"></div>
                        {{ Form::open(['id' => 'create-schedule', 'onsubmit' => 'event.preventDefault()']) }}
                        <div class="form-row">
                            <div class="form-group col col-md-6 col-lg-6">
                                <label for="student">Student</label>
                                <select id="student" class="form-control" name="student">
                                    <option value="">Select Student</option>
                                    @if($students)
                                    @foreach($students as $student)
                                    <option value="{{ $student->id }}">{{ $student->romaji_name }}</option>
                                    @endforeach
                                    @endif
                                </select>
                                <small id="error-student" class="form-text form-error"></small>
                            </div>
                        </div>
                        <div class="line"></div>
                        <div class="schedule-group" data-id="1">
                            <div class="form-row">
                                <div class="form-group col col-md-6 col-lg-6">
                                    <label for="course">Course</label>
                                    <select id="course" class="form-control course" name="course">
                                        <option value="">--Select Item--</option>
                                        @if($course)
                                        @foreach($course as $c)
                                        <option value="{{ $c->id }}">{{ $c->label }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                    <small id="error-course" class="form-text form-error"></small>
                                </div>
                                <div class="form-group col col-md-6 col-lg-6">
                                    <label for="grade">Grade</label>
                                    <select id="grade" class="form-control grade" name="grade" disabled>
                                        <option value="">--Select Item--</option>
                                    </select>
                                    <small id="error-grade" class="form-text form-error"></small>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col col-md-6 col-lg-6">
                                    <label for="frequency">Frequency</label>
                                    <select id="frequency" class="form-control frequency" name="frequency" disabled>
                                        <option value="">--Select Item--</option>
                                        <option value=""></option>
                                    </select>
                                    <small id="error-frequency" class="form-text form-error"></small>
                                </div>
                                <div class="form-group col col-md-6 col-lg-6">
                                    <label for="frequency">Start Day</label>
                                    <select id="start_day" class="form-control start_day" name="start_day" disabled="true">
                                    </select>
                                    <small id="error-start_day" class="form-text form-error"></small>
                                </div>
                            </div>
                            <div id="selection">
                                <div class="form-row">
                                    <div class="form-group col col-md-3 col-lg-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="check_all" checked>
                                            <label class="custom-control-label" for="check_all">Select All </label>
                                            <small id="error-day" class="form-text form-error"></small>
                                        </div>
                                    </div>
                                    <div class="form-group col col-md-3 col-lg-3">
                                        <label>Date</label>
                                    </div>
                                    <div class="form-group col col-md-3 col-lg-3">
                                        <label>Time</label>
                                    </div>
                                    <div class="form-group col col-md-3 col-lg-3">
                                        <label>Teacher</label>
                                    </div>
                                </div>
                            </div>
                            <div id="appData">
                            </div>
                            <div class="line"></div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col col-md-12 col-lg-12">
                                <input type="hidden" name="teacher_id" id="teacher_id">
                                <button type="submit" id="submit-form" data-form="create-schedule" class="btn btn-primary">Save</button>
                                <a href="{{ url('schedules') }}" class="btn btn-warning">Cancel</a>
                                <span class="form-proccessing hidden"><img src="{{ asset('assets/img/Loading/SpinnerOrange.svg') }}" style="height: 50px;"> Proccessing, please wait a moment...</span>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('js')
<script type="text/javascript" src="{{ asset('assets/vendor/timepicker/include/ui-1.10.0/jquery.ui.core.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendor/timepicker/jquery.ui.timepicker.js?v=0.3.3') }}"></script>
<script src="{{ asset('assets/vendor/jquery-ui/custom.js') }}"></script>
<div class="appendDatePicker">
    <script>
        $(function(){
        
        });
    </script>
</div>
<script>
    $(function(){
    
        var dateArray = [];
    
        var teachers  = '';
    
        $("#check_all").click(function(){
    
            $('input:checkbox').not(this).prop('checked', this.checked);
    
            if(this.checked == true){
    
                $('.input_teacher').attr('name', 'teacher[]');
                $('.input_date').attr('name', 'date[]');
                $('.input_time').attr('name', 'time[]');
    
            }else{
    
                $('.input_teacher').attr('name', '');
                $('.input_date').attr('name', '');
                $('.input_time').attr('name', '');
    
            }
    
        });
    
        $(document).on('click', '.subCheckbox', function(){
    
            if(this.checked == true){
    
                $(this).closest('.form-row').find('.input_teacher').attr('name', 'teacher[]');
                $(this).closest('.form-row').find('.input_date').attr('name', 'date[]');
                $(this).closest('.form-row').find('.input_time').attr('name', 'time[]');
    
            }else{
    
                $(this).closest('.form-row').find('.input_teacher').attr('name', '');
                $(this).closest('.form-row').find('.input_date').attr('name', '');
                $(this).closest('.form-row').find('.input_time').attr('name', '');
    
            }
    
        });
    
        $(document).on('change', '#start_day', function(){
    
            let grade = $('#grade').val();
    
            let teachers = '';
    
            let frequency = $('#frequency').val();
    
            let start_day = $('option:selected', this).val();
                start_day = parseInt(start_day);
            
            if(start_day != '' && (start_day >= 1 && start_day <= 16 )){
                
                //Pull Teachers
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: '{{ url('api/schedule/get-teacher') }}',
                    data: {
                        grade: grade
                    },
                    success: function(response){
    
                        teachers = response;
                        
                        appendData(start_day, frequency, teachers);
    
                    }
                });
    
            }else{
                $('#appData').html('');
                $('#selection').hide();
            }
            
        });
    
        $(document).on('change','#frequency', function(){
            
            let frequency = $('option:selected', this).val();
    
            $('#appData').html('');
    
            $('#selection').hide();
    
            $('#start_day').prop('selectedIndex',0);
    
            if(frequency == ''){
                $('#start_day').html('start_day').prop('disabled', true);
    
            }else{
                let option = '<option value="">--Select Item--</option>';
    
                for(let i = 1; i <= frequency; i++){
                    option += '<option value="'+ i +'">Day '+ i +'</option>';
                }
    
                $('#start_day').html(option).prop('disabled', false);
            }
    
        });
    
    
        $(document).on('change', '#grade', function(){
    
            let grade = $('option:selected', this).val();
    
            $('.schedule_time').html('').prop('disabled', true).val('');
    
            $('#appData').html('');
    
            $('#selection').hide();
    
            $('#start_day').html('').prop('disabled', true);
    
            $('#frequency').prop('disabled', false)
    
        });
    
        $(document).on('change', '.course', function(){
    
            let option = '<option value="" >--Select Item--</option>';
    
            let id = $('option:selected', this).val();
    
            let group = $(this).closest('.schedule-group');
            
            
            $('#appData').html('');
            $('#selection').hide();
            $('#start_day').html('').prop('disabled', true);
            $('#teacher').html('').prop('disabled', true);
            $('#start_date').html('').prop('disabled', true);
            
            $('.schedule_time').html('').prop('disabled', true).val('');
    
            if(id != ''){
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: '{{ url('api/schedule/get-grade') }}',
                    data: {
                        id: id
                    },
                    success: function(response){
                        $('#grade').prop('disabled', false).html(response.grade);
                        $('#frequency').prop('disabled', true).html(response.schedule);
                    }
                });
            }else{
                $('#grade').prop('disabled', true).html(option);
                $('#frequency').prop('disabled', true).html(option);
            }
    
        });
    
        $(document).on('change', '.input_teacher', function(){
    
            var dateToday = new Date();
    
            let frequency = $('#frequency').val();
    
            let value = $('option:selected', this).val();
    
            let data_id = $(this).attr('data-id');
    
            $('#date'+ data_id).prop('disabled', false);
    
            for(let x = parseInt(data_id); x <= parseInt(frequency); x++){
              
                $('#teacher'+ x).val(value).prop('disabled', false);  
                
                $('#date'+ x).val('').prop({
                    'disabled': true,
                    'readonly': false
                });
    
                $('#time'+ x).html('').prop({
                    'disabled': true
                });
    
            }
    
            $.ajax({
                type: 'post',
                dataType: 'json',
                url: '{{ url('api/schedule/get-dates') }}',
                data: {
                    data_id: data_id,
                    frequency: frequency,
                    teacher: value,
                },
                success:function(response){
                    dateArray = response;
                    
                    $('#date'+ data_id).val('').prop({
                        'disabled': false,
                        'readonly': true
                    });
    
                    datePicker();
                }
            });
    
        })
    
        $(document).on('change', '.datepicker', function(){
    
            let data_id     = $(this).attr('data-id');
    
            let date        = $(this).val();
    
            let frequency   = $('#frequency').val();
    
            let user_id     = $('#teacher'+ data_id).val();
    
            let start_day   = $('#start_day').val();
    
            $.ajax({
                type: 'post',
                dataType: 'json',
                url: '{{ url('api/schedule/set-date') }}',
                data: {
                    date: date,
                    user_id: user_id,
                    frequency: frequency,
                    start_day: start_day
                },
                success:function(response){
    
                    for(let key in response){
    
                        let date = response[key]['date'];
                        let time = response[key]['time'];
    
                        if(date != null){
                            if(key >= parseInt(data_id)){
                                $('#date'+ key).val(date).prop({
                                    'disabled':false,
                                    'readonly': true
                                });
    
                                $('#time'+ key).html(time).prop({
                                    'disabled': false,
                                });
                            }
                            
                        }else{
    
                            if(key >= parseInt(data_id)){
                                $('#teacher'+ key).val('');
    
                                $('#date'+ key).val('').prop({
                                    'disabled': true,
                                    'readonly': false
                                });
    
                                $('#time'+ key).html('').prop({
                                    'disabled': true,
                                });
                            }
                            
                        }
                    }
    
                }
            });
    
        });
    
        $(document).on('change', '.input_time', function(){
    
            let data_id     = $(this).attr('data-id');
    
            let value       = $('option:selected', this).val();
    
            let frequency   = $('#frequency').val();
    
            for(let x = parseInt(data_id); x <= parseInt(frequency); x++){
    
                $('#time'+ x).val(value);
    
            }
    
        });
    
    
        function appendData(start_day, frequency, teachers){
    
            let html = ``;
    
            let count = 0;
    
            for(let i = start_day; i <= frequency; i++){
    
            count++;
    
                let disabled = count == 1 ? '':'disabled="true"';
    
                let isReadonly = count == 1 ? 'readonly="true"':'';
                
                html += `
                        <div class="form-row">
                            <div class="form-group col col-md-3 col-lg-3">
                                <div class="custom-control custom-checkbox others">
                                    <input type="checkbox" class="custom-control-input subCheckbox" id="day`+ i +`" name="day[]" value="`+ i +`" checked>
                                    <label class="custom-control-label" for="day`+ i +`">Day `+ i +`</label>
                                </div>
                            </div>
                            <div class="form-group col col-md-3 col-lg-3">
                                <input data-id="`+ i +`" type="text" id="date`+ i +`" class="form-control input_date datepicker readOnly" name="date[]" disabled="true">
                            </div>
                            <div class="form-group col col-md-3 col-lg-3">
                                <select data-id="`+ i +`" id="time`+ i +`" class="form-control input_time" name="time[]" disabled="true"></select>
                            </div>
                            <div class="form-group col col-md-3 col-lg-3">
                                <select data-id="`+ i +`" id="teacher`+ i +`" class="form-control input_teacher" name="teacher[]" `+ disabled +`>`+ teachers +`</select>
                            </div>
                        </div>`;
            }
            $('#selection').show();
    
            $('#appData').html(html);
    
        }
    
        function datePicker(){
    
            var dateToday = new Date();
    
            $('.datepicker').datepicker({
                dateFormat: "yy/mm/dd",
                minDate: dateToday,
                beforeShowDay: function(date){
                    
                    var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
    
                    return [ dateArray.indexOf(string) != -1 ]
                }
            });
        }
    
    });
    
</script>
@endsection