@extends('layouts.app')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/jquery-ui/custom.css') }}">
<style>
    .custom-checkbox{
    margin-top: 8px;
    }
    .custom-checkbox.others{
    }
    .custom-checkbox label.custom-control-label{
        padding-top: 2px;
    }
    #selection label.column{
        padding-top: 10px;
    }
    .readOnly:read-only {
    background: #fff;
    }
</style>
@endsection
@section('content')
<!-- Breadcrumb -->
<div class="breadcrumb-holder container-fluid">
    <div class="inner">
        <div class="links">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i> Home</a></li>
                <li class="breadcrumb-item"><a href="{{ url('schedules') }}">Schedule</a></li>
                <li class="breadcrumb-item active">Edit</li>
            </ul>
        </div>
    </div>
</div>
<!-- Breadcrumb -->
<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col col-md-12 col-lg-10">
                @if(Session::has('success_message'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    New user has been successfully created.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                <div class="card">
                    <div class="card-body">
                        <h1 class="card-title">Edit</h1>
                        <div class="line"></div>
                        {{ Form::open(['method', 'put', 'id' => 'update-schedule', 'onsubmit' => 'event.preventDefault()']) }}
                        <input type="hidden" name="id" value="{{ $schedule->id }}">
                        <div class="form-row">
                            <div class="form-group col col-md-6 col-lg-6">
                                <label for="student">Student</label>
                                <select id="student" class="form-control" disabled="true">
                                    <option value="">Select Student</option>
                                    @if($students)
                                    @foreach($students as $student)
                                    <option value="{{ $student->id }}" {{ $schedule->user_id == $student->id ? 'selected':'' }}>{{ $student->romaji_name }}</option>
                                    @endforeach
                                    @endif
                                </select>
                                <small id="error-student" class="form-text form-error"></small>
                            </div>
                        </div>
                        <div class="line"></div>
                        <div class="schedule-group" data-id="1">
                            <div class="form-row">
                                <div class="form-group col col-md-6 col-lg-6">
                                    <label for="course">Course</label>
                                    <select id="course" class="form-control" disabled="true">
                                        <option value="">--Select Item--</option>
                                        @if($course)
                                        @foreach($course as $c)
                                        <option value="{{ $c->id }}" {{ $schedule->course_id == $c->id ? 'selected':'' }}>{{ $c->label }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                    <small id="error-course" class="form-text form-error"></small>
                                </div>
                                <div class="form-group col col-md-6 col-lg-6">
                                    <label for="grade">Grade</label>
                                    <select id="grade" class="form-control" disabled="true">
                                        <option value="">--Select Item--</option>
                                        @if($grades)
                                        @foreach($grades as $grade)
                                        <option value="{{ $grade->grade_level->id }}" {{ $grade->grade_level->id == $schedule->grade_id ? 'selected':'' }}>{{ $grade->grade_level->label }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                    <small id="error-grade" class="form-text form-error"></small>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col col-md-6 col-lg-6">
                                    <label for="frequency">Frequency</label>
                                    <select id="frequency" class="form-control"disabled="true">
                                        <option value="">--Select Item--</option>
                                        <option value="" {{ $schedule->schedule_id == 16 ? 'selected':'' }}>Once a week</option>
                                        <option value="" {{ $schedule->schedule_id == 8 ? 'selected':'' }}>Alternate week</option>
                                        <option value="" {{ $schedule->schedule_id == 1 ? 'selected':'' }}>One time only</option>
                                    </select>
                                    <small id="error-frequency" class="form-text form-error"></small>
                                </div>
                                <div class="form-group col col-md-6 col-lg-6">
                                    <label for="frequency">Start Day</label>
                                    <select id="start_day" class="form-control" disabled="true">
                                        @for($i = 1; $i <= 16; $i++)
                                        <option value="" {{ $schedule->day == $i ? 'selected':'' }}>Day {{ $i }}</option>
                                        @endfor
                                    </select>
                                    <small id="error-start_day" class="form-text form-error"></small>
                                </div>
                            </div>
                            <div id="selection">
                                <div class="form-row">
                                    <div class="form-group col col-md-3 col-lg-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="check_all" checked>
                                            <label class="custom-control-label" for="check_all">Select All </label>
                                            <small id="error-day" class="form-text form-error"></small>
                                        </div>
                                    </div>
                                    <div class="form-group col col-md-3 col-lg-3">
                                        <label class="column">Teacher</label>
                                    </div>
                                    <div class="form-group col col-md-3 col-lg-3">
                                        <label class="column">Date</label>
                                    </div>
                                    <div class="form-group col col-md-3 col-lg-3">
                                        <label class="column">Time</label>
                                    </div>
                                </div>
                            </div>

                            @if($schedules)
                            @foreach($schedules as $res)
                            <div class="form-row">
                                <div class="form-group col col-md-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input subCheckbox" id="day1" name="day[]" value="1" checked>
                                        <label class="custom-control-label" for="day1">Day {{ $res->day }}</label>
                                    </div>
                                </div>
                                <div class="form-group col col-md-3 col-lg-3">
                                    <select data-id="1" id="teacher1" class="form-control input_teacher" name="teacher[]">
                                        @if($teachers)
                                            @foreach($teachers as $teacher)
                                            <option value="{{ $teacher->id }}" {{ $teacher->id == $schedule->teacher_id ? 'selected':'' }}>{{ $teacher->full_name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group col col-md-3 col-lg-3">
                                    <input type="text" class="form-control input_date datepicker" name="date[]" value="{{ date('Y/m/d', strtotime($res->date)) }}">
                                </div>
                                <div class="form-group col col-md-3 col-lg-3">
                                    <select data-id="1" id="teacher1" class="form-control input_time" name="time[]"></select>
                                </div>
                            </div>
                            @endforeach
                            @endif
                            <div class="line"></div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col col-md-12 col-lg-12">
                                <input type="hidden" name="teacher_id" id="teacher_id">
                                <button type="submit" id="submit-form" data-form="update-schedule" class="btn btn-primary">Save</button>
                                <a href="{{ url('schedules') }}" class="btn btn-warning">Cancel</a>
                                <span class="form-proccessing hidden"><img src="{{ asset('assets/img/Loading/SpinnerOrange.svg') }}" style="height: 50px;"> Proccessing, please wait a moment...</span>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('js')
<script type="text/javascript" src="{{ asset('assets/vendor/timepicker/include/ui-1.10.0/jquery.ui.core.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendor/timepicker/jquery.ui.timepicker.js?v=0.3.3') }}"></script>
<script src="{{ asset('assets/vendor/jquery-ui/custom.js') }}"></script>
<div class="appendDatePicker">
    <script>
        $(function(){
        
        });
    </script>
</div>
@endsection