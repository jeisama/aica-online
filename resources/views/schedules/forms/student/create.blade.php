@extends('layouts.app')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/jquery-ui/custom.css') }}">
<style>
    .custom-checkbox{
    margin-top: 8px;
    }
    .custom-checkbox.others{
    }
    .custom-checkbox label{
    padding-top: 2px;
    }
    #selection{
    display: none;
    }
    .readOnly:read-only {
    background: #fff;
    }
    .form-control[readonly]{
        background: #fff;
    }

</style>
@endsection
@section('content')
<!-- Breadcrumb -->
<div class="breadcrumb-holder container-fluid">
    <div class="inner">
        <div class="links">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i> {{__('label.home')}}</a></li>
                <li class="breadcrumb-item"><a href="{{ url('schedules') }}">{{__('label.schedule')}}</a></li>
                <li class="breadcrumb-item active">{{__('label.create')}}</li>
            </ul>
        </div>
    </div>
</div>
<!-- Breadcrumb -->
<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col col-md-12 col-lg-10">
                @if(Session::has('success_message'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    New user has been successfully created.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                <div class="card">
                    <div class="card-body">
                        <h1 class="card-title">{{__('label.create_stu_sched')}}</h1>
                        <div class="line"></div>
                        {{ Form::open(['class' => 'form-main']) }}
                        <div class="form-row">
                            <div class="form-group col col-md-6 col-lg-6">
                                <label for="student">{{__('label.student')}}</label>
                                <select id="student" class="form-control" name="student" required>
                                    <option value="">{{__('label.select_student')}}</option>
                                    @if($students)
                                    @foreach($students as $student)
                                    <option value="{{ $student->id }}">{{ $student->romaji_name }}</option>
                                    @endforeach
                                    @endif
                                </select>
                                <small id="error-student" class="form-text form-error"></small>
                            </div>
                        </div>
                        <div class="line"></div>
                        <div class="schedule-group" data-id="1">
                            <div class="form-row">
                                <div class="form-group col col-md-6 col-lg-6">
                                    <label for="course">{{__('label.course')}}</label>
                                    <select id="course" class="form-control course" name="course" required>
                                    <option value="">{{__('label.select_item')}}</option>
                                        @if($course)
                                        @foreach($course as $c)
                                        <option value="{{ $c->id }}">{{ $c->label }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                    <small id="error-course" class="form-text form-error"></small>
                                </div>
                                <div class="form-group col col-md-6 col-lg-6">
                                    <label for="grade">{{__('label.grade')}}</label>
                                    <select id="grade" class="form-control grade" name="grade" disabled required>
                                        <option value="">{{__('label.select_item')}}</option>
                                    </select>
                                    <small id="error-grade" class="form-text form-error"></small>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col col-md-6 col-lg-6">
                                    <label for="frequency">{{__('label.frequency')}}</label>
                                    <select id="frequency" class="form-control frequency" name="frequency" disabled required>
                                        <option value="">{{__('label.select_item')}}</option>
                                        <option value=""></option>
                                    </select>
                                    <small id="error-frequency" class="form-text form-error"></small>
                                </div>
                                <div class="form-group col col-md-6 col-lg-6">
                                    <label for="frequency">{{__('label.start_day')}}</label>
                                    <select id="start_day" class="form-control start_day" name="start_day" disabled="true" required>
                                    </select>
                                    <small id="error-start_day" class="form-text form-error"></small>
                                </div>
                            </div>
                            <div id="selection">
                                <div class="form-row">
                                    <div class="form-group col col-md-3 col-lg-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="check_all" checked>
                                            <label class="custom-control-label" for="check_all">{{__('label.select_all')}} </label>
                                            <small id="error-day" class="form-text form-error"></small>
                                        </div>
                                    </div>
                                    <div class="form-group col col-md-3 col-lg-3">
                                        <label>{{__('label.date')}}</label>
                                    </div>
                                    <div class="form-group col col-md-3 col-lg-3">
                                        <label>{{__('label.time')}}</label>
                                    </div>
                                    <div class="form-group col col-md-3 col-lg-3">
                                        <label>{{__('label.teacher')}}</label>
                                    </div>
                                </div>
                            </div>
                            <div id="appData">
                            </div>
                            <div class="line"></div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col col-md-12 col-lg-12">
                                <input type="hidden" name="teacher_id" id="teacher_id">
                                <button type="submit" id="submitForm" style="display: none;">Submit</button>
                                <button type="button" id="verify" data-form="create-schedule" class="btn btn-primary save-button">{{ __('label.confirm') }}</button>
                                <a href="{{ url('schedules') }}" class="btn btn-warning">{{ __('label.cancel') }}</a>
                                <span class="form-proccessing hidden"><img src="{{ asset('assets/img/Loading/SpinnerOrange.svg') }}" style="height: 50px;"> {{ __('label.submit_process') }}</span>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ __('label.create_stu_sched') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col col-sm-2">{{ __('label.student') }}</div>
                    <div class="col col-sm-10" id="label-student"></div>
                </div>
                <div class="row">
                    <div class="col col-sm-2">{{ __('label.course') }}</div>
                    <div class="col col-sm-4" id="label-course"></div>
                    <div class="col col-sm-2">{{ __('label.grade') }}</div>
                    <div class="col col-sm-4" id="label-grade"></div>
                </div>
                <div class="row">
                    <div class="col col-sm-2">{{ __('label.frequency') }}</div>
                    <div class="col col-sm-4" id="label-frequency">: sdf</div>
                    <div class="col col-sm-2">{{ __('label.start_day') }}</div>
                    <div class="col col-sm-4" id="label-start_day"></div>
                </div>
                <hr>
                <div class="row">
                    <div class="col col-sm-12">
                        <table class="table table-bordered" id="table-days">
                            <thead>
                                <tr>
                                    <th width="25%">Days</th>
                                    <th width="25%">{{ __('label.date') }}</th>
                                    <th width="25%">{{ __('label.time') }}</th>
                                    <th width="25%">{{ __('label.teacher') }}</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('label.back')}}</button>
                <button type="button" class="btn btn-primary" id="confirm">{{__('label.save')}}</button>
            </div>
            
        </div>
    </div>
</div><!-- Modal -->

<!-- Modal -->
<div class="modal fade" id="returnModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Export Student Schedule</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
    
                <div class="modal-body">
                    Do you want to export this schedule?
                </div>
                
                <div class="modal-footer">
                    <a href="{{ url('schedules') }}" class="btn btn-secondary" id="closeExport">Close</a>
                    <a href="" class="btn btn-primary" id="exportReport">Export</a>
                </div>
                
            </div>
        </div>
    </div><!-- Modal -->

@endsection
@section('js')
<script type="text/javascript" src="{{ asset('assets/vendor/timepicker/include/ui-1.10.0/jquery.ui.core.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendor/timepicker/jquery.ui.timepicker.js?v=0.3.3') }}"></script>
<script src="{{ asset('assets/vendor/jquery-ui/custom.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>


<script>
    $(function(){
        let _token = $('meta[name="csrf-token"]').attr('content');

        let base  = $('base').attr('href');
        //$('#myModal').modal('show');

        $(document).on('click', '#confirm', function(){

            $('#submitForm').click();

        });

        $(document).on('click', '#verify', function(){

            let student     = $('#student option:selected').text();

            let course      = $('#course option:selected').text(); 
            
            let grade       = $('#grade option:selected').text(); 
            
            let frequency   = $('#frequency option:selected').text(); 
            
            let start_day   = $('#start_day option:selected').text(); 

            var days        = checkAllData();

            $('#label-student').html(': '+ student);

            $('#label-course').html(': '+ course);

            $('#label-grade').html(': '+ grade);

            $('#label-frequency').html(': '+ frequency);

            $('#label-start_day').html(': '+ start_day);

            $('#table-days tbody').html(days);

            $('#confirmModal').modal('show');

        });

        function checkAllData(){

            let days = '';

            $('.subCheckbox:checkbox:checked').each(function(){

                let dataId = $(this).val();

                let date    = $('#date'+ dataId).val();
                
                let time    = $('#time'+ dataId +' option:selected').val();
                
                let teacher = $('#teacher'+ dataId +' option:selected').text();

                days += `<tr>
                    <td>Day `+ dataId +`</td>
                    <td>`+ date +`</td>
                    <td>`+ time +`</td>
                    <td>`+ teacher +`</td>
                </tr>`;

            });

            return days;
        }

        var isValid = false;

        var dateArray = [];
    
        var teachers  = '';

        $("#check_all").click(function(){
    
            $('input:checkbox').not(this).prop('checked', this.checked);
    
            if(this.checked == true){
    
                $('.input_teacher').attr('name', 'teacher[]').prop('required', true);
                $('.input_date').attr('name', 'date[]').prop('required', true);
                $('.input_time').attr('name', 'time[]').prop('required', true);
    
            }else{
    
                $('.input_teacher').attr('name', '').prop('required', false);
                $('.input_date').attr('name', '').prop('required', false);
                $('.input_time').attr('name', '').prop('required', false);
    
            }
    
        });
    
        $(document).on('click', '.subCheckbox', function(){
    
            if(this.checked == true){
    
                $(this).closest('.form-row').find('.input_teacher').attr('name', 'teacher[]').prop('required', true);
                $(this).closest('.form-row').find('.input_date').attr('name', 'date[]').prop('required', true);
                $(this).closest('.form-row').find('.input_time').attr('name', 'time[]').prop('required', true);
    
            }else{
    
                $(this).closest('.form-row').find('.input_teacher').attr('name', '').prop('required', false);
                $(this).closest('.form-row').find('.input_date').attr('name', '').prop('required', false);
                $(this).closest('.form-row').find('.input_time').attr('name', '').prop('required', false);
    
            }
    
        });
    
        $(document).on('change', '#start_day', function(){
    
            let grade = $('#grade').val();
    
            let frequency = $('#frequency').val();
    
            let start_day = $('option:selected', this).val();
                start_day = parseInt(start_day);
            
            if(start_day != '' && (start_day >= 1 && start_day <= 16 )){
                
                appendData(start_day, frequency);

            }else{
                
                $('#appData').html('');
                
                $('#selection').hide();
            
            }
            
        });
    
        $(document).on('change','#frequency', function(){
            
            let frequency = $('option:selected', this).val();
    
            $('#appData').html('');
    
            $('#selection').hide();
    
            $('#start_day').prop('selectedIndex',0);
    
            if(frequency == ''){
                $('#start_day').html('start_day').prop('disabled', true);
    
            }else{

                let option = '<option value="">{{__('label.select_item')}}</option>';

                if(frequency == 1){

                    for(let i = 1; i <= 16; i++){

                        option += '<option value="'+ i +'">Day '+ i +'</option>';

                    }

                }else{
                    
                    for(let i = 1; i <= frequency; i++){
                    
                        option += '<option value="'+ i +'">Day '+ i +'</option>';
                    
                    }

                }
    
                $('#start_day').html(option).prop('disabled', false);
            }
    
        });
    
    
        $(document).on('change', '#grade', function(){
    
            let grade = $('option:selected', this).val();

            let course = $('#course').val();
    
            $('.schedule_time').html('').prop('disabled', true).val('');
    
            $('#appData').html('');
    
            $('#selection').hide();
    
            $('#start_day').html('').prop('disabled', true);
    
            $('#frequency').prop('disabled', false)
            
            $.ajax({
                type: 'post',
                dataType: 'json',
                url: '{{ url('api/schedule/get-dates') }}',
                data: {
                    grade: grade,
                    course: course
                },
                success: function(response){

                    dateArray = response;
    
                    
                }
            });

        });
    
        $(document).on('change', '#course', function(){
    
            let option = '<option value="" >{{__('label.select_item')}}</option>';
    
            let id = $('option:selected', this).val();
    
            let group = $(this).closest('.schedule-group');
            
            
            $('#appData').html('');
            $('#selection').hide();
            $('#start_day').html('').prop('disabled', true);
            $('#teacher').html('').prop('disabled', true);
            $('#start_date').html('').prop('disabled', true);
            
            $('.schedule_time').html('').prop('disabled', true).val('');
    
            if(id != ''){
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: '{{ url('api/schedule/get-grade') }}',
                    data: {
                        locale : '{{Config::get('app.locale')}}',
                        id: id
                    },
                    success: function(response){
                        $('#grade').prop('disabled', false).html(response.grade);
                        $('#frequency').prop('disabled', true).html(response.schedule);
                    }
                });
            }else{
                $('#grade').prop('disabled', true).html(option);
                $('#frequency').prop('disabled', true).html(option);
            }
    
        });

        $(document).on('change', '.input_time', function(){

            let data_id     = $(this).attr('data-id');

            let time        = $('option:selected', this).val();

            let frequency   = $('#frequency').val();

            let date    = [];
            
            if(frequency == 1){

                let getDate = $('#date'+ data_id).val();

                date.push(getDate);

            }else{
                for(let x = parseInt(data_id); x <= parseInt(frequency); x ++){

                    let getDate = $('#date'+ x).val();

                    date.push(getDate);

                }
            }
            
            $.ajax({
                type: 'post',
                dataType: 'json',
                url: '{{ url('api/schedule/get-teacher') }}',
                data: {
                    date: date,
                    time: time,
                    data_id: data_id,
                    frequency: frequency
                },success:function(response){

                    for(let key in response){

                        let teacher = response[key]['teacher'];

                        if(teacher != ''){

                            $('#teacher'+ key).html(teacher).prop({
                                disabled: false
                            });

                        }else{

                            $('#teacher'+ key).html('').prop({
                                disabled: true
                            });

                        }
                    }

                }

            });
            
        });
        
        $(document).on('change', '.input_teacher', function(){

            let value   = $('option:selected', this).val();

            let data_id = $(this).attr('data-id');

            let frequency = $('#frequency').val();

            for(let x = parseInt(data_id); x <= parseInt(frequency); x++){

                $('#teacher'+ x).val(value);

            }

        });

        $(document).on('change', '.datepicker', function(){
            
            let data_id     = $(this).attr('data-id');

            let date        = $(this).val();

            let start_day   = $('#start_day').val();

            let frequency   = $('#frequency').val();

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: '{{ url('api/schedule/set-date') }}',
                data: {
                    date: date,
                    frequency: frequency,
                    start_day: start_day
                },
                success:function(response){

                    for(let key in response){

                        let date = response[key]['date'];

                        let time = response[key]['time'];

                        if(date != null){
                            
                            if(key >= parseInt(data_id)){
                                
                                $('#date'+ key).val(date).prop({
                                    disabled:false,
                                    readonly: true
                                });

                                $('#time'+ key).html(time).prop({
                                    disabled: false,
                                });
                            }

                        }else{

                            if(key >= parseInt(data_id)){
                                
                                $('#date'+ key).val('').prop({
                                    disabled: true,
                                    readonly: false
                                });

                                $('#time'+ key).html('').prop({
                                    disabled: true,
                                });

                            }

                        }

                    }

                }
            });

        });
        
    
        $(document).on('change', '.input_time', function(){
    
            let data_id     = $(this).attr('data-id');
    
            let value       = $('option:selected', this).val();
    
            let frequency   = $('#frequency').val();
    
            for(let x = parseInt(data_id); x <= parseInt(frequency); x++){
    
                $('#time'+ x).val(value);
    
            }
    
        });
    
    
        function appendData(start_day, frequency){
            
            console.log(start_day);

            console.log(frequency);

            let html = ``;
    
            let count = 0;

            if(frequency == 1){

                html += `
                    <div class="form-row" id="row`+ start_day +`">
                        <div class="form-group col col-md-3 col-lg-3">
                            <div class="custom-control custom-checkbox others">
                                <input type="checkbox" class="custom-control-input subCheckbox" id="day`+ start_day +`" name="day[]" value="`+ start_day +`" checked>
                                <label class="custom-control-label" for="day`+ start_day +`">Day `+ start_day +`</label>
                            </div>
                        </div>

                        <div class="form-group col col-md-3 col-lg-3">
                            <input data-id="`+ start_day +`" type="text" id="date`+ start_day +`" class="form-control input_date datepicker" name="date[]" required>
                        </div>
                        <div class="form-group col col-md-3 col-lg-3">
                            <select data-id="`+ start_day +`" id="time`+ start_day +`" class="form-control input_time" name="time[]" required></select>
                        </div>
                        <div class="form-group col col-md-3 col-lg-3">
                            <select data-id="`+ start_day +`" id="teacher`+ start_day +`" class="form-control input_teacher" name="teacher[]" required></select>
                        </div>
                    </div>
                `;

            }else{

                for(let i = start_day; i <= frequency; i++){
    
                count++;
        
                    let disabled = count == 1 ? '':'disabled="true"';
        
                    let isReadonly = count == 1 ? 'readonly="true"':'';
                    
                    html += `
                        <div class="form-row" id="row`+ i +`">
                            <div class="form-group col col-md-3 col-lg-3">
                                <div class="custom-control custom-checkbox others">
                                    <input type="checkbox" class="custom-control-input subCheckbox" id="day`+ i +`" name="day[]" value="`+ i +`" checked>
                                    <label class="custom-control-label" for="day`+ i +`">Day `+ i +`</label>
                                </div>
                            </div>
                            <div class="form-group col col-md-3 col-lg-3">
                                <input data-id="`+ i +`" type="text" id="date`+ i +`" class="form-control input_date datepicker" name="date[]" `+ disabled +` required>
                            </div>
                            <div class="form-group col col-md-3 col-lg-3">
                                <select data-id="`+ i +`" id="time`+ i +`" class="form-control input_time" name="time[]" disabled="true" required></select>
                            </div>
                            <div class="form-group col col-md-3 col-lg-3">
                                <select data-id="`+ i +`" id="teacher`+ i +`" class="form-control input_teacher" name="teacher[]" disabled="true" required></select>
                            </div>
                        </div>
                    `;
                }
            }

            /*
            
    
            

            
            */

            $('#selection').show();
    
            $('#appData').html(html);

            datePicker();
        }
        
        function datePicker(){
            
            var dateToday = new Date();
    
            $('.datepicker').datepicker({
                dateFormat: "yy/mm/dd",
                minDate: dateToday,
                beforeShowDay: function(date){
                    
                    var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
    
                    return [ dateArray.indexOf(string) != -1 ]
                }
            });
        }

    });
    
</script>
@endsection