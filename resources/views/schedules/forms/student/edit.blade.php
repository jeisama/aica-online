@extends('layouts.app')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/jquery-ui/custom.css') }}">
<style>
    .custom-checkbox{
    margin-top: 8px;
    }
    .custom-checkbox.others{
    }
    .custom-checkbox label.custom-control-label{
        padding-top: 2px;
    }
    #selection label.column{
        padding-top: 10px;
    }
    .readOnly:read-only {
        background: #fff;
    }
    .form-control[readonly]{
        background: #fff;
    }
</style>
@endsection
@section('content')
<!-- Breadcrumb -->
<div class="breadcrumb-holder container-fluid">
    <div class="inner">
        <div class="links">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i> {{__('label.home')}}</a></li>
                <li class="breadcrumb-item"><a href="{{ url('schedules') }}">{{__('label.schedule')}}</a></li>
                <li class="breadcrumb-item active">{{__('label.edit')}}</li>
            </ul>
        </div>
    </div>
</div>
<!-- Breadcrumb -->
<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col col-md-12 col-lg-10">
                @if(Session::has('success_message'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    New user has been successfully created.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                <div class="card">
                    <div class="card-body">
                        <h1 class="card-title">{{__('label.edit_stu')}}</h1>
                        <div class="line"></div>
                        {{ Form::open(['method' => 'put', 'class' => 'form-main']) }}
                        <input type="hidden" name="id" value="{{ $schedule->id }}">
                        <div class="form-row">
                            <div class="form-group col col-md-6 col-lg-6">
                                <label for="student">{{__('label.student')}}</label>
                                <select id="student" class="form-control" disabled="true">
                                    <option value="">{{__('label.select_student')}}</option>
                                    @if($students)
                                    @foreach($students as $student)
                                    <option value="{{ $student->id }}" {{ $schedule->user_id == $student->id ? 'selected':'' }}>{{ $student->romaji_name }}</option>
                                    @endforeach
                                    @endif
                                </select>
                                <small id="error-student" class="form-text form-error"></small>
                            </div>
                        </div>
                        <div class="line"></div>
                        <div class="schedule-group" data-id="1">
                            <div class="form-row">
                                <div class="form-group col col-md-6 col-lg-6">
                                    <label for="course">{{__('label.course')}}</label>
                                    <select id="course" class="form-control" disabled="true">
                                        <option value="">{{__('label.select_item')}}</option>
                                        @if($course)
                                        @foreach($course as $c)
                                        <option value="{{ $c->id }}" {{ $schedule->course_id == $c->id ? 'selected':'' }}>{{ $c->label }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                    <small id="error-course" class="form-text form-error"></small>
                                </div>
                                <div class="form-group col col-md-6 col-lg-6">
                                    <label for="grade">{{__('label.grade')}}</label>
                                    <select id="grade" class="form-control" disabled="true">
                                        <option value="">{{__('label.select_item')}}</option>
                                        @if($grades)
                                        @foreach($grades as $grade)
                                        <option value="{{ $grade->grade_level->id }}" {{ $grade->grade_level->id == $schedule->grade_id ? 'selected':'' }}>{{ $grade->grade_level->label }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                    <small id="error-grade" class="form-text form-error"></small>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col col-md-6 col-lg-6">
                                    <label for="frequency">{{__('label.frequency')}}</label>
                                    <select id="frequency" class="form-control"disabled="true">
                                        <option value="">{{__('label.select_item')}}</option>
                                        <option value="" {{ $schedule->schedule_id == 16 ? 'selected':'' }}>{{__('label.once_week')}}</option>
                                        <option value="" {{ $schedule->schedule_id == 8 ? 'selected':'' }}>{{__('label.alternate_week')}}</option>
                                        <option value="" {{ $schedule->schedule_id == 1 ? 'selected':'' }}>{{__('label.one_time_only')}}</option>
                                    </select>
                                    <small id="error-frequency" class="form-text form-error"></small>
                                </div>
                                <div class="form-group col col-md-6 col-lg-6">
                                    <label for="frequency">{{__('label.start_day')}}</label>
                                    <select id="start_day" class="form-control" disabled="true">
                                        @for($i = 1; $i <= 16; $i++)
                                        <option value="" {{ $schedule->day == $i ? 'selected':'' }}>Day {{ $i }}</option>
                                        @endfor
                                    </select>
                                    <small id="error-start_day" class="form-text form-error"></small>
                                </div>
                            </div>
                            <div id="selection">
                                <div class="form-row">
                                    <div class="form-group col col-md-3 col-lg-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="check_all" checked>
                                            <label class="custom-control-label" for="check_all">{{__('label.select_all')}} </label>
                                            <small id="error-day" class="form-text form-error"></small>
                                        </div>
                                    </div>
                                    <div class="form-group col col-md-3 col-lg-3">
                                        <label class="column">{{__('label.date')}}</label>
                                    </div>
                                    <div class="form-group col col-md-3 col-lg-3">
                                        <label class="column">{{__('label.time')}}</label>
                                    </div>
                                    <div class="form-group col col-md-3 col-lg-3">
                                        <label class="column">{{__('label.teacher')}}</label>
                                    </div>
                                </div>
                            </div>
                            <?php 

                            $today = \Carbon\Carbon::now();

                            if($schedules):                            

                            $count = $schedule->day - 1;
                            
                            foreach($schedules as $res):
                            
                            $disabled = false;

                            if(strtotime($now) > strtotime($res->date .' '. $res->start_time)):
                            
                                $disabled = true;
                            
                            elseif(date('mY', strtotime($today)) != date('mY', strtotime($res->date))):
                                
                                $disabled = true;

                            else:

                                $date1 = \Carbon\Carbon::parse(date('Y-m-d'));

                                $old   = \Carbon\Carbon::parse($res->date);

                                $diff = $date1->diffInDays($old);

                                

                                if($diff < 2):
                                
                                    $disabled = true;

                                elseif($diff == 2 && date('H', strtotime($today)) >= 20):
                                
                                    $disabled = true;

                                endif;

                            endif;
                            
                            if($res->day >= $schedule->day):
                            
                            $count ++;
                            
                            ?>
                            <input type="hidden" {!! $disabled ? 'disabled':'name="schedule_id[]"' !!} value="{{ $res->id }}">
                            <div class="form-row" id="row{{ $res->id }}">
                                <div class="form-group col col-md-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input {!! $disabled ? '':'subCheckbox' !!}" id="day{{ $count }}" {!! $disabled ? 'disabled':'name="day[]"' !!}  value="{{ $count }}" checked>
                                        <label class="custom-control-label" for="day{{ $count }}">{{__('label.day')}} {{ $res->day }}</label>
                                    </div>
                                </div>
                                
                                <div class="form-group col col-md-3 col-lg-3">
                                    <input type="text" data-id="{{ $count }}" class="form-control input_date datepicker" {!! $disabled ? 'disabled':'name="date[]" id="date'. $count .'"' !!} value="{{ date('Y/m/d', strtotime($res->date)) }}">
                                </div>
                                
                                <div class="form-group col col-md-3 col-lg-3">
                                    <select data-id="{{ $count }}" class="form-control input_time" {!! $disabled ? 'disabled':'name="time[]" id="time'. $count .'"' !!}>
                                        <option value="{{ date('h:i A', strtotime($res->start_time)) }}">{{ date('h:i A', strtotime($res->start_time)) }}</option>
                                    </select>
                                </div>
                                <div class="form-group col col-md-3 col-lg-3">
                                    <select data-id="{{ $count }}" class="form-control input_teacher" {!! $disabled ? 'disabled':'name="teacher[]" id="teacher'. $count .'"' !!}>
                                        <option value="{{ $res->teacher->id }}">{{ $res->teacher->full_name }}</option>    
                                    </select>
                                </div>
                            </div>
                            <?php
                            endif;
                            endforeach;
                            endif;
                            ?>
                            <div class="line"></div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col col-md-12 col-lg-12">
                                <button type="submit" data-form="update-schedule" class="btn btn-primary">{{__('label.save')}}</button>
                                <a href="{{ url('schedules') }}" class="btn btn-warning">{{__('label.back')}}</a>
                                <span class="form-proccessing hidden"><img src="{{ asset('assets/img/Loading/SpinnerOrange.svg') }}" style="height: 50px;"> {{__('label.submit_process')}}</span>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('js')

<script type="text/javascript" src="{{ asset('assets/vendor/timepicker/include/ui-1.10.0/jquery.ui.core.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/vendor/timepicker/jquery.ui.timepicker.js?v=0.3.3') }}"></script>

<script src="{{ asset('assets/vendor/jquery-ui/custom.js') }}"></script>

<div class="appendDatePicker">
    <script>
        $(function(){

            let dateArray = {!! json_encode($getdates) !!};

            datePicker();


            $("#check_all").click(function(){
    
                $('input:checkbox.subCheckbox').not(this).prop('checked', this.checked);
        
                if(this.checked == true){
        
                    $('.input_teacher').attr('name', 'teacher[]').prop('required', true);
                    $('.input_date').attr('name', 'date[]').prop('required', true);
                    $('.input_time').attr('name', 'time[]').prop('required', true);
        
                }else{
        
                    $('.input_teacher').attr('name', '').prop('required', false);
                    $('.input_date').attr('name', '').prop('required', false);
                    $('.input_time').attr('name', '').prop('required', false);
        
                }
        
            });
        
            $(document).on('click', '.subCheckbox', function(){
        
                if(this.checked == true){
        
                    $(this).closest('.form-row').find('.input_teacher').attr('name', 'teacher[]').prop('required', true);
                    $(this).closest('.form-row').find('.input_date').attr('name', 'date[]').prop('required', true);
                    $(this).closest('.form-row').find('.input_time').attr('name', 'time[]').prop('required', true);
        
                }else{
        
                    $(this).closest('.form-row').find('.input_teacher').attr('name', '').prop('required', false);
                    $(this).closest('.form-row').find('.input_date').attr('name', '').prop('required', false);
                    $(this).closest('.form-row').find('.input_time').attr('name', '').prop('required', false);
        
                }
        
            });

            $(document).on('change', '.datepicker', function(){
            
                let data_id     = $(this).attr('data-id');

                let date        = $(this).val();

                let start_day   = '{{ $schedule->day }}';

                let frequency   = '{{ $schedule->schedule_id }}';

                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: '{{ url('api/schedule/set-date') }}',
                    data: {
                        date: date,
                        frequency: frequency,
                        start_day: start_day
                    },
                    success:function(response){

                        for(let key in response){

                            let date = response[key]['date'];

                            let time = response[key]['time'];

                            if(date != null){
                                
                                if(key >= parseInt(data_id)){
                                    
                                    $('#date'+ key).val(date).prop({
                                        disabled:false,
                                        readonly: true
                                    });

                                    $('#time'+ key).html(time).prop({
                                        disabled: false,
                                    });
                                }

                            }else{

                                if(key >= parseInt(data_id)){
                                    
                                    $('#date'+ key).val('').prop({
                                        disabled: true,
                                        readonly: false
                                    });

                                    $('#time'+ key).html('').prop({
                                        disabled: true,
                                    });

                                }

                            }

                        }

                    }
                });

            });

            $(document).on('change', '.input_time', function(){

                let data_id     = $(this).attr('data-id');

                let time        = $('option:selected', this).val();

                let frequency   = '{{ $schedule->schedule_id }}';

                let date        = [];
                
                for(let x = parseInt(data_id); x <= parseInt(frequency); x ++){

                    let getDate = $('#date'+ x).val();

                    date.push(getDate);

                    $('#time'+ x).val(time);

                }

                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: '{{ url('api/schedule/get-teacher') }}',
                    data: {
                        date: date,
                        time: time,
                        data_id: data_id,
                        frequency: frequency
                    },success:function(response){

                        for(let key in response){

                            let teacher = response[key]['teacher'];

                            if(teacher != ''){

                                $('#teacher'+ key).html(teacher).prop({
                                    disabled: false
                                });

                            }else{

                                $('#teacher'+ key).html('').prop({
                                    disabled: true
                                });

                            }
                        }

                    }

                });

            });

            $(document).on('change', '.input_teacher', function(){

                let value   = $('option:selected', this).val();

                let data_id = $(this).attr('data-id');

                let frequency   = '{{ $schedule->schedule_id }}';

                for(let x = parseInt(data_id); x <= parseInt(frequency); x++){

                    $('#teacher'+ x).val(value);

                }

            });

            function datePicker(){
            
                var dateToday = new Date();
        
                $('.datepicker').datepicker({
                    dateFormat: "yy/mm/dd",
                    minDate: dateToday,
                    beforeShowDay: function(date){
                        
                        var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
        
                        return [ dateArray.indexOf(string) != -1 ]
                    }
                });
            }


        });
    </script>
</div>
@endsection