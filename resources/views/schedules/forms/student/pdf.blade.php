<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
	<link rel="stylesheet" href="{{ asset('assets/css/style.blue.css') }}" id="theme-stylesheet">
    <link href="{{ asset('assets/vendor/MaterialDesignWebfont/css/materialdesignicons.css') }}" media="all" rel="stylesheet" type="text/css">
    <!-- Custom stylesheet - for your changes-->
	<link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<script src="{{ asset('assets/vendor/popper.js/umd/popper.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>

</head>
<body>
	
	<table class="table table-bordered" id="table-days">
		<thead>
			<tr>
				<th width="25%">Days</th>
				<th width="25%">{{ __('label.date') }}</th>
				<th width="25%">{{ __('label.time') }}</th>
				<th width="25%">{{ __('label.teacher') }}</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($info as $item)
				<tr>
					<td>Day {{$item->day}}</td>
					<td>{{$item->date}}</td>
					<td>{{$item->start_time}}</td>
					<td>{{$item->teacher->full_name}}</td>	
				</tr>
			@endforeach
		</tbody>
	</table>
</body>
</html>

