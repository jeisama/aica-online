@extends('layouts.app')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/jquery-ui/jquery-ui.css') }}">
<style>
    .button-add{
        margin-top: 28px;
    }
    .no-input {
        pointer-events: none;
    }
</style>
@endsection

@section('content')
    <!-- Breadcrumb -->
    <div class="breadcrumb-holder container-fluid">
        <div class="inner">
            <div class="links">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i> {{__('label.home')}}</a></li>
                    <li class="breadcrumb-item"><a href="{{ url('schedules') }}">{{__('label.schedule')}}</a></li>
                    <li class="breadcrumb-item active">{{__('label.edit')}}</li>
                </ul>
            </div>
        </div>        
    </div><!-- Breadcrumb -->
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="col col-md-12 col-lg-10">
                    @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        New user has been successfully created.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    <div class="card">
                        <div class="card-body">
                            <h1 class="card-title">{{__('label.create_tea_sched')}}</h1>
                            <div class="line"></div>
                            {{ Form::open(['class' => 'form-main']) }}
                            <div class="form-row">
                                <div class="form-group col col-md-4 col-lg-3">
                                    <label for="teacher">{{__('label.teacher')}}</label>
                                    <select id="teacher" class="form-control no-input" name="teacher" readonly>
                                        <option value="">{{__('label.select_item')}}</option>
                                        <option value="{{ $schedules->user_id }}" selected >{{$schedules->user->full_name }}</option>
                                    </select>
                                    <small id="error-teacher" class="form-text form-error"></small>
                                </div>
                            </div>
                            
                            <div class="group-data">
                                <div class="form-row">
                                    <div class="form-group col col-md-4 col-lg-3">
                                        <label>{{__('label.day')}}</label>
                                        <select name="day" class="form-control" required>
                                            <option value="">{{__('label.select_item')}}</option>
                                            <option value="1" {{date('N', strtotime($schedules->date)) == 1 ? "selected" : " "}}>{{__('label.monday')}}</option>
                                            <option value="2" {{date('N', strtotime($schedules->date)) == 2 ? "selected" : " "}}>{{__('label.tuesday')}}</option>
                                            <option value="3" {{date('N', strtotime($schedules->date)) == 3 ? "selected" : " "}}>{{__('label.wednesday')}}</option>
                                            <option value="4" {{date('N', strtotime($schedules->date)) == 4 ? "selected" : " "}}>{{__('label.thursday')}}</option>
                                            <option value="5" {{date('N', strtotime($schedules->date)) == 5 ? "selected" : " "}}>{{__('label.friday')}}</option>
                                            <option value="6" {{date('N', strtotime($schedules->date)) == 6 ? "selected" : " "}}>{{__('label.saturday')}}</option>
                                            <option value="7" {{date('N', strtotime($schedules->date)) == 7 ? "selected" : " "}}>{{__('label.sunday')}}</option>
                                        </select>
                                    </div>
                                    <div class="form-group col col-md-4 col-lg-3">
                                        <label>{{__('label.time_in')}}</label>
                                        <input type="text" class="form-control timepicker" name="timein" required value="{{$schedules->time_in}}">
                                    </div>
                                    <div class="form-group col col-md-4 col-lg-3">
                                        <label>{{__('label.time_out')}}</label>
                                        <input type="text" class="form-control timepicker" name="timeout" required value="{{$schedules->time_out}}">
                                    </div>
                                  
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col col-md-4 col-lg-3">
                                    <label>{{__('label.start_date')}}</label>
                                <input type="text" class="form-control datepicker no-input" name="startdate" value="{{$schedules->date}}" readonly autocomplete="off">
                                    <small id="error-startdate" class="form-text form-error"></small>
                                </div>
                                <div class="form-group col col-md-4 col-lg-3">
                                    <label>{{__('label.end_date')}}</label>
                                <input type="text" class="form-control datepicker no-input" name="enddate" value="{{$dateRange[1]}}" readonly autocomplete="off">
                                    <small id="error-enddate" class="form-text form-error"></small>
                                </div>
                            </div>
                            <div class="line"></div>
                            <div class="form-row">
                                <div class="form-group col col-md-6 col-lg-6">
                                    <button type="submit" class="btn btn-primary">{{__('label.update')}}</button>
                                    <a href="{{ url('schedules') }}" class="btn btn-warning">{{__('label.cancel')}}</a>
                                    <span class="form-proccessing hidden"><img src="{{ asset('assets/img/Loading/loading.gif') }}"> {{__('label.submit_process')}}</span>
                                </div>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

@endsection
@section('js')
    <script type="text/javascript" src="{{ asset('assets/vendor/timepicker/include/ui-1.10.0/jquery.ui.core.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendor/timepicker/jquery.ui.timepicker.js?v=0.3.3') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-ui/jquery-ui.js') }}"></script>

    <script>
           $(function(){

                datepicker();

                timepicker();

                function datepicker(){
                    $('.datepicker').datepicker();
                }

                function timepicker(){
                    $('.timepicker').timepicker();
                }

            });
    </script>
@endsection