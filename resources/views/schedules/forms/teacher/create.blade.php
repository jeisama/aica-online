@extends('layouts.app')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/jquery-ui/jquery-ui.css') }}">
<style>
    .button-add{
        margin-top: 28px;
    }
</style>
@endsection

@section('content')

    <!-- Breadcrumb -->
    <div class="breadcrumb-holder container-fluid">
        <div class="inner">
            <div class="links">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i> {{__('label.home')}}</a></li>
                    <li class="breadcrumb-item"><a href="{{ url('schedules') }}">{{__('label.schedule')}}</a></li>
                    <li class="breadcrumb-item active">{{__('label.create')}}</li>
                </ul>
            </div>
        </div>        
    </div><!-- Breadcrumb -->

    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="col col-md-12 col-lg-10">

                    @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        New user has been successfully created.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif

                     <div class="card">
                        <div class="card-body">
                            <h1 class="card-title">{{__('label.create_tea_sched')}}</h1>
                            <div class="line"></div>

                            {{ Form::open(['files' => true, 'class' => 'form-main']) }}

                            <div class="form-row">
                                <div class="form-group col col-md-4 col-lg-3">
                                    <label for="teacher">{{__('label.teacher')}}</label>
                                    <select id="teacher" class="form-control" name="teacher" required>
                                        <option value="">{{__('label.select_item')}}</option>
                                        @if($teachers)
                                        @foreach($teachers as $teacher)
                                        <option value="{{ $teacher->id }}">{{ $teacher->full_name }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                    <small id="error-teacher" class="form-text form-error"></small>
                                </div>
                            </div>
                            
                            <div class="group-data">
                                <div class="form-row">
                                    <div class="form-group col col-md-4 col-lg-3">
                                        <label>{{__('label.day')}}</label>
                                        <select name="day[]" class="form-control" required>
                                            <option value="">{{__('label.select_item')}}</option>
                                            <option value="1">{{__('label.monday')}}</option>
                                            <option value="2">{{__('label.tuesday')}}</option>
                                            <option value="3">{{__('label.wednesday')}}</option>
                                            <option value="4">{{__('label.thursday')}}</option>
                                            <option value="5">{{__('label.friday')}}</option>
                                            <option value="6">{{__('label.saturday')}}</option>
                                            <option value="7">{{__('label.sunday')}}</option>
                                        </select>
                                    </div>
                                    <div class="form-group col col-md-4 col-lg-3">
                                        <label>{{__('label.time_in')}}</label>
                                        <input type="text" class="form-control timepicker" name="timein[]" required>
                                    </div>
                                    <div class="form-group col col-md-4 col-lg-3">
                                        <label>{{__('label.time_out')}}</label>
                                        <input type="text" class="form-control timepicker" name="timeout[]" required>
                                    </div>
                                    <div class="form-group col col-md-4 col-lg-3">
                                        <span class="btn btn-default button-add addRow"><i class="mdi mdi-plus"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col col-md-4 col-lg-3">
                                    <label>{{__('label.start_date')}}</label>
                                    <input type="text" class="form-control datepicker" name="startdate" value="{{ date('m/d/Y') }}" required autocomplete="off">
                                    <small id="error-startdate" class="form-text form-error"></small>
                                </div>
                                <div class="form-group col col-md-4 col-lg-3">
                                    <label>{{__('label.end_date')}}</label>
                                    <input type="text" class="form-control datepicker" name="enddate" required autocomplete="off">
                                    <small id="error-enddate" class="form-text form-error"></small>
                                </div>
                            </div>
                            <div class="line"></div>

                            
                            <div id="appData"></div>
                            <div class="form-row">
                                <div class="form-group col col-md-6 col-lg-6">
                                    <button type="submit" class="btn btn-primary">{{__('label.save')}}</button>
                                    <a href="{{ url('schedules') }}" class="btn btn-warning">{{__('label.cancel')}}</a>
                                    <span class="form-proccessing hidden"><img src="{{ asset('assets/img/Loading/loading.gif') }}"> {{__('label.submit_process')}}</span>
                                </div>
                            </div>

                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection


@section('js')
    
    <script type="text/javascript" src="{{ asset('assets/vendor/timepicker/include/ui-1.10.0/jquery.ui.core.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendor/timepicker/jquery.ui.timepicker.js?v=0.3.3') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-ui/jquery-ui.js') }}"></script>

    <script>
        
    $(function(){

        datepicker();

        timepicker();
        
        var count = 1;

        /*
        |----------------------------------------
        | removeRow
        |----------------------------------------
        */
        $(document).on('click', '.removeRow', function(){
            $(this).closest('.form-row').remove();
        });

        /*
        |----------------------------------------
        | addRow
        |----------------------------------------
        */

        $(document).on('click', '.addRow', function(){
            
            let row = `
                <div class="form-row">
                    <div class="form-group col col-md-4 col-lg-3">
                        <label>{{__('label.day')}}</label>
                        <select name="day[]" class="form-control" required>
                            <option value="">{{__('label.select_item')}}</option>
                            <option value="1">{{__('label.monday')}}</option>
                            <option value="2">{{__('label.tuesday')}}</option>
                            <option value="3">{{__('label.wednesday')}}</option>
                            <option value="4">{{__('label.thursday')}}</option>
                            <option value="5">{{__('label.friday')}}</option>
                            <option value="6">{{__('label.saturday')}}</option>
                            <option value="7">{{__('label.sunday')}}</option>
                        </select>
                    </div>
                    <div class="form-group col col-md-4 col-lg-3">
                        <label>{{__('label.time_in')}}</label>
                        <input type="text" class="form-control timepicker" name="timein[]" required>
                    </div>
                    <div class="form-group col col-md-4 col-lg-3">
                        <label>{{__('label.time_out')}}</label>
                        <input type="text" class="form-control timepicker" name="timeout[]" required>
                    </div>
                    <div class="form-group col col-md-4 col-lg-3">
                        <span class="btn btn-default button-add addRow"><i class="mdi mdi-plus"></i></span>
                    </div>
                </div>`;
            
            $(this).removeClass('addRow').removeClass('btn-default').addClass('removeRow').addClass('btn-warning').html('<i class="mdi mdi-close"></i>');

            $('.group-data').append(row);

            timepicker();

        });

        $(document).on('click', '.remove_schedule', function(){
            $(this).closest('.schedule-group').remove();
        });

        $(document).on('change', '.timepicker', function(){
            
            let time = $(this).val();
            
            let groudId = $(this).closest('.schedule-group').attr('data-id');

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: '{{ url('api/schedule/end-time') }}',
                data: {
                    time: time
                },
                success: function(response){
                    $('#end_time'+ groudId).val(response.time);
                }
            });

        });

        $(document).on('change', '.course', function(){

            let option = '<option value="" >--Select Item--</option>';

            let id = $('option:selected', this).val();

            let group = $(this).closest('.schedule-group');
            
            if(id != ''){
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: '{{ url('api/schedule/get-grade') }}',
                    data: {
                        id: id
                    },
                    success: function(response){
                        group.find('.grade').prop('disabled', false).html(response.grade);
                        group.find('.schedule').prop('disabled', false).html(response.schedule);

                    }
                });
            }else{
                group.find('.grade').prop('disabled', true).html(option);
                group.find('.schedule').prop('disabled', true).html(option);
            }

        });

        function datepicker(){
            $('.datepicker').datepicker();
        }

        function timepicker(){
            $('.timepicker').timepicker();
        }

    });

    </script>
@endsection
