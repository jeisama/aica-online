@extends('layouts.app')

@section('css')

<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/jquery-ui/jquery-ui.css') }}">

@endsection

@section('content')
    <!-- Dashboard Counts Section-->
    <div class="breadcrumb-holder container-fluid">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i> Home</a></li>
            <li class="breadcrumb-item"><a href="{{ url('/schedules') }}">Schedule</a></li>
            <li class="breadcrumb-item active">Edit Schedule</li>
        </ul>
    </div>

    
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="col col-md-12 col-lg-10">
                    @if(Session::has('success_message'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        New user has been successfully created.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif

                    <div class="card">
                        <div class="card-body">
                            <h1 class="card-title">Create</h1>
                            <div class="line"></div>
                            
                            {{ Form::open(['id' => 'update-schedule']) }}
                            <div class="form-row">
                                <div class="form-group col col-md-4 col-lg-4">
                                    <label for="course">Course</label>
                                    <select id="course" class="form-control course" name="course">
                                        <option value="">--Select Item--</option>
                                        @if($course)
                                        @foreach($course as $c)
                                        <option value="{{ $c->id }}" {{ $schedule->course_id == $c->id ? 'selected':'' }}>{{ $c->label }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                    @if($errors->has('course'))
                                        <small class="form-text form-error">{{ $errors->first('course') }}</small>
                                    @endif
                                </div>
                                <div class="form-group col col-md-4 col-lg-4">
                                    <label for="grade1">Grade</label>

                                    <select id="grade1" class="form-control grade" name="grade">
                                        <option value="">--Select Item--</option>
                                        @if($grades)
                                        @foreach($grades as $grade)
                                        <option value="{{ $grade->id }}" {{ $grade->id == $schedule->grade_id ? 'selected':'' }}>{{ $grade->label }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                    @if($errors->has('grade'))
                                        <small class="form-text form-error">{{ $errors->first('grade') }}</small>
                                    @endif
                                </div>
                                <div class="form-group col col-md-4 col-lg-4">
                                    <label for="schedule1">Schedule</label>
                                    <select id="schedule1" class="form-control schedule" name="schedule">
                                        <option value="">--Select Item--</option>
                                        @if($schedule->course_id == 2 || $schedule->course_id == 4 || $schedule->course_id == 5)
                                        <option value="1" {{ $schedule->schedule_id == '1' ? 'selected':'' }}>One time only</option>
                                        @else
                                        <option value="16" {{ $schedule->schedule_id == '16' ? 'selected':'' }}>Once a week</option>
                                        <option value="8" {{ $schedule->schedule_id == '8' ? 'selected':'' }}>Alternate week</option>
                                        @endif
                                    </select>
                                    @if($errors->has('schedule'))
                                        <small class="form-text form-error">{{ $errors->first('schedule') }}</small>
                                    @endif
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col col-md-4 col-lg-4">
                                    <label for="start_date">Start Date</label>
                                    <input type="text" id="start_date" class="form-control start_date datepicker" name="start_date" value="{{ date('m/d/Y', strtotime($schedule->date)) }}">
                                    @if($errors->has('start_date'))
                                        <small class="form-text form-error">{{ $errors->first('start_date') }}</small>
                                    @endif
                                </div>
                                <div class="form-group col col-md-2 col-lg-2">
                                    <label for="start_time">Start Time</label>
                                    <input type="text" id="start_time" class="form-control start_time timepicker" name="start_time" value="{{ date('H:i', strtotime($schedule->start_time)) }}">
                                    @if($errors->has('start_time'))
                                        <small class="form-text form-error">{{ $errors->first('start_time') }}</small>
                                    @endif
                                </div>
                                <div class="form-group col col-md-2 col-lg-2">
                                    <label for="end_time1">End Time</label>
                                    <input type="text" id="end_time" class="form-control end_time" name="end_time" readonly="true" value="{{ date('H:i', strtotime($schedule->end_time)) }}">
                                    @if($errors->has('end_time'))
                                        <small class="form-text form-error">{{ $errors->first('end_time') }}</small>
                                    @endif
                                </div>
                            </div>

                            <div class="line"></div>

                            <div class="form-row">
                                <div class="form-group col col-md-6 col-lg-6">
                                    <button type="submit" data-form="update-schedule" class="btn btn-primary">Save</button>
                                    <a href="{{ url('schedules') }}" class="btn btn-warning">Cancel</a>
                                    <span class="form-proccessing hidden"><img src="{{ asset('assets/img/Loading/loading.gif') }}"> {{__('label.submit_process')}}</span>
                                </div>
                            </div>
                            {{ Form::close() }}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection


@section('js')
    <script src="{{ asset('assets/js/app.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/vendor/timepicker/include/ui-1.10.0/jquery.ui.core.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendor/timepicker/jquery.ui.timepicker.js?v=0.3.3') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-ui/jquery-ui.js') }}"></script>

    <script>
        
    $(function(){

        datepicker();

        timepicker();
        
        var count = 1;

        $(document).on('change', '.timepicker', function(){
            
            let time = $(this).val();
            
            $.ajax({
                type: 'post',
                dataType: 'json',
                url: '{{ url('api/schedule/end-time') }}',
                data: {
                    time: time
                },
                success: function(response){
                    $('#end_time').val(response.time);
                }
            });

        });

        $(document).on('change', '.course', function(){

            let option = '<option value="" >--Select Item--</option>';

            let id = $('option:selected', this).val();

            if(id != ''){
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: '{{ url('api/schedule/get-grade') }}',
                    data: {
                        id: id
                    },
                    success: function(response){
                        $('.grade').prop('disabled', false).html(response.grade);
                        $('.schedule').prop('disabled', false).html(response.schedule);

                    }
                });
            }else{
                $('.grade').prop('disabled', true).html(option);
                $('.schedule').prop('disabled', true).html(option);
            }

        });

        function datepicker(){
            $('.datepicker').datepicker();
        }

        function timepicker(){
            $('.timepicker').timepicker();
        }

    });

    </script>
@endsection
