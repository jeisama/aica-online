@extends('layouts.app')

@section('css')
<style>
    table td,
    table th{
        text-align: center;
    }
    table th{
        white-space: nowrap;
    }
    table th a{
        display: inline-block;
        margin-top: 10px;
    }
    i.mdi-menu-swap{
        line-height: 0;
        float: left;
        margin-bottom: -2px;
    }
</style>
@endsection

@section('content')
    
    <!-- Page Header-->
    <div class="breadcrumb-holder container-fluid">
        <div class="inner">
            <div class="links">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i> {{ __('label.home') }}</a></li>
                    <li class="breadcrumb-item active">{{__('label.report')}}</li>
                    <li class="breadcrumb-item active">{{__('label.teacher')}}</li>
                </ul>
            </div>
            @if(count($reports) > 0)
            <div class="buttons">
                <a href="{{ url('reports/teacher/export?'. $parsedUrl) }}" class="btn btn-primary btn-sm"> {{__('label.export')}}</a>
            </div>
            @endif
        </div>        
    </div>

    <section class="tables">   
        <div class="container-fluid">
            @if(Session::has('success_message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ Session::get('success_message') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif

            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="h4">{{ __('label.filter_search') }}</h3>
                        </div>

                        <div class="card-body">
                            {!! Form::open(['method' => 'GET']) !!}
                            <div class="form-row">
                                <div class="form-group col col-md-3">
                                    <label for="year">{{__('label.year')}}</label>
                                    <select id="year" name="year" class="form-control">
                                        <option value="">{{ __('label.select_item') }}</option>
                                       @if ($year != null)
                                            @for($y = date('Y'); $y >= date('Y', strtotime($year->date)); $y--)
                                            <option value="{{ $y }}" {{ $y == $q['year'] ? 'selected':'' }}>{{ $y }}</option>
                                            @endfor  
                                       @endif
                                    </select>
                                </div>
                                <div class="form-group col col-md-3">
                                    <label for="month">{{__('label.month')}}</label>
                                    <select id="month" name="month" class="form-control">
                                        <option value="">{{ __('label.select_item') }}</option>
                                        @for($m = 1; $m <= 12; $m++)
                                        <option value="{{ date('m', mktime(0, 0, 0, $m)) }}" {{ $q['month'] == date('m', mktime(0, 0, 0, $m)) ? 'selected':'' }}>{{ date('F', mktime(0, 0, 0, $m)) }}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="form-group col col-md-3">
                                    <label>&nbsp;</label>
                                    <button class="btn btn-primary" style="margin-top: 28px;">{{__('label.search')}}</button>
                                </div>
                            </div>
                            {!! Form::close() !!}

                            <div class="line"></div>
                            @if(count($reports) > 0)
                            <?php 
                            $order = $sort['order'] != '' ? ($sort['order'] == 'asc' ? 'desc' : 'asc') : 'asc';
                            ?>
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th colspan="4">AIC Online English</th>
                                        <th></th>
                                        <th colspan="3">{{ $q['year'] ? $q['year']: __('label.year') }}　{{ $q['month'] ? date('F', mktime(0, 0, 0, $q['month'])) : __('label.month') }}　{{__('label.report')}}</th>
                                    </tr>
                                    <tr>
                                        <th>
                                            氏名
                                            <a href="{!! url('reports/teacher?year='. $q['year'] .'&month='. $q['month'] .'&odb=name&order='. $order) !!}"><i class="mdi mdi-menu-swap mdi-24px"></i></a>
                                        </th>
                                        <th>
                                            授業レッスン期間
                                            <a href="{!! url('reports/teacher?year='. $q['year'] .'&month='. $q['month'] .'&odb=date&order='. $order) !!}"><i class="mdi mdi-menu-swap mdi-24px"></i></a>
                                        </th>
                                        <th>
                                            コース
                                            <a href="{!! url('reports/teacher?year='. $q['year'] .'&month='. $q['month'] .'&odb=course&order='. $order) !!}"><i class="mdi mdi-menu-swap mdi-24px"></i></a>
                                        </th>
                                        <th>
                                            級
                                            <a href="{!! url('reports/teacher?year='. $q['year'] .'&month='. $q['month'] .'&odb=grade&order='. $order) !!}"><i class="mdi mdi-menu-swap mdi-24px"></i></a>
                                        </th>
                                        <th>
                                            レッスン名
                                            <a href="{!! url('reports/teacher?year='. $q['year'] .'&month='. $q['month'] .'&odb=lesson&order='. $order) !!}"><i class="mdi mdi-menu-swap mdi-24px"></i></a>
                                        </th>
                                        <th>
                                            進捗
                                            <a href="{!! url('reports/teacher?year='. $q['year'] .'&month='. $q['month'] .'&odb=progress&order='. $order) !!}"><i class="mdi mdi-menu-swap mdi-24px"></i></a>
                                        </th>
                                        <th>
                                            生徒名
                                            <a href="{!! url('reports/teacher?year='. $q['year'] .'&month='. $q['month'] .'&odb=student&order='. $order) !!}"><i class="mdi mdi-menu-swap mdi-24px"></i></a>
                                        </th>
                                        <th>
                                            態度
                                        </th>
                                    </tr>
                                    
                                    <?php 
                                    $i = 0;
                                    ?>
                                    @foreach($reports as $report)
                                    <?php 
                                    $i++;
                                    ?>
                                    <tr>
                                        <td {!! $i == 1 ? 'style="width: 10%;"':'' !!}>{{ $report->full_name }}</td>
                                        <td {!! $i == 1 ? 'style="width: 12%;"':'' !!}>{{ $report->date ? date('l, Y/m/d', strtotime($report->date)):'' }} {{ $report->start_time ? date('h:i A', strtotime($report->start_time)) :'' }}</td>
                                        <td {!! $i == 1 ? 'style="width: 10%;"':'' !!}>{{ $report->course }}</td>
                                        <td {!! $i == 1 ? 'style="width: 10%;"':'' !!}>{{ $report->grade }}</td>
                                        <td {!! $i == 1 ? 'style="width: 10%;"':'' !!}>{{ $report->lesson_name }}</td>
                                        <td {!! $i == 1 ? 'style="width: 10%;"':'' !!}>{{ $report->lesson_day }}回</td>
                                        <td {!! $i == 1 ? 'style="width: 10%;"':'' !!}>{{ $report->romaji_name }}</td>
                                        <td {!! $i == 1 ? 'style="width: 8%;"':'' !!}>{{ $report->teacher }}</td>
                                    </tr>
                                    @endforeach
                                   
                                </tbody>
                            </table>
                            @else 
                                <span><strong>No Record Found</strong></span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            
        </div>

    </section><!--end of section table-->

@endsection

@section('js')
    <script>
        $(document).ready(function(){

            let _csrf = $('meta[name="csrf-token"]').attr('content');

        });
    </script>
@endsection
