@extends('layouts.app')

@section('css')
<style>
    table td,
    table th{
        text-align: center;
    }
    table th{
        white-space: nowrap;
    }
    table th a{
        display: inline-block;
        margin-top: 10px;
    }
    i.mdi-menu-swap{
        line-height: 0;
        float: left;
        margin-bottom: -2px;
    }
</style>
@endsection

@section('content')
    
    <!-- Page Header-->
    <div class="breadcrumb-holder container-fluid">
        <div class="inner">
            <div class="links">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i> {{ __('label.home') }}</a></li>
                    <li class="breadcrumb-item active">{{__('label.report')}}</li>
                    <li class="breadcrumb-item active">{{__('label.student')}}</li>
                </ul>
            </div>
            @if(count($reports) > 0)
            <div class="buttons">
                <a href="{{ url('reports/student/export?'. $parsedUrl) }}" class="btn btn-primary btn-sm">{{__('label.export')}}</a>
            </div>
            @endif
        </div>        
    </div>

    <section class="tables">   
        <div class="container-fluid">
            @if(Session::has('success_message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ Session::get('success_message') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif

            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="h4">{{ __('label.filter_search') }}</h3>
                        </div>

                        <div class="card-body">
                            {!! Form::open(['method' => 'GET']) !!}
                            
                            <div class="form-row">
                                <div class="form-group col col-md-2">
                                    <label for="classroom">{{__('label.classroom')}}</label>
                                    <select id="classroom" name="classroom" class="form-control">
                                        <option value="all">{{__('label.all')}}</option>
                                        @if($classroom)
                                        @foreach($classroom as $room)
                                        <option value="{{ $room->id }}" {{ $room->id == $q['room'] ? 'selected':'' }}>{{ $room->class_name }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                                
                                <div class="form-group col col-md-3">
                                    <label for="student">{{__('label.student')}}</label>
                                    <select id="student" name="student" class="form-control">
                                        <option value="">{{__('label.select_student')}}</option>
                                        @if ($students != null)
                                            @foreach($students as $student)
                                                <option value="{{ $student->id }}" {{ $student->id == $q['student'] ? 'selected':'' }}>{{ $student->kanji_name }}</option>
                                            @endforeach
                                        @endif
                                     
                                    </select>
                                </div>
                                <div class="form-group col col-md-3">
                                    <label for="year">{{__('label.year')}}</label>
                                    <select id="year" name="year" class="form-control">
                                        <option value="">{{ __('label.select_item') }}</option>
                                        @if ($year != null)
                                            @for($y = date('Y'); $y >= date('Y', strtotime($year->date)); $y--)
                                            <option value="{{ $y }}" {{ $y == $q['year'] ? 'selected':'' }}>{{ $y }}</option>
                                            @endfor 
                                        @endif
                                     
                                    </select>
                                </div>
                                <div class="form-group col col-md-3">
                                    <label for="month">{{__('label.month')}}</label>
                                    <select id="month" name="month" class="form-control">
                                        <option value="">{{ __('label.select_item') }}</option>
                                        @for($m = 1; $m <= 12; $m++)
                                        <option value="{{ date('m', mktime(0, 0, 0, $m)) }}" {{ $q['month'] == date('m', mktime(0, 0, 0, $m)) ? 'selected':'' }}>{{ date('F', mktime(0, 0, 0, $m)) }}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="form-group col col-md-1 ">
                                    <label>&nbsp;</label>
                                    <button class="btn btn-primary" style="margin-top: 28px;">{{__('label.search')}}</button>
                                </div>
                            </div>
                            {!! Form::close() !!}

                            <div class="line"></div>
                            <?php 
                                    
                            if(count($reports) > 0):
                               
                            ?>
                            <table class="table-responsive table-bordered">
                                <tbody>
                                    <tr>
                                        <th colspan="5">AIC Online English - {{ $roomName ? $roomName->class_name:__('label.all') }}</th>
                                        <th></th>
                                        <th colspan="7">{{ $q['year'] ? $q['year']:__('label.year') }}　{{ $q['month'] ? date('F', mktime(0, 0, 0, $q['month'])) :__('label.month') }}　{{__('label.report')}}</th>
                                    </tr>
                                    <tr>
                                        <th colspan="2">
                                            氏名
                                            <?php 
                                            $order = $sort['order'] != '' ? ($sort['order'] == 'asc' ? 'desc' : 'asc') : 'asc';
                                            ?>
                                            <a href="{!! url('reports/student?classroom='. $q['room'] .'&year='. $q['year'] .'&month='. $q['month'] .'&odb=name&order='. $order) !!}"><i class="mdi mdi-menu-swap mdi-24px"></i></a>
                                        </th>
                                        <th rowspan="2">
                                            授業レッスン期間
                                            <a href="{!! url('reports/student?classroom='. $q['room'] .'&year='. $q['year'] .'&month='. $q['month'] .'&odb=date&order='. $order) !!}"><i class="mdi mdi-menu-swap mdi-24px"></i></a>
                                        </th>
                                        <th rowspan="2">
                                            コース
                                            <a href="{!! url('reports/student?classroom='. $q['room'] .'&year='. $q['year'] .'&month='. $q['month'] .'&odb=course&order='. $order) !!}"><i class="mdi mdi-menu-swap mdi-24px"></i></a>
                                        </th>
                                        <th rowspan="2">
                                            級
                                            <a href="{!! url('reports/student?classroom='. $q['room'] .'&year='. $q['year'] .'&month='. $q['month'] .'&odb=grade&order='. $order) !!}"><i class="mdi mdi-menu-swap mdi-24px"></i></a>
                                        </th>
                                        <th rowspan="2">
                                            レッスン名
                                            <a href="{!! url('reports/student?classroom='. $q['room'] .'&year='. $q['year'] .'&month='. $q['month'] .'&odb=lesson&order='. $order) !!}"><i class="mdi mdi-menu-swap mdi-24px"></i></a>
                                        </th>
                                        <th rowspan="2">
                                            進捗
                                            <a href="{!! url('reports/student?classroom='. $q['room'] .'&year='. $q['year'] .'&month='. $q['month'] .'&odb=progress&order='. $order) !!}"><i class="mdi mdi-menu-swap mdi-24px"></i></a>
                                        </th>
                                        <th rowspan="2">
                                            教師名
                                            <a href="{!! url('reports/student?classroom='. $q['room'] .'&year='. $q['year'] .'&month='. $q['month'] .'&odb=teacher&order='. $order) !!}"><i class="mdi mdi-menu-swap mdi-24px"></i></a>
                                        </th>
                                        <th colspan="5">評価観点</th>
                                    </tr>
                                    <tr>
                                        <th>漢字</th>
                                        <th>ローマ字</th>
                                        <th>態度</th>
                                        <th>聞いてわかる力</th>
                                        <th>文章を作る力</th>
                                        <th>文法知識</th>
                                        <th>発音・流暢さ</th>
                                    </tr>
                                    <?php
                                        $i = 0;
                                        foreach($reports as $report):
                                        $i++;
                                    ?>
                                    <tr>
                                        <td {!! $i == 1 ? 'style="width: 7.6%; "':'' !!}>{{ $report->kanji_name }}</td>
                                        <td {!! $i == 1 ? 'style="width: 7.6%; "':'' !!}>{{ $report->romaji_name }}</td>
                                        <td {!! $i == 1 ? 'style="width: 7.6%; "':'' !!}>{{ date('l, Y/m/d h:i A', strtotime($report->date .' '. $report->start_time)) }}</td>
                                        <td {!! $i == 1 ? 'style="width: 7.6%; "':'' !!}>{{ $report->label }}</td>
                                        <td {!! $i == 1 ? 'style="width: 7.6%; "':'' !!}>{{ $report->grade }}</td>
                                        <td {!! $i == 1 ? 'style="width: 7.6%; "':'' !!}>{{ $report->lesson_name }}</td>
                                        <td {!! $i == 1 ? 'style="width: 7.6%; "':'' !!}>{{ $report->lesson_day }}回</td>
                                        <td {!! $i == 1 ? 'style="width: 7.6%; "':'' !!}>{{ $report->full_name }}</td>
                                        <td {!! $i == 1 ? 'style="width: 7.6%; "':'' !!}>{{ $report->student }}</td>
                                        <td {!! $i == 1 ? 'style="width: 7.6%; "':'' !!}>{{ $report->listening }}</td>
                                        <td {!! $i == 1 ? 'style="width: 7.6%; "':'' !!}>{{ $report->writing }}</td>
                                        <td {!! $i == 1 ? 'style="width: 7.6%; "':'' !!}>{{ $report->grammar }}</td>
                                        <td {!! $i == 1 ? 'style="width: 7.6%; "':'' !!}>{{ $report->speaking }}</td>
                                    </tr>
                                    <?php
                                        endforeach;
                                    
                                    ?>
                                </tbody>
                            </table>
                            <?php
                            else:
                                echo "<span><strong>No record found</strong></span";
                            endif;
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section><!--end of section table-->

@endsection

@section('js')
    <script>
        $(document).ready(function(){

            let _csrf = $('meta[name="csrf-token"]').attr('content');

        });
    </script>
@endsection
