@extends('layouts.app')

@section('css')

@endsection

@section('content')
    
    <!-- Page Header-->
    <div class="breadcrumb-holder container-fluid">
        <div class="inner">
            <div class="links">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i> {{ __('label.home') }}</a></li>
                    <li class="breadcrumb-item active">Reports</li>
                </ul>
            </div>
            <div class="buttons">
                <a href="#" class="btn btn-primary btn-sm"><i class="mdi mdi-file-excel"></i> Export</a>
            </div>
        </div>        
    </div>

    <section class="tables">   
        <div class="container-fluid">
            @if(Session::has('success_message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ Session::get('success_message') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif

            sdf
        </div>

    </section><!--end of section table-->

@endsection

@section('js')
    <script>
        $(document).ready(function(){

            let _csrf = $('meta[name="csrf-token"]').attr('content');

        });
    </script>
@endsection
