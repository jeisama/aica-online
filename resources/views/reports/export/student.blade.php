<table class="table table-bordered">
    <tbody>
        <tr>
            <th colspan="5">AIC Online English - {{ $roomName ? $roomName->class_name: __('label.all') }}</th>
            <th></th>
            <th colspan="7">{{ $q['year'] ? $q['year']:__('label.year') }}　{{ $q['month'] ? date('F', mktime(0, 0, 0, $q['month'])) :__('label.year') }}　{{__('label.report')}}</th>
        </tr>
        <tr>
            <th colspan="2">氏名</th>
            <th rowspan="2">授業レッスン期間</th>
            <th rowspan="2">コース</th>
            <th rowspan="2">級</th>
            <th rowspan="2">レッスン名</th>
            <th rowspan="2">進捗</th>
            <th rowspan="2">教師名</th>
            <th colspan="5">評価観点</th>
        </tr>
        <tr>
            <th>漢字</th>
            <th>ローマ字</th>
            <th>態度</th>
            <th>聞いてわかる力</th>
            <th>文章を作る力</th>
            <th>文法知識</th>
            <th>発音・流暢さ</th>
        </tr>
        <?php 
        
        if($reports):
            foreach($reports as $report):
        ?>
        <tr>
            <td>{{ $report->kanji_name }}</td>
            <td>{{ $report->romaji_name }}</td>
            <td>{{ date('l, Y/m/d h:i A', strtotime($report->date .' '. $report->start_time)) }}</td>
            <td>{{ $report->label }}</td>
            <td>{{ $report->grade }}</td>
            <td>{{ $report->lesson_name }}</td>
            <td>{{ $report->lesson_day }}回</td>
            <td>{{ $report->full_name }}</td>
            <td>{{ $report->student }}</td>
            <td>{{ $report->listening }}</td>
            <td>{{ $report->writing }}</td>
            <td>{{ $report->grammar }}</td>
            <td>{{ $report->speaking }}</td>
        </tr>
        <?php
            endforeach; 
        endif;
        ?>
    </tbody>
</table>