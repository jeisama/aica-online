<table>
    <tbody>
        <tr>
            <th colspan="4">AIC Online English</th>
            <th></th>
            <th colspan="3">{{ $q['year'] ? $q['year']:__('label.year') }}　{{ $q['month'] ? date('F', mktime(0, 0, 0, $q['month'])) :__('label.year') }}　{{__('label.report')}}</th>
        </tr>
        <tr>
            <th>氏名</th>
            <th>授業レッスン期間</th>
            <th>コース</th>
            <th>級</th>
            <th>レッスン名</th>
            <th>進捗</th>
            <th>生徒名</th>
            <th>態度</th>
        </tr>
        @if($reports)
        <?php 
        $i = 0;
        ?>
        @foreach($reports as $report)
        <?php 
        $i++;
        ?>
        <tr>
            <td>{{ $report->full_name }}</td>
            <td>{{ $report->date ? date('l, Y/m/d', strtotime($report->date)):'' }} {{ $report->start_time ? date('h:i A', strtotime($report->start_time)) :'' }}</td>
            <td>{{ $report->course }}</td>
            <td>{{ $report->grade }}</td>
            <td>{{ $report->lesson_name }}</td>
            <td>{{ $report->lesson_day }}回</td>
            <td>{{ $report->romaji_name }}</td>
            <td>{{ $report->teacher }}</td>
        </tr>
        @endforeach
        @endif
    </tbody>
</table>