@extends('layouts.app')

@section('css')

<link rel="stylesheet" href="{{ asset('assets/vendor/confirm/jquery-confirm.css') }}">

@endsection

@section('content')
    
    <!-- Breadcrumb -->
    <div class="breadcrumb-holder container-fluid">
        <div class="inner">
            <div class="links">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i> {{__('label.home')}}</a></li>
                    <li class="breadcrumb-item active">{{__('label.grade')}}</li>
                </ul>
            </div>
            @if($user_data->role == 1)
            <div class="buttons">
                <a href="{{ url('grades/create') }}" class="btn btn-primary btn-sm"><i class="mdi mdi-plus"></i> {{__('label.create_grade')}}</a>
            </div>
            @endif
        </div>        
    </div><!-- Breadcrumb -->

    <section class="tables">   
        <div class="container-fluid">
            @if(Session::has('success_message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ Session::get('success_message') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif

            
            @if($grades)

            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="h4">{{__('label.grade_list')}}</h3>
                        </div>
                        <div class="card-body nopadding">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>{{__('label.grade')}}</th>
                                            <th width="25%">{{__('label.status')}}</th>
                                            <th width="100">&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($grades as $grade)
                                        <tr id="row-{{ $grade->id }}">
                                            <td>{{ $grade->label }}</td>
                                            <td>{{ $grade->status == 1 ? __('label.active_sts') : __('label.inactive_sts') }}</td>
                                            <td class="text-center action">
                                                <div class="buttons">
                                                    <a href="{{ url('grades/edit/'. $grade->id) }}" title="Edit"><i class="mdi mdi-pen"></i></a>
                                                    <!-- <a href="javascript:void(0);" class="archive" data-id="{{ $grade->id }}" title="Archive"><i class="mdi mdi-close"></i></a> -->
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @else
            <div class="no-record">
                <div class="info"><i class="mdi mdi-information-outline"></i></div>
                <p>No record found for the given selection!</p>
                <div><a href="{{ url('grades/create') }}" class="btn btn-primary btn-sm"><i class="mdi mdi-plus"></i> {{__('label.create_grade')}}</a></div>
            </div>
            @endif
        </div>

    </section><!--end of section table-->

@endsection

@section('js')
    
    <script src="{{ asset('assets/vendor/confirm/jquery-confirm.js') }}"></script>

    <script src="{{ asset('assets/js/app.js') }}"></script>

@endsection
