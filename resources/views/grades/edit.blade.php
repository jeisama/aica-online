@extends('layouts.app')

@section('css')

<link rel="stylesheet" href="{{ asset('assets/vendor/confirm/jquery-confirm.css') }}">

@endsection

@section('content')
<!-- Page Header-->
    <div class="breadcrumb-holder container-fluid">
        <div class="inner">
            <div class="links">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i> {{__('label.home')}}</a></li>
                    <li class="breadcrumb-item"><a href="{{ url('grades') }}">{{__('label.grade')}}</a></li>
                    <li class="breadcrumb-item active">{{__('label.edit')}}</li>
                </ul>
            </div>
        </div>        
    </div>

    <section class="tables">   
        <div class="container-fluid">
            @if(Session::has('success_message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ Session::get('success_message') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif

            <div class="row">
                <div class="col-md-12 col-lg-10">
                    <div class="card">
                        <div class="card-body">
                            <h1 class="card-title">{{__('label.edit_grade')}}</h1>
                            <div class="line"></div>
                            {{ Form::open([ 'id' => 'update-grade', 'onsubmit' => 'event.preventDefault()', 'method' => 'put' ]) }}
                            <input type="hidden" name="id" value="{{ $grade->id }}">
                            <div class="form-row">
                                <div class="form-group col col-md-6 col-lg-6">
                                    <label for="grade_level">{{__('label.grade')}}</label>
                                    <input type="text" id="grade_level" class="form-control" name="grade_level" value="{{ $grade->label }}">
                                    <small id="error-grade_level" class="form-text form-error"></small>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col col-md-6 col-lg-6">
                                    <label for="status">{{__('label.status')}}</label>
                                    <select class="form-control" id="status" name="status">
                                        <option value="" disabled>{{__('label.select_item')}}</option>
                                        <option value="1" {{ $grade->status == 1 ? 'selected' : '' }}>{{__('label.active_sts')}}</option>
                                        <option value="0" {{ $grade->status == 0 ? 'selected' : '' }}>{{__('label.inactive_sts')}}</option>
                                    </select>
                                    <small id="error-status" class="form-text form-error"></small>
                                </div>
                            </div>

                            <div class="line"></div>
                            
                            <div class="form-row">
                                <div class="form-group col col-md-6 col-lg-6">
                                    <button type="submit" id="submit-form" data-form="update-grade" class="btn btn-primary validate_user">{{__('label.update')}}</button>
                                    <a href="{{ url('grades') }}" class="btn btn-warning">{{__('label.cancel')}}</a>
                                    <span class="form-proccessing hidden"><img src="{{ asset('assets/img/Loading/loading.gif') }}"> {{__('label.submit_process')}}</span>
                                </div>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section><!--end of section table-->

@endsection

@section('js')
    
@endsection
