@extends('layouts.app')
@section('css')
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" />
@endsection
<style>
    #drop-zone {
    width: 100%;
    min-height: 150px;
    border: 3px dashed rgba(0, 0, 0, .3);
    border-radius: 5px;
    position: relative;
    font-size: 20px;
    color: #7E7E7E;
    padding-left: 1em;
    }
    #drop-zone input {
    position: absolute;
    cursor: pointer;
    left: 0px;
    top: 0px;
    opacity: 0;
    }
    /*Important*/
    #drop-zone.mouse-over {
    border: 3px dashed rgba(0, 0, 0, .3);
    color: #7E7E7E;
    }
    /*If you dont want the button*/
    #clickHere {
    display: inline-block;
    cursor: pointer;
    color: white;
    font-size: 17px;
    width: 250px;
    border-radius: 4px;
    background-color: #4679BD;
    padding: 10px;
    }
    #clickHere:hover {
    background-color: #376199;
    }
    #filename {
    margin-top: 10px;
    margin-bottom: 10px;
    font-size: 14px;
    line-height: 1.5em;
    }
    .file-preview {
    background: #ccc;
    border: 5px solid #fff;
    box-shadow: 0 0 4px rgba(0, 0, 0, 0.5);
    display: inline-block;
    width: 60px;
    height: 60px;
    text-align: center;
    font-size: 14px;
    margin-top: 5px;
    }
    .closeBtn:hover {
    color: red;
    display:inline-block;
    }
    textarea::placeholder{
        font-size: 24px !important;
        text-align: left;
    }
    .write{
        font-size: 16px !important;
    }

    .write-input{
        text-align: left;
        font-size: 18px !important;
    }
</style>
@section('content')
<!-- Page Header-->
<div class="breadcrumb-holder container-fluid">
    <div class="inner">
        <div class="links">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i> {{__('label.home')}}</a></li>
                <li class="breadcrumb-item"><a href="{{ url('/template') }}">{{__('label.template')}}</a></li>
                <li class="breadcrumb-item active">{{__('label.create_template')}}</li>
            </ul>
        </div>
    </div>
</div>
<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col col-md-12 col-lg-10">
                <div class="card">
                    <div class="card-body">
                        {{ Form::open(['files'=> true, 'id' => 'create-template', 'onsubmit' => 'event.preventDefault()']) }}
                        <div class="form-row">
                            <div class="form-group col col-md-4 col-lg-4">
                                <label for="course_type">{{__('label.course')}}</label>
                                <select id="course_type" class="form-control"  name="course_type">
                                    <option value=" ">{{__('label.select_item')}} </option>
                                    @foreach ($courses as $course)
                                    <option value="{{$course->id}}">{{$course->label}}</option>
                                    @endforeach
                                </select>
                                <small id="error-course_type" class="form-text form-error"></small>
                            </div>
                            <div class="form-group col col-md-4 col-lg-4">
                                <label for="lesson_type">{{__('label.grade')}}</label>
                                <select id="lesson_type" class="form-control"  name="lesson_type" disabled>
                                    <option value="" >{{__('label.select_item')}} </option>
                                </select>
                                <small id="error-lesson_type" class="form-text form-error"></small>
                            </div>
                            <div class="form-group col col-md-4 col-lg-4">
                                <label for="lesson_day">{{__('label.day')}}</label>
                                <select id="lesson_day" class="form-control"  name="lesson_day" disabled>
                                    <option value="" >{{__('label.select_item')}} </option>
                                    <option value="1" >Day 1</option>
                                    <option value="2" >Day 2</option>
                                    <option value="3" >Day 3</option>
                                    <option value="4" >Day 4</option>
                                    <option value="5" >Day 5</option>
                                    <option value="6" >Day 6</option>
                                    <option value="7" >Day 7</option>
                                    <option value="8" >Day 8</option>
                                    <option value="9" >Day 9</option>
                                    <option value="10" >Day 10</option>
                                    <option value="11" >Day 11</option>
                                    <option value="12" >Day 12</option>
                                    <option value="13" >Day 13</option>
                                    <option value="14" >Day 14</option>
                                    <option value="15" >Day 15</option>
                                    <option value="16" >Day 16</option>
                                </select>
                                <small id="error-lesson_day" class="form-text form-error"></small>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col col-md-12 col-lg-12">
                                <label for="lesson_name" class="form-control-label">{{__('label.lesson_name')}}</label>
                                <input type="text" id="lesson_name" name="lesson_name" class="form-control">
                                <small id="error-lesson_name" class="form-text form-error"></small>
                            </div>
                        </div>
                        <div id="form_information">
                        </div>
                        <br>
                        <div class="form-row">
                            <div class="form-group col col-md-6 col-lg-6">
                                <button type="submit" id="create-btn" data-form="create-template" class="btn btn-primary">{{__('label.save')}}</button>
                                <a href="{{ url('template') }}" class="btn btn-warning">{{__('label.cancel')}}</a>
                                <span class="form-proccessing hidden"><img src="{{ asset('assets/img/Loading/loading.gif') }}"> {{__('label.submit_process')}}</span>
                            </div>
                        </div>
                        {{ Form::close() }}  
                    </div>
                    <!--card body end -->    
                </div>
                <!--card end -->
            </div>
        </div>
    </div>
</section>
@endsection
@section('js')
<script>
    $(document).ready(function(){
    
              let _token = $('meta[name="csrf-token"]').attr('content');
    
              let form_data = new FormData($('#create-template')[0]);
              
              $(document).on('change', '#course_type', function(){
    
                  let option = '<option value=""></option>';
    
                  let id = $('option:selected', this).val();
    
                  if(id != ''){
                      $.ajax({
                          type: 'post',
                          dataType: 'json',
                          url: '{{ url('api/schedule/get-grade') }}',
                          data: {
                            locale : '{{Config::get('app.locale')}}',
                              id: id
                          },
                          success: function(response){
                              //console.log(response);
                              $('#lesson_type').prop('disabled', false).html(response.grade);
                              $('#lesson_day').prop('disabled', false).html();
    
                          }
                      });
                  }else{
                      $('#form_information').html('');
                      $('#lesson_type').prop('disabled', true).html(option);
                      $('#lesson_day').prop('disabled', true).html();
                  }
    
              });
    
              /*
              |-----------------------------------------------------------
              | Get Form Input
              |-----------------------------------------------------------
              */
              $(document).on('change', '#lesson_type', function(){
                  
                  let grade_id = $('option:selected', this).val();
                  let course_id = $("#course_type option:selected").val();
                  
                  console.log(course_id);
                  console.log(grade_id);
                  
                  if(course_id != '' || grade_id != ''){
                      $.ajax({
                          type: 'post',
                          dataType: 'json',
                          url: '{{ url('api/template/get_form') }}',
                          cache: false,
                          data: {
                              course_id: course_id,
                              grade_id: grade_id,
                          },
                          beforeSend: function() {
                              //$('#room_role').html('');
                              $('#form_information').html('');
                          },
                          success: function(response){
                              //$('#room_role').html(response.room_role);
                              $('#form_information').html(response.form_information);
                              //initDatePicker();
                          }
                      });
                  }else{
                  // $('#room_role').html('');
                      $('#form_information').html('');
                  }
    
                  
              });
    
    
              /*Upload attachement or files*/
              var fileMedia      = [];
    
              var fileAttachment = [];
    
              $(document).on('change', '.select_file', function(){
                  let target = $(this).attr('id');
    
                      var files = $(this)[0].files;
    
                      var formData = form_data;
                      console.log(formData);
                      for (var i = 0; i < files.length; i++) {
    
                          var file = files[i];
    
                          if(target == 'media'){
                              
                              fileMedia.push(file);
    
                              formData.append('media[]', file, file.name);
    
                          }else{
    
                              fileAttachment.push(file);
    
                              formData.append('attachment[]', file, file.name);
    
                          }
    
                          let lines = `
                              <div id="file_`+ i +`">` + file.name + `&nbsp;&nbsp;<span class="fa fa-times-circle fa-md removeLine" data-index="`+ i +`" data-target="`+ target +`" title="remove"></span></div>`;
                              
                          $('#filelist_'+ target).append(lines);
    
                      }
              });
    
              // Remove attachment
    
              $(document).on('click', '.removeLine', function(){
                      
                      let target = $(this).attr('data-target');
    
                      var index  = $(this).attr('data-index');
    
                      $(this).closest('div').remove(); 
                      
                      if(target == 'media'){
                          
                          form_data.delete('media[]');
    
                          delete fileMedia[index];
                          
                          for(let key in fileMedia){
                              form_data.append('media[]', fileMedia[key], fileMedia[key]['name']);
                          }
                          console.log(fileMedia);
                      }else{
    
                          form_data.delete('attachment[]');
    
                          delete fileAttachment[index];
                          
                          for(let key in fileAttachment){
                              form_data.append('attachment[]', fileAttachment[key], fileAttachment[key]['name']);
                          }
                          console.log(fileAttachment);
                      }
                      
                  });
                  //submit form modified
    
                  $('#create-btn').click(function(){
    
                      let form_data2 = new FormData($('#create-template')[0]);
    
    
                      for (var pair of form_data2.entries()) {
                          form_data.append(pair[0], pair[1]);
                      }
    
                      
                      let form =  $('#create-template');
                      let action = $('#create-template').attr('action');
    
                      $.ajax({
                          url: action, 
                          type: 'POST',
                          dataType: 'json',
                          data: form_data,
                          cache: false,
                          contentType: false,
                          processData: false,
                          beforeSend: function(){
    
                              $('.form-text').text('');
                              $('.form-proccessing').removeClass('hidden');
    
                          },
                          success: function(response){
    
                              window.location.replace(response.redirect);
    
                              $('#'+ form).find('.form-proccessing').addClass('hidden');
    
                          },
                          error: function(json){
    
                              if(json.status === 422){
    
                                  $('.form-proccessing').addClass('hidden');
    
                                  $.each(json.responseJSON.errors, function (key, value) {
                                      $('#error-'+ key).text(value);
                                  });
    
                              }
                          }
                      });
                  });
    
      });
      
      
</script>
@endsection