@extends('layouts.app')
@section('css')

<style>
.box-content{
 border: solid 1px #c5c5cc;
}

.box-content span{
 line-height: 1.5em !important;
}
.black {
    border: 1px solid #000 !important;
}
textarea::placeholder{
        font-size: 24px !important;
        
    }
.write{
    font-size: 16px !important;
}
.modal-content{
    font-family:"ヒラギノ角ゴ Pro W3", "Hiragino Kaku Gothic Pro",Osaka, "メイリオ", Meiryo, "ＭＳ Ｐゴシック", "MS PGothic", sans-serif !important;

}

</style>

@endsection
@section('content')
    
    <!-- Page Header-->
    <div class="breadcrumb-holder container-fluid">
        <div class="inner">
            <div class="links">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i> {{__('label.home')}}</a></li>
                    <li class="breadcrumb-item active">{{__('label.template')}}</li>
                </ul>
            </div>
            <div class="buttons">
                <a href="{{ url('template/create') }}" class="btn btn-primary btn-sm"><i class="mdi mdi-plus"></i> {{__('label.create_lesson_template')}}</a>
            </div> 
        </div>        
    </div>
    <section>
            <div class="container-fluid">
                    @if(Session::has('success_message'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ Session::get('success_message') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                <div class="row">
                    <div class="col col-md-12 col-lg-10">
                        <div class="card">
                            <div class="card-body">
                                {!! Form::open(['url' => 'template', 'method' => 'GET', 'id' => 'search_form']) !!} 
                                <div class="form-row">
                                    
                                    <div class="form-group col col-md-6 col-lg-6">
                                        <label for="course_type">{{__('label.course')}} </label>
                                        <select id="course_type" class="form-control"  name="course_type" value="{{ $qData['course_type'] }}">
                                            <option value="">{{__('label.select_item')}} </option>
                                            @foreach ($courses as $course)
                                                <option value="{{$course->id}}">{{$course->label}}</option>
                                            @endforeach
                                        </select>
                                        
                                    </div>
                                    <div class="form-group col col-md-6 col-lg-6">
                                            <label for="lesson_type">{{__('label.grade')}}</label>
                                            <select id="lesson_type" class="form-control"  name="lesson_type">
                                                    <option value="" >{{__('label.select_item')}} </option>
                                            </select>
                                            
                                        </div>
                                </div>
                                <div class="form-row">
                                        <div class="form-group col col-md-6 col-lg-6">
                                            <button type="submit" id="submit-form" data-form="create-template" class="btn btn-primary">{{__('label.search')}}</button>
                                            
                                        </div>
                                </div>
                                {{ Form::close() }}  
                       
                                <div class="form-row">
                                    <div class="form-group col col-md-12 col-lg-12">
                                            <div class="card">
                                            <div class="card-header">
                                                <h3 class="h4">{{__('label.template_list')}}</h3>
                                            </div>
                                            <div class="card-body nopadding">
                                                <div class="table-responsive">
                                                    <table class="table table-hover">
                                                        
                                                        <thead>
                                                            <tr>
                                                                <th width="10%">#</th>
                                                                <th>{{__('label.course')}}</th>
                                                                <th>{{__('label.grade')}}</th>
                                                                <th>{{__('label.day')}}</th>
                                                                <th>{{__('label.lesson_name')}}</th>
                                                                <th>&nbsp;</th>  
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                           
                                                            @foreach ($templates as $template)
                                                            <tr>
                                                                
                                                                <td>{{$template->id}}</td>
                                                                <td><a href="javascript:void(0);"  data-id="{{ $template->id }}" class="details">{{ $template->course_label }}</a></td>
                                                                <td>{{ $template->grade_label }}</td>
                                                                <td>Day {{ $template->lesson_day }}</td>
                                                                <td>{{ $template->lesson_name }}</td>
                                                                <td>
                                                                    <div class="text-center action">
                                                                        <a href="{{ url('template/edit/'. $template->id) }}" title="Edit"><i class="mdi mdi-pen"></i></a>
                                                                    </div>
                                                                </td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!--card body end -->    
                        </div><!--card end -->
                    </div>
                </div>
            </div>
 <!-- Modal -->
    <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" >
                    <div id="template_modal"></div>
                      
                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('label.back')}}</button>
                
                </div>
                </div>
            </div>
      </div>
    </section> 
@endsection

@section('js')
<script>
        let _token = $('meta[name="csrf-token"]').attr('content');
		$(document).ready(function(){
            $(document).on('change', '#course_type', function(){

            let option = '<option value="">--Select Item--</option>';

            let id = $('option:selected', this).val();

            if(id != ''){
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: '{{ url('api/schedule/get-grade') }}',
                    data: {
                        id: id
                    },
                    success: function(response){
                        $('#lesson_type').prop('disabled', false).html(response.grade);

                    }
                });
            }else{
                $('#lesson_type').prop('disabled', true).html(option);
              
            }

            });
            $('.details').each(function(){
                $(this).click(function(){
                    let id = $(this).attr('data-id');
                    
                        $.ajax({
                            type: 'get',
                            dataType: 'json',
                            url: '{{ url('template/show') }}/'+ id,
                            success:function(response){
                                //console.log(response);
                                showModal(response.data);
                            }
                    });
                    
                });
            });
            function showModal(data){
                console.log(data[0]);
                // Object.keys(data[0]).forEach(function(key) {
                //     $('#val_'+ key).text(data[0][key]);
                //     console.log(key);
                // });
                $('#template_modal').html(data);
                $('#exampleModalLong').modal('show');
            }
            $(document).on('click', '.expander', function(){
                $('#listen-div').is(":visible") ? $('#btn-control').text("開く") : $('#btn-control').text("閉じる");
                $('#listen-div').slideToggle();
               
            });
		});
	</script>
@endsection