@extends('layouts.app')

@section('css')
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" />

@endsection
<style>
    #drop-zone {
        width: 100%;
        min-height: 150px;
        border: 3px dashed rgba(0, 0, 0, .3);
        border-radius: 5px;
        position: relative;
        font-size: 20px;
        color: #7E7E7E;
        padding-left: 1em;
    }
    #drop-zone input {
        position: absolute;
        cursor: pointer;
        left: 0px;
        top: 0px;
        opacity: 0;
    }
    /*Important*/
    
    #drop-zone.mouse-over {
        border: 3px dashed rgba(0, 0, 0, .3);
        color: #7E7E7E;
    }
    /*If you dont want the button*/
    
    #clickHere {
        display: inline-block;
        cursor: pointer;
        color: white;
        font-size: 17px;
        width: 250px;
        border-radius: 4px;
        background-color: #4679BD;
        padding: 10px;
    }
    #clickHere:hover {
        background-color: #376199;
    }
    #filename {
        margin-top: 10px;
        margin-bottom: 10px;
        font-size: 14px;
        line-height: 1.5em;
    }
    .file-preview {
        background: #ccc;
        border: 5px solid #fff;
        box-shadow: 0 0 4px rgba(0, 0, 0, 0.5);
        display: inline-block;
        width: 60px;
        height: 60px;
        text-align: center;
        font-size: 14px;
        margin-top: 5px;
    }
    .closeBtn:hover {
        color: red;
        display:inline-block;
    }
    textarea::placeholder{
        font-size: 24px !important;
        
    }
    .black{
        border: 1px solid #000 !important;
        text-align: left;
        font-size: 23px !important;
    }
    .gray {
        border: 1px solid #c5c5c5 !important;
    }
    .write{
        font-size: 16px !important;
    }
    
</style>

@section('content')
    
    <!-- Page Header-->
    <div class="breadcrumb-holder container-fluid">
        <div class="inner">
            <div class="links">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i> {{__('label.home')}}</a></li>
                    <li class="breadcrumb-item"><a href="{{ url('/template') }}"> {{__('label.template')}}</a></li>
                    <li class="breadcrumb-item active">{{__('label.edit_template')}}</li>
                </ul>
            </div>
        </div>        
    </div>
    
    <section>
        <div class="container-fluid">
               
            <div class="row">
                <div class="col col-md-12 col-lg-10">
                    <div class="card">
                        <div class="card-body">
                            {{ Form::open(['file'=>true, 'id' => 'update-template', 'onsubmit' => 'event.preventDefault()']) }}
                            <div class="form-row">
                               
                                <div class="form-group col col-md-4 col-lg-4">
                                    <label for="course_type">{{__('label.course')}}</label>
                                    <select id="course_type" class="form-control"  name="course_type">
                                        <option value=" ">{{__('label.select_item')}} </option>
                                        
                                        @foreach ($courses as $course)
                                            <option value="{{$course->id}}" {{ $course->id == $templates->course_type ? 'selected': ' ' }}>{{$course->label}}</option>
                                        @endforeach
                                    </select>
                                    <small id="error-course_type" class="form-text form-error"></small>
                                </div>
                                <div class="form-group col col-md-4 col-lg-4">
                                    <label for="lesson_type">{{__('label.grade')}}</label>
                                    <select id="lesson_type" class="form-control"  name="lesson_type">
                                            <option value="" >{{__('label.grade')}} </option>
                                            @foreach ($grades as $grade)
                                                <option value="{{$grade->id}}" {{ $grade->id == $templates->lesson_type ? 'selected': ' ' }}>{{$grade->label}}</option>
                                            @endforeach
                                    </select>
                                    <small id="error-lesson_type" class="form-text form-error"></small>
                                </div>
                                <div class="form-group col col-md-4 col-lg-4">
                                    <label for="lesson_day" class="form-control-label">{{__('label.day')}}</label>
                                    <select id="lesson_day" class="form-control"  name="lesson_day">
                                        <option value="" >{{__('label.select_item')}} </option>
                                        <option value="1" {{ $templates->lesson_day == 1 ? 'selected': ' ' }} >Day 1</option>
                                        <option value="2" {{ $templates->lesson_day == 2 ? 'selected': ' ' }}>Day 2</option>
                                        <option value="3" {{ $templates->lesson_day == 3 ? 'selected': ' ' }}>Day 3</option>
                                        <option value="4" {{ $templates->lesson_day == 4 ? 'selected': ' ' }}>Day 4</option>
                                        <option value="5" {{ $templates->lesson_day == 5 ? 'selected': ' ' }}>Day 5</option>
                                        <option value="6" {{ $templates->lesson_day == 6 ? 'selected': ' ' }}>Day 6</option>
                                        <option value="7" {{ $templates->lesson_day == 7 ? 'selected': ' ' }}>Day 7</option>
                                        <option value="8" {{ $templates->lesson_day == 8 ? 'selected': ' ' }}>Day 8</option>
                                        <option value="9" {{ $templates->lesson_day == 9 ? 'selected': ' ' }}>Day 9</option>
                                        <option value="10" {{ $templates->lesson_day == 10 ? 'selected': ' ' }}>Day 10</option>
                                        <option value="11" {{ $templates->lesson_day == 11 ? 'selected': ' ' }}>Day 11</option>
                                        <option value="12" {{ $templates->lesson_day == 12 ? 'selected': ' ' }}>Day 12</option>
                                        <option value="13" {{ $templates->lesson_day == 13 ? 'selected': ' ' }}>Day 13</option>
                                        <option value="14" {{ $templates->lesson_day == 14 ? 'selected': ' ' }}>Day 14</option>
                                        <option value="15" {{ $templates->lesson_day == 15 ? 'selected': ' ' }}>Day 15</option>
                                        <option value="16" {{ $templates->lesson_day == 16 ? 'selected': ' ' }}>Day 16</option>
                                </select>
                                    <small id="error-lesson_day" class="form-text form-error"></small>
                                        <small id="error-lesson_day" class="form-text form-error"></small>
                                </div>
                             
                            </div>
                            @if ($templates->form_type == '1')
                            <div class="form-row">
                                    <div class="form-group col col-md-12 col-lg-12">
                                            <label for="lesson_name" class="form-control-label">{{__('label.lesson_name')}}</label>
                                    <input type="text" id="lesson_name" name="lesson_name" class="form-control" value="{{$templates->lesson_name}}">
                                            <small id="error-lesson_name" class="form-text form-error"></small>
                                    </div>
                                   
                            </div>
                            <div class="homework-box" style="border: 1px solid #c5c5c5; padding: 15px; width: 100%;">
                                <div class="tbl" style="display: table; width: 100%;">
                                    <div style="display: table-cell; vertical-align: top; width:30px;"></div>
                                        <div style="display: table-cell; vertical-align: top;">
                                            <div class="form-row">
                                                <div class="form-group col col-md-12 col-lg-12">
                                                    <label for="" class="form-control-label"><strong>Speaking part用 課題</strong></label>
                                                    <p>次回の授業で以下のトピックについて先生から問われます。事前にあなたの意見と、そのように
                                                            思う理由を2～3文の英文でまとめましょう。<br>
                                                            どのように英語で表現すれば良いか分からない部分は、辞書などを用いて調べましょう。
                                                            </p>
                                                    
                                                    <label for="speaking_topic" class="form-control-label"><strong>次週のSpeaking Topic:</strong></label>
                                                    <textarea rows="4" cols="50" id="speaking_topic" name="speaking_topic" class="form-control black" placeholder="Speaking Topicを入力してください。" >{{$templates->speaking_topic}}</textarea>
                                                    <small id="error-speaking_topic" class="form-text form-error"></small>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                <div class="tbl" style="display: table; width: 100%;">
                                    <div style="display: table-cell; vertical-align: top; width:30px;"></div>
                                        <div style="display: table-cell; vertical-align: top;">
                                            <div class="form-row">
                                                <div class="form-group col col-md-12 col-lg-12">
                                                    
                                                    <textarea rows="4" cols="50" id="stu_opinion" name="stu_opinion" class="form-control" placeholder="こちらに回答してください。" disabled></textarea>
                                                    <small id="error-writing_student_2_ans" class="form-text form-error"></small>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                                <div class="tbl" style="display: table; width: 100%;">
                                    <div style="display: table-cell; vertical-align: top; width:30px;"></div>
                                        <div style="display: table-cell; vertical-align: top;">
                                            <div class="form-row">
                                                <div class="form-group col col-md-12 col-lg-12">
                                                    <label for="listening" class="form-control-label"><strong>Listening part用 課題</strong></label>
                                                    <p>Step 1. 音声を再生して、流れる会話文の内容を聞き取りましょう。繰り返し聞いても内容を理解<br>
                                                            できないときは、下の“展開”ボタンを押して、スクリプトを確認しながらリスニングを行いましょう。
                                                    </p>
                                                    <div>
                                                    <div class="form-group col-lg-12">
                                                            @if($media)
                                                            @foreach($media as $audio)
                                                
                                                                <div style="display: inline-block; padding-right: 20px; text-align:center;">
                                                                        <audio controls>
                                                                            <source src="{{asset('uploads/video/'. $audio->source_data)}}" type="audio/mp3">
                                                                                Your browser does not support the audio element.
                                                                          </audio><br>
                                                                          <span style="margin: 5px 0 10px 0;">{{ $audio->original_name ?? $audio->source_data }}</span>
                                                                          <a href="javascript:void(0);" class="delete_attachment" data-target="delete-media" data-id="{{ $audio->id }}"><i class="mdi mdi-close"></i></a>
                                                                </div>
                                                                {{-- <div>
                                                                    <a href="{{asset('uploads/video/'. $audio->source_data)}}" target="_blank"> {{ $audio->original_name ?? $audio->source_data }}</a> 
                                                                    <a href="javascript:void(0);" class="delete_attachment" data-target="delete-media" data-id="{{ $audio->id }}"><i class="mdi mdi-close"></i></a>
                                                                </div> --}}
                                                            @endforeach
                                                            @endif
                                                    
                                                    </div>
                                                    
                                                    <div class="form-group col-lg-12">
                                                            <div class="form-row">																
                                                                <div id="drop-zone">
                                                                    <br>
                                                                    <label id="clickHere">音声添付ファイル<i class="fa fa-upload"></i>
                                                                        <input type="file" class="select_file" id="media" multiple accept="audio/*"/>
                                                                    </label>
                                                                    <div id="filelist_media"></div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                    </div>
                                                    
                                                </div>
            
                                            </div>
                                        </div>
                                </div>
                                <div class="tbl" style="display: table; width: 100%;">
                                    <div style="display: table-cell; vertical-align: top; width:30px;"></div>
                                        <div style="display: table-cell; vertical-align: top;">
                                            <div class="form-row">
                                                <div class="form-group col col-md-12 col-lg-12">
                                                    <p>Step 2. スクリプトを見ながら、音声の後に続いて音読しましょう。その際、英文の内容や意味を<br>
                                                            意識しながら音読するようにしましょう。<br>
                                                            わからない単語や表現がある場合は、辞書などを用いて調べましょう。
                                                            </p>
                                                    
                                                  
                                                    <label for="next_listening" class="form-control-label"><strong>今週のListening 課題：</strong></label>
                                                    <textarea rows="4" cols="50" id="next_listening" name="next_listening" class="form-control" placeholder="Listening 課題を入力してください。">{{$templates->next_listening}}</textarea>
                                                    <small id="error-next_listening" class="form-text form-error"></small><br>
                                                        <div class="form-row">
                                                            <div class="form-group col col-md-12 col-lg-12">
                                                                <label for="attachment" class="form-control-label"><strong>レッスン添付ファイル</strong></label><br>
                                                                <div style="border:solid 1px #c5c5c5; padding:10px;">
                                                                    <div class="form-group col-lg-12">
                                                                        @if($attachment)
                                                                        @foreach($attachment as $file)
                                                                            <div>
                                                                                <a href="{{asset('uploads/lessons/'. $file->attachment)}}" target="_blank"> {{ $file->original_name ?? $file->attachment }}</a> 
                                                                                <a href="javascript:void(0);" data-target="delete-attachment" class="delete_attachment" data-id="{{ $file->id }}"><i class="mdi mdi-close"></i></a>
                                                                            </div>
                                                                        @endforeach
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                               
                                                        <div id="drop-zone">
                                                            <br>
                                                            <label id="clickHere">レッスン添付ファイル<i class="fa fa-upload"></i>
                                                                <input type="file" class="select_file" id="attachment" multiple accept="image/*, application/pdf"/>
                                                            </label>
                                                            <div id='filelist_attachment'></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div><!--homework-box-->
                            @endif
                
                            @if ($templates->form_type == 2)
                            <div class="form-row">
                                    <div class="form-group col col-md-12 col-lg-12">
                                            <label for="lesson_name" class="form-control-label">{{__('label.lesson_name')}}</label>
                                    <input type="text" id="lesson_name" name="lesson_name" class="form-control" value="{{$templates->lesson_name}}">
                                            <small id="error-lesson_name" class="form-text form-error"></small>
                                    </div>
                                   
                            </div>
                            <div class="homework-box" style="border: 1px solid #c5c5c5; padding: 15px; width: 100%;">
                                    <div class="tbl" style="display: table; width: 100%;">
                                        <div style="display: table-cell; vertical-align: top; width:30px;"></div>
                                            <div style="display: table-cell; vertical-align: top;">
                                                <div class="form-row">
                                                    <div class="form-group col col-md-12 col-lg-12">
                                                        <label for="" class="form-control-label"><strong>Speaking part用課題</strong></label><br>
                                                        <p>この1週間の出来事について書きましょう。何をやったのかという文章と、更に内容を詳しくする<br>
                                                                文章を1文ずつ書きましょう。作った文章を次回の授業の中で発表しましょう。
                                                                </p>
                                                        <textarea rows="4" cols="50" id="stu_opinion" name="stu_opinion" class="form-control" placeholder="こちらに回答してください。" disabled></textarea>
                                                        <small id="error-stu_opinion" class="form-text form-error"></small>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="tbl" style="display: table; width: 100%;">
                                        <div style="display: table-cell; vertical-align: top; width:30px;"></div>
                                            <div style="display: table-cell; vertical-align: top;">
                                                <div class="form-row">
                                                    <div class="form-group col col-md-12 col-lg-12">
                                                        <label for="listening" class="form-control-label"><strong>Listening part用課題</strong></label>
                                                        <p>Step 1. 音声を再生して、流れる会話文の内容を聞き取りましょう。繰り返し聞いても内容を理解<br>
                                                                できないときは、下の“展開”ボタンを押して、スクリプトを確認しながらリスニングを行いましょう。
                                                                </p>
                                                        <div>
                                                            <div class="form-group col-lg-12">
                                                                    @if($media)
                                                                    @foreach($media as $audio)
                                                                    <div style="display: inline-block; padding-right: 20px; text-align:center;">
                                                                            <audio controls>
                                                                                <source src="{{asset('uploads/video/'. $audio->source_data)}}" type="audio/mp3">
                                                                                    Your browser does not support the audio element.
                                                                              </audio><br>
                                                                              <span style="margin: 5px 0 10px 0;">{{ $audio->original_name ?? $audio->source_data }}</span>
                                                                              <a href="javascript:void(0);" class="delete_attachment" data-target="delete-media" data-id="{{ $audio->id }}"><i class="mdi mdi-close"></i></a>
                                                                    </div>
                                                                    @endforeach
                                                                    @endif
                                                            
                                                            </div>
                                                            <div class="form-row">																
                                                            <div id="drop-zone">
                                                                    <br>
                                                                    <label id="clickHere">音声添付ファイル<i class="fa fa-upload"></i>
                                                                        <input type="file" class="select_file" id="media" multiple accept="audio/*"/>
                                                                    </label>
                                                                    <div id="filelist_media"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                
                                                </div>
                                            </div>
                                    </div>
                                    <div class="tbl" style="display: table; width: 100%;">
                                        <div style="display: table-cell; vertical-align: top; width:30px;"></div>
                                            <div style="display: table-cell; vertical-align: top;">
                                                <div class="form-row">
                                                    <div class="form-group col col-md-12 col-lg-12">
                                                        <p>Step 2. スクリプトを見ながら、音声の後に続いて音読しましょう。その際、英文の内容や意味を<br>
                                                                意識しながら音読するようにしましょう。<br>
                                                                わからない単語や表現がある場合は、辞書などを用いて調べましょう。
                                                                </p>
                                                        
                                                       
                                                        <label for="next_listening" class="form-control-label"><strong>今週のListening 課題：</strong></label>
                                                        <textarea rows="4" cols="50" id="next_listening" name="next_listening" class="form-control" placeholder="Listening 課題を入力してください。">{{ $templates->next_listening }}</textarea>
                                                        <small id="error-next_listening" class="form-text form-error"></small>
                                            
                                                    </div>
                                             
                                                </div>
                                                
                                                <div class="form-row">
                                                        <div class="form-group col col-md-12 col-lg-12">
                                                            <label for="attachment" class="form-control-label"><strong>レッスン添付ファイル</strong></label><br>
                                                            <div style="border:solid 1px #c5c5c5; padding:10px;">
                                                                <div class="form-group col-lg-12">
                                                                    @if($attachment)
                                                                    @foreach($attachment as $file)
                                                                        <div>
                                                                            <a href="{{asset('uploads/lessons/'. $file->attachment)}}" target="_blank"> {{ $file->original_name ?? $file->attachment }}</a> 
                                                                            <a href="javascript:void(0);" data-target="delete-attachment" class="delete_attachment" data-id="{{ $file->id }}"><i class="mdi mdi-close"></i></a>
                                                                        </div>
                                                                    @endforeach
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <div class="form-row">
                                                           
                                                    <div id="drop-zone">
                                                        <br>
                                                        <label id="clickHere">レッスン添付ファイル<i class="fa fa-upload"></i>
                                                            <input type="file" class="select_file" id="attachment" multiple accept="image/*, application/pdf"/>
                                                        </label>
                                                        <div id='filelist_attachment'></div>
                                                    </div>
                                                </div>    
                                        </div>
                                    </div>
                                    
                                </div>
                            @endif
                            @if ($templates->form_type == 3)
                            <div class="form-row">
                                    <div class="form-group col col-md-12 col-lg-12">
                                            <label for="lesson_name" class="form-control-label">{{__('label.lesson_name')}}</label>
                                    <input type="text" id="lesson_name" name="lesson_name" class="form-control" value="{{$templates->lesson_name}}">
                                            <small id="error-lesson_name" class="form-text form-error"></small>
                                    </div>
                                   
                            </div>
                            <div class="homework-box" style="border: 1px solid #c5c5c5; padding: 15px; width: 100%;">
                                    <div class="tbl" style="display: table; width: 100%;">
                                        <div style="display: table-cell; vertical-align: top; width:30px;"><strong></strong></div>
                                            <div style="display: table-cell; vertical-align: top;">
                                                <div class="form-row">
                                                    <div class="form-group col col-md-12 col-lg-12">
                                                        <label for="" class="form-control-label write">今回の課題（以下の課題に対して「記入欄」に英作文を記入しましょう）</label>
                                                    <textarea rows="4" cols="50" id="writing_topic" name="writing_topic" class="form-control black " placeholder="トピックを入力してください。" >{{$templates->writing_topic}}</textarea>
                                                        <small id="error-writing_topic" class="form-text form-error"></small>
                                                        <br>
                                                        <textarea rows="6" cols="50" id="chat_history" name="chat_history" class="form-control" placeholder="チャット履歴" disabled></textarea>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                               
                                    <div class="tbl" style="display: table; width: 100%;">
                                        <div style="display: table-cell; vertical-align: top; width:30px;"></div>
                                            <div style="display: table-cell; vertical-align: top;">
                                                <div class="form-row">
                                                    <div class="form-group col col-md-12 col-lg-12">
                                                        <label for="writing_stu_ans" class="form-control-label write">記入欄</label>
                                                        <textarea rows="6" cols="50" id="writing_stu_ans" name="writing_stu_ans" class="form-control"  placeholder="こちらに回答してください。" disabled></textarea>
                                            
                                                    </div>
                                             
                                                </div>
                                        </div>
                                    </div>
                                    
                                    <div class="tbl" style="display: table; width: 100%;">
                                    <div style="display: table-cell; vertical-align: top; width:30px;"></div>
                                        <div style="display: table-cell; vertical-align: top;">
                                            <div class="form-row">
                                                <div class="form-group col col-md-12 col-lg-12">
                                                    <label for="writing_tea_res" class="form-control-label write">添削結果</label>
                                                    <textarea rows="6" cols="50" id="writing_tea_res" name="writing_tea_res" class="form-control" disabled></textarea>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col col-md-12 col-lg-12">
                                                    <label for="attachment" class="form-control-label write">レッスン添付ファイル</label><br>
                                                    <div style="border:solid 1px #c5c5c5; padding:10px;">
                                                        <div class="form-group col-lg-12">
                                                            @if($attachment)
                                                            @foreach($attachment as $file)
                                                                <div>
                                                                    <a href="{{asset('uploads/lessons/'. $file->attachment)}}" target="_blank"> {{ $file->original_name ?? $file->attachment }}</a> 
                                                                    <a href="javascript:void(0);" data-target="delete-attachment" class="delete_attachment" data-id="{{ $file->id }}"><i class="mdi mdi-close"></i></a>
                                                                </div>
                                                            @endforeach
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div id="drop-zone">
                                                    <br>
                                                    <label id="clickHere">レッスン添付ファイル<i class="fa fa-upload"></i>
                                                        <input type="file" class="select_file" id="attachment" multiple accept="image/*, application/pdf"/>
                                                    </label>
                                                    <div id='filelist_attachment'></div>
                                                </div>
                                            </div>
                                    </div>
                                    
                                </div>
                            @endif<br>
                            <div class="form-row">
                                <div class="form-group col col-md-6 col-lg-6">
                                    <button type="submit" id="update-btn" data-form="update-template" class="btn btn-primary">{{__('label.update')}}</button>
                                    <a href="{{ url('template') }}" class="btn btn-warning">{{__('label.cancel')}}</a>
                                    <span class="form-proccessing hidden"><img src="{{ asset('assets/img/Loading/loading.gif') }}"> {{__('label.submit_process')}}</span>
                                </div>
                            </div>
                        {{ Form::close() }}            
                            
                        </div><!--card body end -->    
                    </div><!--card end -->
               
            </div>
        </div>
    </section>
@endsection
@section('js')
<script>
        let _token = $('meta[name="csrf-token"]').attr('content');
        let form_data =  new FormData($('#update-template')[0]);
        console.log(form_data);
		$(document).ready(function(){

            $('.delete_attachment').each(function(){
				$(this).click(function(){
					let id = $(this).attr('data-id');
					let target = $(this).attr('data-target');
					let ans= confirm('Are you sure you want to delete this item?');
					if(ans){
						$.ajax({
							type: 'post',
							url: '{{ url('template') }}/' + target,
							data:{
								id: id,
								_token: _token
							},success:function(data){
								//$('#parent-'+ id).remove();
								window.location = window.location;
							}
						});
					}
				});
			});


            $(document).on('change', '#course_type', function(){

            let option = '<option value="">--Select Item--</option>';

            let id = $('option:selected', this).val();

            if(id != ''){
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: '{{ url('api/schedule/get-grade') }}',
                    data: {
                        id: id
                    },
                    success: function(response){
                        $('#lesson_type').prop('disabled', false).html(response.grade);

                    }
                });
            }else{
                $('#lesson_type').prop('disabled', true).html(option);
              
            }

            });

            		/*Upload attachement or files*/
			//let form_data =  new FormData($('#update-template')[0]);

            var fileMedia      = [];

            var fileAttachment = [];

            $('.select_file').each(function(){

                $(this).change(function(){
                    
                    let target = $(this).attr('id');

                    var files = $(this)[0].files;

                    var formData = form_data;
                    
                    for (var i = 0; i < files.length; i++) {

                        var file = files[i];

                        if(target == 'media'){
                            
                            fileMedia.push(file);

                            formData.append('media[]', file, file.name);

                        }else{

                            fileAttachment.push(file);

                            formData.append('attachment[]', file, file.name);

                        }

                        let lines = `
                            <div id="file_`+ i +`">` + file.name + `&nbsp;&nbsp;<span class="fa fa-times-circle fa-md removeLine" data-index="`+ i +`" data-target="`+ target +`" title="remove"></span></div>`;
                            
                        $('#filelist_'+ target).append(lines);

                    }
                });
            });


            // Remove attachment

            $(document).on('click', '.removeLine', function(){
                
                let target = $(this).attr('data-target');

                var index  = $(this).attr('data-index');

                $(this).closest('div').remove(); 
                
                if(target == 'media'){
                    
                    form_data.delete('media[]');

                    delete fileMedia[index];
                    
                    for(let key in fileMedia){
                        form_data.append('media[]', fileMedia[key], fileMedia[key]['name']);
                    }
                    console.log(fileMedia);
                }else{

                    form_data.delete('attachment[]');

                    delete fileAttachment[index];
                    
                    for(let key in fileAttachment){
                        form_data.append('attachment[]', fileAttachment[key], fileAttachment[key]['name']);
                    }
                    console.log(fileAttachment);
                }
                
            });
            //submit form modified
            //$(document).on('click', '#update-btn', function(){
            $('#update-btn').click(function(){	
                let form_data2 = new FormData($('#update-template')[0]);
    
    
                for (var pair of form_data2.entries()) {
                    form_data.append(pair[0], pair[1]);
                }


                let action = $('#lesson-form').attr('action');

                $.ajax({
                    url: action, 
                    type: 'POST',
                    dataType: 'json',
                    data: form_data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function(){

                        $('.form-text').text('');
                        $('.form-proccessing').removeClass('hidden');

                    },
                    success: function(response){

                        window.location.replace(response.redirect);

                        $('#'+ form).find('.form-proccessing').addClass('hidden');

                    },
                    error: function(json){

                        if(json.status === 422){

                            $('.form-proccessing').addClass('hidden');

                            $.each(json.responseJSON.errors, function (key, value) {
                                $('#error-'+ key).text(value);
                            });

                        }
                    }
                });
            });
		});
	</script>
@endsection