<div class="form-row">
    <div class="form-group col col-md-6 col-lg-6">
        <label for="kanji_name" class="jp">氏名</label>
        <input type="text" id="kanji_name" class="form-control" name="kanji_name" value="{{ $user->kanji_name }}">
        <small id="error_kanji_name" class="form-text form-error"></small>
    </div>
    <div class="form-group col col-md-6 col-lg-6">
        <label for="romaji_name" class="jp">ローマ字表記　フルネーム</label>
        <input type="text" id="romaji_name" class="form-control" name="romaji_name" value="{{ $user->romaji_name }}">
        <small id="error_romaji_name" class="form-text form-error"></small>
    </div>
    
</div>
<div class="form-row">
    <div class="form-group col col-md-2 col-lg-2">
        <label for="birth_date" class="jp">誕生日（年/月/日）</label>
        <select id="birth_date" class="form-control" name="b_year">
            @for($y= date('Y'); $y >= 1971; $y-- )
            <option value="{{ $y }}" {{ date('Y', strtotime($user->birth_date)) == $y ? 'selected':'' }}>{{ $y }}</option>
            @endfor
        </select>
        <small id="error_birth_date" class="form-text form-error"></small>
    </div>
    <div class="form-group col col-md-2 col-lg-2">
        <label>&nbsp;</label>
        <select class="form-control" name="b_month">
            @foreach($temp_month as $key => $value)
                 <option value="{{ $key }}" {{ date('m', strtotime($user->birth_date)) == $key ? 'selected':'' }}>{{ $value }}</option>
            @endforeach
            
        </select>
    </div>

    <div class="form-group col col-md-2 col-lg-2">
        <label>&nbsp;</label>
        <select class="form-control" name="b_day">
            @for($d= 1; $d <= 31; $d++ )
            <option value="{{ $d }}" {{ date('d', strtotime($user->birth_date)) == $d ? 'selected':'' }}>{{ $d }}</option>
            @endfor
        </select>
    </div>
    <div class="form-group col col-md-6 col-lg-6">
        <label for="prefecture" class="jp">お住まい（○○県）</label>
        <select id="prefecture" class="form-control" name="prefecture">
            <option value="">---お選びください---</option>
            @if($prefectures)
            @foreach($prefectures as $prefecture)
            <option value="{{ $prefecture->District }}" {{ $user->prefecture == $prefecture->District ? 'selected':'' }}>{{ $prefecture->District }}</option>
            @endforeach
            @endif
        </select>
        <small id="error_prefecture" class="form-text form-error"></small>
    </div>
</div>
<div class="form-row">
    <div class="form-group col col-md-6 col-lg-6">
        <label for="cram_school" class="jp">教室名</label>
        <input type="text" id="cram_school" class="form-control" name="cram_school" value="{{ $user->cram_school }}">
        <small id="error_cram_school" class="form-text form-error"></small>
    </div>
    <div class="form-group col col-md-6 col-lg-6">
        <label for="english_learning" class="jp">英語学習暦</label>
        <select name="english_learning" id="english_learning" class="form-control">
            <option value="">---お選びください---</option>
            <option value="なし" {{ $user->english_learning == 'なし' ? 'selected':'' }}>なし</option>
            <option value="1年未満" {{ $user->english_learning == '1年未満' ? 'selected':'' }}>1年未満</option>
            <option value="１～２年" {{ $user->english_learning == '１～２年' ? 'selected':'' }}>１～２年</option>
            <option value="２～３年" {{ $user->english_learning == '２～３年' ? 'selected':'' }}>２～３年</option>
            <option value="３～４年" {{ $user->english_learning == '３～４年' ? 'selected':'' }}>３～４年</option>
            <option value="４～５年" {{ $user->english_learning == '４～５年' ? 'selected':'' }}>４～５年</option>
            <option value="５年以上" {{ $user->english_learning == '５年以上' ? 'selected':'' }}>５年以上</option>
        </select>
        <small id="error_english_learning" class="form-text form-error"></small>
    </div>
</div>
<div class="form-row">
    <div class="form-group col col-md-6 col-lg-6">
        <label for="english_schedule" class="jp">入会時の英語学習状況</label>
        <input type="text" id="english_schedule" class="form-control" name="english_schedule" value="{{ $user->englist_schedule }}" placeholder="学校○○時間／週、塾○○時間／週、その他○○時間／週">
        <small id="error_english_schedule" class="form-text form-error"></small>
    </div>
    <div class="form-group col col-md-6 col-lg-6">
        <label for="account_prof" class="jp">取得済みの語学力検定・資格など</label>
        <input type="text" id="account_prof" class="form-control" name="account_prof" value="{{ $user->account_prof }}" placeholder="試験名：○○、級／スコア：○○、取得年月日○○">
        <small id="error_account_prof" class="form-text form-error"></small>
    </div>
</div>
<div class="form-row">
    <div class="form-group col col-md-12 col-lg-12">
        <label for="remark" class="jp">特記事項</label>
        <textarea id="remark" class="form-control" name="remark" cols="8">{{ $user->remark }}</textarea>
        <small id="error_remark" class="form-text form-error"></small>
    </div>
</div>
<div class="form-row">
    <div class="form-group col col-md-6 col-lg-6">
        <label for="email">Eメール</label>
        <input type="email" id="email" class="form-control" name="email" value="{{ $user->email }}">
        <small id="error_email" class="form-text form-error"></small>
    </div>
    <div class="form-group col col-md-6 col-lg-6">
        <label for="confirm_email">Eメール確認</label>
        <input type="email" id="confirm_email" class="form-control" name="confirm_email">
        <small id="error_confirm_email" class="form-text form-error"></small>
    </div>
</div>

<div class="line"></div>
<input type="hidden" name="role" value="student">
<input type="hidden" name="target" value="detail">
<button type="button" data-form="user_detail" class="btn btn-primary validate_user">保存</button>
<a href="{{ url('user') }}" class="btn btn-warning">戻る</a>