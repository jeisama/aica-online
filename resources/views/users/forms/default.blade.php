<div class="form-row">
    <div class="form-group col col-md-6 col-lg-6">
        <div class="form-preview">
            <label for="avatar" id="avatar_id">
                @if($user->avatar == null || !File::exists('uploads/avatar/'. $user->avatar))
                    <div class="icon">
                        <i class="mdi mdi-plus"></i>
                        <span>{{ __('label.select_avatar') }}</span>
                    </div>
                @else
                    <img src="{{ asset('uploads/avatar/'. $user->avatar) }}">
                @endif
            </label>
            <input type="file" id="avatar" name="avatar" accept="image/*">
        </div>
        <small id="error_avatar" class="form-text form-error"></small>
    </div>
</div>
<div class="form-row">
    <div class="form-group col col-md-6 col-lg-6">
        <label for="full_name">{{ __('label.full_name') }}</label>
        <input type="text" id="full_name" class="form-control" name="full_name" value="{{ $user->full_name }}">
        <small id="error_full_name" class="form-text form-error"></small>
    </div>
    <div class="form-group col col-md-6 col-lg-6">
        <label for="gender">{{ __('label.gender') }}</label>
        <select id="gender" class="form-control" name="gender">
            <option value="">{{ __('label.select_gender') }}</option>
            <option value="Male" {{ $user->gender == 'Male' ? 'selected':'' }} >{{ __('label.male') }}</option>
            <option value="Female" {{ $user->gender == 'Female' ? 'selected':'' }} >{{ __('label.female') }}</option>
        </select>
        <small id="error_gender" class="form-text form-error"></small>
    </div>
</div>
<div class="form-row">
    <div class="form-group col col-md-6 col-lg-6">
        <label for="email">{{ __('label.email') }}</label>
        <input type="email" id="email" class="form-control" name="email" value="{{ $user->email }}">
        <small id="error_email" class="form-text form-error"></small>
    </div>
    <div class="form-group col col-md-6 col-lg-6">
        <label for="confirm_email">{{ __('label.confirm_email') }}</label>
        <input type="email" id="confirm_email" class="form-control" name="confirm_email" value="">
        <small id="error_confirm_email" class="form-text form-error"></small>
    </div>
</div>

@if($user->role == 5)
<div class="form-row">
    <div class="form-group col col-md-6 col-lg-6">
        <label for="level">{{ __('label.level') }}</label>
        <select name="level" id="level" class="form-control">
            @if($levels)
            @foreach($levels as $level)
            <option value="{{ $level->id }}" {{ $user->teacher_level == $level->id ? 'selected':'' }}>{{ $level->level }}</option>
            @endforeach
            @endif
        </select>
        <small id="error_level" class="form-text form-error"></small>
    </div>
</div>
@endif

<div class="line"></div>
<input type="hidden" name="role" value="default">
<input type="hidden" name="target" value="detail">
<button type="button" data-form="user_detail" class="btn btn-primary validate_user">{{ __('label.save') }}</button>
<a href="{{ url('user') }}" class="btn btn-warning">{{ __('label.back') }}</a>