<table class="table table-bordered">
    <tbody>
        <tr>
            <th>{{ __('label.name') }}</th>
            <th width="20%">{{ __('label.username') }}</th>
            <th width="20%">{{ __('label.email') }}</th>
            <th width="10%">{{ __('label.role') }}</th>
            <th width="10%">{{ __('label.status') }}</th>
            <th width="100">&nbsp;</th>
        </tr>
        @foreach($users as $user)
        <tr>
            <td class="tdNoPadding">
                <div class="image_name">
                    <div class="avatar get_user_detail" data-username="{{ $user->username }}" data-toggle="modal" data-target="#user_details">
                        @if($user->avatar == null || !File::exists('uploads/avatar/'. $user->avatar))
                            <img src="{{ asset('uploads/avatar/noavatar.jpg') }}">
                        @else
                            <img src="{{ asset('uploads/avatar/'. $user->avatar) }}">
                        @endif
                    </div>
                    <div class="name">
                        <span class="get_user_detail" data-username="{{ $user->username }}" data-toggle="modal" data-target="#user_details">{{ $user->full_name ?? $user->kanji_name }}</span>
                    </div>
                </div>
            </td>
            <td>{{ $user->username }}</td>
            <td>{{ $user->email }}</td>
            <td>
                @if($user->role == 1)
                 {{ __('label.super_admin') }}
                @elseif($user->role == 2)
                {{ __('label.class_admin') }}
                @elseif($user->role == 3)
                {{ __('label.aica_admin') }}
                @elseif($user->role == 4)
                {{ __('label.student') }}
                @elseif($user->role == 5)
                {{ __('label.teacher') }}
                @else
                    I.T.
                @endif
            </td>
            <td>
                @if($user->status == 1)
                {{ __('label.active_sts') }}
                @elseif($user->status == 2)
                {{ __('label.inactive_sts') }}
                @else
                    I.T.
                @endif
            </td>
            <td class="text-center action">
                <div class="buttons">
                    <a href="{{ url('user/edit/'. $user->username) }}" title="Edit"><i class="mdi mdi-pen"></i></a>
                    <a href="javascript:void(0);" class="deleteUser " data-id="{{ $user->id }}" title="Archive"><i class="mdi mdi-close"></i></a>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>