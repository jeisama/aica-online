@extends('layouts.app')

@section('css')

<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/jquery-ui/jquery-ui.css') }}">

@endsection

@section('content')

    <div class="breadcrumb-holder container-fluid">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i> {{__('label.home')}}</a></li>
            <li class="breadcrumb-item"><a href="{{ url('user') }}">{{__('label.users')}}</a></li>
            <li class="breadcrumb-item active">{{__('label.edit_user')}}</li>
        </ul>
    </div>

    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="col col-md-12 col-lg-10">
                    @if(Session::has('success_message'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        New user has been successfully created.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif


                    <div class="card">
                        <div class="card-body">
                            @if ($user->role==4)
                            <h1 class="card-title">詳細</h1>
                            @else
                            <h1 class="card-title">{{ __('label.user_detail') }}</h1>
                            @endif
                           
                            <div class="line"></div>
                            
                            {{ Form::open(['files' => true, 'id' => 'user_detail']) }}
                            <input type="hidden" name="username_data" value="{{ $user->username }}">
                            @if($user->role == 4)
                                @include('users.forms.student')
                            @else
                                @include('users.forms.default')
                            @endif
                            
                            {{ Form::close() }}

                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            @if ($user->role == 4)
                            <h1 class="card-title">ログイン詳細</h1> 
                            @else
                            <h1 class="card-title">{{ __('label.login_detail') }}</h1>    
                            @endif
                                
                            
                            <div class="line"></div>

                            {{ Form::open(['id' => 'login_detail']) }}
                            @if ($user->role == 4)
                            <div class="form-row">
                                <div class="form-group col col-md-6 col-lg-6">
                                    <label for="username">ユーザーID</label>
                                    <input type="text" id="username" class="form-control" name="username" value="{{ $user->username }}">
                                    <small id="error_username" class="form-text form-error"></small>
                                </div>
                                
                            </div>
                            <div class="form-row">
                                <div class="form-group col col-md-6 col-lg-6">
                                    <label for="password">パスワード</label>
                                    <input type="password" id="password" class="form-control" name="password">
                                    <small id="error_password" class="form-text form-error"></small>
                                </div>
                                <div class="form-group col col-md-6 col-lg-6">
                                    <span class="viewPassword" data-target="password"><i class="mdi mdi-eye"></i></span>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col col-md-6 col-lg-6">
                                    <label for="status">状態</label>
                                    <input type="hidden" name="status_preview" id="status_preview" value="{{ $user->status == 1 ? '有効':'無効' }}">
                                    <select id="status" class="form-control get_preview" data-target="status" name="status">
                                        <option value="">Select Status</option>
                                        <option value="1" {{ $user->status == 1 ? 'selected':'' }} data-value="Active">有効</option>
                                        <option value="2" {{ $user->status == 2 ? 'selected':'' }} data-value="Inactive">無効</option>
                                    </select>
                                    <small id="error_status" class="form-text form-error"></small>
                                </div>
                            </div>                                 
                            @else
                            <div class="form-row">
                                <div class="form-group col col-md-6 col-lg-6">
                                    <label for="username">{{ __('label.username') }}</label>
                                    <input type="text" id="username" class="form-control" name="username" value="{{ $user->username }}">
                                    <small id="error_username" class="form-text form-error"></small>
                                </div>
                                
                            </div>
                            <div class="form-row">
                                <div class="form-group col col-md-6 col-lg-6">
                                    <label for="password">{{ __('label.password') }}</label>
                                    <input type="password" id="password" class="form-control" name="password">
                                    <small id="error_password" class="form-text form-error"></small>
                                </div>
                                <div class="form-group col col-md-6 col-lg-6">
                                    <span class="viewPassword" data-target="password"><i class="mdi mdi-eye"></i></span>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col col-md-6 col-lg-6">
                                    <label for="status">{{ __('label.status') }}</label>
                                    <input type="hidden" name="status_preview" id="status_preview" value="{{ $user->status == 1 ? __('label.active_sts') : __('label.inactive_sts') }}">
                                    <select id="status" class="form-control get_preview" data-target="status" name="status">
                                        <option value="">Select Status</option>
                                        <option value="1" {{ $user->status == 1 ? 'selected':'' }} data-value="Active">Active</option>
                                        <option value="2" {{ $user->status == 2 ? 'selected':'' }} data-value="Inactive">Inactive</option>
                                    </select>
                                    <small id="error_status" class="form-text form-error"></small>
                                </div>
                            </div>  
                            @endif
                  

                            <div class="line"></div>
                            <input type="hidden" name="username_data" value="{{ $user->username }}">
                            <input type="hidden" name="target" value="login">
                            <button type="button" data-form="login_detail" class="btn btn-primary validate_user">{{ __('label.save') }}</button>
                            <a href="{{ url('user') }}" class="btn btn-default">{{ __('label.back') }}</a>

                            {{ Form::close() }}
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade confirm_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">User Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="form_confirm">
                    <div class="modal_loading">
                        <img src="{{ asset('assets/img/Loading/loading.gif') }}">
                        <div class="status">{{__('label.submit_process')}}</div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('label.back') }}</button>
                    <button type="button" class="btn btn-primary" id="save_and_confirm">{{ __('label.save') }}</button>
                </div>
            </div>
        </div>
    </div><!-- Modal -->

@endsection

@section('js')

    <script src="{{ asset('assets/vendor/jquery-ui/jquery-ui.js') }}"></script>

    <script>
    $(document).ready(function(){
        
        let _csrf = $('meta[name="csrf-token"]').attr('content');

        $(document).on('click', '.viewPassword', function(){
            
            let target = $(this).attr('data-target');

            if(password == true){

                $('#'+ target).attr('type', 'text');

                password = false;
                
                $(this).html('<i class="mdi mdi-eye-off"></i>');

            }else{

                $('#'+ target).attr('type', 'password');

                password = true;

                $(this).html('<i class="mdi mdi-eye"></i>');

            }
            
        });
        /*
        |-----------------------------------------------------------
        | Init
        | 1. Date Picker
        |-----------------------------------------------------------
        */
        initDatePicker();

        /*
        |-----------------------------------------------------------
        | Avatar OnChange
        |-----------------------------------------------------------
        */
        $(document).on('change', '#avatar', function(){
            avatarPreview(this);
        });

        /*
        |-----------------------------------------------------------
        | OnChange
        |-----------------------------------------------------------
        */
        $(document).on('change', '.get_preview', function(){
            
            let target = $(this).attr('data-target');

            let data = $('option:selected', this).attr('data-value');

            $('#'+ target +'_preview').val(data);

        });

        $(document).on('click', '#save_and_confirm', function(){
            
            let target_form = $('#target_form').val();
            let proccessing = `
                    <div class="modal_loading">
                        <img src="{{ asset('assets/img/Loading/loading.gif') }}">
                        <div class="status">{{__('label.submit_process')}}</div>
                    </div>`;

            if(target_form){
                
                var formData = new FormData($('#'+ target_form)[0]);

                $.ajax({
                    url: '{{ url('user/update') }}', 
                    type: 'POST',
                    dataType: 'json',
                    data: formData,
                    async: false,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function(){
                        $('.modal-header, .modal-footer').addClass('hidden');
                        $('#form_confirm').html(proccessing);
                    },
                    success: function (response) {
                        if(response.message == 200){
                            window.location = '{{ url('user') }}';
                        }else{
                            $('.modal-header, .modal-footer').removeClass('hidden');
                            $('#form_confirm').html('Opps! Something went wrong, please try again.');
                        }
                    }
                });
            }
        });


        $('.validate_user').each(function(){
            
            $(this).each(function(){

                $(this).click(function(){
                    let form_id = $(this).attr('data-form');
                    
                    var formData = new FormData($('#'+ form_id)[0]);

                    $.ajax({
                        url: '{{ url('user/validate-update') }}', 
                        type: 'POST',
                        dataType: 'json',
                        data: formData,
                        async: false,
                        cache: false,
                        contentType: false,
                        processData: false,
                        beforeSend: function(){
                            $('.modal-header, .modal-footer').removeClass('hidden');
                            $('.form-text').html('');
                        },
                        success: function (response) {
                            
                            if(response.response == 417){
                                let errors = response.data;

                                Object.keys(errors).forEach(function(key) {
                                    $('#error_'+ key).text(errors[key][0]);
                                });

                            }else if(response.response == 200){
                                
                                $('.form-text').html('');

                                $('#form_confirm').html(response.data);

                                $('.confirm_modal').modal('show');
                            }
                        }
                    });
                });
                
            });

        });

        /*
        |-----------------------------------------------------------
        | Function Collections
        |-----------------------------------------------------------
        */

        function initDatePicker(){
            $(".datepicker").datepicker({
                dateFormat: "yy/mm/dd"
            });
        }


        function avatarPreview(image){
            if (image.files && image.files[0]){

                let reader = new FileReader();

                reader.onload = function(e) {
                    let data = `<img src="`+ e.target.result +`">`;
                    $('#avatar_id').html(data);
                }

                reader.readAsDataURL(image.files[0]);
            }else{
                let data = `<div class="icon">
                            <i class="mdi mdi-plus"></i>
                            <span>Select Avatar</span>
                        </div>`;
                $('#avatar_id').html(data);
            }
        }
    });
    </script>

@endsection
