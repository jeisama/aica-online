@extends('layouts.app')

@section('css')

<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/jquery-ui/jquery-ui.css') }}">

@endsection

@section('content')
    <div class="breadcrumb-holder container-fluid">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i> {{ __('label.home') }}</a></li>
            <li class="breadcrumb-item"><a href="{{ url('user') }}">{{ __('label.users') }}</a></li>
            <li class="breadcrumb-item active">{{ __('label.create_user') }}</li>
        </ul>
    </div>

    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="col col-md-12 col-lg-10">
                    @if(Session::has('success_message'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        New user has been successfully created.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif

                     <div class="card">
                        <div class="card-body">
                            <h1 class="card-title">{{ __('label.create_user') }}</h1>
                            <div class="line"></div>
                            {{ Form::open(['files' => true, 'id' => 'user_registration']) }}
                                @php
                                $has_franchise = UserHelper::has_franchise(Auth::id());
                                @endphp
                                <div class="form-row">
                                    <div class="form-group col col-md-6 col-lg-6">
                                        <label for="user_role">{{ __('label.role') }}</label>
                                        <input type="hidden" name="user_role_preview" id="user_role_preview">
                                        <select id="user_role" class="form-control get_preview" data-target="user_role" name="user_role">
                                            <option value="" >{{ __('label.select_item') }}</option>
                                            
                                            @if($user_data->role == 1)
                                            <option value="1" data-value="Super Admin">{{ __('label.super_admin') }}</option>
                                            @endif

                                            @if(($user_data->role == 1) || ($user_data->role == 2) || ($user_data->role == 3 && $has_franchise > 0) )
                                            <option value="2" data-value="Classroom Admin">{{ __('label.class_admin') }}</option>
                                            @endif
                                            
                                            @if($user_data->role == 1 || $user_data->role == 3)
                                            <option value="3" data-value="AICA Admin">{{ __('label.aica_admin') }}</option>
                                            @endif

                                            @if(($user_data->role == 1) || ($user_data->role == 2 && $has_franchise > 0) || ($user_data->role == 3 && $has_franchise > 0))
                                            <option value="4" data-value="Student">{{ __('label.student') }}</option>
                                            @endif
                                            
                                            @if($user_data->role == 1 || $user_data->role == 3)
                                            <option value="5" data-value="Teacher">{{ __('label.teacher') }}</option>
                                            @endif

                                        </select>
                                        <small id="error_user_role" class="form-text form-error"></small>
                                    </div>
                                    <div class="form-group col col-md-6 col-lg-6" id="room_role">
                                    </div>
                                </div>

                                <div id="form_information">
                                    
                                </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade confirm_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ __('label.user_detail') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="form_confirm">
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('label.back') }}</button>
                    <button type="button" class="btn btn-primary" id="save_and_confirm">{{ __('label.save') }}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade schedule_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="alert_modal">
                        <i class="mdi mdi-information-outline"></i>
                        <p>Student has been created. Do you want to create schedule now?</p>
                        <div id="modal_link"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
@endsection

@section('js')

<script src="{{ asset('assets/vendor/jquery-ui/jquery-ui.js') }}"></script>

<script>
    $(document).ready(function(){
        /*
        |-----------------------------------------------------------
        | Init
        | 1. Date Picker
        |-----------------------------------------------------------
        */
        initDatePicker();


        let password = true;

        $(document).on('click', '.viewPassword', function(){
            
            let target = $(this).attr('data-target');

            if(password == true){

                $('#'+ target).attr('type', 'text');

                password = false;
                
                $(this).html('<i class="mdi mdi-eye-off"></i>');

            }else{

                $('#'+ target).attr('type', 'password');

                password = true;

                $(this).html('<i class="mdi mdi-eye"></i>');

            }
            
        });

        /*
        |-----------------------------------------------------------
        | Avatar OnChange
        |-----------------------------------------------------------
        */
        $(document).on('change', '#avatar', function(){
            avatarPreview(this);
        });

        /*
        |-----------------------------------------------------------
        | Get Form Input
        |-----------------------------------------------------------
        */
        $(document).on('change', '#user_role', function(){
            
            let role = $('option:selected', this).val();
            let _token = $('meta[name="csrf-token"]').attr('content');
            
            if(role != ''){
                $.ajax({
                 
                    type: 'post',
                    dataType: 'json',
                    url: '{{ url('user/get_form') }}',
                    cache: false,
                    data: {
                        role: role,
                        user_id: '{{ Auth::id() }}',
                        _token : _token
                        
                    },
                    beforeSend: function() {
                        $('#room_role').html('');
                        $('#form_information').html('');
                    },
                    success: function(response){
                        $('#room_role').html(response.room_role);
                        $('#form_information').html(response.form_information);
                        initDatePicker();
                    }
                });
            }else{
                $('#room_role').html('');
                $('#form_information').html('');
            }
        });


        /*
        |-----------------------------------------------------------
        | Validate Form
        |-----------------------------------------------------------
        */
        $(document).on('click', '#validate_user', function(){

            var formData = new FormData($('#user_registration')[0]);
            
            let user_role = $('#user_role').val();

            $.ajax({
                url: '{{ url('user/validate_user') }}', 
                type: 'POST',
                dataType: 'json',
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function(){
                    $('.form-text').html('');
                },
                success: function (response) {
                    if(response.response == 417){
                        let errors = response.data;

                        Object.keys(errors).forEach(function(key) {
                            $('#error_'+ key).text(errors[key][0]);
                        });

                    }else if(response.response == 200){
                        
                        $('#form_confirm').html(response.data);

                        $('.confirm_modal').modal('show');
                    }
                }
            });

        });

        /*
        |-----------------------------------------------------------
        | Save And Confirm
        |-----------------------------------------------------------
        */
        $(document).on('click', '#save_and_confirm', function(){

            var formData = new FormData($('#user_registration')[0]);
            
            let user_role = $('#user_role').val();

            $.ajax({
                url: '{{ url('user/store_user') }}', 
                type: 'POST',
                dataType: 'json',
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function(response){
                    window.location = '{{ url('user') }}';
                    // if(user_role == '4'){
                    //     // let user_id = response.id;
                    //     //  let link = `
                    //     //     <a href="{{ url('schedule/create/`+ user_id +`') }}" class="btn btn-primary">Yes</a>
                    //     //     <a href="{{ url('user') }}" class="btn btn-primary">Later</a>`;
                    //     // $('#modal_link').html(link);
                    //     // $('.confirm_modal').modal('hide');
                    //     // $('.schedule_modal').modal('show');
                    //     //window.location = '{{ url('user') }}';
                    // }else{
                    //     window.location = '{{ url('user') }}';
                    // }
                    
                }
            });
        });

        /*
        |-----------------------------------------------------------
        | OnChange
        |-----------------------------------------------------------
        */
        $(document).on('change', '.get_preview', function(){
            
            let target = $(this).attr('data-target');

            let data = $('option:selected', this).attr('data-value');

            $('#'+ target +'_preview').val(data);

        });

        /*
        |-----------------------------------------------------------
        | Function Collections
        |-----------------------------------------------------------
        */

        function initDatePicker(){
            $(".datepicker").datepicker({
                dateFormat: "yy/mm/dd"
            });
        }


        function avatarPreview(image){
            if (image.files && image.files[0]){

                let reader = new FileReader();

                reader.onload = function(e) {
                    let data = `<img src="`+ e.target.result +`">`;
                    $('#avatar_id').html(data);
                }

                reader.readAsDataURL(image.files[0]);
            }else{
                let data = `<div class="icon">
                            <i class="mdi mdi-plus"></i>
                            <span>{{ __('label.select_avatar') }}</span>
                        </div>`;
                $('#avatar_id').html(data);
            }
        }
    });
</script>
@endsection
