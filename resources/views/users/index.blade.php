@extends('layouts.app')

@section('css')

<link rel="stylesheet" href="{{ asset('assets/vendor/confirm/jquery-confirm.css') }}">

@endsection

@section('content')
    
    <!-- Page Header-->
    <div class="breadcrumb-holder container-fluid">
        <div class="inner">
            <div class="links">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i> {{ __('label.home') }}</a></li>
                    <li class="breadcrumb-item active">{{ __('label.users') }}</li>
                </ul>
            </div>
            @if($user_data->role != 5 || $user_data != 4)
            <div class="buttons">
                <a href="{{ url('user/create') }}" class="btn btn-primary btn-sm"><i class="mdi mdi-plus"></i> {{ __('label.create') }}</a>
            </div>
            @endif
        </div>        
    </div>

    <section class="tables">   
        <div class="container-fluid">
            @if(Session::has('success_message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ Session::get('success_message') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif

            
            @if($users)
            
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="h4">{{ __('label.filter_search') }}</h3>
                        </div>

                        <div class="card-body">
                            {!! Form::open(['url' => 'user', 'method' => 'GET', 'id' => 'search_form']) !!}
                            <div class="form-row">
                                <div class="form-group col col-md-6">
                                    <label for="name">{{ __('label.full_name') }}</label>
                                    <input type="text" class="form-control" name="name" value="{{ $qData['name'] }}">
                                </div>
                                <div class="form-group col col-md-3">
                                    <label for="role">{{ __('label.role') }}</label>
                                    
                                    <select name="role" class="form-control" >
                                        <option value="">{{ __('label.all_user') }}</option>
                                        @if($user_data->role == 1)
                                        <option value="1" {{ $qData['role'] == 1 ? 'selected':'' }}>{{ __('label.super_admin') }}</option>
                                        @endif

                                        @if(($user_data->role == 1) || ($user_data->role == 2) || ($user_data->role == 3 && $hasFranchise > 0) )
                                        <option value="2" {{ $qData['role'] == 2 ? 'selected':'' }}>{{ __('label.class_admin') }}</option>
                                        @endif

                                        @if($user_data->role == 1 || $user_data->role == 3)
                                        <option value="3" {{ $qData['role'] == 3 ? 'selected':'' }}>{{ __('label.aica_admin') }}</option>
                                        @endif

                                        @if(($user_data->role == 1) || ($user_data->role == 2 && $hasFranchise > 0) || ($user_data->role == 3 && $hasFranchise > 0))
                                        <option value="4" {{ $qData['role'] == 4 ? 'selected':'' }}>{{ __('label.student') }}</option>
                                        @endif

                                        @if($user_data->role == 1 || $user_data->role == 3)
                                        <option value="5" {{ $qData['role'] == 5 ? 'selected':'' }}>{{ __('label.teacher') }}</option>
                                        @endif
                                    </select>
                                </div>

                                <div class="form-group col col-md-3">
                                    @if (Auth::user()->role == '1')
                                        <div class="custom-control custom-checkbox" style="border:#000 solid 0; padding-top: 34px;">
                                            <input type="checkbox" class="custom-control-input" name="inactive" id="inactive" {{ $qData['inactive'] ? 'checked':'' }}>
                                            <label class="custom-control-label" for="inactive">{{ __('label.inactive_include') }}</label>
                                        </div>  
                                    @endif
                                  
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" id="submit_button" class="btn btn-primary">{{ __('label.search') }} </button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
              

            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col col-sm-6"><h3 class="h4" >{{ __('label.users') }}</h3></div>
                              
                                <div class="col col-sm-6 text-right"><a href="{{ url('user/export/?name='. $qData['name'] .'&role='. $qData['role'] .'&inactive='. $qData['inactive']) }}" target="_blank" class="btn btn-primary btn-sm">Download</a></div>
                            </div>
                            
                            
                        </div>
                        <div class="card-body nopadding">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>{{ __('label.name') }}</th>
                                            <th width="20%">{{ __('label.username') }}</th>
                                            <th width="20%">{{ __('label.email') }}</th>
                                            <th width="10%">{{ __('label.role') }}</th>
                                            <th width="10%">{{ __('label.status') }}</th>
                                            <th width="100">&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($users as $user)
                                        <tr>
                                            <td class="tdNoPadding">
                                                <div class="image_name">
                                                    <div class="avatar get_user_detail" data-username="{{ $user->username }}" data-toggle="modal" data-target="#user_details">
                                                        @if($user->avatar == null || !File::exists('uploads/avatar/'. $user->avatar))
                                                            <img src="{{ asset('uploads/avatar/noavatar.jpg') }}">
                                                        @else
                                                            <img src="{{ asset('uploads/avatar/'. $user->avatar) }}">
                                                        @endif
                                                    </div>
                                                    <div class="name">
                                                        <span class="get_user_detail" data-username="{{ $user->username }}" data-toggle="modal" data-target="#user_details">{{ $user->full_name ?? $user->kanji_name }}</span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>{{ $user->username }}</td>
                                            <td>{{ $user->email }}</td>
                                            
                                            <td>
                                                @if($user->role == 1)
                                                {{ __('label.super_admin') }}
                                                @elseif($user->role == 2)
                                                {{ __('label.class_admin') }}
                                                @elseif($user->role == 3)
                                                {{ __('label.aica_admin') }}
                                                @elseif($user->role == 4)
                                                {{ __('label.student') }}
                                                @elseif($user->role == 5)
                                                {{ __('label.teacher') }}
                                                @else
                                                    I.T.
                                                @endif
                                            </td>
                                            <td>
                                                @if($user->status == 1)
                                                {{ __('label.active_sts') }}
                                                @elseif($user->status == 2)
                                                {{ __('label.inactive_sts') }}
                                                @else
                                                    I.T.
                                                @endif
                                            </td>
                                            <td class="text-center action">
                                                <div class="buttons">
                                                    <a href="{{ url('user/edit/'. $user->username) }}" title="Edit"><i class="mdi mdi-pen"></i></a>
                                                    @if ($user->status == 1)
                                                        <a href="javascript:void(0);" class="deleteUser " data-id="{{ $user->id }}" title="Archive"><i class="mdi mdi-close"></i></a> 
                                                    @endif
                                                    
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{ $users->links() }}
                </div>
            </div>
            @else
            <div class="no-record">
                <div class="info"><i class="mdi mdi-information-outline"></i></div>
                <p>No record found for the given selection!</p>
                <div><a href="{{ url('user/create') }}" class="btn btn-primary btn-sm"><i class="mdi mdi-plus"></i> Create</a></div>
            </div>
            @endif
        </div>
        
    </section><!--end of section table-->

    <div class="modal fade" id="user_details" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    
                    {{-- <div class="user-detail">
                        <div class="u-group">
                            <div class="a-container">
                                <div class="a-image">
                                    <img src="{{ asset('uploads/avatar/1542776683.8751.png') }}">
                                </div>
                            </div>
                        </div>
                        <div class="u-group">
                            <div class="u-label">Full Name</div>
                            <div class="u-value">Julius Faigmani</div>
                        </div>
                        <div class="u-group">
                            <div class="u-label">Gender</div>
                            <div class="u-value">Male</div>
                        </div>
                        <div class="u-group last">
                            <div class="u-label">Email</div>
                            <div class="u-value">jeisama12@gmail.com</div>
                        </div>

                        <div class="u-line">
                            <span>Login Detail</span>
                        </div>

                        <div class="u-group">
                            <div class="u-label">Username</div>
                            <div class="u-value">Julius Faigmani</div>
                        </div>

                        <div class="u-group">
                            <div class="u-label">Password</div>
                            <div class="u-value">Julius Faigmani</div>
                        </div>

                        <div class="u-group last">
                            <div class="u-label">Status</div>
                            <div class="u-value">Julius Faigmani</div>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    
    <script src="{{ asset('assets/vendor/confirm/jquery-confirm.js') }}"></script>

    <script>
        $(document).ready(function(){

            let _csrf = $('meta[name="csrf-token"]').attr('content');

            $('.get_user_detail').each(function(){
                $(this).click(function(){

                    let username = $(this).attr('data-username');
                    let loading  = `<div class="modal_loading">
                            <img src="{{ asset('assets/img/Loading/loading.gif') }}">
                            <div class="status">{{__('label.submit_process')}}</div>
                        </div>`;

                    $.ajax({
                        type: 'post',
                        dataType: 'json',
                        url: '{{ url('user/get-user-detail') }}',
                        data: {
                            _token : _csrf,
                            username: username
                        },
                        beforeSend: function() {
                            $('.modal-header').addClass('hidden');
                            $('.modal-body').html(loading);
                        },
                        success: function(response){
                            setTimeout(function() {
                                $('.modal-header').removeClass('hidden');
                                $('.modal-title').text(response.title);
                                $('.modal-body').html(response.content);
                            }, 500);
                        }
                    });

                });
            });

            $('.deleteUser').each(function(){
                $(this).click(function(){
                    
                    let id = $(this).attr('data-id');

                    $.confirm({
                        title: 'Confirm!',
                        content: 'Are you sure you want to make this user inactive?',
                        buttons: {
                            confirm: function () {
                                //Ajax Statement Here
                                var data = {
                                    _token: _csrf,
                                    id: id
                                }
                                archive_user(data);
                            },
                            cancel: function() {
                                
                            }
                        }
                    });
                });
            });


            /*
            |-----------------------------------------------------
            | Function Collections
            |-----------------------------------------------------
            */

            function archive_user(data){
                //console.log(data);

                $.ajax({
                    url: '{{ url('user/archive-user') }}',
                    type: 'POST',
                    dataType: 'json',                    
                    data: data,
                    success:function(response){
                        
                        window.location = window.location;

                    }
                });

            }
        });
    </script>
@endsection
