@extends('layouts.app')

@section('css')

<link rel="stylesheet" href="{{ asset('assets/vendor/confirm/jquery-confirm.css') }}">

@endsection

@section('content')
<!-- Page Header-->
    <div class="breadcrumb-holder container-fluid">
        <div class="inner">
            <div class="links">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i> {{__('label.home')}}</a></li>
                    <li class="breadcrumb-item active">{{__('label.teacher_level')}}</li>
                </ul>
            </div>
            @if($user_data->role == 1)
            <div class="buttons">
                <a href="{{ url('level/create') }}" class="btn btn-primary btn-sm"><i class="mdi mdi-plus"></i> {{__('label.create_tea_level')}}</a>
            </div>
            @endif
        </div>        
    </div>

    <section class="tables">   
        <div class="container-fluid">

            @if(Session::has('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ Session::get('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif


            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="h4">{{__('label.teacher_level_list')}}</h3>
                        </div>
                        <div class="card-body nopadding">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>{{__('label.level')}}</th>
                                            <th width="20%" class="text-center">{{__('label.set_level_tea')}}</th>
                                            <td width="150">&nbsp;</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if($levels)
                                        @foreach($levels as $level)
                                        <tr>
                                            <td>{{ $level->level }}</td>
                                            <td class="text-center">
                                                <button type="button" class="btn btn-primary btn-sm update_level" data-toggle="modal" data-target="#teacher_level" data-level="{{ $level->id }}"><i class="mdi mdi-format-list-checks"></i> {{__('label.settings')}}</button>
                                            </td>
                                            <td class="text-center action">
                                                <div class="buttons">
                                                    <a href="{{ url('level/edit/'. $level->id) }}" title="Edit"><i class="mdi mdi-pen"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @else
                                        <tr>
                                            <td colspan="3"><p>No record found!</p></td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section><!--end of section table-->

    <div class="modal fade show" id="teacher_level" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                
                <div class="modal-header">
                    <h5 class="modal-title">{{__('label.set_level_tea')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {{ Form::open(['id' => 'teacher-level', 'onsubmit' => 'event.preventDefault()']) }}
                <div class="modal-body">
                    
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection

@section('js')
    
    <script src="{{ asset('assets/vendor/confirm/jquery-confirm.js') }}"></script>

    <script>
        $(function(){
            
            $(document).on('click', '.update_level', function(){
                
                let level = $(this).attr('data-level');

                let modalData = `
                    <div class="modal_loading">
                        <img src="http://aic.net/assets/img/Loading/loading.gif">
                        <div class="status">{{__('label.submit_process')}}</div>
                    </div>`;

                $('.modal-body').html(modalData);

                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: '{{ url('api/level/get') }}',
                    data: {
                        locale : '{{Config::get('app.locale')}}',
                        level:level
                    },
                    success:function(response){
                        $('.modal-body').html(response.data);
                    }
                });
            });

        });
    </script>
@endsection
