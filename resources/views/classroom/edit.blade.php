@extends('layouts.app')


@section('css')
<style>
.req{
    font-size: 11px;
    vertical-align: middle;
    display: inline-block;
    color: #fff;
    background-color: #c00;
    height: 16px;
    line-height: 16px;
    padding: 0 4px;
    margin-top: 0px;
    font-weight: normal;
}
.label-cla {
    margin-top: 10px;
}
body {
    font-family: 'Noto Sans JP', sans-serif;
}
.m-th {
    width: 33%;
}

table  {
    font-family: 'Noto Sans JP', sans-serif;
}
  </style>   
@endsection

@section('content')

    <!-- Breadcrumb -->
    <div class="breadcrumb-holder container-fluid">
        <div class="inner">
            <div class="links">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i> {{__('label.home')}}</a></li>
                    <li class="breadcrumb-item"><a href="{{ url('classroom') }}">{{__('label.classroom')}}</a></li>
                    <li class="breadcrumb-item active">{{__('label.edit')}}</li>
                </ul>
            </div>
        </div>        
    </div><!-- Breadcrumb -->
         
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            @if (Session::has('message'))
                <div class="alert alert-info">{{ Session::get('message') }}</div>                      
            @endif
            
            <div class="card">
                
                <div class="card-header d-flex align-items-center">
                    <h3 class="h4">{{__('label.classroom_edit')}}</h3>
                </div>

                <div class="card-body">
                {!! Form::open(['id' => 'çlass_room']) !!} 
                    <input type="hidden" name="id" value="{{$results->id}}"/>
                    <div class="form-row">
                            <div class="form-group col">
                                <div class="col-sm-3">
                                    <span class="label-cla">会社名</span>
                                    <span class="req">必須</span>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="class_orgname" id="class_orgname" value="{{$results->class_orgname}}">
                                    <div class="invalid-feedback" id="error_class_orgname"></div>
                                </div>
                            </div>
                        </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <div class="col-sm-3">
                                <span class="label-cla">教室名</span>
                                <span class="req">必須</span>
                            </div>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="class_name" id="class_name" value="{{$results->class_name}}">
                                <div class="invalid-feedback" id="error_class_name"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <div class="col-sm-3">
                                <span class="label-cla">連絡先氏名</span>
                                <span class="req">必須</span>
                            </div>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" name="class_fullname" id="class_fullname" value="{{$results->class_fullname}}">
                                <div class="invalid-feedback" id="error_class_fullname"></div>
                            </div>
                        </div>
                    </div>
              
                    <div class="form-row">
                        <div class="form-group col">
                            <div class="col-sm-3">
                                <span class="label-cla">電話番号</span>
                                <span class="req">必須</span>
                            </div>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="class_phone" id="class_phone" value="{{$results->class_phone}}">
                                <div class="invalid-feedback" id="error_class_phone"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <div class="col-sm-3">
                                <span class="label-cla">携帯電話番号</span>
                               
                            </div>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="class_mobile" value="{{$results->class_mobile}}" id="class_mobile">
                                <div class="invalid-feedback" id="error_class_mobile"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <div class="col-sm-3">
                                <span class="label-cla">連絡先メールアドレス</span>
                                <span class="req">必須</span>
                            </div>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="class_email" id="class_email" value="{{$results->class_email}}">
                                <div class="invalid-feedback" id="error_class_email"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <div class="col-sm-4">
                                <span class="label-cla">連絡先メールアドレス（確認）</span>
                                <span class="req">必須</span>
                            </div>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="class_confirm_email" id="class_confirm_email" value="{{$results->class_confirm_email}}">
                                <div class="invalid-feedback" id="error_class_confirm_email"></div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="line"></div>

                    <div class="form-row">
                        <div class="form-group col">
                            <div class="col-md-12 col-sm-12">
                                <button type="button" class="btn btn-primary mr-2" id="classBtn">{{__('label.confirm')}}</button>
                                <button type="reset" class="btn btn-warning mr-2" onclick="window.history.go(-1); return false;">{{__('label.cancel')}}</button>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
                </div>    
            </div>
        </div>
    </section>   
            
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">{{__('label.classroom_detail')}}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <table class="table">
                <tbody>
                    <tr>
                        <th scope="row" class="m-th">会社名 :</th>
                        <td><span id="val_class_orgname"></td>
                    </tr>
                  <tr>
                    <th scope="row" class="m-th">教室名 :</th>
                    <td><span id="val_class_name"></span></td>
                  </tr>
                  <tr>
                    <th scope="row" class="m-th">連絡先氏名 :</th>
                    <td><span id="val_class_fullname"></span></td>
                  </tr>
                  
                  <tr>
                    <th scope="row" class="m-th">電話番号 :</th>
                    <td><span id="val_class_phone"></span></td>
                  </tr>
                  <tr>
                    <th scope="row" class="m-th">携帯電話番号 :</th>
                    <td><span id="val_class_mobile"></span></td>
                  </tr>
                  <tr>
                    <th scope="row" class="m-th">連絡先メールアドレス :</th>
                    <td><span id="val_class_email"></span></td>
                  </tr>
                  <tr>
                    <th scope="row" class="m-th">連絡先メールアドレス（確認）:</th>
                    <td><span id="val_class_confirm_email"></td>
                  </tr>

                </tbody>
              </table>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('label.back')}}</button>
            <button type="button" class="btn btn-primary" id="save_changes">{{__('label.save')}}</button>
        </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
let _token = $('meta[name="csrf-token"]').attr('content');
$('#classBtn').click(function(){
    let formData = $('#çlass_room').serialize();
    //console.log(formData);
   $.ajax({
      type: 'post',
      dataType: 'json',
      url: '{{ url('/classroom/validate') }}',
      data: formData,
      success:function(response){
        
        $('.invalid-feedback').html('');
        //$('.form-control').removeClass('is-invalid');
        if(response.message === 'error'){
            let errors = response.data;
            Object.keys(errors).forEach(function(key) {
                //$('#'+ key).addClass('is-invalid');
                $('#error_'+ key).text(errors[key][0]);
            });
        }else{
            showModal(response.data);
        }
      }
    });

    
});
        
function showModal(data){
    Object.keys(data).forEach(function(key) {
        $('#val_'+ key).text(data[key]);
    });
    $('#exampleModal').modal('show');
}
        
$('#save_changes').click(function(){
    $('#çlass_room').submit();
});
</script>

@endsection