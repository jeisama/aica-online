@extends('layouts.app')

@section('css')

<style>
.m-th {
    width: 33%;
}
</style>

@endsection


@section('content')

    <!-- Breadcrumb -->
    <div class="breadcrumb-holder container-fluid">
        <div class="inner">
            <div class="links">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i> {{__('label.home')}}</a></li>
                    <li class="breadcrumb-item active">{{__('label.classroom')}}</li>
                </ul>
            </div>
            
            <div class="buttons">
                <a href="{{ url('classroom/create') }}" class="btn btn-primary btn-sm"><i class="mdi mdi-plus"></i> {{__('label.create')}}</a>
            </div>

        </div>        
    </div><!-- Breadcrumb -->
           
    
   
    
    <section class="tables">   
        <div class="container-fluid">
            @if (Session::has('message'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ Session::get('message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif

            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <h1 class="card-title">{{__('label.filter_search')}}</h1>
                            {{ Form::open(['url' => 'classroom', 'method' => 'GET', 'id' => 'search_form']) }}
                            <div class="form-row">
                                 <div class="form-group col col-md-4 col-lg-5">
                                    <label for="organization">会社名</label>
                                    <input type="text" id="organization" class="form-control" name="class_orgname" value="">
                                </div>
                                <div class="form-group col col-md-4 col-lg-5">
                                    <label for="class">教室名</label>
                                    <input type="text" id="class" class="form-control" name="class" value="">
                                </div>
                               
                            </div>

                            <div class="line"></div>
                            
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">{{__('label.search')}} </button>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>

            @if($results)

            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="h4">{{__('label.classroom')}}</h3>
                        </div>
                        <div class="card-body nopadding">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th width="30%">会社名 </th>
                                            <th>教室名</th>
                                            <th width="30%">連絡先氏名</th>
                                            <th width="150" class="text-center">{{__('label.set_admin')}}</th>
                                            <th width="100">&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($results as $res)
                                        <tr>
                                            <td>{{ $res->class_orgname }}</td>
                                            <td><a href="javascript:void(0);"  data-id="{{ $res->id }}" class="details">{{ $res->class_name }}</a></td>
                                            <td>{{ $res->class_fullname }}</td>
                                            
                                            <td class="text-center">
                                                <button class="btn btn-primary btn-sm classroom_admin" data-id="{{ $res->id }}" data-title="{{ $res->class_name }}" data-toggle="modal" data-target="#class_admin"><i class="mdi mdi-format-list-checks"></i> {{__('label.settings')}}</button>
                                            </td>
                                            <td class="text-center action">
                                                <div class="buttons">
                                                    <a href="{{ url('classroom/edit/'. $res->id) }}" title="Edit"><i class="mdi mdi-pen"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @else

            <div class="no-record">
                <div class="info"><i class="mdi mdi-information-outline"></i></div>
                <p>No record found for the given selection!</p>
                <div><a href="{{ url('classroom/create') }}" class="btn btn-primary btn-sm"><i class="mdi mdi-plus"></i> {{__('label.create')}}</a></div>
            </div>
            
            @endif
        </div>
    </section>
            
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('label.classroom_detail')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <table class="table">
                        <tbody>
                            <tr>
                                <th scope="row" class="m-th">会社名 :</th>
                                <td><span id="val_class_orgname"></td>
                            </tr>
                            <tr>
                                <th scope="row" class="m-th">教室名 :</th>
                                <td><span id="val_class_name"></span></td>
                            </tr>
                            <tr>
                                <th scope="row" class="m-th">連絡先氏名 :</th>
                                <td><span id="val_class_fullname"></span></td>
                            </tr>
                            
                            <tr>
                                <th scope="row" class="m-th">電話番号 :</th>
                                <td><span id="val_class_phone"></span></td>
                            </tr>
                            <tr>
                                <th scope="row" class="m-th">携帯電話番号 :</th>
                                <td><span id="val_class_mobile"></span></td>
                            </tr>
                            <tr>
                                <th scope="row" class="m-th">連絡先メールアドレス :</th>
                                <td><span id="val_class_email"></span></td>
                            </tr>
                            <tr>
                                <th scope="row" class="m-th">連絡先メールアドレス（確認）:</th>
                                <td><span id="val_class_confirm_email"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('label.back')}}</button>
                </div>
            </div>
        </div>
    </div><!-- Modal -->

    <!-- Modal -->
    <div class="modal fade" id="class_admin" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {{ Form::open(['url' => 'classroom/admin', 'id' => 'update-class-admin', 'onsubmit' => 'event.preventDefault()']) }}
                <div class="modal-body">

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
                    <button type="submit" class="btn btn-primary" id="submit-form" data-form="update-class-admin">Save</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div><!-- Modal -->

@endsection
@section('js')
<script>

    $( function() {
        let _csrf = $('meta[name="csrf-token"]').attr('content');
        
        $('.trash').each(function(){
            $(this).click(function(){
                let id = $(this).attr('data-id');
                let ans = confirm('Are you `sure you want to delete this item?');
                if(ans){
                    $.ajax({
                        type: 'post',
                        dataType: 'json',
                        url: '{{ url('classroom/delete') }}',
                        data: {
                        id: id,
                        _token: _csrf
                        },success:function(response){
                            window.location.reload();
                        }
                    });
                }
            });
        });

        $('.details').each(function(){
            $(this).click(function(){
                let id = $(this).attr('data-id');
                    $.ajax({
                        type: 'get',
                        dataType: 'json',
                        url: '{{ url('classroom/show') }}/'+ id,
                        success:function(response){
                            console.log(response);
                            showModal(response.data);
                        }
                });
                
            });
        });

        function showModal(data){
            Object.keys(data).forEach(function(key) {
                $('#val_'+ key).text(data[key]);
            });
            $('#exampleModal').modal('show');
        }

        $('.classroom_admin').each(function(){
            
            $(this).click(function(){
                let id = $(this).attr('data-id');

                let title = $(this).attr('data-title');

                let modal = $('#class_admin');

                $(modal).find('.modal-title').text(title);

                let modalData = `
                    <div class="modal_loading">
                        <img src="http://aic.net/assets/img/Loading/loading.gif">
                        <div class="status">{{__('label.submit_process')}}</div>
                    </div>`;

                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: '{{ url('api/classroom/admin') }}',
                    data: {
                        id: id,
                        type: '{{ $user_data->role }}'
                    },
                    beforeSend: function(){
                        $(modal).find('.modal-body').html(modalData);
                        $(modal).find('.modal-header, .modal-footer').addClass('hidden');
                    },
                    success: function(response){
                        $(modal).find('.modal-body').html(response.data);
                        $(modal).find('.modal-header, .modal-footer').removeClass('hidden');
                    }
                });

            });
            
        });
    });
   
</script>
@endsection
