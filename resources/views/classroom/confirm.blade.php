@extends('layouts.app')
@section('content')
<!-- Page Header-->
            <header class="page-header">
                <div class="container-fluid">
                <h2 class="no-margin-bottom">Classroom Create</h2>
                </div>
            </header>
  <style>
    .req{
        font-size: 11px;
        vertical-align: middle;
        display: inline-block;
        color: #fff;
        background-color: #c00;
        height: 16px;
        line-height: 16px;
        padding: 0 4px;
        margin-top: 0px;
        font-weight: normal;
    }
    .label-cla {
        margin-top: 10px;
    }
  </style>          
            <!-- Dashboard Counts Section-->
            <section class="dashboard-counts no-padding-bottom">
                <div class="container-fluid">
                      @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>                      
                      @endif
                      <div class="card">
                        <div class="card-header d-flex align-items-center">
                            <h3 class="h4">Classroom Detail Form</h3>
                        </div>
                        <div class="card-body">
                        {!! Form::open(['url' => 'classroom/create']) !!} 
                                <div class="form-row">
                                        <div class="form-group col">
                                        <div class="col-sm-3">
                                            <span class="label-cla">教室名</span>
                                            <span class="req">必須</span>
                                        </div>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="class_name">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col">
                                            <div class="col-sm-3">
                                                <span class="label-cla">連絡先氏名</span>
                                                <span class="req">必須</span>
                                            </div>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" name="class_fullname">
                                            </div>
                                        </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col">
                                            <div class="col-sm-3">
                                                <span class="label-cla">組織名</span>
                                                <span class="req">必須</span>
                                            </div>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" name="class_orgname">
                                            </div>
                                        </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col">
                                            <div class="col-sm-3">
                                                <span class="label-cla">電話番号</span>
                                                <span class="req">必須</span>
                                            </div>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" name="class_phone">
                                            </div>
                                        </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col">
                                            <div class="col-sm-3">
                                                <span class="label-cla">携帯電話番号</span>
                                               
                                            </div>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" name="class_mobile">
                                            </div>
                                        </div>
                                </div>
                                <div class="form-row">
                                        <div class="form-group col">
                                                <div class="col-sm-3">
                                                    <span class="label-cla">連絡先メールアドレス</span>
                                                    <span class="req">必須</span>
                                                </div>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" name="class_email">
                                                </div>
                                            </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col">
                                            <div class="col-sm-3">
                                                <span class="label-cla">連絡先メールアドレス（確認）</span>
                                                <span class="req">必須</span>
                                            </div>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" name="class_confirm_email">
                                            </div>
                                        </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col">
                                        <div class="col-md-12 col-sm-12">
                                                <button type="submit" class="btn btn-primary mr-2">Save</button>
                                                <button type="reset" class="btn btn-warning mr-2" onclick="window.history.go(-1); return false;">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                        {!! Form::close() !!}
                        </div>    
                      </div>
                </div>
            </section>   
     
@endsection

