<!-- Demo version: 2018.10.26 -->

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $title ?? config('app.name', 'Laravel') }}</title>
    <meta name="description" content="">
    <meta name="socket-io" content="{{ env('SOCKET_IO','https://192.168.12.65:3000') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
   
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{ asset('assets/css/style.blue.css') }}" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{ asset('assets/img/favicon.ico') }}">
    <script src="{{ asset('assets/js/canvas-designer-widget.js') }}"></script>
    <script src="{{asset('assets/dist/FileBufferReader.js')}}"></script>
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/adapterjs/0.15.3/adapter.min.js"></script>
    <script src="https://rtcmulticonnection.herokuapp.com/dist/RTCMultiConnection.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.js"></script>
    
    <script src="{{asset('assets/dist/FileBufferReader.js')}}"></script>
    <script src="{{asset('assets/js/canvas-designer/dev/webrtc-handler.js')}}"></script>
    <script src="{{asset('assets/js/canvas-designer/canvas-designer-widget.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    
    <script src="{{ asset('assets/js/main.js') }}"></script>
  </head>
  <body>
  
    <video id="main-video"  style="display: none;" controls playsinline autoplay></video>
<style>

#other-videos:first-child{
    position: relative;
    width: 100%;
    height: 100%;
    border: 3px solid #73AD21;
}
video{
    width: 100%;
}
/* #other-videos:last-child{
    position: absolute;
    top: 20px;
    right: 0;
    width: 50%;
    height: 40%;
    border: 3px solid #73AD21;
} */
</style>

<div class="page">
  <!-- Main Navbar-->
  <header class="header">
      <nav class="navbar">
          <div class="container-fluid">
               
              <div class="navbar-holder d-flex align-items-center justify-content-between">
                      
              <!-- Navbar Header-->
                  <div class="navbar-header">
                      <!-- Navbar Brand --><a href="#" class="navbar-brand d-none d-sm-inline-block">
                      <div class="brand-text d-none d-lg-inline-block"><span></span>AIC-Kids &nbsp;</span></div>
                      <div class="brand-text d-none d-sm-inline-block d-lg-none"><strong>BD</strong></div></a>
                  </div>
                  <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                          <li class="nav-item mr-3">Lesson #1 Body Parts</button></li>
                          <li class="nav-item">
                              @if (Auth::user()->role == 2)
                              <button type="button" id="teach-btn" class="btn btn-primary float-right endSession" >End Video Class</button>    
                              @endif
                          </li>
                  </ul>
              </div>
          </div>
      </nav>
  </header>
 <input type="hidden" class="getRoomName" value="<?php echo $_GET['sessionid']; ?> ">
  <input class="usernameInput" type="hidden" maxlength="14" value="{{Auth::user()->username}}"/>
  <section class="video-call">
      <div class="chat chat page">
          <div class="inner chatArea">
              <ul class="messages" id="chatMessageIo"></ul>
          </div>
          <div class="innerInput">
              {{-- <input type="text" class="inputMessage" placeholder="Type here..."/> --}}
              <input type="hidden" id="user_id" value="{{ Auth::id() }}">
              <input type="hidden" id="lesson_id" value="">
              <textarea  class="inputMessage" id="inputMessage" placeholder="Type here..." cols="2"></textarea>
          </div>
      </div> 
      <div class="bigbox">
          <div class="upper">
              <div class="table">
                  <div class="td video">
                    <div class="bordered-box">
                        <div class="others" id="other_video"></div>
                        <div class="initiator" id="initiator_video"></div>
                        {{-- <div id="other-videos"></div> --}}
                    </div>                      
                  </div>
                  <div class="td scribble">
                      <div class="bordered-box"> 
                          
                      </div>
                  </div>
              </div>
          </div>
          <div class="lower">
              <div class="table">
                  <div class="td youtube">
                      <div class="bordered-box"><p>Media Player</p></div>
                  </div>
                  <div class="td scribble">
                      <div class="bordered-box"><div id="widget-container" style="width: 100%;  "></div></div>
                  </div>
              </div>
          </div>
      </div>
  </section>
<!-- Modal For Teacher-->
<div class="modal fade" id="ratingModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      @if (Auth::user()->role == 2)
      <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">

                    <div class="row">
                            <div class="col">2018/10/12</div>
                            <div class="col text-center">Grade2</div>
                            <div class="col text-right">no.6</div>
                        </div>
              </h5>
            </div>
             
            <div class="modal-body">
                    <div class="form-group row">
                            <div class="col-md-2 col-sm-12 text-right">
                                <span class="category"><i class="fa fa-smile-o fa-2x" data-toggle="tooltip"  data-placement="bottom" 
                                    title="態度"></i></span> 
                            </div>
                            <div class="col-md-8 col-sm-12 ">
                                    <div class="rating" id="student">
                                        <span><input type="radio" name="student" id="str5" value="5"><label for="str5" data-toggle="tooltip"  data-placement="bottom" title=""><i class="fa fa-star fa-2x"></i></label></span>
                                        <span><input type="radio" name="student" id="str4" value="4"><label for="str4" data-toggle="tooltip"  data-placement="bottom" title=""><i class="fa fa-star fa-2x"></i></label></span>
                                        <span><input type="radio" name="student" id="str3" value="3"><label for="str3" data-toggle="tooltip"  data-placement="bottom" title="特に気になることはなく、積極的に授業に参加しており、相手へのリスペクトが欠けることは無かった。"><i class="fa fa-star fa-2x"></i></label></span>
                                        <span><input type="radio" name="student" id="str2" value="2"><label for="str2" data-toggle="tooltip"  data-placement="bottom" title="ごく稀に生徒の言動・態度がネガティブな印象を与えることがあったが、実際のコミュニケーションの場でも起こりうる程度のものである。"><i class="fa fa-star fa-2x"></i></label></span>
                                        <span><input type="radio" name="student" id="str1" value="1"><label for="str1" data-toggle="tooltip"  data-placement="bottom" title="会話に対する意欲が見られない。または、態度・所作などの問題で実際のコミュニケーションの場でも相手に不快感を与えることが予想される。"><i class="fa fa-star fa-2x"></i></label></span>
                                    </div>
                            </div>
                    </div>

                    <hr>
                     <div class="form-group row">
                        <div class="col-md-2 col-sm-12 text-right">
                        <span class="category"><img src="{{asset('assets/img/customer-service-svgrepo-com.svg')}}" width="30" height="35" data-toggle="tooltip" data-placement="bottom" 
                                title="聞いてわかる力"></span> 
                        </div>
                        <div class="col-md-8 col-sm-12 ">
                            <div class="rating" id="listening"> 
                                <span><input type="radio" name="listening" id="str6" value="5"><label for="str6"  data-toggle="tooltip" data-placement="bottom" title="通常のスピード・喋り方で伝えても、教師側の説明や指示を完全に理解することができる。"><i class="fa fa-star fa-2x"></i></label></span>
                                <span><input type="radio" name="listening" id="str7" value="4"><label for="str7" data-toggle="tooltip" data-placement="bottom" title="指示をくり返したり、スピードを緩めたり、言い換えて説明すれば、こちらの意図する内容が問題無く伝わる。" ><i class="fa fa-star fa-2x"></i></label></span>
                                <span><input type="radio" name="listening" id="str8" value="3"><label for="str8" data-toggle="tooltip" data-placement="bottom" title="全体的には会話が成立しているように見えるが、部分的に指示や説明が伝わっていないと感じることがある。"><i class="fa fa-star fa-2x"></i></label></span>
                                <span><input type="radio" name="listening" id="str9" value="2"><label for="str9" data-toggle="tooltip" data-placement="bottom" title="教師の指示・説明が伝わらないことが頻繁にあり、授業がスムーズに進行しない。"><i class="fa fa-star fa-2x"></i></label></span>
                                <span><input type="radio" name="listening" id="str10" value="1"><label for="str10"  data-toggle="tooltip" data-placement="bottom" title="授業の進行が頻繁に滞る。指示・説明が通らない。"><i class="fa fa-star fa-2x"></i></label></span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <div class="col-md-2 col-sm-12 text-right">
                            <span class="category"><i class="fa fa-pencil fa-2x" data-toggle="tooltip" data-placement="bottom" 
                                title="文章を作る力"></i></span> 
                        </div>
                        <div class="col-md-8 col-sm-12 ">
                                <div class="rating" id="reading">
                                    <span><input type="radio" name="speaking" id="str11" value="5"><label for="str11" data-toggle="tooltip"  data-placement="bottom" title="英語圏や英語を公用語として使用する場においても問題無く発話することができ、会話の展開や進行速度に関しても違和感が無い。"><i class="fa fa-star fa-2x"></i></label></span>
                                    <span><input type="radio" name="speaking" id="str12" value="4"><label for="str12" data-toggle="tooltip"  data-placement="bottom" title="全体的に求められる内容を、十分な発話量で発話できているが、返答のスピードが遅かったり、不自然に単語のみで回答したりと、通常の英語環境での会話と比べると違和感がある。"><i class="fa fa-star fa-2x"></i></label></span>
                                    <span><input type="radio" name="speaking" id="str13" value="3"><label for="str13" data-toggle="tooltip"  data-placement="bottom" title="意思疎通はできるが、極端に返答時間が遅かったり、極端に単語のみでの返答が続いたり、自分の知っている表現を使用する会話の方向に無理に展開しようとしたりする等、時折コミュニケーションに不自然さや難しさを感じる。"><i class="fa fa-star fa-2x"></i></label></span>
                                    <span><input type="radio" name="speaking" id="str14" value="2"><label for="str14" data-toggle="tooltip"  data-placement="bottom" title="言葉だけでは何を伝えようとしているのかわからないことがしばしばある。"><i class="fa fa-star fa-2x"></i></label></span>
                                    <span><input type="radio" name="speaking" id="str15" value="1"><label for="str15" data-toggle="tooltip"  data-placement="bottom" title="言葉だけでは何を伝えようとしているのかわからない。"><i class="fa fa-star fa-2x"></i></label></span>
                                </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2 col-sm-12 text-right">
                            <span class="category"><img src="{{asset('assets/img/brain-on-head-svgrepo-com.svg')}}" width="30" height="35" data-toggle="tooltip" data-placement="bottom" 
                                title="文法知識"></span> </span> 
                        </div>
                        <div class="col-md-8 col-sm-12 ">
                                <div class="rating" id="grammar">
                                    <span><input type="radio" name="grammar" id="str16" value="3"><label for="str16" data-toggle="tooltip"  data-placement="bottom" title="文法のミスは気にならない程度である。"><i class="fa fa-star fa-2x"></i></label></span>
                                    <span><input type="radio" name="grammar" id="str17" value="2"><label for="str17" data-toggle="tooltip"  data-placement="bottom" title="文法ミスは多々あるが、コミュニケーションをとるうえでは問題ない。"><i class="fa fa-star fa-2x"></i></label></span>
                                    <span><input type="radio" name="grammar" id="str18" value="1"><label for="str18" data-toggle="tooltip"  data-placement="bottom" title="文法ミスが非常に多く、それが原因で意図する内容を伝えることや会話の展開に難しさを感じる。"><i class="fa fa-star fa-2x"></i></label></span>

                                </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2 col-sm-12 text-right">
                            <span class="category"><i class="fa fa-headphones  fa-2x" data-toggle="tooltip" data-placement="bottom" 
                                title="発音・流暢さ"></i></span>  
                        </div>
                        <div class="col-md-8 col-sm-12 ">
                                <div class="rating" id="grammar">
                                    <span><input type="radio" name="grammar" id="str19" value="3"><label for="str19" data-toggle="tooltip"  data-placement="bottom" title="コミュニケーションを取るうえでは全く問題ない。"><i class="fa fa-star fa-2x"></i></label></span>
                                    <span><input type="radio" name="grammar" id="str20" value="2"><label for="str20" data-toggle="tooltip"  data-placement="bottom" title="時折聞き取りづらいことはあるが、コミュニケーション自体は成立する。"><i class="fa fa-star fa-2x"></i></label></span>
                                    <span><input type="radio" name="grammar" id="str21" value="1"><label for="str21" data-toggle="tooltip"  data-placement="bottom" title="会話に支障が出るレベルで、発音やスピード、抑揚に難がある。"><i class="fa fa-star fa-2x"></i></label></span>
                                   
                                </div>
                        </div>
                    </div>
            </div>
      @elseif(Auth::user()->role == 3)
          <div class="modal-header">
                  <h5 class="modal-title text-center" id="exampleModalLabel">
  
                          <div class="row">
                                  <div class="col">2018/10/12</div>
                                  <div class="col text-center">Grade2</div>
                                  <div class="col text-right">no.6</div>
                          </div>
                          <div class="row">
                                  
                                  <div class="col text-center">Mr. Tom Cruise</div>
                              
                          </div>
  
                  </h5>
              </div>
              
              <div class="modal-body">
                      <div class="form-group row">
                              <div class="col-md-2 col-sm-12 text-right">
                                  <span class="category"><i class="fa fa-smile-o fa-2x" data-toggle="tooltip"  data-placement="bottom" 
                                      title="態度"></i></span> 
                              </div>
                              <div class="col-md-8 col-sm-12 ">
                                      <div class="rating" id="teacher">
                                          <span><input type="radio" name="teacher" id="str26" value="5"><label for="str26" data-toggle="tooltip"  data-placement="bottom" title=""><i class="fa fa-star fa-2x"></i></label></span>
                                          <span><input type="radio" name="teacher" id="str27" value="4"><label for="str27" data-toggle="tooltip"  data-placement="bottom" title=""><i class="fa fa-star fa-2x"></i></label></span>
                                          <span><input type="radio" name="teacher" id="str28" value="3"><label for="str28" data-toggle="tooltip"  data-placement="bottom" title="特に気になることはなく、積極的に授業に参加しており、相手へのリスペクトが欠けることは無かった。"><i class="fa fa-star fa-2x"></i></label></span>
                                          <span><input type="radio" name="teacher" id="str29" value="2"><label for="str29" data-toggle="tooltip"  data-placement="bottom" title="ごく稀に生徒の言動・態度がネガティブな印象を与えることがあったが、実際のコミュニケーションの場でも起こりうる程度のものである。"><i class="fa fa-star fa-2x"></i></label></span>
                                          <span><input type="radio" name="teacher" id="str30" value="1"><label for="str30" data-toggle="tooltip"  data-placement="bottom" title="会話に対する意欲が見られない。または、態度・所作などの問題で実際のコミュニケーションの場でも相手に不快感を与えることが予想される。"><i class="fa fa-star fa-2x"></i></label></span>
                                      </div>
                              </div>
                      </div>
              </div>   
      @endif
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary" onclick="javascript:window.top.close();">Submit</button>
      </div>
    </div>
  </div>{{-- Modal body end --}}
</div>
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip(); 
            $('span').click(function () {
                $(this).closest('.rating').find('span').removeClass('checked');
                $(this).closest('span').addClass('checked');
        });
    
    });
</script>




<script>
(function() {
    var params = {},
        r = /([^&=]+)=?([^&]*)/g;

    function d(s) {
        return decodeURIComponent(s.replace(/\+/g, ' '));
    }
    var match, search = window.location.search;
    while (match = r.exec(search.substring(1)))
        params[d(match[1])] = d(match[2]);
    window.params = params;
    console.log(params);
})();

var connection = new RTCMultiConnection();

connection.socketURL = 'https://192.168.12.65:9001/';
//connection.socketURL = 'https://rtcmulticonnection.herokuapp.com:443/';

connection.extra.userFullName = params.userFullName;
console.log(connection.extra.userFullName);
/// make this room public
//connection.publicRoomIdentifier = params.publicRoomIdentifier;

connection.socketMessageEvent = 'canvas-dashboard-demo';

// keep room opened even if owner leaves
connection.autoCloseEntireSession = false;

// https://www.rtcmulticonnection.org/docs/maxParticipantsAllowed/
connection.maxParticipantsAllowed = 3;
// set value 2 for one-to-one connection
// connection.maxParticipantsAllowed = 2;

// https://www.rtcmulticonnection.org/docs/password/
// connection.password = 'something';

// here goes canvas designer
var designer = new CanvasDesigner();

// you can place widget.html anywhere
designer.widgetHtmlURL = "https://192.168.12.65:3000/canvas-designer/widget.html";
designer.widgetJsURL = "https://192.168.12.65:3000/canvas-designer/widget.js";
        

designer.addSyncListener(function(data) {
    connection.send(data);
});

designer.setSelected('pencil');

designer.setTools({
    pencil: true,
    text: true,
    image: true,
    pdf: true,
    eraser: true,
    line: true,
    arrow: true,
    dragSingle: true,
    dragMultiple: true,
    arc: true,
    rectangle: true,
    quadratic: false,
    bezier: true,
    marker: true,
    zoom: false,
    lineWidth: false,
    colorsPicker: false,
    extraOptions: false,
    code: false,
    undo: true
});

// here goes RTCMultiConnection

connection.chunkSize = 16000;
connection.enableFileSharing = true;

connection.session = {
    audio: true,
    video: true,
    data: true
};
connection.sdpConstraints.mandatory = {
    OfferToReceiveAudio: true,
    OfferToReceiveVideo: true
};

// connection.onUserStatusChanged = function(event) {
//     var infoBar = document.getElementById('onUserStatusChanged');
//     var names = [];
//     connection.getAllParticipants().forEach(function(pid) {
//         names.push(getFullName(pid));
//     });

//     if (!names.length) {
//         names = ['Only You'];
//     } else {
//         names = [connection.extra.userFullName || 'You'].concat(names);
//     }

//     //infoBar.innerHTML = '<b>Active users:</b> ' + names.join(', ');
// };

connection.onopen = function(event) {
    connection.onUserStatusChanged(event);

    if (designer.pointsLength <= 0) {
        // make sure that remote user gets all drawings synced.
        setTimeout(function() {
            connection.send('plz-sync-points');
        }, 1000);
    }

    // document.getElementById('btn-chat-message').disabled = false;
    // document.getElementById('txt-chat-message').disabled = false;
    // document.getElementById('btn-attach-file').style.display = 'inline-block';
};

connection.onclose = connection.onerror = connection.onleave = function(event) {
    connection.onUserStatusChanged(event);
};

connection.onmessage = function(event) {
    if (event.data.chatMessage) {
        appendChatMessage(event);
        return;
    }

    if (event.data.checkmark === 'received') {
        var checkmarkElement = document.getElementById(event.data.checkmark_id);
        if (checkmarkElement) {
            checkmarkElement.style.display = 'inline';
        }
        return;
    }

    if (event.data === 'plz-sync-points') {
        designer.sync();
        return;
    }

    designer.syncData(event.data);
};

// extra code

function beforeOpenRoom(callback) {
    // capture canvas-2d stream
    // and share in realtime using RTCPeerConnection.addStream
    // requires: dev/webrtc-handler.js
    designer.captureStream(function(stream) {
        stream.isScreen = true;
        stream.streamid = stream.id;
        stream.type = 'local';
        
        /*
        var video = document.createElement('video');
        video.muted = true;
        video.srcObject = stream;
        video.play();
        */

        connection.attachStreams.push(stream);
        connection.onstream({
            stream: stream,
            type: 'local',
            streamid: stream.id,
            // mediaElement: video
        });
        console.log(stream.id);

        callback();
    });
}

connection.onstream = function(event) {
    var isInitiator = connection.isInitiator;
        //console.log(isInitiator);
        console.log(event.type);
    if (event.stream.isScreen) {
        var video = document.getElementById('main-video');
        video.setAttribute('data-streamid', event.streamid);
        video.style.display = 'none';
        video.srcObject = event.stream;
    } else {
        event.mediaElement.controls = false;
        //initiator_video
        //other_video
        
        if(event.type == 'local'){
            var other_video = document.querySelector('#other_video');
            other_video.appendChild(event.mediaElement);
        }else{
        
            var initiator_video = document.querySelector('#initiator_video');
            initiator_video.appendChild(event.mediaElement);
        }
        
    }

    connection.onUserStatusChanged(event);
};

connection.onstreamended = function(event) {
    var video = document.querySelector('video[data-streamid="' + event.streamid + '"]');
    if (!video) {
        video = document.getElementById(event.streamid);
        if (video) {
            video.parentNode.removeChild(video);
            return;
        }
    }
    if (video) {
        video.srcObject = null;
        video.style.display = 'none';
    }
};
//chat
// var conversationPanel = document.getElementById('conversation-panel');

// function appendChatMessage(event, checkmark_id) {
//     var div = document.createElement('div');

//     div.className = 'message';

//     if (event.data) {
//         div.innerHTML = '<b>' + (event.extra.userFullName || event.userid) + ':</b><br>' + event.data.chatMessage;

//         if (event.data.checkmark_id) {
//             connection.send({
//                 checkmark: 'received',
//                 checkmark_id: event.data.checkmark_id
//             });
//         }
//     } else {
//         div.innerHTML = '<b>You:</b> <img class="checkmark" id="' + checkmark_id + '" title="Received" src="https://webrtcweb.com/checkmark.png"><br>' + event;
//         div.style.background = '#cbffcb';
//     }

//     conversationPanel.appendChild(div);

//     conversationPanel.scrollTop = conversationPanel.clientHeight;
//     conversationPanel.scrollTop = conversationPanel.scrollHeight - conversationPanel.scrollTop;
// }
// document.getElementById('btn-chat-message').onclick = function() {
//     var chatMessage = document.getElementById('txt-chat-message').value;
//     document.getElementById('txt-chat-message').value = '';

//     if (!chatMessage || !chatMessage.replace(/ /g, '').length) return;

//     var checkmark_id = connection.userid + connection.token();

//     appendChatMessage(chatMessage, checkmark_id);

//     connection.send({
//         chatMessage: chatMessage,
//         checkmark_id: checkmark_id
//     });
// };

// document.getElementById('txt-chat-message').onkeyup = function(e) {
//     var code = e.keyCode || e.which;
//     if (code == 13) {
//         document.getElementById('btn-chat-message').click(); // you can even call "onclick"
//     }
// };

// document.getElementById('btn-attach-file').onclick = function() {
//     var file = new FileSelector();
//     file.selectSingleFile(function(file) {
//         connection.send(file);
//     });
// };

// function getFileHTML(file) {
//     var url = file.url || URL.createObjectURL(file);
//     var attachment = '<a href="' + url + '" target="_blank" download="' + file.name + '">Download: <b>' + file.name + '</b></a>';
//     if (file.name.match(/\.jpg|\.png|\.jpeg|\.gif/gi)) {
//         attachment += '<br><img crossOrigin="anonymous" src="' + url + '">';
//     } else if (file.name.match(/\.wav|\.mp3/gi)) {
//         attachment += '<br><audio src="' + url + '" controls></audio>';
//     } else if (file.name.match(/\.pdf|\.js|\.txt|\.sh/gi)) {
//         attachment += '<iframe class="inline-iframe" src="' + url + '"></iframe></a>';
//     }
//     return attachment;
// }

// function getFullName(userid) {
//     var _userFullName = userid;
//     if (connection.peers[userid] && connection.peers[userid].extra.userFullName) {
//         _userFullName = connection.peers[userid].extra.userFullName;
//     }
//     return _userFullName;
// }

// connection.onFileEnd = function(file) {
//     var html = getFileHTML(file);
//     var div = progressHelper[file.uuid].div;

//     if (file.userid === connection.userid) {
//         div.innerHTML = '<b>You:</b><br>' + html;
//         div.style.background = '#cbffcb';
//     } else {
//         div.innerHTML = '<b>' + getFullName(file.userid) + ':</b><br>' + html;
//     }
// };

// to make sure file-saver dialog is not invoked.
connection.autoSaveToDisk = false;

var progressHelper = {};

connection.onFileProgress = function(chunk, uuid) {
    var helper = progressHelper[chunk.uuid];
    helper.progress.value = chunk.currentPosition || chunk.maxChunks || helper.progress.max;
    updateLabel(helper.progress, helper.label);
};

connection.onFileStart = function(file) {
    var div = document.createElement('div');
    div.className = 'message';

    if (file.userid === connection.userid) {
        div.innerHTML = '<b>You:</b><br><label>0%</label> <progress></progress>';
        div.style.background = '#cbffcb';
    } else {
        div.innerHTML = '<b>' + getFullName(file.userid) + ':</b><br><label>0%</label> <progress></progress>';
    }

    div.title = file.name;
    conversationPanel.appendChild(div);
    progressHelper[file.uuid] = {
        div: div,
        progress: div.querySelector('progress'),
        label: div.querySelector('label')
    };
    progressHelper[file.uuid].progress.max = file.maxChunks;

    conversationPanel.scrollTop = conversationPanel.clientHeight;
    conversationPanel.scrollTop = conversationPanel.scrollHeight - conversationPanel.scrollTop;
};

function updateLabel(progress, label) {
    if (progress.position == -1) return;
    var position = +progress.position.toFixed(2).split('.')[1] || 100;
    label.innerHTML = position + '%';
}
console.log(params.open);
designer.appendTo(document.getElementById('widget-container'), function() {

    if (params.open === true || params.open === 'true') {
        beforeOpenRoom(function() {
            connection.open(params.sessionid, function(isRoomOpened, roomid, error) {
                if (error) {
                    if (error === connection.errors.ROOM_NOT_AVAILABLE) {
                        alert('Someone already created this room. Please either join or create a separate room.');
                        return;
                    }
                    alert(error);
                }

                connection.socket.on('disconnect', function() {
                    location.reload();
                });
            });
        });
        
    } else {
        connection.join(params.sessionid, function(isRoomJoined, roomid, error) {
            if (error) {
                if (error === connection.errors.ROOM_NOT_AVAILABLE) {
                    alert('This room does not exist. Please either create it or wait for moderator to enter in the room.');
                    return;
                }
                if (error === connection.errors.ROOM_FULL) {
                    alert('Room is full.');
                    return;
                }
                alert(error);
            }

            connection.socket.on('disconnect', function() {
                location.reload();
            });
        });
    }
});
</script>
</body>
</html>
