
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title></title>
</head>
<body>
    <div>
        <h4>Hi {{$info['to_name']}},</h4>
          <p>{{$info['from_name']}} posted a new comment</p>  
          <p><a href="{{ $info['link'] }}"> Click here to view</a> </p>
          <p>Thank you.</p>
          
          <p>Best Regards,<br>
          System Admin</p>
          
          <p>This is an automated email, please do not reply to it. </p>
      </div>
</body>
</html>