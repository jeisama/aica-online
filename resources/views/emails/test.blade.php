<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title></title>
</head>
<body>
    <div>
        <h4>Hi {{$info->romaji_name}},</h4>
      
          <p><a href="{{url('/lessons')}}/{{$info->slug}}"> {{$info->slug}} </a> lesson has been created.</p>
          <p>Thank you.</p>
          
          <p>Best Regards,<br>
          System Admin</p>
          
          <p>This is an automated email, please do not reply to it. </p>
      </div>
</body>
</html>