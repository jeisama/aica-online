@extends('layouts.app')

@section('css')

<link rel="stylesheet" href="{{ asset('assets/vendor/confirm/jquery-confirm.css') }}">

@endsection

@section('content')
<!-- Page Header-->
    <div class="breadcrumb-holder container-fluid">
        <div class="inner">
            <div class="links">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i> {{__('label.home')}}</a></li>
                    <li class="breadcrumb-item active">{{__('label.course')}}</li>
                </ul>
            </div>
            {{-- @if($user_data->role == 1)
            <div class="buttons">
                <a href="{{ url('courses/create') }}" class="btn btn-primary btn-sm"><i class="mdi mdi-plus"></i> {{__('label.create_course')}}</a>
            </div>
            @endif --}}
        </div>        
    </div>

    <section class="tables">   
        <div class="container-fluid">
            @if(Session::has('success_message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ Session::get('success_message') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif

            
            @if($courses)

            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="h4">{{__('label.course_list')}}</h3>
                        </div>
                        <div class="card-body nopadding">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>{{__('label.course')}}</th>
                                            <th width="30%" class="text-center">{{__('label.set_grade_level')}}</th>
                                            <th width="10%" class="text-center">{{__('label.status')}}</th>
                                            <th width="100">&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($courses as $course)
                                        <tr>
                                            <td>{{ $course->label }}</td>
                                            <td class="text-center">
                                                <button type="button" class="btn btn-primary btn-sm manage-grade-level" data-title="{{ $course->label }}" data-id="{{ $course->id }}" data-toggle="modal" data-target="#grade_level"><i class="mdi mdi-format-list-checks"></i> {{__('label.settings')}} ({{ count($course->count) }})</button>
                                            </td>
                                            <td class="text-center">{{ $course->status == 1 ? __('label.active_sts') : __('label.inactive_sts')  }}</td>
                                            <td class="text-center action">
                                                <div class="buttons">
                                                    <a href="{{ url('courses/edit/'. $course->id) }}" title="Edit"><i class="mdi mdi-pen"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @else
            <div class="no-record">
                <div class="info"><i class="mdi mdi-information-outline"></i></div>
                <p>No record found for the given selection!</p>
                <div><a href="{{ url('courses/create') }}" class="btn btn-primary btn-sm"><i class="mdi mdi-plus"></i> Create</a></div>
            </div>
            @endif
        </div>

    </section><!--end of section table-->

    <!-- Modal -->
    <div class="modal fade" id="grade_level" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {{ Form::open(['url' => 'courses/set-grade', 'id' => 'course-level', 'onsubmit' => 'event.preventDefault()']) }}
                <div class="modal-body">
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">{{__('label.back')}}</button>
                    <button type="submit" class="btn btn-primary" id="submit-form" data-form="course-level">{{__('label.save')}}</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div><!-- Modal -->

@endsection

@section('js')
    
    <script src="{{ asset('assets/vendor/confirm/jquery-confirm.js') }}"></script>

    <script>
        $(function(){
            //$('#grade_level').modal('show');
            $(document).on('click', '.manage-grade-level', function(){

                let id = $(this).attr('data-id');

                let title = $(this).attr('data-title');

                $('.modal-title').text(title);

                let modalData = `
                    <div class="modal_loading">
                        <img src="{{ asset('assets/img/Loading/loading.gif') }}">
                        <div class="status">{{__('label.submit_process')}}</div>
                    </div>`;
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: '{{ url('api/courses/grade-level') }}',
                    data: {
                        id: id
                    },
                    beforeSend: function(){
                        $('.modal-body').html(modalData);
                        $('.modal-header, .modal-footer').addClass('hidden');
                    },
                    success: function(response){
                        $('.modal-body').html(response.data);
                        $('.modal-header, .modal-footer').removeClass('hidden');
                    }
                });

            });

        });
    </script>
@endsection
