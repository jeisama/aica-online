<?php

return [
    //sidebar
    'main_menu' => 'Main Menu',
    'home' => 'Home',
    'users' => 'Users',
    'schedules' => 'Schedules',
    'lessons' => 'Lessons',
    'classroom' => 'Classroom',
    'settings' => 'Settings',
    'grade_level' => 'Grade Level',
    'course' => 'Course',
    'template' => 'Templates',
    'teacher_level' => 'Teacher Level',

    //logout
    'logout' => 'Logout',

    //dashboard
    'dashboard' => 'Dashboard',
    'next_week_lesson' => 'After Next Lesson',
    'this_week_lesson' => 'Next Lesson',
    'last_week_lesson' => 'Previous Lesson',
    'course' => 'Course',
    'grade' => 'Grade',
    'day' => 'Day',
    'lesson_name' => 'Lesson Name',
    'teacher'   => 'Teacher',
    'student'   => 'Student',
    'date_time' => 'Date & Time',
    'no_lesson' => 'No lesson available',

    //users index
    'create' => 'Create',
    'filter_search' => 'Filter Search',
    'full_name' => 'Full Name',
    'role' => 'Role',
    'all_user' => 'All User',
    'super_admin' => 'Super Admin',
    'class_admin' => 'Class Room Admin',
    'aica_admin' => 'AICA Admin',
    'inactive_include' => 'Include inactive users',
    'search' => 'Search',
    'name' => 'Name',
    'username' => 'Username',
    'password' => 'Password',
    'email' => 'Email',

    //create users
    'create_user' => 'Create User',
    'user_detail' => 'User Detail',
    'select_avatar' => 'Select Avatar',
    'gender' => 'Gender',
    'select_gender' => 'Select Gender',
    'male' => 'Male',
    'female' => 'Female',
    'confirm_email' => 'Confirm Email',
    'login_detail' => 'Login Detail',
    'status' => 'Status',
    'active_sts' => 'Active',
    'inactive_sts' => 'Inactive',
    'save' => 'Save',
    'cancel' => 'Cancel',
    'select_prefecture' => 'Select Prefecture',
    'select_span' => 'Select Span',
    'class_room' => 'Class Room',
    'select_class' => 'Select Class Room',
    'select_item' => '--- Select Item ----',
    'back' => 'Back',
    'teacher_level' => 'Teacher Level',
    'level' => 'Level',
    'Level_1' =>	'Level 1',
    'Level_2' =>	'Level 2',
    'Level_3' =>	'Level 3',
    'user_create_message' => 'New user has been successfully created.',
    'edit_user'=>'Edit User',
    'update_user_message' => 'User detail successfully updated.',
    //calendar
    'jan' =>        'January',
    'feb' =>        'February',
    'mar' =>        'March',
    'apr' =>        'April',
    'may' =>        'May',
    'jun' =>        'June',
    'jul' =>        'July',
    'aug' =>        'August',
    'sep' =>        'September',
    'oct' =>        'October',
    'nov' =>        'November',
    'dec' =>        'December',

    //schedules
   'schedule' => 'Schedule',
   'legend' => 'Legends',
   'create_stu_sched' => 'Create Student Schedule', 
   'create_tea_sched' => 'Create Teacher Schedule',
   'pdf_header_stu' => 'Student Schedule',
   'select_student' => 'Select Student',
   'frequency' => 'Frequency',
   'start_day' => 'Start Day',
   'select_all' => 'Select All',
   'once_week' => 'Once a week',
   'alternate_week' => 'Alternate week',
   'one_time_only' => 'One time only', 
   'date' => 'Date',
   'time' => 'Time',
   'edit' => 'Edit',
   'edit_stu' => 'Edit Student Schedule',
   'sun' => 'Sun',
   'mon' => 'Mon',
   'tue' => 'Tue',
   'wed' => 'Wed',
   'thu' => 'Thu',
   'fri' => 'Fri',
   'sat' => 'Sat',
    'sunday' => 'Sunday',
    'monday' => 'Monday',
    'tuesday' => 'Tuesday',
    'wednesday' => 'Wednesday',
    'thursday' => 'Thursday',
    'friday' => 'Friday',
    'saturday' => 'Saturday',
    'time_in' => 'Time In',
    'time_out' => 'Time Out',
    'start_date' => 'Start Date',
    'end_date' => 'End Date',
    //lesson
    'lesson' => 'Lesson',
    'schedule_details' => 'Schedule Details',
    'schedule_details' => 'Schedule Details',
    'lesson_details' => 'Lesson Details',
    'update' => 'Update',
    'start_video' => 'Start Video',
    'join_video' => 'Join Video',
    'end_video' => 'End Video',
    'lesson_added' => 'Lesson has been added',

    //classroom
    'set_admin' => 'Set Administrator',
    'classroom_detail' => 'Classroom Information',
    'confirm' => 'Confirm',
    'classroom_edit' => 'Classroom Edit',
    //courses
    'course_list' => 'Course List',
    'set_grade_level' => 'Set Applicable Grade',
    'create_course' => 'Create Course',
    'edit_course' => 'Edit Course',
    //Grade
    'grade_list' => 'Grade List',
    'create_grade' => 'Create Grade',
    'edit_grade' => 'Edit Grade',
    'grade_message' => 'Grade has been successfuly updated.',
    //teacher level
    'teacher_level_list' => 'Teacher Level List',
    'create_tea_level' => 'Create Teacher Level',
    'set_level_tea' => 'Set Applicable Grade to Teach',
    'edit_tea_level' => 'Edit Teacher Level',
    //templates
    'template_list' => 'Template Lists',
    'create_lesson_template' => 'Create Lesson Template',
    'create_template' => 'Create Templates',
    'edit_template' => 'Edit Templates',
    'submit_process' => 'Proccessing, please wait a moment...',

    //reports
    'report' => 'Report',
    'export' => 'Export',
    'year' => 'Year',
    'month' => 'Month',
    'all' => 'All',
    'aic_online' => 'AIC Online System',

    //messages
    'teacher_level_created' => 'Teacher level has been created.',
    'template_created' => 'Template has been successfully created.',
    'template_updated' => 'Template has been successfully updated.',
    'course_updated' => 'Course has been successfully updated.',
    'course_change' => 'Changes has been saved.'









    
];
