<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords must be at least six characters and match the confirmation.',
    'reset' => 'パスワードをリセットできました。',
    'sent' => 'パスワードをリセットするためのリンクを登録したメールを送信しました。',
    'token' => 'こパスワードの有効期限が過ぎたため、ログインページに戻り、<br> もう一度[パスワードを忘れた場合]をクリッ',
    'user' => "E-mailアドレスを未登録の方、アドレスが不明な方は教室管理者にお問合わせください。",

];
