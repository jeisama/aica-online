<?php

$developement = false;

$chatsocket  = $developement === true ? 'https://aic-online.glvibox.com:3000/':'https://aic-online.webstarterz.com:3000';
$widgethtml  = $developement === true ? 'https://aic-online.glvibox.com:3000/canvas-designer/widget.html':'https://aic-online.webstarterz.com:3000/canvas-designer/widget.html';
$widgetjs    = $developement === true ? 'https://aic-online.glvibox.com:3000/canvas-designer/widget.js':'https://aic-online.webstarterz.com:3000/canvas-designer/widget.js';
//
define('multi_rtc_connection', 'https://aic-online.webstarterz.com:9001/');

define('chat_socket', $chatsocket);

define('widget_html', $widgethtml);

define('widget_js', $widgetjs);


define('admin', 1);
define('class_room', 2);
define('aica', 3);
define('student', 4);
define('teacher', 5);