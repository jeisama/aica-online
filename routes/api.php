<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*
|--------------------------------------------------------------------------
| Schedule API
|--------------------------------------------------------------------------
*/
//Route::get('/schedule/calendar-repeat/{date}', 'Api\ScheduleController@index');
//Route::post('/schedule/get-schedule', 'Api\ScheduleController@get_schedule');
//Route::post('/schedule/get-all-schedule', 'Api\ScheduleController@get_all_schedule');
Route::post('/schedule/get-grade', 'Api\SchedulesApi@get_grade');
// Route::post('/schedule/end-time', 'Api\SchedulesApi@end_time');//
// Route::post('/schedule/student-get-time', 'Api\SchedulesApi@studentGetTime');//
// Route::post('/schedule/get-teacher-dates', 'Api\SchedulesApi@get_teacher_dates');
// Route::post('/schedule/getdates', 'Api\SchedulesApi@getdates');
Route::post('/schedule/get-teacher', 'Api\SchedulesApi@get_teacher');
Route::post('/schedule/get-dates', 'Api\SchedulesApi@get_dates');
Route::post('/schedule/set-date', 'Api\SchedulesApi@set_date');


/*
|--------------------------------------------------------------------------
| UserRegistration
|--------------------------------------------------------------------------
*/
Route::post('/user/get_form', 'Api\UsersApi@get_form');
Route::post('/user/get-user-detail', 'Api\UsersApi@get_user_detail');

/*
|--------------------------------------------------------------------------
| Course
|--------------------------------------------------------------------------
*/
Route::post('/courses/grade-level', 'Api\CourseApi@grade_level');

/*
|--------------------------------------------------------------------------
| Schedule
|--------------------------------------------------------------------------
*/
//Route::post('/schedule/get-grade', 'Api\ScheduleApi@get_grade');

/*
|--------------------------------------------------------------------------
| Classroom
|--------------------------------------------------------------------------
*/
Route::post('/classroom/admin', 'Api\ClassroomApi@admin');

/*
|--------------------------------------------------------------------------
| Teacher Level
|--------------------------------------------------------------------------
*/
Route::post('/level/get', 'Api\LevelApi@get');

/*
|--------------------------------------------------------------------------
| TemplateMaker Level
|--------------------------------------------------------------------------
*/
Route::post('/template/get_form', 'Api\TemplateMakerApi@get_form');


/*
|--------------------------------------------------------------------------
| Time
|--------------------------------------------------------------------------
*/
Route::post('/timer', 'Api\TimerApi@get_time');
