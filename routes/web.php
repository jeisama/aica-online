<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/



Auth::routes();

/*
|-------------------------------------
| Logout
|-------------------------------------
*/

Route::post('/set-locale', 'HomeController@setLocale');
Route::get('/sample', 'HomeController@sample');


Route::group(['middleware' => ['auth']], function () {
    /*
    |-------------------------------------
    | Landing Page
    |-------------------------------------
    */
    Route::get('/', 'PagesController@index');

    /*
    |-------------------------------------
    | User Management
    |-------------------------------------
    */
    Route::get('/user', 'UsersController@index');
    Route::get('/user/create', 'UsersController@create');
    Route::post('/user/validate_user', 'UsersController@validate_user');
    Route::post('/user/store_user', 'UsersController@store_user');
    Route::get('/user/edit/{username}', 'UsersController@edit');
    Route::post('/user/validate-update', 'UsersController@validate_update');
    Route::post('/user/update', 'UsersController@update');
    Route::post('/user/archive-user', 'UsersController@archive_user');
    Route::post('/user/get_form', 'UsersController@get_form');
    Route::post('/user/get-user-detail', 'UsersController@get_user_detail');
    Route::get('/user/export', 'UsersController@export');

    /*
    |-------------------------------------------------------------------------
    | Schedules
    |-------------------------------------------------------------------------
    */
    Route::get('schedules','ScheduleController@index')->name('schedule.index');
    //Route::get('schedules/create', 'ScheduleController@create')->name('schedule.create');
    //Route::post('schedules/create', 'ScheduleController@store')->name('schedule.store');
    //Route::get('schedules/edit/{id}', 'ScheduleController@edit')->name('schedule.edit');
    //Route::post('schedules/edit/{id}', 'ScheduleController@update')->name('schedule.update');

    Route::get('schedules/teacher/create','ScheduleController@teacher_create');
    Route::post('schedules/teacher/create','ScheduleController@teacher_store');
    Route::get('schedules/teacher/edit/{id}','ScheduleController@teacher_edit');
    Route::post('schedules/teacher/edit/{id}','ScheduleController@teacher_update');

    Route::get('schedules/student/create','ScheduleController@student_create');
    Route::post('schedules/student/create','ScheduleController@student_store');
    Route::get('schedules/student/download/{uid}','ScheduleController@downloadSchedule');
    
    Route::get('schedules/student/edit/{id}','ScheduleController@student_edit');
    Route::put('schedules/student/edit/{id}','ScheduleController@student_update');
    Route::get('schedules/export/{uid}','ScheduleController@export_create');
   
    /*
    |-------------------------------------------------------------------------
    | Lessons
    |-s------------------------------------------------------------------------
    */
    Route::get('lessons','LessonController@index')->name('lesson.index');
    Route::get('lessons/create','LessonController@create')->name('lesson.create')->middleware('teacher');
    Route::post('lessons/create','LessonController@store')->name('lesson.store')->middleware('teacher');
    Route::get('lessons/edit/{id}','LessonController@edit')->name('lesson.edit')->middleware('teacher');
    Route::post('lessons/edit/{id}','LessonController@update')->name('lesson.update')->middleware('teacher');
    Route::post('lessons/delete-attachment','LessonController@delete_attachment')->name('lesson.delete_attachment');
    Route::post('lessons/delete-media','LessonController@delete_media')->name('lesson.delete_media');
    Route::get('lessons/conference/{token}','LessonController@conference');
    Route::post('lessons/save-chat','LessonController@save_chat')->name('lesson.save_chat');
    Route::get('lessons/test','LessonController@sendmail')->name('lesson.test');
    Route::post('lessons/video-start-email','LessonController@send_mail_video')->name('lesson.send_mail_video');
    Route::post('lessons/add_attachment','LessonController@add_attachment')->name('lesson.add_attachment');
    //Route::post('lessons/save-answer','LessonController@save_answer');
    Route::post('lessons/get-pdf','LessonController@get_pdf');
    Route::post('lessons/save-rating','LessonController@save_rating');
    Route::get('lessons/{slug}','LessonController@thread')->name('lesson.thread');
    Route::post('lessons/{slug}','LessonController@add_comment')->name('lesson.addcomment');
    Route::get('lessons/show/{id}','LessonController@show')->name('lesson.show');
    Route::post('lessons/show/{id}','LessonController@save_answer')->name('lesson.show');

    

    /*
    |-------------------------------------
    | Grade Level
    |-------------------------------------
    */
    Route::get('grades', 'GradeLevelController@index');
    Route::get('grades/create', 'GradeLevelController@create');
    Route::post('grades/create', 'GradeLevelController@store');
    Route::get('grades/edit/{id}', 'GradeLevelController@edit');
    Route::put('grades/edit/{id}', 'GradeLevelController@update');

    /*
    |-------------------------------------
    | Courses
    |-------------------------------------
    */
    Route::get('courses', 'CourseController@index');
    Route::get('courses/create', 'CourseController@create');
    Route::post('courses/create', 'CourseController@store');
    Route::get('courses/edit/{id}', 'CourseController@edit');
    Route::put('courses/edit/{id}', 'CourseController@update');
    Route::post('courses/set-grade', 'CourseController@set_grade');


    Route::get('/lesson', 'PagesController@lesson');
    Route::get('/mainte', 'PagesController@mainte');

    /*
    |-------------------------------------
    | Classroom
    |-------------------------------------
    */
    Route::get('/classroom', 'ClassroomController@index');
    Route::get('/classroom/create', 'ClassroomController@create');
    Route::post('/classroom/validate', 'ClassroomController@validateClass');
    Route::post('/classroom/create', 'ClassroomController@store');
    Route::post('/classroom/delete', 'ClassroomController@delete');
    Route::get('/classroom/edit/{id}', 'ClassroomController@edit');
    Route::post('/classroom/edit/{id}', 'ClassroomController@update');
    Route::get('/classroom/show/{id}', 'ClassroomController@show');
    Route::post('/classroom/admin', 'ClassroomController@room_admin');


    /*
    |-------------------------------------
    | Teachers Level
    |-------------------------------------
    */
    Route::get('/level', 'LevelController@index');
    Route::get('/level/create', 'LevelController@create');
    Route::post('/level/create', 'LevelController@store');
    Route::get('/level/edit/{id}', 'LevelController@edit');
    Route::put('/level/edit/{id}', 'LevelController@update');
    Route::post('/level', 'LevelController@set_grade');

    /*
    |-------------------------------------
    | Template Module
    |-------------------------------------
    */
    Route::get('/template', 'TemplateController@index');
    Route::get('/template/create', 'TemplateController@create');
    Route::post('/template/get-lesson', 'TemplateController@get_lesson');
    Route::post('/template/create', 'TemplateController@store');
    Route::get('/template/show/{id}', 'TemplateController@show');
    Route::get('/template/edit/{id}', 'TemplateController@edit');
    Route::post('/template/edit/{id}', 'TemplateController@update');
    Route::post('template/delete-attachment','TemplateController@delete_attachment');
    Route::post('template/delete-media','TemplateController@delete_media');
    Route::post('/template/load-template', 'TemplateController@load');

    /*
    |-------------------------------------
    | Reports
    |-------------------------------------
    */
    Route::get('/reports', 'ReportController@index');
    Route::get('/reports/student', 'ReportController@student');
    Route::get('/reports/student/export', 'ReportController@export_student');
    Route::get('/reports/teacher', 'ReportController@teacher');
    Route::get('/reports/teacher/export', 'ReportController@export_teacher');

    Route::get('logout', function(){
        Auth::logout();
        
        return redirect('/login');
    });
});