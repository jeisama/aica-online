

$(function() {
  
  var socket_io = 'https://aic-online.webstarterz.com:3000';

  //var socket_io = 'https://localhost:3001';

  var socket = io(socket_io);

  var FADE_TIME = 150; // ms
  var TYPING_TIMER_LENGTH = 400; // ms
  var COLORS = [
    '#e21400', '#91580f', '#f8a700', '#f78b00',
    '#58dc00', '#287b00', '#a8f07a', '#4ae8c4',
    '#3b88eb', '#3824aa', '#a700ff', '#d300e7'
  ];

  // Initialize variables 
  var $window = $(window);
  var $usernameInput = $('.usernameInput'); // Input for username
  var $messages = $('.messages'); // Messages area
  var chatArea = $('.chatArea');
  var $inputMessage = $('.inputMessage'); // Input message input box

  var $loginPage = $('.login.page'); // The login page
  var $chatPage = $('.chat.page'); // The chatroom page
  
  var endSession = $('.endSession');

  var roomId  = $('#getRoomName').val();
  
  //Chat Message Variables
  var lesson_id = $('#lesson_id').val();
  var user_id   = $('#user_id').val();
  let user_role = $('#user_role').val();
  let baseUrl   = $('base').attr('href');

  let _token = $('meta[name="csrf-token"]').attr('content');

  // // Prompt for setting a username
  var username = $('.usernameInput').val();

  var connected = false;
  var typing = false;
  var lastTypingTime;
  var $currentInput = $usernameInput.focus();

  var positions = {};

  var total = 0;

  

  const addParticipantsMessage = (data) => {
    var message = '';
    if (data.numUsers === 1) {
      message += "there's 1 participant";
    } else {
      message += "there are " + data.numUsers + " participants";
    }
    //log(message);
  }
  $loginPage.hide();
  $chatPage.show();

  socket.emit('add user', {
    username: username,
    roomId: roomId
  });
  //socket.emit('joinRoom', roomId);

  // Sets the client's username
  //To Delete
  const setUsername = () => {
    username = cleanInput($usernameInput.val().trim());

    // If the username is valid
    if (username) {
      $loginPage.fadeOut();
      $chatPage.show();
      $loginPage.off('click');
      $currentInput = $inputMessage.focus();

      // Tell the server your username
      socket.emit('add user', username);
    }
  }

  // Sends a chat message
  const sendMessage = () => {
    var message = $inputMessage.val();
    // Prevent markup from being injected into the message
    message = cleanInput(message);
    // if there is a non-empty message and a socket connection
    if (message && connected) {
      
      addChatMessage({
        username: username,
        message: message,
        userClass: 'right'
      });
      // tell server to execute 'new message' and send along one parameter
      socket.emit('new message', message);
      $inputMessage.val('');
      $inputMessage.blur();
    }
  }

  // Log a message
    const log = (message, options) => {
    var $el = $('<li>').addClass('log').text(message);
    addMessageElement($el, options);
  }

  // Adds the visual chat message to the message list
  const addChatMessage = (data, options) => {
    // Don't fade the message in if there is an 'X was typing'
    let userClass      = 'left';

    if(typeof data.userClass != 'undefined'){
      userClass   = data.userClass;
    }

    var $typingMessages = getTypingMessages(data);
    options = options || {};
    if ($typingMessages.length !== 0) {
      options.fade = false;
      $typingMessages.remove();
    }

    let userImage   = $('<span class="avatar">').html('<i class="mdi mdi-account-circle"></i>');

    var $usernameDiv = $('<span class="username"/>')
      .text(data.username);
    var $messageBodyDiv = $('<span class="messageBody">')
      .text(data.message);

    var typingClass = data.typing ? 'typing' : '';
    var $messageDiv = $('<li class="message '+ userClass +'"/>')
      .data('username', data.username)
      .addClass(typingClass)
      .append(userImage, $usernameDiv, $messageBodyDiv);

    addMessageElement($messageDiv, options);
  }

  // Adds the visual chat typing message
  const addChatTyping = (data) => {
    data.typing = true;
    data.message = 'is typing';
    addChatMessage(data);
  }

  // Removes the visual chat typing message
  const removeChatTyping = (data) => {
    getTypingMessages(data).fadeOut(function () {
      $(this).remove();
    });
  }

  // Adds a message element to the messages and scrolls to the bottom
  // el - The element to add as a message
  // options.fade - If the element should fade-in (default = true)
  // options.prepend - If the element should prepend
  //   all other messages (default = false)
  const addMessageElement = (el, options) => {
    var $el = $(el);

    // Setup default options
    if (!options) {
      options = {};
    }
    if (typeof options.fade === 'undefined') {
      options.fade = true;
    }
    if (typeof options.prepend === 'undefined') {
      options.prepend = false;
    }

    // Apply options
    if (options.fade) {
      $el.hide().fadeIn(FADE_TIME);
    }
    if (options.prepend) {
      $messages.prepend($el);
    } else {
      $messages.append($el);
    }
    
    chatArea[0].scrollTop = $messages[0].scrollHeight;
  }

  // Prevents input from having injected markup
  const cleanInput = (input) => {
    return $('<div/>').text(input).html();
  }

  // Updates the typing event
  const updateTyping = () => {
    if (connected) {
      if (!typing) {
        typing = true;
        socket.emit('typing');
      }
      lastTypingTime = (new Date()).getTime();

      setTimeout(() => {
        var typingTimer = (new Date()).getTime();
        var timeDiff = typingTimer - lastTypingTime;
        if (timeDiff >= TYPING_TIMER_LENGTH && typing) {
          socket.emit('stop typing');
          typing = false;
        }
      }, TYPING_TIMER_LENGTH);
    }
  }

  // Gets the 'X is typing' messages of a user
  const getTypingMessages = (data) => {
    return $('.typing.message').filter(function (i) {
      return $(this).data('username') === data.username;
    });
  }

  // Gets the color of a username through our hash function
  const getUsernameColor = (username) => {
    // Compute hash code
    var hash = 7;
    for (var i = 0; i < username.length; i++) {
       hash = username.charCodeAt(i) + (hash << 5) - hash;
    }
    // Calculate color
    var index = Math.abs(hash % COLORS.length);
    return COLORS[index];
  }

  // Keyboard events

  $window.keydown(event => {
    // Auto-focus the current input when a key is typed
    if (!(event.ctrlKey || event.metaKey || event.altKey)) {
      $currentInput.focus();
    }
    // When the client hits ENTER on their keyboard
    if (event.which === 13 && !event.shiftKey) {
      if (username) {
        let messageData = $('#inputMessage').val();
        
        sendMessage();
        
        saveChat(messageData);

        socket.emit('stop typing');
        typing = false;

      } else {
        setUsername();
      }
    }
  });

  $inputMessage.on('input', () => {
    updateTyping();
  });

  



  // Click events

  // Focus input when clicking anywhere on login page
  $loginPage.click(() => {
    $currentInput.focus();
  });

  // Focus input when clicking on the message input's border
  $inputMessage.click(() => {
    $inputMessage.focus();
  });

  endSession.click(() => {
    socket.emit('trigger_modal');
  });

  // Socket events

  // Whenever the server emits 'login', log the login message
  socket.on('login', (data) => {
    //console.log(data);
    connected = true;
    // Display the welcome message
    var message = "Welcome to Socket.IO Chat – ";
    // log(message, {
    //   prepend: true
    // });
    addParticipantsMessage(data);
  });

  // Whenever the server emits 'new message', update the chat body
  socket.on('new message', (data) => {
    //console.log('message', data);
    addChatMessage(data);
  });

  const alertModal = (data) => {
    $('#ratingModal').modal('show');
  }

  socket.on('showModal', (data) => {
    alertModal(data);
  });



  // Whenever the server emits 'user joined', log it in the chat body
  socket.on('user joined', (data) => {
    //console.log(user_role)

    //log(data.username + ' joined');
    addParticipantsMessage(data);
  });

  // Whenever the server emits 'user left', log it in the chat body
  socket.on('user left', (data) => {
    //log(data.username + ' left');
    addParticipantsMessage(data);
    removeChatTyping(data);
  });

  // Whenever the server emits 'typing', show the typing message
  socket.on('typing', (data) => {
    addChatTyping(data);
  });

  // Whenever the server emits 'stop typing', kill the typing message
  socket.on('stop typing', (data) => {
    removeChatTyping(data);
  });

  socket.on('disconnect', () => {
    log('you have been disconnected');
  });

  socket.on('reconnect', () => {
    log('you have been reconnected');
    if (username) {
      socket.emit('add user', {
        username: username,
        roomId: roomId
      });
    }
  });

  socket.on('reconnect_error', () => {
    log('attempt to reconnect has failed');
  });

  socket.on('user image', function(data){
    
    let overview = $('#slideoverview');
    
    let image = data.image;
    
    let html  = `<li><div class="drawer"><img src="`+ image +`"></div></li>`;

    overview.append(data);

  });


  $(document).on('change', '#attachment', function(){
    
    var formData = new FormData($('#add_attachment')[0]);

      $.ajax({
          url: baseUrl +'/lessons/add_attachment', 
          type: 'POST',
          data: formData,
          async: false,
          cache: false,
          contentType: false,
          processData: false,
          success: function (response) {

            let html = `<li><div class="drawer"><img src="`+ response.image +`" data-base="`+ response.base +`" data-type="`+ response.type +`"></div></li>`;
            
            if(response.type == 'pdf'){

              let dataBase = baseUrl + '/uploads/lessons/' + response.base;

              html = `<li><div class="drawer"><img src="data:image/jpg;base64,`+ response.image +`" data-base="`+ dataBase +`" data-type="`+ response.type +`"></div></li>`;
            
            }

            $('#slideoverview').append(html);

            socket.emit('user image', html);
          }
      });
  });

  $('.overview').on('click', 'img', function(){
    /*
    let src = $(this).attr('src');
    let type= $(this).attr('data-type');
    let base= $(this).attr('data-base');
    let image = `<img src="`+ src +`">`;
    let appendData = '';

    let data = {
      type: type,
      base: base,
      image: image
    }


    

    if(type == 'pdf'){

      $.ajax({
        type: 'post',
        dataType: 'json',
        url: baseUrl +'/lessons/get-pdf',
        data: {
          _token: _token,
          page: '0',
          src: base
        },
        success:function(response){
        
          var img = `<img src='data:image/jpg;base64,`+ response.thumbnail +`' data-src="`+ response.src +`" data-page="`+ response.page +`" data-max="`+ response.max +`"/>`;

          var appData = `
                    <div class="pdf-control">
                      <div class="button-action pdf-prev" data-action="prev"><i class="mdi mdi-arrow-left"></i></div>
                      <div class="button-action pdf-next" data-action="next"><i class="mdi mdi-arrow-right"></i></div>
                      <div class="minimize_maximize" data-tag="maximize_pdf" data-target="pdf" id="target-pdf"><i class="mdi mdi-arrow-expand-all"></i></div>
                    </div>
                    <div class="pdf-preview">
                    `+ img +`
                    </div>
                    `;

          $('#prev_image').html(appData);

          var data2 = {
            type: type,
            base: base,
            image: response.thumbnail
          };
          
          console.log(data2);

          socket.emit('user image_preview', data2);
        }
      });

    }else{
      
      $('#prev_image').html(image);

      socket.emit('user image_preview', data);

    }*/
    /*
    if(type == 'image'){
      appendData = image;
    }else{
      appendData = `<div class="pdf-control">
                        <div class="button-action pdf-prev" data-action="prev"><i class="mdi mdi-arrow-left"></i></div>
                        <div class="button-action pdf-next" data-action="next"><i class="mdi mdi-arrow-right"></i></div>
                        <div class="minimize_maximize" data-tag="maximize_pdf" data-target="pdf" id="target-pdf"><i class="mdi mdi-arrow-expand-all"></i></div>
                        <input type="hidden" id="pdf-page" value="1">
                    </div>
                    <object data="`+ data.base +`#toolbar=0&view=FitH&scrollbar=1&page=1" type="application/pdf" style="width: 100%; height: 100%; overflow-x: hidden; overflow-y: hidden;">
                    </object>`;
    }
    $('#prev_image').html(appendData);

    socket.emit('user image_preview', data);

    */

    let src = $(this).attr('src');
    let type= $(this).attr('data-type');
    let base= $(this).attr('data-base');
    let image = `<img src="`+ src +`">`;
    let appendData = '';

    let data = {
      type: type,
      base: base,
      image: image
    }

    if(type == 'image'){
      appendData = image;
    }else{
      appendData = `<div class="pdf-control">
                        <div class="button-action pdf-prev" data-action="prev"><i class="mdi mdi-arrow-left"></i></div>
                        <div class="button-action pdf-next" data-action="next"><i class="mdi mdi-arrow-right"></i></div>
                        <div class="minimize_maximize" data-tag="maximize_pdf" data-target="pdf" id="target-pdf"><i class="mdi mdi-arrow-expand-all"></i></div>
                        <input type="hidden" id="pdf-page" value="1">
                    </div>
                    <object data="`+ data.base +`#toolbar=0&view=FitH&scrollbar=1&page=1" type="application/pdf" style="width: 100%; height: 100%; overflow-x: hidden; overflow-y: hidden;">
                    </object>`;
    }
    $('#prev_image').html(appendData);

    socket.emit('user image_preview', data);
  });

  socket.on('user image_preview', function(data){
    /*
    let appendData = '';

    if(data.type == 'image'){

      $('#prev_image').html(data.image);
    
    }else{
      var appData = `
                  <div class="pdf-preview">
                    <img src="data:image/jpg;base64,`+ data.image +`"/>
                  </div>`;
      $('#prev_image').html(appData);
    }
    */
   // console.log(appendData);
    
    let appendData = '';
    if(data.type == 'image'){
      appendData = data.image;
    }else{
      appendData = `
                    <object data="`+ data.base +`#toolbar=0&view=FitH&scrollbar=1&page=1" type="application/pdf" style="width: 100%; height: 100%; overflow-x: hidden; overflow-y: hidden;">
                    </object>`;
    }
   // console.log(appendData);
    $('#prev_image').html(appendData);
  });

  $(document).on('click', '.button-action', function(){
    /*
    var action = $(this).attr('data-action');

    var pdfpreview = $('.pdf-preview').find('img');

    var src = pdfpreview.attr('data-src');

    var page= pdfpreview.attr('data-page');

    var max = pdfpreview.attr('data-max');

    var newPage = 0;

    if(action == 'prev' && parseInt(page) > 0){

      newPage = parseInt(page) - 1;

    }else if(action == 'next' && parseInt(page) < parseInt(max)){
      
      newPage = parseInt(page) + 1;

    }

    $.ajax({
      type: 'post',
      dataType: 'json',
      url: baseUrl +'/lessons/get-pdf',
      data:{
        _token: _token,
        page: newPage,
        src: src
      },
      success:function(response){
        
        var img = `<img src='data:image/jpg;base64,`+ response.thumbnail +`' data-src="`+ response.src +`" data-page="`+ response.page +`" data-max="`+ response.max +`"/>`;

        var appData = `
                  <div class="pdf-control">
                    <div class="button-action pdf-prev" data-action="prev"><i class="mdi mdi-arrow-left"></i></div>
                    <div class="button-action pdf-next" data-action="next"><i class="mdi mdi-arrow-right"></i></div>
                    <div class="minimize_maximize" data-tag="maximize_pdf" data-target="pdf" id="target-pdf"><i class="mdi mdi-arrow-expand-all"></i></div>
                  </div>
                  <div class="pdf-preview">
                  `+ img +`
                  </div>
                  `;

        $('#prev_image').html(appData);

        var data = {
          type: 'pdf',
          image: response.thumbnail
        };


        socket.emit('user image_preview', data);

      }
    });
    */
    
    let pdfpage = $('#pdf-page').val();
    let action  = $(this).attr('data-action');
    let setPage = 1;
    if(action == 'next'){
        setPage = parseInt(pdfpage) + 1;
    }else if(action == 'prev' && pdfpage > 1){
        setPage = parseInt(pdfpage) - 1;
    }

    $('#pdf-page').val(setPage);
   
    let object_data = $('#prev_image').find('object').attr('data');


    let url = new URL(object_data);
    
    let origin = url.origin;
    let pathname = url.pathname;
    let hash = '#toolbar=0&view=FitV&scrollbar=1&page='+ setPage;

    let new_url = origin + pathname + hash;
    
    $('#prev_image').find('object').attr('data', new_url);
    
    socket.emit('set-pdf', new_url);
    
  });

  $(document).on('click', '.expand', function(){
    let target = $(this).attr('data-target');
    let id     = $(this).attr('id');
    let width  = $('#'+ target).outerWidth();
    
    if(target == 'scribble_data'){
        width  = parseInt(width) + 5;
    }
    
    let data = {
      target: target,
      width: width
    }
    
    socket.emit('expand', data);

  });

  socket.on('expand', function(data){
    
    let width  = $('#'+ data.target).outerWidth();

    if(data.target == 'scribble_data'){
        width  = parseInt(width) + 5;
    }

    $('#'+ data.target).addClass('expand-vertical').css({
        'width': width +'px'
    });

  });
  
  $(document).on('click', '.collapse', function(){
    let target = $(this).attr('data-target');
    let data = {
      target: target
    }

    socket.emit('collapse', data);
  });

  socket.on('collapse', function(data){
    $('#'+ data.target).removeClass('expand-vertical').removeAttr('style');
  })

  function saveChat(messageData){
    //lesson_id
    //user_id
    //_token
    $.ajax({
      type: 'post',
      dataType: 'json',
      url: baseUrl +'/lessons/save-chat',
      data: {
        _token: _token,
        lesson_id: lesson_id,
        user_id: user_id,
        message: messageData
      },success:function(response){
        //console.log(response);
      }
    });
  }


  /*
  /* Video 
  */

  var video = document.getElementById('lesson_video');

  if(video != null){
    var jquery_video = $('#lesson_video');

    var currentTime = video.currentTime;

    $(video).on('play', function(){
      let currentTime = video.currentTime;
      socket.emit('video play', currentTime);
    });

    $(video).on('pause', function(){

      currentTime = video.currentTime;
      
      socket.emit('video pause', currentTime);
    });

    socket.on('video play', function(data){
      
      video.play();

      video.currentTime = data;
      
    });

    socket.on('video pause', function(data){
      video.pause();
      
    });
  }

  //Show/Hide Canvas

  $('.showCanvas').click(function(){
    socket.emit('canvas show_hide', 'show');
  });

  $('.close_scribble').click(function(){
    socket.emit('canvas show_hide', 'hide');
  });

  socket.on('canvas show_hide', function(data){
    
    let width = $('#widget-container').width();

    if(data == 'show'){
      $('#widget-container').css({ 'right': '0' });
    }else{
      $('#widget-container').css({ 'right': '-'+ width +'px' });
    }
    
  });

  //Minimize/Maximize Video
  $(document).on('click', '.minimize_maximize', function(){
    //alert();
    let tag = $(this).attr('data-tag');
    socket.emit('minimize_maximize', tag);
  });


   socket.on('minimize_maximize', function(data){
     
      if(data == 'maximize'){
          $(this).attr('data-tag', 'minimize');
          $('.video_data').addClass('maximized');
    
      }else if(data == 'minimize'){
          $(this).attr('data-tag', 'maximize');
          $('.video_data').removeClass('maximized');
    
      }else if(data == 'maximize_pdf'){
          $(this).attr('data-tag', 'minimize_pdf');
          $('#prev_image').addClass('maximized_pdf');
          
      }else if(data == 'minimize_pdf'){
          $(this).attr('data-tag', 'maximize_pdf');
          $('#prev_image').removeClass('maximized_pdf');
      }
  });


  $('.help').click(function(){

    var role = $(this).attr('data-role');

    if(role == 4){

      $('.info-box').toggle();

    }else{

      $('.info-box').hide();

    }

  });


  $('.need-help').each(function(){

    $(this).click(function(){

      var data = $(this).attr('data-id');

      if(data != ''){

        socket.emit('help', data);

      }

    });

  });


  socket.on('help', function(data){

    var message = '';

    if(data == 'repeat'){

      message = `<li>
          <div>もう一度言ってください。</div>
          <div class="en">Once more please.</div>
        </li>`;

    }else if(data == 'speak'){

      message = `<li>
          <div>ゆっくり言ってください。</div>
          <div class="en">Speak more slowly.</div>
        </li>`;

    }else if(data == 'cant_see'){

      message = `<li>
          <div>よく見えません。</div>
          <div class="en">I can’t see well.</div>
        </li>`;

    }else if(data == 'cant_hear'){

      message = `<li>
          <div>よく聞こえません</div>
          <div class="en">I cant' hear well.</div>
        </li>`;

    }else if(data == 'cant_understand'){

      message = `<li>
          <div>よくわかりません。</div>
          <div class="en">I don’t understand.</div>
        </li>`;

    }

    $('.info-box ul').html(message);
    $('.info-box').show();
  });


  /*
  * jPlayer Controls
  */

  if(user_role == 5){
    
    $(document).on('click', '#playerLists .jp-playlist-item', function(){
      var index =  $(this).closest('li').index();
      socket.emit('jplayer select', index);
    });

    $('.jp-previous').click(function(){
      socket.emit('jplayer previous');
    });

    $('.jp-play').click(function(){
      socket.emit('jplayer play');
    });

    $('.jp-next').click(function(){
      socket.emit('jplayer next');
    });
  }

  if(user_role == 4 || user_role == 1 || user_role == 2 || user_role == 3 ){
   
    socket.on('jplayer select', function(data){
      
      let index = $( "#playerLists li" )[data];
      $(index).find('.jp-playlist-item').click();
    });

    socket.on('jplayer previous', function(){
      $('.jp-previous').click();
    });

    socket.on('jplayer play', function(){
      //console.log('play');
      $('.jp-play').click();
    });

    socket.on('jplayer next', function(){
      $('.jp-next').click();
    });

    socket.on('set-pdf', function(data){
      let appendData =  `<object data="`+ data +`" type="application/pdf" style="width: 100%; height: 100%; overflow-x: hidden; overflow-y: hidden;">
                    </object>`;
      // let appendData = `<object data="https://aic-online.webstarterz.com/uploads/lessons/1301545973948.2666.pdf#toolbar=0&amp;view=FitV&amp;scrollbar=1&amp;page=2" type="application/pdf" style="width: 100%; height: 100%; overflow-x: hidden; overflow-y: hidden;" internalinstanceid="214">
      //               </object>`;

      //console.log(appendData);
      $('#prev_image').html(appendData);

    });
  }

  if(user_role == 5){
    // $(document).mousemove(function(ev){

    //   var pos = {
    //     x: ev.clientX,
    //     y: ev.clientY
    //   }

    //   socket.emit('mouse move', { pos: pos });

    // });
    /*
    $(window).on('mousemove', function(ev){
      // var pos = {
      //   x: ev.clientX,
      //   y: ev.clientY
      // }

      //socket.emit('mouse move', { pos: pos });

      var vp = viewport();
      // console.log(vp);
      var w  = vp.width;

      var h  = vp.height;

      var nW = (parseInt(ev.clientX) / parseInt(w)) * 100;

      var nH = (parseInt(ev.clientY) / parseInt(h)) * 100;

      var position = {
        x: nW,
        y: nH
      }

      //socket.emit('mouse move', { pos: position });

    });
    */
    // document.onmousemove = function (ev) {
    //   var pos = {
    //     x: ev.clientX,
    //     y: ev.clientY
    //   }

    //   socket.emit('mouse move', { pos: pos });
    // }; 

    // $('embed').on('mousemove',function(ev){
    //   var pos = {
    //     x: ev.clientX,
    //     y: ev.clientY
    //   }

    //   console.log(pos);
    // });
  }



  socket.on('mouse update', function (mouse) {
    //console.log(mouse);
    //virtualMouse.move(mouse.id, mouse.pos);

  });

  socket.on('mouse disconnect', function (mouse) {
    //virtualMouse.remove(mouse.id);
  });

  // virtual mouse module
  /*
  var virtualMouse = {
    // moves a cursor with corresponding id to position pos
    // if cursor with that id doesn't exist we create one in position pos
    move: function (id, pos) {
      var cursor = document.getElementById('cursor-' + id);

      if (!cursor) {
        cursor = document.createElement('img');
        cursor.className = 'virtualMouse';
        cursor.id = 'cursor-' + id;
        cursor.src = '/assets/img/cursor1.png'; 
        cursor.style.position = 'absolute';
        cursor.style.zIndex  = '1000';
        //document.body.appendChild(cursor);
      }

      cursor.style.left = pos.x + '%';
      cursor.style.top = pos.y + '%';
    },
    // remove cursor with corresponding id
    remove: function (id) {
      var cursor = document.getElementById('cursor-' + id);
      cursor.parentNode.removeChild(cursor);
    }
  }


  function viewport(){
    // var e = window;
    // var a = 'inner';
    // if (!('innerWidth' in window)){
    //     a = 'client';
    //     e = document.documentElement || document.body;
    // }
    
    var x = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

    var y = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

    return { width : x , height : y }
  }
  */
});

/*
/ Youtube Controls
*/
/*
var player;
let user_role = $('#user_role').val();

function onYouTubeIframeAPIReady() {
    player = new YT.Player('video-placeholder', {
        events: {
            onReady: initialize,
            onStateChange: onPlayerStateChange
        }
    });
}

function initialize(){

}

function onPlayerStateChange(event) {
    if(user_role == 2){
      let curTime = player.getCurrentTime();
      
      console.log(event);

      if(event.data == 1){
        socket.emit('youtube play', curTime);
        console.log('play @ '+ curTime);
      }
      if(event.data == 2){
        socket.emit('youtube pause', curTime);
        console.log('pause @ '+ curTime);
      }

      if(event.data == 3){
        socket.emit('youtube seekto', curTime);
      }
    }
    
}

socket.on('youtube play', function(data){
  player.playVideo();
  console.log('play @'+ data);
});

socket.on('youtube pause', function(data){
  //player.seekTo(data);
  player.pauseVideo();
  console.log('pause @'+ data);
});

socket.on('youtube seekto', function(data){
  player.seekTo(data);
});

*/