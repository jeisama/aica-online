$(function(){

	let base  = $('base').attr('href');


	setInterval(function(){

		checkTimer();

	},500);

	

	function checkTimer(){
		
		$(".checkTimer").each(function(){

		    let data = {
		    	now: $(this).attr('data-now'),
				date: $(this).attr('data-date'),
				target: $(this).attr('data-target'),
				role: $(this).attr('data-role'),
				token: $(this).attr('data-token'),
		    }

		    getTimer(data);

		});

	}

	
	function getTimer(data){

		let now 	= new Date(data.now);

		let date 	= new Date(data.date);

		let target 	= data.target;

		let role 	= data.role;

		let token 	= data.token;

		let DateDiff = {

			inSeconds: function(d1, d2){

				var t2 = d2.getTime();
		        var t1 = d1.getTime();

		        return parseInt((t1 - t2) / (1000)); //miliseconds

			},

			inMinutes: function(d1, d2){

				var t2 = d2.getTime();
		        var t1 = d1.getTime();

		        return parseInt((t1 - t2) / (60 * 1000)); //seconds * miliseconds

			},

			inHours: function(d1, d2){

				var t2 = d2.getTime();
		        var t1 = d1.getTime();

		        return parseInt((t1 - t2) / (60 * 60 * 1000));//minutes * seconds * miliseconds

			},

		    inDays: function(d1, d2) {

		        var t2 = d2.getTime();
		        var t1 = d1.getTime();

		        return parseInt((t1 - t2) / (24 * 60 * 60 * 1000));//hrs * minutes * seconds * miliseconds

		    },

		    inWeeks: function(d1, d2) {

		        var t2 = d2.getTime();
		        var t1 = d1.getTime();

		        return parseInt((t1 - t2) / (7 * 24 * 60 * 60 * 1000));//weeks * hrs * minutes * seconds * miliseconds

		    },

		    inMonths: function(d1, d2) {

		        var d1Y = d1.getFullYear();
		        var d2Y = d2.getFullYear();
		        var d1M = d1.getMonth();
		        var d2M = d2.getMonth();

		        return (d1M+12*d1Y) - (d2M+12*d2Y);

		    },

		    inYears: function(d1, d2) {

		        return d1.getFullYear() - d2.getFullYear();

		    }
		}
		
		let seconds = DateDiff.inSeconds(date, now);
		let minutes = DateDiff.inMinutes(date, now);
		let hours 	= DateDiff.inHours(date, now);
		let days 	= DateDiff.inDays(date, now);

		let attribute = {};

		if(days > 0){

			let label = days == 1 ? '':'s';

			attribute = {
				class: 'btn-warning',
				text: 'Lesson start in '+ days +' day'+ label,
				href: '',
			}

		}else if(hours > 0){

			let label = hours == 1 ? '':'s';

			attribute = {
				class: 'btn-warning',
				text: 'Lesson start in '+ hours +' hour'+ label,
				href: '',
			}

		}else if(minutes > 0){

			let label = minutes == 1 ? '':'s';

			attribute = {
				class: 'btn-warning',
				text: 'Lesson start in '+ minutes +' minute'+ label,
				href: '',
			}

		}else if(seconds > 0){

			let label = seconds == 1 ? '':'s';

			attribute = {
				class: 'btn-warning',
				text: 'Lesson start in '+ seconds +' second'+ label,
				href: '',
			}

		}else if(-900 <= seconds){

			let label = role == 5 ? 'Start Lesson':'Join Lesson';

			attribute = {
				class: 'btn-primary',
				text: label,
				href: base + '/lessons/conference/' + token,
			}

		}else{
	
			attribute = {
				class: 'btn-secondary',
				text: 'Lesson Ended',
				href: '',
			}
	
		}

		$('#timer-' + target)
			.removeClass('btn-warning')
			.removeClass('btn-primary')
			.addClass(attribute.class)
			.text(attribute.text);

		if(attribute.href != ''){
	
			$('#timer-' + target)
				.attr('href', attribute.href)
				.attr('target', '_blank');
	
		}else{
	
			$('#timer-' + target)
				.removeAttr('href')
				.removeAttr('target');
	
		}
		
	}


});