$(document).ready(function(){
	
	let _token = $('meta[name="csrf-token"]').attr('content');

	let base  = $('base').attr('href');
	
	/*
	|-------------------------------------------------------
	| Submit and Save Record
	|-------------------------------------------------------
	*/
    var count = 0;

	$(document).on('click', '#submit-form', function(){
        count ++;
        
		let form   = $(this).attr('data-form');

		let form_data =  new FormData($('#'+ form)[0]);

		let action = $('#'+ form).attr('action');

		submitForm(form_data, action);

	});


    $('.form-main').submit(function(element){

        element.preventDefault();


        let form_data =  new FormData($(this)[0]);

        let action = $(this).attr('action');
        
        submitForm(form_data, action);

    });


    function submitForm(form_data, action){

        $.ajax({
            url: action, 
            type: 'POST',
            dataType: 'json',
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function(){

                $('.form-text').text('');
                $('.form-proccessing').removeClass('hidden');
            
            },
            success: function(response){
                if(response.redirect != 'false'){
                    
                    window.location.replace(response.redirect);

                }else{
                    
                    //var link = base + '/schedules/student/download/' + response.data;
                    var link = base + '/schedules/export/' + response.data;

                    $('#confirmModal').modal('hide');

                    $('#exportReport').attr('href', link);

                    $('#returnModal').modal('show');

                }
                

                $('#'+ form).find('.form-proccessing').addClass('hidden');

            },
            error: function(json){
                
                if(json.status === 422){

                    $('.form-proccessing').addClass('hidden');

                    $.each(json.responseJSON.errors, function (key, value) {
                        $('#error-'+ key).text(value);
                    });

                }

            }
        });
    }

	/*
	|-------------------------------------------------------
	| Delete Record
	|-------------------------------------------------------
	*/
	$(document).on('click', '.archive', function(){

		let id = $(this).attr('data-id');

		$.confirm({
            title: 'Confirm!',
            content: 'Are you sure you want to archive this item?',
            buttons: {
                confirm: function () {
                    $.ajax({

                    });
                },
                cancel: function() {
                    
                }
            }
        });
	});


    /*
    |-------------------------------------------------------
    | Set Locale
    |-------------------------------------------------------
    */
    $(document).on('click', '#setLocale', function(){

        let locale = $(this).attr('data-locale');
        
        $.ajax({
            type: 'POST',
            url: base +'/set-locale',
            data: {
                locale: locale,
                _token: _token
            },
            success: function(data){
                window.location = window.location;
            }
        });

    });

});